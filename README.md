# Renamator #

## Představení ##

Aplikace, která slouží pro (zkopírování a) přejmenování souborů a adresářů. Najde své využití například při řazení většího množství fotek například z dovolené apod.


*(Je to pouze má "zkušební" aplikace napsaná v minulosti za účelem vyzkoušení nových funkcí, algoritmizace a bližšího poznání jazyka Java. Nejedná se o projekt s cíleným či komerčním zaměřením.)*


Jedná se o desktopovou aplikaci spustitelnou bez instalace. Je zaměřena na běh na zařízeních s OS Windows a Linux s nainstalovanou verzí Javy 1.8 a vyšší.

*Vygenerovaná aplikace (soubor s příponou ".jar") se nachází na cestě: .../Renamator/out/artifacts/Renamator_jar/Renamator.jar*



## Postup ##

Na začátku je třeba vybrat zdrojový adresář a volitelně cílový adresář.

*  Zdrojový adresář je adresář, kde se budou přejmenovávat názvy nebo nahrazovat znaky v názvech souborů a adresářů.

*  Pokud bude zadán cílový adresář, pak se nejprve zkopírují soubory ze zdrojového adresáře do cílového adresáře. Poté se v cílovém adresáři provedou změny v názvech souborů.


Poté na záložce "Přejmenovat" můžeme nastavit potřebné vlastnosti / podmínky pro přejmenování souborů a adresářů. Až tak učiníme, ve spodní části záložky je tlačítko "Přejmenovat". Kliknutím na něj se přejmenují názvy souborů a adresářů dle nastavených parametrů.

Podobné je to na záložce "Nahradit". Po označení adresářů stačí nastavit potřebné parametry a ve spodní části záložky kliknout na tlačítko "Nahradit".


V nastavení (v menu Aplikace -> Nastavení) máme k dispozici možnosti pro nastavení jazyka aplikace. Zda se mají zobrazovat skryté soubory a adresáře ve stromové struktuře v dialogu průzkumník souborů a nastavení formátu velikosti souborů ve zmíněném dialogu.

Z časových důvodů se nastavené vlastnosti neukládají do konfiguračních souborů. Proto jsou změny provedené v nastavení uloženy pouze po dobu běhu aplikace.

*V případě, že máme aplikaci spuštěnou na zařízení s OS Windows, je vhodné si nejprve v nastavení označit položku "Zobrazit skryté soubory ve stromové struktuře.". Toto je potřeba v případě, že otevřeme průzkumník souborů, ale stromová struktura bude prázdná. To je způsobeno tím, že některé verze OS Windows mohou mít nastavené kořenové adresáře jako skryté. Proto by nebyly zobrazeny.*



## Ovládání ##

*  Nejprve je třeba vybrat zdrojový a dle potřeby i cílový adresář. To můžeme provést kliknutím na tlačítka "Vybrat" v horní části okna aplikace.


*  Další možností je využít dialog průzkumník souborů.

    * V tomto dialogu bude otevřen kořenový adresář disku, kde můžeme procházet celou stromovou strukturu.

    * V kontextovém menu nad stromovou strukturou máme k dispozici "základní" možnosti se soubory a adresáři. Dále můžeme označený adresář nastavit jako "zdrojový / cílový".

    * V pravé části dialogu máme k dispozici přehled souborů v označeném adresáři ve stromové struktuře.

        * V horní části dialogu máme k dispozici 3 nástrojové lišty. (Od shora) na první liště máme k dispozici tlačítka pro vytvoření nového souboru a adresáře. Dále můžeme označit soubory (první sloupec v tabulce), které chceme přejmenovat nebo odstranit.

        * Druhá nástrojová lišta obsahuje komponenty pro filtrování souborů ve zvoleném adresáři. Soubor můžeme filtrovat například podle názvu, přípony, data apod.

        * Poslední (třetí) nástrojová lišta obsahuje možnosti nad soubory v označeném adresáři. Můžeme je všechny označit / odznačit, skrýt či zviditelnit apod.



* Záložka "Přejmenovat"

    * Nejprve zadáme název, na který se mají soubory přejmenovat.

    * Poté zvolíme způsob indexování pomocí čísel nebo písmen. V obou případech můžeme nastavit, zda chceme indexy přidávat za nebo před zadaný název. V případě indexování dle písmen můžeme nastavit, zda chceme aplikovat velká nebo malá písmena.

    * Dále můžeme nastavit, zda chceme aplikovat změny i pro vnořené adresáře. Tzn. zda se budou procházet i adresáře, které se nachází ve zvoleném adresáři.

        * Můžeme nastavit, zda chceme ponechat nebo změnit vybrané typy souborů (přípony).

    * Dále můžeme nastavit filtrování souborů, které chceme nebo nechceme přejmenovat.

        * Například, zda chceme přejmenovat veškeré soubory, pouze ty, které obsahují zadaný text nebo jsou určitého typu (/ přípony). Nebo ty, které splňují [regulární výraz](https://docs.oracle.com/javase/7/docs/api/java/util/regex/Pattern.html), který můžeme i otestovat.

    * Dále máme možnost vynechat soubory s určitou příponou nebo námi vybrané.



* Záložka "Nahradit"

    * Zde máme možnosti pro to, zda se mají ignorovat velikosti písmen při testování názvů souborů, které se mají přejmenovat (nahradit text v jejich názvu).

    * Zda se mají testovat i názvy adresářů.

    * Zda se mají prohledat i vnořené adresáře.

    * Do tabulky zadáme vždy do sloupce "Původní text" text, který obsahují názvy souborů a adresářů, které se mají přejmenovat. Do druhého sloupce ("Nový text") zadáme text, který nahradí ten původní.

        * Například pokud zadáme text do prvního sloupce"xxx" a do druhého sloupce "yy". Pak se budou prohledávat zadané názvy souborů a případně i adresářů. Pokud se v názvu bude vyskytovat text "xxx", bude nahrazen za "yy". Tedy Pokud bude název například "neco_xxx_dalsi", pak po provedení operace bude výsledek v syntaxi "neco_yy_dalsi".

    * Ve spodní části záložky máme k dispozici možnosti pro vynechání vybraných souborů nebo souborů s definovanou příponou.



***
---



## Performance ##

An application that is used to (copy and) rename files and folders. It finds its use, for example, when sorting more photos, for example from vacation, etc.


*(It's just a "trial" application written in the past to try out new features, algorithms and a closer understanding of Java. It's not a project with a targeted or commercial focus.)*


It is a desktop application executable without installation. It is designed to run on Windows and Linux devices with Java version 1.8 or higher installed.


*The generated application (a ".jar" file) is located on the path: .../Renamator/out/artifacts/Renamator_jar/Renamator.jar*



## Procedure ##

At the beginning we need to select the source directory and optionally the destination directory.

* Source directory is a directory where will be renamed names or replace characters in file names and directories.

* If the target directory is specified, then the files from the source directory are first copied to the destination directory. Then changes to file names are made in the destination directory.


Then, on the "Rename" tab, we can set the necessary properties / conditions for rename files and directories. When we do this, the "Rename" button is at the bottom of the tab. Clicking on it will rename the file and directory names according to the set parameters.

Similar is on the "Replace" tab. Once we have tagged an directories, simply set the required parameters and click the "Replace" button at the bottom of the tab.


In the settings (in the Application menu -> Settings), we have options for setting the application language. Whether to view hidden files and directories in the tree structure of the file explorer dialog and to set the file size format in the dialog.

For time reasons, the configured properties are not stored in configuration files. Therefore, changes made to the settings are saved only for the duration of the application run.

*If we have an application running on device with Windows, we should first select the "Show hidden files in the tree structure." in the settings. This is required if we open a file explorer, but the tree structure will be empty. This is because some Windows OS versions may have their root directories hidden. Therefore, they would not be displayed.*



## Control ##

* First we need to select the source and if necessary the target directory. This can be done by clicking on the "Select" button at the top of the application window.


* Another option is to use the file explorer dialog.

    * In this dialog, the root of the disk will open, where we can browse the entire tree structure.

    * In the popup menu above the tree structure we have "basic" options with files and directories. Furthermore, we can set the labeled directory as "source / target" directory.

    * In the right part of the dialog, we have a list of files in the designated directory in the tree structure.

        * There are 3 toolbars at the top of the dialog. From the top of the first bar we have buttons for creating a new file and directory. We can also label the files (the first column in the table) that we want to rename or delete.

        * The second toolbar contains components for filtering files in the selected directory. We can filter the file by name, suffix, date, etc.

        * The last (third) toolbar contains options over the files in the tagged directory. We can mark / unmark them, hide or make them visible, etc.



* "Rename" tab

    * First, enter the name to rename the files.

    * Then select the way of indexing using numbers or letters. In both cases, we can set whether we want to add indexes behind or before the specified name. In case of indexing by letters, we can set whether we want to apply uppercase or lowercase letters.

    * Next, we can set whether we want to apply changes even for nested directories. Respectively whether the directories that are located in the selected directory will be browsed.

        * We can set whether to keep or change selected file types (suffixes).

    * Next, we can set up filtering of files that we want or do not want to rename.

        * For example, if we want to rename all files, only those that contain the specified text or are of a certain type (/ suffix). Or those who meet [regular expression] (https://docs.oracle.com/javase/7/docs/api/java/util/regex/Pattern.html), which we can also test.

    * We also have the option to omit files with a certain extension or selected by us.



* Replace tab

    * Here we have options for ignoring font sizes when testing filenames to be renamed (replace text in their name).

    * Whether directory names are to be tested.

    * Whether to search for nested directories.

    * In the table, always enter the text that contains the filenames and directories to be renamed into the "Original text" column. Enter the text in the second column ("New text") to replace the original text.

        * For example, if we enter the text in the first column "xxx" and in the second column "yy". Then, the app will search for the specified filenames and possibly also the directories. If "xxx" appears in the name, it will be replaced by "yy". So, if the name is "something_xxx_else" then the result will be "something_yy_else" after performing the operation.

    * There are options for skipping selected files or files with a defined extension at the bottom of the tab.