package kruncik.jan.renamator.loadPictures;

/**
 * Tento výčet obsahuje pouze typy obrázků, které lze načíst pro aplikaci. Dle
 * daného výčtového typu se obrázku nastaví nebo nenastaví nová velikost.
 * 
 * @see Enum
 * 
 * @author Jan Krunčík
 * @version 1.0
 */

public enum KindOfPicture {

	/**
	 * Jedná se o ikonu pro aplikaci, není třeba editovat velikost obrázku
	 */
	APP_ICON,

	/**
	 * Jedná se o ikonu do menu aplikace, je třeba upravit velikost obrázku.
	 */
	MENU_ICON,
	
	/**
	 * Jedná se o obrázek, který se bude nacházet v dialogu - ve třídě
	 * AboutAuthorForm v balíčku forms.
	 */
	AUTHOR_ICON,
	
	/**
	 * Jedná se o ikonu do komponenty JTabbedPane v hlavním okně aplikace - záložky
	 * pro přejmenování a nahrazení.
	 */
	TABBED_PANE_ICON,
	
	/**
	 * Jedná se o ikonu do komponenty Jtree v průzkumníku souborů.
	 */
	JTREE_ICON,
	

	/**
	 * Jedná se o ikonu do komponenty Abstract action, které budou zobrazeny v menu,
	 * popupMenu a toolbaru v průzkumníku souborů.
	 */
	TOOLBAR_FILE_EXPLORER,
	
	/**
	 * Jedná se o ikonu do komponenty Jtable - jedná se o zobrazení obrázku v
	 * JTable, tak jej jen zmenším na požadovaný rozměry. jedná se o tabulku ve
	 * třídě FileExplorer v balíčku fileExplorer.
	 */
	FILE_EXPLORER_TABLE_ICON
}
