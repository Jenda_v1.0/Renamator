package kruncik.jan.renamator.loadPictures;

import java.awt.Image;
import java.io.File;
import java.net.URL;

import javax.activation.MimetypesFileTypeMap;
import javax.swing.ImageIcon;

/**
 * Toto rozhraní obsahuje pouze metodu, která slouží pro načtení obrázků do
 * aplikace.
 * 
 * @see URL
 * 
 * @author Jan Krunčík
 * @version 1.0
 */

public interface Pictures {

	/**
	 * Metoda, která slouží pro načtení obrázku ze zdrojového adresáře s obrázky v
	 * aplikaci (pokud se jedná o obrázek, který neslouží jako ikona aplikace či
	 * dialogu, pak se načtený obrázek zmenší, resp. nastaví se mu nová velikost).
	 * 
	 * @param pathToImage
	 *            - Cesta k obrázku (od zdrojového adresáře, ale bez něj)
	 * @param kindOfPicture
	 *            - Typ obrázku, je zde pouze proto, aby se definovala velikost
	 *            obrázku, například, když se jedná o ikonu pro menu, je třeba
	 *            obrázek zmenšit, jinak by byl v původní velikost, což
	 *            jenepřípustné pro aplikaci.
	 * 
	 * @return Načtený obrázek nebo null, pokud obrázek nebude nalezen.
	 */
	static ImageIcon loadImage(final String pathToImage, final KindOfPicture kindOfPicture) {
		final URL url = Pictures.class.getResource(pathToImage);

		if (url != null) {
			final ImageIcon img = new ImageIcon(url);

			if (kindOfPicture == KindOfPicture.APP_ICON)
				return img;

			else if (kindOfPicture == KindOfPicture.MENU_ICON)
				return resizeImage(img, 20, 20);

			else if (kindOfPicture == KindOfPicture.AUTHOR_ICON)
				return resizeImage(img, 200, 200);

			else if (kindOfPicture == KindOfPicture.TABBED_PANE_ICON)
				return resizeImage(img, 15, 15);

			else if (kindOfPicture == KindOfPicture.JTREE_ICON)
				return resizeImage(img, 17, 17);

			else if (kindOfPicture == KindOfPicture.TOOLBAR_FILE_EXPLORER)
				return resizeImage(img, 16, 16);

			else if (kindOfPicture == KindOfPicture.FILE_EXPLORER_TABLE_ICON)
				return resizeImage(img, 25, 25);

		}

		return null;
	}
	
	
	
	
	
	
	/**
	 * Metoda, která vrátí ten samý obrázek (image) akorát bude s rozměnry width, height.
	 * 
	 * @param image - obrázek, kterému se mají změnit rozměry.
	 * 
	 * @param width - nová šířka obrázku.
	 * 
	 * @param height - nový výška obrázku.
	 * 
	 * @return obrázek, který bud
	 */
	static ImageIcon resizeImage(final Image image, final int width, final int height) {
		return new ImageIcon(image.getScaledInstance(width, height, Image.SCALE_SMOOTH));
	}
	
	
	/**
	 * Metoda, která vrátí ten samý obrázek (image) akorát bude s rozměnry width, height.
	 * 
	 * @param image - obrázek, kterému se mají změnit rozměry.
	 * 
	 * @param width - nová šířka obrázku.
	 * 
	 * @param height - nový výška obrázku.
	 * 
	 * @return obrázek, který bud
	 */
	static ImageIcon resizeImage(final ImageIcon image, final int width, final int height) {
		return new ImageIcon(image.getImage().getScaledInstance(width, height, Image.SCALE_SMOOTH));
	}
	
	
	
	
	
	
	
	
	/**
	 * Metoda, která zjistí, zda je obrázek takového typu, který je možné zobrazit v
	 * tabulce, například .jpg, .png apod.
	 * 
	 * V podstatě se zjistí, zda se jedná o obrázek, který je možné zobrazit v
	 * tabulce, postup na zdroji:
	 * https://stackoverflow.com/questions/9643228/test-if-file-is-an-image
	 * 
	 * @param file
	 *            - soubor, o kterémse má zjistit, zda se jedná o obrázek, který
	 *            jemožné zobrazit.
	 * 
	 * @return true, pokud se jedná o obrázek takového typu, které je možné
	 *         zobrazitv tabulce, jinak false.
	 */
	static boolean isImageForShow(final File file) {
		final String mimetype = new MimetypesFileTypeMap().getContentType(file);
		final String type = mimetype.split("/")[0];

		return type.equals("image");
	}
}
