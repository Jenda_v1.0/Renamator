package kruncik.jan.renamator.panels.replacePanel;

import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.util.Properties;

import javax.swing.JCheckBox;
import javax.swing.JLabel;
import javax.swing.JPanel;

import kruncik.jan.renamator.forms.AbstractForm;
import kruncik.jan.renamator.localization.Constants;
import kruncik.jan.renamator.localization.LocalizationImpl;

/**
 * Tato třída slouží puze jako panel, který obsahuje komponenty JCheckbox, které
 * slouží pro nastavení různých parametrů ohledně nahrazení textu v názvech
 * souborů.
 * 
 * @see Properties
 * @see JPanel
 * 
 * @author Jan Krunčík
 * @version 1.0
 */

public class ChcbsPanel extends JPanel implements LocalizationImpl {

	
	private static final long serialVersionUID = 1L;

	
	/**
	 * Label pro nadpis "Nahradit".
	 */
	private final JLabel lblTitle;
	
	
	
	
	private final JCheckBox chcbIgnoreLetterSize, chcbReplaceDirectoryName, chcbSearchSubdirectories;
	
	
	
	
	
	/**
	 * Konstruktor této třídy.
	 */
	ChcbsPanel() {
		super(new GridBagLayout());
		
		/*
		 * "Komponenta" pro rozmístění kompnent v okně.
		 */
		final GridBagConstraints gbc = AbstractForm.createGbc(5, 10, 5, 0);
		
		int rowIndex = 1;
		
		
		
		lblTitle = new JLabel();
		lblTitle.setFont(Constants.FONT_FOR_MAIN_TITLE);
		add(lblTitle, gbc);
		
		
		
		
		chcbIgnoreLetterSize = new JCheckBox();
		AbstractForm.setGbc(gbc, rowIndex, 0);
		add(chcbIgnoreLetterSize, gbc);
		
		
		
		
		
		chcbReplaceDirectoryName = new JCheckBox();
		
		AbstractForm.setGbc(gbc, ++rowIndex, 0);
		add(chcbReplaceDirectoryName, gbc);
		
		
		
		
		
		chcbSearchSubdirectories = new JCheckBox();
		AbstractForm.setGbc(gbc, ++rowIndex, 0);
		add(chcbSearchSubdirectories, gbc);
		
		
		
		/*
		 * Jako výchozí hodnotu zněpřístupním komponenty pro označení nastavení pro
		 * přejmenování podadresářů a jejich souborů ve zvoleném zdrojovém adresáři
		 */
		enableChcbsForSubDirs(false);
	}
	
	
	
	
	
	@Override
	public void setLoadedText(final Properties prop) {
		if (prop != null) {
			lblTitle.setText(prop.getProperty("CP_Lbl_Title", Constants.CP_LBL_TITLE));
			
			
			chcbIgnoreLetterSize.setText("? " + prop.getProperty("CP_Chcb_Ignore_Letter_Size_Text", Constants.CP_CHCB_IGNORE_LETTER_SIZE_TEXT));
			chcbIgnoreLetterSize.setToolTipText(prop.getProperty("CP_Chcb_Ignore_Letter_Size_TT", Constants.CP_CHCB_IGNORE_LETTER_SIZE_TT));
			
			chcbReplaceDirectoryName.setText(prop.getProperty("CP_Chcb_Replace_Directory_Name", Constants.CP_CHCB_REPLACE_DIRECTORY_NAME));
			chcbSearchSubdirectories.setText(prop.getProperty("CP_Chcb_Search_Subdirectories", Constants.CP_CHCB_SEARCH_SUBDIRECTORIES));
		}
		
		else {
			lblTitle.setText(Constants.CP_LBL_TITLE);
			
			
			chcbIgnoreLetterSize.setText("? " + Constants.CP_CHCB_IGNORE_LETTER_SIZE_TEXT);
			chcbIgnoreLetterSize.setToolTipText(Constants.CP_CHCB_IGNORE_LETTER_SIZE_TT);
			
			chcbReplaceDirectoryName.setText(Constants.CP_CHCB_REPLACE_DIRECTORY_NAME);
			chcbSearchSubdirectories.setText(Constants.CP_CHCB_SEARCH_SUBDIRECTORIES);
		}
	}
	
	
	
	
	
	/**
	 * Metoda, která vrátí referenci na komponentu JCheckBox, která zančí, zda se
	 * mají přejmenovat i názvy adresářů nebo ne.
	 * 
	 * @return - reference na JComboBox, který určuje, zda se mají přejmenovat i
	 *         názvy adresářů nebo ne.
	 */
	JCheckBox getChcbReplaceDirectoryName() {
		return chcbReplaceDirectoryName;
	}
	
	
	
	
	/**
	 * Metoda, která vrátí referenci na jCheckBox, který slouží pro definování toho,
	 * že se mají prohledat i podadresáře ve zvoleném zdrojovém adresáři.
	 * 
	 * @return reference na výše popsaný JCheckBox.
	 */
	JCheckBox getChcbSearchSubdirectories() {
		return chcbSearchSubdirectories;
	}
	
	
	
	
	/**
	 * Metoda, která vrátí referenci na JCheckBox, který slouží pro to, zda se mají
	 * při porovnávání názvů souborů se zadanými texty pro nahrazení rozlišovat
	 * velikosti písmen.
	 * 
	 * @return referenci na výše popsaný JCheckBox
	 */
	JCheckBox getChcbIgnoreLetterSize() {
		return chcbIgnoreLetterSize;
	}
	
	
	
	
	
	
	/**
	 * Metoda, která zpřístupní / znepřístupní komponenty JCehckBox pro manipulaci s
	 * podadresáři ve zvoleném zdrojovém adresáři dle toho, zda zvolený zdrojový
	 * adresář obsahuje podadresáře nebo ne.
	 * 
	 * Pokud zvolený zdrojový adresář obsahuje podadresáře, tak se JCheckBoxy pro
	 * manipulaci s názvy podadresářů a souborů v nich zpřístupní, jinak ne.
	 * 
	 * @param enable
	 *            - logická hodnota, true = zpřístupní se výše popsané komponenty,
	 *            false = zněpřístupní se výše popsané komponenty.
	 */
	final void enableChcbsForSubDirs(final boolean enable) {
		chcbSearchSubdirectories.setEnabled(enable);
		chcbReplaceDirectoryName.setEnabled(enable);
	}
}
