package kruncik.jan.renamator.panels.replacePanel;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import java.util.*;

import javax.swing.BorderFactory;
import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;

import kruncik.jan.renamator.app.App;
import kruncik.jan.renamator.forms.AbstractForm;
import kruncik.jan.renamator.jListWithJRadioButtons.ListItem;
import kruncik.jan.renamator.jListWithJRadioButtons.MyJList;

import javax.swing.JTable;

import org.apache.commons.io.FilenameUtils;

import kruncik.jan.renamator.localization.Constants;
import kruncik.jan.renamator.localization.CreateLocalization;
import kruncik.jan.renamator.localization.LocalizationImpl;
import kruncik.jan.renamator.panels.AbstractTableValue;
import kruncik.jan.renamator.panels.ReplaceTextTableModel;
import kruncik.jan.renamator.panels.ReplaceTextTableValue;
import kruncik.jan.renamator.panels.SkipFilesWithNamePanel;
import kruncik.jan.renamator.panels.foldersPanel.FoldersPanel;
import kruncik.jan.renamator.panels.renamePanel.RenamePanel;

/**
 * Tato třída slouží jako panel, který obsahuje komponenty pro nastavení
 * parametrů pro přejmenování souborů.
 * 
 * @see Properties
 * @see ActionListener
 * @see FilenameUtils
 * 
 * @author Jan Krunčík
 * @version 1.0
 * @since 1.8
 */

public class ReplacePanel extends JPanel implements LocalizationImpl, ActionListener {

	private static final long serialVersionUID = 1L;

	
	/**
	 * Komponenta pro vložení tabulky a tlačítek pro manipulaci s daty v tabulce, je
	 * to zde jako globální nebo instanční proměnná, protože tomu chci nastavit
	 * titulek ve zvoleném jazyce.
	 */
	private final JPanel pnlTableValues;
	
	
	
	// Tlačítka pro přidání a odebrání řádku v tabulce a pro prohození textů v
	// sloupcích na označeném řádku a načtení názvů všech souborů, kdyby je chtěl
	// uživatel všechny přejmenovat:
	private final JButton btnAddRow, btnDeleteRow, btnSwapColumns, btnLoadAllNames;
	
	
	
	
	
	/**
	 * Model dat pro tabulku.
	 */
	private final ReplaceTextTableModel tableModel;
	
	/**
	 * Tabulka pro texty.
	 */
	private final JTable table;
	
	
	
	
	/**
	 * Tlačítko pro potvrzení.
	 * 
	 * Po kliknutí na toto tlačítko se nahradí texty v názvech souborů.
	 */
	private final JButton btnConfirm;
	
	
	
	/**
	 * Panel, který obsahuje text pro chcb
	 */
	private final ChcbsPanel chcbsPanel;
	
	
	
	
	
	/**
	 * JcheckBox pro označení, že se mají vynechat soubory s danou příponou.
	 */
	private final JCheckBox chcbSkipFilesWithExtensions;
	
	
	
	/**
	 * JcheckBox pro označení souborů, které se mají vynechat.
	 */
	private final JCheckBox chcbSkipSpecificFiles;
	
	
	
	
	/**
	 * Tlačítko pro označení všech přípon.
	 */
	private final JButton btnSelectAllItemExtensions;
	
	
	
	/**
	 * Tlačítko pro odznačení všech přípon.
	 */
	private final JButton btnDeselectAllItemExtensions;
	
	
	
	
	
	/**
	 * Komponenta JList, která obsahuje veškeré přípony souborů, které se nacházejí
	 * ve zvoleném adresáři. Označené pložky (přípony) budou vynechány s nahrazení
	 * textů v názvech souborů.
	 */
	private final MyJList listSkipFilesWithExtensions;
	
	
	
	
	
	/**
	 * JLabel, který obsahuje nadpis "Vynechat soubory". Pod tímto nadpisem -
	 * labelem jsou dvě pole - listy s příponami a názvy souborů pro vynechání.
	 */
	private final JLabel lblSkipFiles;
	
	
	
	
	
	/**
	 * Panel, který obsahuje komponenty pro vynechání souborů s danou příponou.
	 */
	private final JPanel pnlSkipFilesWithExtensions;
	
	
		
	
	
	
	/**
	 * Panel, který obsahuje kompnenty pro vyhledání a zobrazení souborů ve zvoleném
	 * zdrojovém adresáři.
	 */
	private final SkipFilesWithNamePanel pnlSkipFilesWithName;
	
	
	
	
	
	/**
	 * Reference na instanci panelu folderPanel, ve kterém se zadává cesta ke
	 * zdrojovému a cílovému adresáři a obsahuje chcb pro označení, zda se mají
	 * započítat skryté soubory nebo ne.
	 * 
	 * A to je důvod, proč na tento panel potřebuji referenci, abych zjistil, zda se
	 * mají započítat i skryté soubory nebo ne. Což zjistím z JCheckBoxu v tom
	 * panelu.
	 */
	private final FoldersPanel folderPanel;
	
	
	
	
	
	// Proměnné pro texty do chybových hlášek:
	private String txtUnSelectedRowText, txtUnSelectedRowTitle, txtEmptyTableText, txtEmptyTableTitle,
			txtUnselectedRowForTextSwapText, txtUnselectedRowForTextSwapTitle, txtCopyErrorText, txtCopyErrorTitle,
			txtEmptyFieldInTableText, txtEmptyFieldInTableTitle, txtRenameFileErrorText_1, txtRenameFileErrorText_2,
			txtRenameFileErrorTitle, txtSourceDirDoesntExistOrDidntSetText, txtSourceDirDoesntExistOrDidntSetTitle;
	
	
	
	
	

	/**
	 * Konstruktor této třídy.
	 */
	public ReplacePanel(final Properties prop, final FoldersPanel folderPanel) {
		super(new BorderLayout());
		
		/*
		 * Panel, do kterého vložím všechny komponenty, jinak by byly roztahané po colém
		 * okně, protože by se roztáhly dle velikost okna aplikace, ale já chci, aby
		 * byly u sebe, tak je takto v tom panelu "smrsknu".
		 */
		final JPanel pnlAllComponents = new JPanel(new GridBagLayout());
		
		this.folderPanel = folderPanel;
		
		final GridBagConstraints gbc = AbstractForm.createGbc(5, 10, 5, 0);
		
		int rowIndex = 1;
		
		
		chcbsPanel = new ChcbsPanel();
		chcbsPanel.setLoadedText(prop);
		
		pnlAllComponents.add(chcbsPanel, gbc);
		
		
		
		
		// Panel pro mezeru - odsazení komponent:
		AbstractForm.setGbc(gbc, ++rowIndex, 0);
		pnlAllComponents.add(RenamePanel.getPanelForSpace(), gbc);
		
		
		
		
		
		
		
		// Tabulka:
		tableModel = new ReplaceTextTableModel(prop);		
		table = new JTable(tableModel);		
		final JScrollPane jspTable = new JScrollPane(table);
		
		
		
		
		// Panel, do kterého příjde tabulka a tlačítka pro přidání a odebrání řádku v tabulce:
		pnlTableValues = new JPanel(new BorderLayout());		
		pnlTableValues.add(jspTable, BorderLayout.CENTER);
		
		
		// Panel a tlačítka - pro zarovnání na střed:
		final JPanel pnlButtons = new JPanel(new FlowLayout(FlowLayout.CENTER, 15, 0));
		btnAddRow = new JButton();
		btnDeleteRow = new JButton();
		btnSwapColumns = new JButton();
		btnLoadAllNames = new JButton();
		
		btnAddRow.addActionListener(this);
		btnDeleteRow.addActionListener(this);
		btnSwapColumns.addActionListener(this);
		btnLoadAllNames.addActionListener(this);
		
		pnlButtons.add(btnAddRow);
		pnlButtons.add(btnDeleteRow);
		pnlButtons.add(btnSwapColumns);
		pnlButtons.add(btnLoadAllNames);

		// Přidání panelu s tlačítky a tabulkou:
		pnlTableValues.add(pnlButtons, BorderLayout.SOUTH);		
		
		
		
		AbstractForm.setGbc(gbc, ++rowIndex, 0);
		pnlAllComponents.add(pnlTableValues, gbc);
		
		
		
		
		
		
		
		
		// Panel pro mezeru - odsazení komponent:
		AbstractForm.setGbc(gbc, ++rowIndex, 0);
		pnlAllComponents.add(RenamePanel.getPanelForSpace(), gbc);
		
		
		
		
		
		
		
		
		
		
		// Nadpis pro "vynechání souborů"
		lblSkipFiles = new JLabel();
		lblSkipFiles.setFont(Constants.FONT_FOR_MAIN_TITLE);
		
		AbstractForm.setGbc(gbc, ++rowIndex, 0);
		pnlAllComponents.add(lblSkipFiles, gbc);
		
		
		
		
		
		// chcb pro označení, že se mají přeskočit soubory s danou příponou.
		chcbSkipFilesWithExtensions = new JCheckBox();
		chcbSkipFilesWithExtensions.addActionListener(this);
		
		AbstractForm.setGbc(gbc, ++rowIndex, 0);
		pnlAllComponents.add(chcbSkipFilesWithExtensions, gbc);
		
		// List pro označení přípon souborů, které se mají vynechat.
		listSkipFilesWithExtensions = new MyJList(new ListItem[] {}, JList.HORIZONTAL_WRAP);
		final JScrollPane jspSkipFilesWithExtensions = new JScrollPane(listSkipFilesWithExtensions);
		jspSkipFilesWithExtensions.setPreferredSize(new Dimension(530, 150));
		
		
		btnSelectAllItemExtensions = new JButton();
		btnSelectAllItemExtensions.addActionListener(Event -> listSkipFilesWithExtensions.markAllItems(true));
		
		btnDeselectAllItemExtensions = new JButton();
		btnDeselectAllItemExtensions.addActionListener(Event -> listSkipFilesWithExtensions.markAllItems(false));
		
		
		// Panel pro přidání komponent:
		pnlSkipFilesWithExtensions = new JPanel(new GridBagLayout());
		final GridBagConstraints gbcSkipFilesWEithExtension = AbstractForm.createGbc(5, 20, 5, 0);
		
		
		gbcSkipFilesWEithExtension.gridheight = 2;
		pnlSkipFilesWithExtensions.add(jspSkipFilesWithExtensions, gbcSkipFilesWEithExtension);
		
		
		gbcSkipFilesWEithExtension.gridheight = 1;
		AbstractForm.setGbc(gbcSkipFilesWEithExtension, 0, 1);
		pnlSkipFilesWithExtensions.add(btnSelectAllItemExtensions, gbcSkipFilesWEithExtension);
		
		
		AbstractForm.setGbc(gbcSkipFilesWEithExtension, 1, 1);
		pnlSkipFilesWithExtensions.add(btnDeselectAllItemExtensions, gbcSkipFilesWEithExtension);
		
		
		// Přidání panelu s komponentami:
		AbstractForm.setGbc(gbc, ++rowIndex, 0);
		pnlAllComponents.add(pnlSkipFilesWithExtensions, gbc);
		
		// Výchozí stav panelu je "zněpřístupněn":
		RenamePanel.enabledAllComponents(pnlSkipFilesWithExtensions.getComponents(), false);
		
		
		
		
		
		
		
		
		
		
		
		
		// List pro označení konkrétních souborů, které se mají vynechat.	
		chcbSkipSpecificFiles = new JCheckBox();
		chcbSkipSpecificFiles.addActionListener(this);
		
		AbstractForm.setGbc(gbc, ++rowIndex, 0);
		pnlAllComponents.add(chcbSkipSpecificFiles, gbc);
		
		// Panel, který obsahuje komponenty pro zobrazení listu se všemi načtemými
		// soubory ve zvoleném zdrojovém adresáři a jejich vyhledávání.
		pnlSkipFilesWithName = new SkipFilesWithNamePanel();
		pnlSkipFilesWithName.setLoadedText(prop);
		
		// Výchozí stav:
		RenamePanel.enabledAllComponents(pnlSkipFilesWithName.getComponents(), false);
		
		AbstractForm.setGbc(gbc, ++rowIndex, 0);
		pnlAllComponents.add(pnlSkipFilesWithName, gbc);
		
		
		
		
		
		
		
		
		
		
		
		
		// Tlačítko pro potvrzení:
		btnConfirm = new JButton();
		btnConfirm.addActionListener(this);
		
		// Panel, aby bylo tlačítko na středu v "normální" velikosti:
		final JPanel pnlBtnConfirm = new JPanel(new FlowLayout(FlowLayout.CENTER));
		pnlBtnConfirm.add(btnConfirm);
		
		AbstractForm.setGbc(gbc, ++rowIndex, 0);
		pnlAllComponents.add(pnlBtnConfirm, gbc);
		
		
		// Vložím panel se všemi komponentami do panelu "tohoto" panelu, aby se
		// zobrazili v aplikaci
		add(pnlAllComponents, BorderLayout.NORTH);
	}


	
	
	
	
	
	/**
	 * Metoda, která nastaví data (přípony) do listu, který v okně aplikace
	 * zobrazuje přípony souborů. Soubory s označenými příponymi budou vynechány v
	 * případě nahrazení názvů souborů.
	 * 
	 * @param list
	 *            - list s daty do JList - načtené přípony ze zvoleného adresáře.
	 */
	public final void setDataToListSkipFilesWithExtensions(final List<ListItem> list) {
		listSkipFilesWithExtensions.setListData(list.toArray(new ListItem[] {}));
	}
	

	/**
	 * Metoda, která slouží pro nastavení položek do listu, kteerý v okně aplikace
	 * zobrazuje konkrétní soubory načtené ze zvoleného zdrojového adresáře.
	 * Označené soubory budou vynechány v případě nahrazení textů v názvech souborů.
	 * 
	 * @param list
	 *            - list s daty pro Jlist - načtené soubory ze zdrojového adresáře.
	 */
	public final void setDataToListSkipSpecificFiles(final List<ListItem> list) {
		pnlSkipFilesWithName.setDataToListSkipFilesWithName(list);
	}
	
	
	
	
	
	
	
	

	
	
	@Override
	public void setLoadedText(final Properties prop) {		
		if (prop != null) {
			chcbsPanel.setLoadedText(prop);
			pnlSkipFilesWithName.setLoadedText(prop);
			tableModel.setLoadedText(prop);
			
			
			pnlTableValues.setBorder(BorderFactory.createTitledBorder(prop.getProperty("RP_Pnl_Table_Values", Constants.RP_PNL_TABLE_VALUES)));
			
			btnAddRow.setText(prop.getProperty("RP_Btn_Add_Row_Text", Constants.RP_BTN_ADD_ROW_TEXT));
			btnAddRow.setToolTipText(prop.getProperty("RP_Btn_Add_Row_Tt", Constants.RP_BTN_ADD_ROW_TT));
			
			btnDeleteRow.setText(prop.getProperty("RP_Btn_Delete_Row_Text", Constants.RP_BTN_DELETE_ROW_TEXT));
			btnDeleteRow.setToolTipText(prop.getProperty("RP_Btn_Delete_Row_Tt", Constants.RP_BTN_DELETE_ROW_TT));
			txtUnSelectedRowText = prop.getProperty("RP_TxtUnselected_Row_Text", Constants.RP_TXT_UNSELECTED_ROW_TEXT);
			txtUnSelectedRowTitle = prop.getProperty("RP_Txt_Unselected_Row_Title", Constants.RP_TXT_UNSELECTED_ROW_TITLE);
						
			btnSwapColumns.setText(prop.getProperty("RP_Btn_Swap_Columns_Text", Constants.RP_BTN_SWAP_COLUMNS_TEXT));
			btnSwapColumns.setToolTipText(prop.getProperty("RP_Btn_Swap_Columns_Tt", Constants.RP_BTN_SWAP_COLUMNS_TT));
			txtEmptyTableText = prop.getProperty("RP_Txt_Empty_Table_Text", Constants.RP_TXT_EMPTY_TABLE_TEXT);
			txtEmptyTableTitle = prop.getProperty("RP_Txt_Empty_Table_Title", Constants.RP_TXT_EMPTY_TABLE_TITLE);
			txtUnselectedRowForTextSwapText = prop.getProperty("RP_Txt_Unselected_Row_For_Text_Swap_Text", Constants.RP_TXT_UNSELECTED_ROW_FOR_TEXT_SWAP_TEXT);
			txtUnselectedRowForTextSwapTitle = prop.getProperty("RP_Txt_Unselected_Row_For_Text_Swap_Title", Constants.RP_TXT_UNSELECTED_ROW_FOR_TEXT_SWAP_TITLE);
			
			btnLoadAllNames.setText(prop.getProperty("RP_Btn_Load_All_Name_Text", Constants.RP_BTN_LOAD_ALL_NAMES_TEXT));
			btnLoadAllNames.setToolTipText(prop.getProperty("RP_Btn_Load_All_Name_TT", Constants.RP_BTN_LOAD_ALL_NAMES_TT));
			txtSourceDirDoesntExistOrDidntSetText = prop.getProperty("RP_Txt_Source_Dir_Doesnt_Exist_Or_Didnt_Set_Text", Constants.RP_TXT_SOURCE_DIR_DOESNT_EXIST_OR_DIDNT_SET_TEXT);
			txtSourceDirDoesntExistOrDidntSetTitle = prop.getProperty("RP_Txt_Source_Dir_Doesnt_Exist_Or_Didnt_Set_TT", Constants.RP_TXT_SOURCE_DIR_DOESNT_EXIST_OR_DIDNT_SET_TT);
			
			lblSkipFiles.setText(prop.getProperty("RP_Lbl_Skip_Files", Constants.RP_LBL_SKIP_FILES));
			
			
			chcbSkipFilesWithExtensions.setText(prop.getProperty("RP_Chcb_Skip_Files_With_Extensions", Constants.RP_CHCB_SKIP_FILES_WITH_EXTENSIONS) + ":");
			chcbSkipSpecificFiles.setText(prop.getProperty("RP_Chcb_Skip_Specific_Files", Constants.RP_CHCB_SKIP_SPECIFIC_FILES) + ":");
			
			
			btnSelectAllItemExtensions.setText(prop.getProperty("RP_Btn_Select_All_Item_Extensions", Constants.RP_BTN_SELECT_ALL_ITEM_EXTENSIONS));
			btnDeselectAllItemExtensions.setText(prop.getProperty("RP_Btn_Deselect_All_Item_Extensions", Constants.RP_BTN_DESELECT_ALL_ITEM_EXTENSIONS));
			
			
			
			btnConfirm.setText(prop.getProperty("RP_Btn_Confirm", Constants.RP_BTN_CONFIRM));
			txtCopyErrorText = prop.getProperty("RP_Txt_Copy_Error_Text", Constants.RP_TXT_COPY_ERROR_TEXT);
			txtCopyErrorTitle = prop.getProperty("RP_Txt_Copy_Error_Title", Constants.RP_TXT_COPY_ERROR_TITLE);
			txtEmptyFieldInTableText = prop.getProperty("RP_Txt_Empty_Field_In_Table_Text", Constants.RP_TXT_EMPTY_FIELD_IN_TABLE_TEXT);
			txtEmptyFieldInTableTitle = prop.getProperty("RP_Txt_Empty_Field_In_Table_Title", Constants.RP_TXT_EMPTY_FIELD_IN_TABLE_TITLE);
			
			
			
			txtRenameFileErrorText_1 = prop.getProperty("RP_Txt_Rename_File_Error_Text_1", Constants.RP_TXT_RENAME_FILE_ERROR_TEXT_1);
			txtRenameFileErrorText_2 = prop.getProperty("RP_Txt_Rename_File_Error_Text_2", Constants.RP_TXT_RENAME_FILE_ERROR_TEXT_2);
			txtRenameFileErrorTitle = prop.getProperty("RP_Txt_Rename_File_Error_Title", Constants.RP_TXT_RENAME_FILE_ERROR_TITLE);
			
			
			
			
			
			
			
			// Načtení textů do sloupců:			
			String[] tempColumns = CreateLocalization.getArrayFromText(prop.getProperty("RTTM_Columns"));
			
			/*
			 * Pokud se pole nenačetlo nebo je prázdné - například jej uživatel smazal apod.
			 * Nebo nemá požadovanou délku, pak se vezme původní:
			 */
			if (tempColumns == null || tempColumns.length == 0 || tempColumns.length != 2)
				tempColumns = Constants.REPLACE_TEXT_COLUMNS;
			
			// Přenačtu sloupce v tabulce:									
			RenamePanel.setTableHeaders(table, tempColumns);
		}
		
		
		else {
			pnlTableValues.setBorder(BorderFactory.createTitledBorder(Constants.RP_PNL_TABLE_VALUES));
			
			btnAddRow.setText(Constants.RP_BTN_ADD_ROW_TEXT);
			btnAddRow.setToolTipText(Constants.RP_BTN_ADD_ROW_TT);
			
			btnDeleteRow.setText(Constants.RP_BTN_DELETE_ROW_TEXT);
			btnDeleteRow.setToolTipText(Constants.RP_BTN_DELETE_ROW_TT);
			txtUnSelectedRowText = Constants.RP_TXT_UNSELECTED_ROW_TEXT;
			txtUnSelectedRowTitle = Constants.RP_TXT_UNSELECTED_ROW_TITLE;
						
			btnSwapColumns.setText(Constants.RP_BTN_SWAP_COLUMNS_TEXT);
			btnSwapColumns.setToolTipText(Constants.RP_BTN_SWAP_COLUMNS_TT);
			txtEmptyTableText = Constants.RP_TXT_EMPTY_TABLE_TEXT;
			txtEmptyTableTitle = Constants.RP_TXT_EMPTY_TABLE_TITLE;
			txtUnselectedRowForTextSwapText = Constants.RP_TXT_UNSELECTED_ROW_FOR_TEXT_SWAP_TEXT;
			txtUnselectedRowForTextSwapTitle = Constants.RP_TXT_UNSELECTED_ROW_FOR_TEXT_SWAP_TITLE;
			
			btnLoadAllNames.setText(Constants.RP_BTN_LOAD_ALL_NAMES_TEXT);
			btnLoadAllNames.setToolTipText(Constants.RP_BTN_LOAD_ALL_NAMES_TT);
			txtSourceDirDoesntExistOrDidntSetText = Constants.RP_TXT_SOURCE_DIR_DOESNT_EXIST_OR_DIDNT_SET_TEXT;
			txtSourceDirDoesntExistOrDidntSetTitle = Constants.RP_TXT_SOURCE_DIR_DOESNT_EXIST_OR_DIDNT_SET_TT;
			
			lblSkipFiles.setText(Constants.RP_LBL_SKIP_FILES);
			
			
			chcbSkipFilesWithExtensions.setText(Constants.RP_CHCB_SKIP_FILES_WITH_EXTENSIONS + ":");
			chcbSkipSpecificFiles.setText(Constants.RP_CHCB_SKIP_SPECIFIC_FILES + ":");
			
			
			btnSelectAllItemExtensions.setText(Constants.RP_BTN_SELECT_ALL_ITEM_EXTENSIONS);
			btnDeselectAllItemExtensions.setText(Constants.RP_BTN_DESELECT_ALL_ITEM_EXTENSIONS);
			
			
			
			btnConfirm.setText(Constants.RP_BTN_CONFIRM);
			txtCopyErrorText = Constants.RP_TXT_COPY_ERROR_TEXT;
			txtCopyErrorTitle = Constants.RP_TXT_COPY_ERROR_TITLE;
			txtEmptyFieldInTableText = Constants.RP_TXT_EMPTY_FIELD_IN_TABLE_TEXT;
			txtEmptyFieldInTableTitle = Constants.RP_TXT_EMPTY_FIELD_IN_TABLE_TITLE;
			
			
			
			txtRenameFileErrorText_1 = Constants.RP_TXT_RENAME_FILE_ERROR_TEXT_1;
			txtRenameFileErrorText_2 = Constants.RP_TXT_RENAME_FILE_ERROR_TEXT_2;
			txtRenameFileErrorTitle = Constants.RP_TXT_RENAME_FILE_ERROR_TITLE;
			
			
			// Přenačtu sloupce v tabulce:
			RenamePanel.setTableHeaders(table, Constants.REPLACE_TEXT_COLUMNS);
		}
	}





	
	
	
	@Override
	public void actionPerformed(ActionEvent e) {
		if (e.getSource() instanceof JButton) {
			if (e.getSource() == btnAddRow) {
				/*
				 * Nejprve zjistím, zda se v tabulce neenachází nějaký řádek, který je prázdný
				 * (alespon jeden sloupec), pokud ano, pak žádnou hodnotu nepřídám, pokud ne,
				 * pak přidám nový prázdný řádek.
				 * 
				 * A zkusím oznčit ten samý řádke - pokud nějaký byl:
				 */
				
				if (tableModel.containsEmptyField())
					return;
				
				final int selectedRow = table.getSelectedRow();
				
				tableModel.addValueToList(new ReplaceTextTableValue("", ""));
				
				if (selectedRow > -1)
					table.setRowSelectionInterval(selectedRow, selectedRow);
			}
			
			
			else if (e.getSource() == btnDeleteRow) {
				final int selectedRow = table.getSelectedRow();
				
				if (selectedRow == -1) {
					JOptionPane.showMessageDialog(this, txtUnSelectedRowText, txtUnSelectedRowTitle,
							JOptionPane.ERROR_MESSAGE);
					return;
				}
				
				tableModel.deleteValueAtIndex(selectedRow);				
				
				// Zkusím znovu označit řádek na stejném indexu - pokud to nebyl poslední:
				if (selectedRow < table.getRowCount())
					table.setRowSelectionInterval(selectedRow, selectedRow);
				
				// Označím poslední řádek, ale jen jestli tam nějaký je:
				else if (table.getRowCount() > 0)
					table.setRowSelectionInterval(table.getRowCount() - 1, table.getRowCount() - 1);
			}
			
						
			else if (e.getSource() == btnSwapColumns) {
				/*
				 * Toto tlačítko slouží pro prohození přípon z levého sloupce do pravého a
				 * naopak.
				 * 
				 * Zjistí se, zda není tabulka prázdná (žádné položky - přípony) a zda je
				 * označen nějaký řádek v tabulce, pak se data prohodí, ale jen pokud nebudou
				 * žádné duplicity. - Toto by ale nastat nemělo.
				 */
				if (tableModel.getValuesList().isEmpty()) {
					JOptionPane.showMessageDialog(this, txtEmptyTableText, txtEmptyTableTitle,
							JOptionPane.ERROR_MESSAGE);
					return;
				}
				
				
				final int selectedRow = table.getSelectedRow();
				
				if (selectedRow == -1) {
					JOptionPane.showMessageDialog(this, txtUnselectedRowForTextSwapText,
							txtUnselectedRowForTextSwapTitle, JOptionPane.ERROR_MESSAGE);
					return;
				}
				
				
				/*
				 * Zde je vše v pořádku, pak mohu prohodit texty na označeném řádku a otestovat
				 * duplicity, případně vrátit změny.
				 * 
				 * A označit zpět řádek, který byl označen - kvůli přenačtení tabulky.
				 */
				if (tableModel.swapColumns(selectedRow))
					table.setRowSelectionInterval(selectedRow, selectedRow);
			}
			
			
			else if (e.getSource() == btnLoadAllNames) {
				/*
				 * Zjistím si, zda je označeno, že se mají prohledat i podadresáře nebo ne.
				 * Pokud ano, pak se načtou všechnynázvy ve zvoleném zdrojovém adresáři, pokud
				 * není označen, pak se načtou pouze názvy souborů ve zvoleném zdrojovém
				 * adresáři, ale už se nebudou prohledávat jeho podadresáře.
				 * 
				 * Postup:
				 * - Zda existuje zdrojový adresář,
				 * - Zjistím, zda se mají prohledat podadresáře nebo ne, dle toho se zvolí alg a:
				 * - Načtu si jeho soubory a získám si z nich jen názvy
				 * - Výsledek vložím do tabulky
				 */
				final File file = App.getFileSourceDir();
				
				if (file == null || !file.exists()) {
					JOptionPane.showMessageDialog(this, txtSourceDirDoesntExistOrDidntSetText,
							txtSourceDirDoesntExistOrDidntSetTitle, JOptionPane.ERROR_MESSAGE);
					return;
				}

				/*
				 * List, do kterého se vloží názvy souborů, které se mají zobrazit v tabulce
				 */
				final List<ReplaceTextTableValue> list = new ArrayList<>();

				/*
				 * Pro každý soubor ve zvoleném zdrojovém adresáři zavolám metodu pro
				 * otestování, zda se mjí jednotlivé názvy vložit do toho listu list nebo ne a
				 * zda se mají projít i podadresáře, testovat skryté soubory a adresaáře apod.
				 */
				Arrays.asList(Objects.requireNonNull(file.listFiles()))
						.forEach(f -> loadFileNames(f, list, chcbsPanel.getChcbSearchSubdirectories().isSelected()));

				// Vložím do tabulky hodnoty v listu list výše:
				tableModel.setValuesList(new ArrayList<>(list));
			}
			
			
			
			
			
			
			
			
			
			
			
			
			
			else if (e.getSource() == btnConfirm) {
				// Otestuji zadané adresáře, případně vytvořím cílový adresář - pokud je zadán:
				if (!App.checkDirs())
					return;
				
				/*
				 * Sem se dostaqnu, pokud existuje zdrojový adresář, a cílový pokud byl zadán, byl
				 * vytvořen pokud neexistval, tak jako tak, cílový adresář již existuje, pokud byl zadán.
				 * 
				 * Tak to otestuji a případně zkopíruji soubory do cílového adresáře:
				 */
				if (App.getFileDestinationDir() != null && !App.copySourceDirToDestinationDir()) {
					JOptionPane.showMessageDialog(this, txtCopyErrorText, txtCopyErrorTitle, JOptionPane.ERROR_MESSAGE);
					return;
				}
				
				
				
				// Nyní mohu pokračovat v nahrazení návzů souborů
				
				/*
				 * Nyní otestuji, zda je tabulka prázdná, nebo nějaké její pole, pokud ano, tak
				 * to oznámím uživateli.
				 */
				if (tableModel.getValuesList().isEmpty() || tableModel.isSomeFieldEmpty()) {
					JOptionPane.showMessageDialog(this, txtEmptyFieldInTableText, txtEmptyFieldInTableTitle,
							JOptionPane.ERROR_MESSAGE);
					return;
				}
				
				
				
				/*
				 * V tuto chvíli vím, že existuje minimálne zdrojový adresář, tak pouze
				 * otestuji, zda existuje cílový adresář, když ano, tak budu pracovat se soubory
				 * v něm, pokud cílový adresář neexistuje, pak jej uživatel nezadal a chce
				 * pracovat pouze se soubory ve zdrojovém adresáří.
				 * 
				 * Note:
				 * Toto bych si mohl zjistit z toho, zda se výše zkopírovaly soubory nebo ne,
				 */
				final File file;
				
				if (App.getFileDestinationDir() != null && App.getFileDestinationDir().exists())
					file = App.getFileDestinationDir();
				else
					file = App.getFileSourceDir();
				
				
				/*
				 * Nyní mám v proměnné file adresář, se kterým se bude pracovat, tak jej cyklem
				 * projdu a vždy budu zavolám metodu, která ten soubor otestuje, případně
				 * přejmenuje:
				 */
				Arrays.asList(Objects.requireNonNull(file.listFiles())).forEach(this::checkFile);
				
				
				/*
				 * Na konec otestuji, zda byl zadán jen zdrojový adresář - tj. změnila se data
				 * ve zdrojovém adresáři, pokud ano, pak znovu načtu data do aplikace, protože
				 * už tam nějspíše budou nebo mohou být nové soubory - jejich názvy apod.
				 */
				if (file == App.getFileSourceDir())
					/*
					 * Zde by bylo nejlepší zavolat metodu checkPathFields ve FoldersPanel, nejlépe
					 * dvakrát pro každé textové pole - pro cesty ke zdrojovému a cílovému adresář,
					 * ale v tuto chvíli již vím, že stačí zavolat metodu pro nastavení zdrojového
					 * adresáře, který přenčte potřebná data.
					 * 
					 * Je to možné, protože v tuto chvíli vím, že zdrojový adresář byl zadán a
					 * existuje, protože se v něm změnily data - nejspíše nebyl zadán vhodný cílový
					 * adresář.
					 */
					App.setFileSourceDir(file, FoldersPanel.containsDirSubDirs(file.getAbsolutePath()));
			}
		}
		
		
		
		
		
		
		
		if (e.getSource() instanceof JCheckBox) {
			if (e.getSource() == chcbSkipFilesWithExtensions) {
				RenamePanel.enabledAllComponents(pnlSkipFilesWithExtensions.getComponents(),
						chcbSkipFilesWithExtensions.isSelected());
			}
			
			
			else if (e.getSource() == chcbSkipSpecificFiles) {
				RenamePanel.enabledAllComponents(pnlSkipFilesWithName.getComponents(),
						chcbSkipSpecificFiles.isSelected());
			}		
		}
	}
	
	
	
	
	
	
	
	/**
	 * Metoda, která načte soubory do listu list.
	 * 
	 * V metodě se zjistí, zda se mají testovat kryté soubory nebo ne, pokud ne a
	 * soubor či adresář je skrytý, tak se skončí. Pokud je to adresář, tak se
	 * zjistí, zda se mají prohledavat i adresáře, jinak se také skončí, a pokud je
	 * to soubor, který se má přidat do tabulky, pak se vloží do listu list.
	 * 
	 * Před přidáním do tabulky se zjistí, zda se v listu list nevyskytují žádné
	 * duplicity, ty tam nebudou. Rozeznávají se velikosti písmen. Protože se mi už
	 * nechtělo testovat, o jaký OS se jedná, v linuxu je to třeba ve Windows ne.
	 * 
	 * @param file
	 *            - soubor, který se právě testuje.
	 * 
	 * @param list
	 *            - list, do kterého se vkládají hodnoty - názvy souborů pro vložení
	 *            do tabulky.
	 * 
	 * @param searchSubdirs
	 *            - logická proměnná o tom, zda se mají prohledat i podadresáře.
	 */
	private void loadFileNames(final File file, final List<ReplaceTextTableValue> list,
			final boolean searchSubdirs) {
		if (file.isHidden() && file.isHidden() != folderPanel.getChcbCountHiddenFiles().isSelected())
			return;

		if (file.isDirectory() && searchSubdirs)
			Arrays.asList(Objects.requireNonNull(file.listFiles())).forEach(f -> loadFileNames(f, list, searchSubdirs));

		else if (!containsListValue(list, FilenameUtils.removeExtension(file.getName())))
			list.add(new ReplaceTextTableValue(FilenameUtils.removeExtension(file.getName()), ""));
	}
	
	
	
	
	/**
	 * Metoda, která zjistí, zda se v listu list již nachází hodnota value nebo ne.
	 * Pokud ano, vrátí se true, jinak se projde celý list a vrátí se false.
	 * 
	 * "Metoda slouží pro otestování duplicitních hodnot v listu list."
	 * 
	 * @param list
	 *            - list, ve kterém se má zjistit, zda již obsahuje hodnot value
	 *            nebo ne.
	 * 
	 * @param value
	 *            - hodnota, o které se má zjistit, zda se již nachází v listu list
	 *            nebo ne.
	 * 
	 * @return true, poud se hodnota value již nachází v listu list, jinak false.
	 */
	private static boolean containsListValue(final List<ReplaceTextTableValue> list, final String value) {
		return list.stream().anyMatch(v -> v.getOriginalValue().equals(value));
	}
	
	
	
	
	
	
	
	
	
	
	/**
	 * Metoda, která "otestuje" soubor file. Jedná se o následující, nejprve je
	 * třeba zjistit, zda soubor ještě existuje, ale toto by mělo vždy nastat, tak
	 * by to mělo být zbytečné, pak se otestuje, zda se jendá o adresář, pokud ano,
	 * pak se bude rekurzí pokračovat, ale se soubory z daného adresáře, pak se
	 * otestuje název toho adresáře (v případě, že je označeno, otestovat i názvy
	 * adresářů). A pokud je to "normální" - běžný soubor, pak se zavolá metoda pro
	 * přejmenování.
	 * 
	 * 
	 * @param file
	 *            - soubor / adresář, který se má otestovat na výše zmíněné věci.
	 */
	private void checkFile(final File file) {
		/*
		 * Pokud soubor neexistuje (nemělo by nastat) nebo je skrytý a skryté soubory se
		 * nemají započítat, tak mohu skončit.
		 */
		if (!file.exists() || (file.isHidden() && !folderPanel.getChcbCountHiddenFiles().isSelected()))
			return;

		
		// Pokud to není adresář, a je to kompresní formát, pak se zavolá metoda pro
		// přejmenování souboru
		if (!file.isDirectory() && App.isFileCompressionFormat(file))
			renameFile(file);

		
		else if (file.isDirectory()) {
			// Nejprve otestuji, zda je označeno, že se mají prohledat i adresáře:
			if (chcbsPanel.getChcbSearchSubdirectories().isSelected())
				// Zde se jedná o adresář, tak budu rekurzí pokračovat:
				Arrays.asList(Objects.requireNonNull(file.listFiles())).forEach(this::checkFile);

			/*
			 * Dále musím zjistit, zda se mají přejmenovat i názvy adresářů, pokud ano, pak
			 * musím přejmenovat i tento adresář, tak pro to zavolám metodu:
			 */
			if (chcbsPanel.getChcbReplaceDirectoryName().isSelected())
				renameFile(file);
		}

		
		else
			renameFile(file);
	}
	
	
	
	
	
	
	
	
	
	
	/**
	 * Metoda, která slouží pro přejmenování souborů.
	 * 
	 * Nejprve se otestuje, zda je dany soubor v listu pro vynechání souborů ohledně
	 * změny názvu - nahrazení textů v názvu. Pokud ne, pak otestuji, zda se přípona
	 * souboru nachází v listu s příponami, které se mají vynechat - soubory s touto
	 * příponou také nebudou přejmenovány Pokud je to OK, pak se testuje
	 * následující:
	 * 
	 * Zda název souboru obsahue slova, která se nachází v tabulce ve sloupci
	 * "Původní text", a ještě u toho, zda se při tomto testování má testovat i
	 * velikost písmen nebo ne.
	 * 
	 * Pokud jsou příslušné podmínky splněny, soubor bude přejmenován.
	 * 
	 * @param file
	 *            - soubor / adresář, který se má otestovat, zda se má přejmenovat
	 *            (nahradit texty v názvu souboru), případně se přejmenuje.
	 */
	private void renameFile(final File file) {
		// Získám si příponu daného souboru
		final String extension = FilenameUtils.getExtension(file.getAbsolutePath());
		
		// Pokud není přípona prázdný, tj. není to adresář, pak zjistím, zda se mají
		// soubory
		// s danou příponou vynechat nebo ne, pokud ano, mohu skončit:
		if (chcbSkipFilesWithExtensions.isSelected() && !extension.isEmpty() && isExtensionSelectedForSkip(extension))
			// Zde se má soubor s danou příponou vynechat, pak mohu skončit:
			return;

		
		
		
		
		
		// Zjistím, zda je soubor označen v listu pro přeskočení souborů:
		if (chcbSkipSpecificFiles.isSelected() && isFileSelectedForSkip(file))
			// Tento soubor se má vynechat, tak jej přeskočím, tj. zde mohu skončit:
			return;

		

		
		// Název souboru bez přípony:
		final String fileName = FilenameUtils.removeExtension(file.getName());
		
		final AbstractTableValue value = containsTableFileName(fileName,
				chcbsPanel.getChcbIgnoreLetterSize().isSelected());
		
		/*
		 * Info:
		 * Nový název vezmu tak, že nahradím text z value v fileName novým textem ve
		 * value a vytvořím nový File, na který se přejmenuje ten původní, ten nový file
		 * vytvořím pomocí metody getParent, protože úplně každý soubor bude mít alespoň
		 * jednoho předka - adresář, ve kterém se nachází.
		 */
		
		if (value != null) {
			// Zde byla nalezena shoda, tak mohu přejmenovat soubor, ale opět musím hlídat, zda se
			// mají nahradit texty v názvu s tím, že se hlídají nebo nehlídají velikosti písmen:
			
			// Nový název souboru:
			final File newFile;
			
			if (chcbsPanel.getChcbIgnoreLetterSize().isSelected()) {				
				// Zde se hlídají velikosti písmen:
				
				// Pokud má soubor nějaku příponu, tak ji musím zpět přidat:
				if (!extension.isEmpty())
					newFile = new File(file.getParent() + File.separator
							+ fileName.replaceAll("(?i)" + value.getOriginalValue(), value.getNewValue()) + "."
							+ extension);

				else
					newFile = new File(file.getParent() + File.separator
							+ fileName.replaceAll("(?i)" + value.getOriginalValue(), value.getNewValue()));
			}

			
			// Zde se nemusí hlídat velikosti písmen:
			else {
				// Pokud má soubor nějaku příponu, tak ji musím zpět přidat:
				if (!extension.isEmpty())
					newFile = new File(file.getParent() + File.separator
							+ fileName.replaceAll(value.getOriginalValue(), value.getNewValue()) + "." + extension);

				else
					newFile = new File(file.getParent() + File.separator
							+ fileName.replaceAll(value.getOriginalValue(), value.getNewValue()));
			}
			
			
			final boolean success = file.renameTo(newFile);

			if (!success)
				JOptionPane.showMessageDialog(
						this, txtRenameFileErrorText_1 + ": " + file.getAbsolutePath() + "\n:"
								+ txtRenameFileErrorText_2 + newFile.getAbsolutePath(),
						txtRenameFileErrorTitle, JOptionPane.ERROR_MESSAGE);
		}
	}
	
	
	
	
	
	
	

	
	/**
	 * Metoda, která zjistí, zda název souboru obsahuje nějaký z uživatelem zadaných
	 * názvů v tabulce. Pokud ano, vrátí položku z tabulky, abych si mohl vzít nový
	 * název a nahradit jím ten původní.
	 * 
	 * @param name
	 *            - název aktuálně iterovaného souboru.
	 * @param ignoreLettersSize
	 *            - logická hodnota o tom, zda se mají ignorovat velikosti písmen.
	 * @return null, pokud tabulka neobsahuje text, který se nachází v name, jiaak
	 *         se vrátí AbstractTableValue, která osabuje původní text, který se má
	 *         nahradit a nový text, který má nahradit ten původní v názvu souboru.
	 */
	private AbstractTableValue containsTableFileName(final String name, final boolean ignoreLettersSize) {
		// Data z tabulky:
		final List<AbstractTableValue> values = tableModel.getValuesList();		

		/*
		 * Musí být v bloku try catch, protože pokud nebude nalezena shoda, pak se
		 * vyhodí vyjímka, kterou musím zahytit a vrátit tak null hodnotu, dle které
		 * poznám, že se soubor nemá přejmenovat - v tabulce není zadán text, který by
		 * název souboru obsahovat.
		 */
		
		try {
			if (ignoreLettersSize)
				// Zde se mají ignorovat velikosti písmen:
				return values.stream().filter(v -> name.toUpperCase().contains(v.getOriginalValue().toUpperCase()))
						.findFirst().get();

			// Zde se nemá ignorovat velikost písmen, tak zjistím, zda název souboru
			// obsahuje nějaký text z tabulky:
			return values.stream().filter(v -> name.contains(v.getOriginalValue())).findFirst().get();
		} catch (NoSuchElementException e) {
			return null;
		}
	}
	
	
	
	
	
	
	
	
	
	/**
	 * Metoda, která zjistí, zda je soubor file označen v aplikaci jako soubor, který se má vynechat.
	 * Tj. je označen v listu, který je v panelu pnlSkipFilesWithName.
	 * 
	 * @param file - Soubor, o kterém se má zjistit, zda se má vynechat nebo ne. (Zda je označen v listu v aplikaci,
	 * jako že semá přeskočit)
	 * 
	 * @return true, pokud se má soubor vynchat, jinak false.
	 */
	private boolean isFileSelectedForSkip(final File file) {
		/*
		 * Zde potřebuji otestovat, zda byl zadán cílový adresář nebo ne. Pokud ano, pak
		 * se do něj soubory zkopírovaly a musím při každém souboru v listu v aplikaci
		 * zaměnit cesty dle toho, zda byl zadán cílový adresář nebo ne.
		 * 
		 * Pokud nebyl zadán cílový adresář, pak to nemusím řešit.
		 * 
		 * Pokud byl zadán cílový adresář, pak musím zaměnit cestu ke zdrojovém adresáři
		 * za cestu k cílovému adresáři.
		 * 
		 * V listu v okně aplikace jsou zobrazeny soubory od zdrojového adresáře dál,
		 * tak když je zadán cílový adresář změním tuto cestu
		 * 
		 * V aplikaci SourceDir:
		 * file 1
		 * xxx/file 2
		 * sss/aaa/file 3
		 * 
		 * jen zaměním cestu při testování:
		 * DestinationDir -> a zde přiložím soubory výše, to stačí otestovat.
		 */
		
		if (App.getFileDestinationDir() != null)
			return pnlSkipFilesWithName.getAllItemsForListSkipFilesWithName().stream()
					.anyMatch(v -> (App.getFileDestinationDir().getAbsolutePath() + "/" + v.toString())
							.equals(file.getAbsolutePath()) && v.isSelected());

		return pnlSkipFilesWithName.getAllItemsForListSkipFilesWithName().stream()
				.anyMatch(v -> v.getFile().getAbsolutePath().equals(file.getAbsolutePath()) && v.isSelected());
	}
	
	
	
	
	
	
	
	
	
	/**
	 * Metoda, která zjistí, zda se přípona extension nachází v listu jako přípona
	 * souboru, který se má vynechat. Přípony se v aplikaci nachází v listu
	 * listSkipFilesWithExtensions.
	 * 
	 * @param extension
	 *            - přípona souboru, o které se má zjistit, zda ji uživatel označil
	 *            jako příponu, která se má vynechat nebo ne.
	 * 
	 * @return - true, pokud je přípona označena, jako, že se má vynechat, jinak
	 *         false.
	 */
	private boolean isExtensionSelectedForSkip(final String extension) {
		final int size = listSkipFilesWithExtensions.getModel().getSize();

		for (int i = 0; i < size; i++) {
			final ListItem item = listSkipFilesWithExtensions.getModel().getElementAt(i);

			if (item.isSelected() && FilenameUtils.getExtension(item.getFile().getAbsolutePath()).equals(extension))
				return true;
		}

		return false;
	}
	
	
	
	
	
	/**
	 * Metoda, která zpřístupní / znepřístupní komponenty JCehckBox pro manipulaci s
	 * podadresáři ve zvoleném zdrojovém adresáři v panelu ChcbsPanel dle toho, zda
	 * zvolený zdrojový adresář obsahuje podadresáře nebo ne.
	 * 
	 * Pokud zvolený zdrojový adresář obsahuje podadresáře, tak se JCheckBoxy pro
	 * manipulaci s názvy podadresářů a souborů v nich zpřístupní, jinak ne.
	 * 
	 * @param enable
	 *            - logická hodnota, true = zpřístupní se výše popsané komponenty,
	 *            false = zněpřístupní se výše popsané komponenty.
	 */
	public final void enableChcbsForSubDirs(final boolean enable) {
		chcbsPanel.enableChcbsForSubDirs(enable);
	}
}
