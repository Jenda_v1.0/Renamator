package kruncik.jan.renamator.panels;

import java.util.List;

import javax.swing.table.AbstractTableModel;

/**
 * Tato třída slouží jako abstraktní model pro tabulky pro nahrazení textů a
 * přípon.
 * 
 * @see AbstractTableModel
 * @see List
 * 
 * @author Jan Krunčík
 * @version 1.0
 * @since 1.8
 */

public abstract class AbstTableModel extends AbstractTableModel {

	
	private static final long serialVersionUID = 1L;

	

	/**
	 * List hodnot do tabulky.
	 */
	List<AbstractTableValue> valuesList;
	
	
	/**
	 * Jednorozměrné pole, které obsahuje názvy sloupců.
	 */
	String[] columns;
	
	
	
	
	
	
	/**
	 * Metoda, která vloží list hodnot do tabulky.
	 * 
	 * @param valuesList
	 *            - list hodnot do tabulky.
	 */
	public void setValuesList(final List<AbstractTableValue> valuesList) {
		this.valuesList = valuesList;

		fireTableDataChanged();
	}
	
	
	
	/**
	 * Metoda, která odebere objekt z listu (z tabulky) na zadaném indexu
	 * 
	 * @param index
	 *            - Index, na kterém se má odebrat objekt.
	 */
	public void deleteValueAtIndex(final int index) {
		valuesList.remove(index);
		
		fireTableDataChanged();
	}
	
	
	
	
	/**
	 * Metoda, která do tabulky - do listu přidá novou hodnotu (value) a aktualizuje
	 * tabulku.
	 * 
	 * @param value
	 *            -nová hodnota, která se má přidat do tabulky - do listu dat v
	 *            tabulce.
	 */
	public void addValueToList(final AbstractTableValue value) {
		valuesList.add(value);

		fireTableDataChanged();
	}
	
	
	
	/**
	 * Metoda, která vrátí list s hodnotami v tabulce.
	 * 
	 * @return list, který obsahuje hodnoty v tabulce.
	 */
	public List<AbstractTableValue> getValuesList() {
		return valuesList;
	}
	
	
	
	
	
	@Override
	public int getRowCount() {
		return valuesList.size();
	}

	@Override
	public int getColumnCount() {
		return columns.length;
	}

	@Override
	public Object getValueAt(int rowIndex, int columnIndex) {
		final AbstractTableValue value = valuesList.get(rowIndex);

		switch (columnIndex) {
		case 0:
			return value.getOriginalValue();

		case 1:
			return value.getNewValue();

		default:
			break;
		}

		return "?";
	}

	
	
	
	
	
	@Override
	public String getColumnName(int column) {
		return columns[column];
	}
	
	
	
	
	@Override
	public Class<?> getColumnClass(int columnIndex) {
		final Object obj = getValueAt(0, columnIndex);

		if (obj != null)
			return obj.getClass();

		return super.getColumnClass(columnIndex);
	}
	
	
	
	@Override
	public boolean isCellEditable(int rowIndex, int columnIndex) {
		return true;
	}
	
	
	
	
	
	
	
	/**
	 * Metoda, která otestuje, zda se v listu nachází nějaké duplicity.
	 * 
	 * Konkrétně zjistí, zda se v listu nachází nějaké hodnoty:
	 * 
	 * původní hodnota stejná jako nová hodnota na stejném řádku (stejná původní
	 * hodnota jako ta nová v jednom objektu)
	 * nebo
	 * stejná hodnota v jednom sloupci - plati pro oba sloupce.
	 * 
	 * @return false, pokud se v listu nevyskytují žádné z uvedených duplicit, jinak
	 *         true, pokud se vlistu nachází nějká duplicita, buď v řádku nebo v
	 *         jednom ze sloupců.
	 */
	final boolean containsListDuplicates() {
//		mohla byt tecka v pravo - pak aby to nebyla duplicita
//		v levem sloupci nejsou duplicity - OK
//		v pravem sloupci nejsou duplicity - OK
//		hodnota v levem sloupci neni v pravem sloupci v jednom radku - v jednom objektu - OK
//		hodnota v levem sloupci neni kdekoli v pravem sloupci - v různych objektech - OK
		
		return valuesList.stream()
				// hodnota v levem sloupci neni v pravem sloupci v jednom radku - v jednom
				// objektu
				.anyMatch(v -> v.getOriginalValue().equalsIgnoreCase(v.getNewValue()) || valuesList.stream()
						// v levem sloupci nejsou duplicity
						.anyMatch(c -> c != v && (v.getOriginalValue().equalsIgnoreCase(c.getOriginalValue())
								// v pravem sloupci nejsou duplicity, zde ještě testuji, zda není v pravém
								// sloupci tečka, protože v tomto sloupci jich může být více v případě, že se
								// načtou přípony z adresáře. A zároveň v pravém sloupci nesmí být prázdná
								// hodnota, protože,
								// pokud se načtou názvy souborů pro tabulku nahrazení textů v názvech souborů,
								// pak ty buňky budou prázdné,
								// pak je také nemohu testovat:
								|| (v.getNewValue().equalsIgnoreCase(c.getNewValue()) && !v.getNewValue().equals(".")
										&& !v.getNewValue().isEmpty())))
		// hodnota v levem sloupci neni kdekoli v pravem sloupci - v různych objektech
						|| valuesList.stream()
								.anyMatch(q -> q != v && v.getOriginalValue().equalsIgnoreCase(q.getNewValue())));
	}
	
	
	
	
	
	
	
	
	
	
	
	/**
	 * Metoda, která prohodí data v slopcích na označeném řádku (objekt v listu
	 * valuesList na indexu rowIndex).
	 * 
	 * Nejprve se získá objekt na označeném řádku v tabulce (rowIndex), pak se
	 * prohodí hodnoty (hodnota v levém sloupci se vloží do pravého sloupce a
	 * naopak).
	 * 
	 * Pak se zjístí, zda tabulka obsahuje nějaké duplicity - dle metody
	 * containsListDuplicates - nemělo by nastat, ale pro jistotu.
	 * 
	 * Pokud se najdou nějaké duplicity (nemělo by), pak se vrátí původní změny,
	 * pokud se nenajdou žádné duplicity (mělo by nastat vždy), pak se aktualizje
	 * tabulka, aby si uživatel všiml změn a vrátí se true, jinak false.
	 * 
	 * @param rowIndex
	 *            - Index řádku - objektu v listu valuesList, kde se mají prohodit
	 *            data ve sloupcích.
	 * 
	 * @return true, pokud se data ve sloupcích prohodí v pořádku, jinak false.
	 */
	public final boolean swapColumns(final int rowIndex) {
		// Získám si objekt z tabulky:
		final AbstractTableValue tableValue = valuesList.get(rowIndex);
		
		// Uložím si do proměnných data v ze získaného objektu v tabulce:
		final String originalValue = tableValue.getOriginalValue();
		final String newValue = tableValue.getNewValue();

		// Prohodím hodnoty na označeném řádku v tabulce:
		tableValue.setOriginalValue(newValue);
		tableValue.setNewValue(originalValue);

		// Otestuji duplicity:
		if (containsListDuplicates()) {
			/*
			 * Zde byly v tabulce nalezeny duplicity, tak vrátím původní hodnoty:
			 */
			tableValue.setOriginalValue(originalValue);
			tableValue.setNewValue(newValue);

			/*
			 * Zde tabulka obsahuje duplicity, pak vrátím false - data se nezmění - je to
			 * potřeba, aby se nemusel označovat původní řádek
			 */
			return false;
		} else {
			/*
			 * Zde se data ve sloupcích v pořádku prohodila a neobsahují duplicity, tak
			 * aktualizuji tabulku, aby se projevily změny a vrátím true, jako že je to OK a
			 * "znamění", že se může znovu označit původní řádek, aby se v tom "lépe"
			 * uživatel zorientoval a všiml si změny.
			 */
			fireTableDataChanged();
			return true;
		}
	}
}
