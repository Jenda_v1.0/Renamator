package kruncik.jan.renamator.panels;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Properties;

import javax.swing.JOptionPane;

import kruncik.jan.renamator.localization.Constants;
import kruncik.jan.renamator.localization.CreateLocalization;
import kruncik.jan.renamator.localization.LocalizationImpl;

/**
 * Tato třída slouží pouze jako model pro tabulku, do které se zadávají data pro
 * nahrazení textů vnázvech souborů, případně i adresářů.
 * 
 * Do levého / prvního sloupce se zadá nějaký text, a do druhého / pravého
 * sloupce se zadá nový text, kterým se má nahradit.
 * 
 * @see Properties
 * 
 * @author Jan Krunčík
 * @version 1.0
 * @since 1.8
 */

public class ReplaceTextTableModel extends AbstTableModel implements LocalizationImpl {

	
	private static final long serialVersionUID = 1L;
	
	
	
	
	// Texty s výchozími hodnotami pro nahrazení textů jako ukázka uživateli:
	private String txtOriginalValue, txtNewValue;
	
	
	// Texty do chybových hlášení:
	private String txtWrongSyntaxText, txtWrongSyntaxTitle;
	
	
	
	
	public ReplaceTextTableModel(final Properties prop) {
		super();
		
		/*
		 * Kvůli názvu sloupců
		 */
		setLoadedText(prop);
		
		/*
		 * Zde když mám načtené potřebné texty, tak ještě přidám výchozí hodnotu s texty
		 * ve zvoleném jazyce.
		 */
		setValuesList(new ArrayList<>(Collections.singletonList(new ReplaceTextTableValue(txtOriginalValue, txtNewValue))));
	}
	
	
	
	
	
	@Override
	public void setValueAt(Object aValue, int rowIndex, int columnIndex) {
		final String text = ((String) aValue).trim();
		
		if (!text.matches(Constants.REG_EX_FILE_NAME)) {
			JOptionPane.showMessageDialog(null, txtWrongSyntaxText, txtWrongSyntaxTitle, JOptionPane.ERROR_MESSAGE);
			return;
		}
		
		
		final ReplaceTextTableValue item = (ReplaceTextTableValue) valuesList.get(rowIndex);
		
		
		switch (columnIndex) {
		case 0:
			/*
			 * Aby nebyly duplicity ani ve stejnem sloupci, ani v druhem sloupci, protoze
			 * kdyz bude v levem sloupci původni nazev, a stejny nazev bude v novem sloupci,
			 * tak je to, jako by nedoslo ke zmene, ale nehlidam velikost pismen - kvůli
			 * jednomu pismenu velkemu nebo malemu, je to asi jedno.
			 * 
			 * Postup: Nejprve nastavím text do objektu, pak prohledám list na výše uvedené
			 * duplicity a pokud žádné nejsou, pak to nechám, ale pokud se najde nějaká
			 * duplicita, pak vrátím změny a nedovolím uživateli napsat daný text do
			 * tabulky.
			 */
			final String originalText = item.getOriginalValue();
			
			item.setOriginalValue(text);
			
			if (containsListDuplicates())
				item.setOriginalValue(originalText);	
			break;

		case 1:
			/*
			 * Zde se jedná o stejný postup jako výše, ale s tímto sloupcem.
			 */
			final String newValue = item.getNewValue();
			
			item.setNewValue(text);
			
			if (containsListDuplicates())
				item.setNewValue(newValue);
			
			break;

		default:
			break;
		}
	}

	
	
	
	
	
	/**
	 * Metoda, která projde celý list a pokud se najde nějaké pole, které je
	 * prázdné, tj. je alespoň jedno prázdné pole buď jako původní text nebo nový
	 * text (alespoň jedno z toho), pak se nedovolí přidat nový řádek do tabulky.
	 * 
	 * @return true, pokud existuje v tabulce alespoň jedno prázdné pole (viz výše),
	 *         jinak false.
	 */
	public final boolean containsEmptyField() {
		return valuesList.stream().anyMatch(v -> v.getOriginalValue().isEmpty() || v.getNewValue().isEmpty());
	}
	
	
	
	
	
	
	
	/**
	 * Metoda, která projde všechny hodnoty v tabulce a zjistí, zda je nějaká
	 * hodnota prázdná, tj. zda jsou naplněna všech hodnoty v tabulce. Jak původní,
	 * tak nové názvy, resp. texty pro názvy souborů
	 * 
	 * @return true, pokud je nějaké pole v tabulce prázdné, jinak false, když jsou
	 *         všechny políčka v tabulce obsazena.
	 */
	public final boolean isSomeFieldEmpty() {
		return valuesList.stream().anyMatch(v -> v.getOriginalValue() == null || v.getOriginalValue().trim().isEmpty()
				|| v.getNewValue() == null || v.getNewValue().trim().isEmpty());
	}
	
	
	
	
	
	
	
	@Override
	public void setLoadedText(final Properties prop) {
		if (prop != null) {
			// Načtení textů do sloupců:			
			String[] tempColumns = CreateLocalization.getArrayFromText(prop.getProperty("RTTM_Columns"));
			
			/*
			 * Pokud se pole nenačetlo nebo je prázdné - například jej uživatel smazal apod.
			 * Nebo nemá požadovanou délku, pak se vezme původní:
			 */
			if (tempColumns == null || tempColumns.length == 0 || tempColumns.length != 2)
				tempColumns = Constants.REPLACE_TEXT_COLUMNS;
			
			columns = tempColumns;
			
			
			txtOriginalValue = prop.getProperty("RTTM_Txt_Original_Value", Constants.TXT_ORIGINAL_VALUE);
			txtNewValue = prop.getProperty("RTTM_Txt_New_Value", Constants.TXT_NEW_VALUE);
			
			txtWrongSyntaxText = prop.getProperty("RTTM_Jop_Txt_Wrong_Syntax_Text", Constants.RTTM_JOP_TXT_WRONG_SYNTAX_TEXT);
			txtWrongSyntaxTitle = prop.getProperty("RTTM_Jop_Txt_Wrong_Syntax_Title", Constants.RTTM_JOP_TXT_WRONG_SYNTAX_TITLE);
		}
		
		else {
			columns = Constants.REPLACE_TEXT_COLUMNS;
			
			
			
			txtOriginalValue = Constants.TXT_ORIGINAL_VALUE;
			txtNewValue = Constants.TXT_NEW_VALUE;
			
			txtWrongSyntaxText = Constants.RTTM_JOP_TXT_WRONG_SYNTAX_TEXT;
			txtWrongSyntaxTitle = Constants.RTTM_JOP_TXT_WRONG_SYNTAX_TITLE;
		}
	}
}
