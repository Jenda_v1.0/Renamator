package kruncik.jan.renamator.panels;

/**
 * Tato třída slouží puuze jako objekt pro vkládání do tabulky.
 * 
 * Jedná se o "ukládání" a předávání hodnota coby původní a nové připony
 * souborů.
 * 
 * @author Jan Krunčík
 * @version 1.0
 */

public class ChangeFileType extends AbstractTableValue {

	/**
	 * Konstruktor této třídy.
	 * 
	 * @param originalFileType
	 *            - Proměnná, která obsahuje původní příponu souboru, resp. ten typ
	 *            souboru, který se má změnit. Tento typ se nahradí novým typem -
	 *            newFileType.
	 * 
	 * @param newFileType
	 *            - Proměnná, která osabuje novou připonu souborů. Tato přípona
	 *            nahradí původní příponu.
	 */
	public ChangeFileType(final String originalFileType, final String newFileType) {
		super(originalFileType, newFileType);
	}
}
