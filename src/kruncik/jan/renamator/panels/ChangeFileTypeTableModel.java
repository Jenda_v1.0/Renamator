package kruncik.jan.renamator.panels;

import java.util.NoSuchElementException;
import java.util.Properties;

import javax.swing.JOptionPane;

import kruncik.jan.renamator.localization.Constants;
import kruncik.jan.renamator.localization.CreateLocalization;
import kruncik.jan.renamator.localization.LocalizationImpl;

/**
 * Tato třída slouží jako model pro tabulky, které slouží pro zadání hodnot coby
 * změna typu (přípony) souboru.
 * 
 * Tabulka obsahuje pouze dva sloupce, do jednoho se zadává aktuální přípona
 * souboru a do druhého se zadává nová přípona souboru, resp. přípona, která
 * nahradí tu původní.
 *
 * @see Properties
 * 
 * @author Jan Krunčík
 * @version 1.0
 * @since 1.8
 */

public class ChangeFileTypeTableModel extends AbstTableModel implements LocalizationImpl {

	
	private static final long serialVersionUID = 1L;

	
	
	
	
	// Proměnné pro texty do chybových hlášek.
	private String txtWrongExtensionText, txtWrongExtensionTitle;
	
	
	
	
	
	
	
	/**
	 * Konstruktor této třídy.
	 */
	public ChangeFileTypeTableModel(final Properties prop) {
		super();	
		
		/*
		 * Na začátku zde potřebuji načíst alespoň výchozí texty do sloupců ve zvoleném
		 * jazyce, Pak u přejmenování už je to v pohodě, ale na začátku, zda potřebuji
		 * načíst alespoň výchozí text.
		 */
		setLoadedText(prop);
	}
	
	
	
	
	
	
	@Override
	public void setValueAt(Object aValue, int rowIndex, int columnIndex) {
		final String fileType = ((String) aValue).trim();
		
		if (!fileType.matches(Constants.REG_EX_FOR_FILE_EXTENSION)) {
			JOptionPane.showMessageDialog(null, txtWrongExtensionText, txtWrongExtensionTitle,
					JOptionPane.ERROR_MESSAGE);
			return;
		}
		
		
		
		final ChangeFileType row = (ChangeFileType) valuesList.get(rowIndex);
		
		switch (columnIndex) {
		case 0:
			/*
			 * Nejprve nastavím novou hodnotu do příslušné proměnné, pak zjistím, zda jsou
			 * nějaké duplicity v listu všech zadaných přípon a pokud ne, je to OK, pokud
			 * ano, tak vrátím původní hodnotu.
			 */
			final String original_1 = row.getOriginalValue();
			
			row.setOriginalValue(fileType);
			
			if (containsListDuplicates())
				// Zde byla nalezena duplicita, tak vrátím původní hodnotu
				row.setOriginalValue(original_1);
			break;

		case 1:
			/*
			 * Nejprve nastavím novou hodnotu do příslušné proměnné, pak zjistím, zda jsou
			 * nějaké duplicity v listu všech zadaných přípon a pokud ne, je to OK, pokud
			 * ano, tak vrátím původní hodnotu.
			 */
			final String original_2 = row.getNewValue();
			
			row.setNewValue(fileType);

			if (containsListDuplicates())
				// Zde byla nalezena duplicita, tak vrátím původní hodnotu
				row.setNewValue(original_2);
			break;

		default:
			break;
		}
	}


	
	
	
	
	
	
	/**
	 * Metoda, která zjistí, zda je nějaké pole nevyplněno, tj. zda je někdě pouze
	 * výchozí hodnota a sice pouze desetinná tečka.
	 * 
	 * @return true, pokud bude nalezeno pole, kde je pouze tečka, jinak false.
	 */
	public final boolean containsSomePoint() {
		return valuesList.stream().anyMatch(v -> v.getOriginalValue().replaceAll("\\s*", "").equalsIgnoreCase(".")
				|| v.getNewValue().replaceAll("\\s*", "").equalsIgnoreCase("."));
	}
	
	
	
	
	
	

	/**
	 * Metoda, která zjistí, zda se v tabulce (v table modelu) nachází řádek, který
	 * obsahuje jako původní příponu příponu v parametru této metody -
	 * originalExtension.
	 * 
	 * Pokud se v tabulce najde takoý čádek, pak se celý vrátí.
	 * 
	 * @param originalExtension
	 *            - Přípona nějakého souboru, o kterém se má zjistit, zda se nachází
	 *            v tabulce, pokud ano, bude vrácen celý řádek z tabulky. Pokud ne,
	 *            vrátí se null.
	 * @return AbstractTableValue - řádek v tabulce, který jako původní příponu
	 *         obsahuje originalExtensio a jako novou příponu obsahuje novou přípnu
	 *         souboru, na který se má ten původní změnit, vrátí se zmíněná
	 *         AbstractTableValue, resp. řáádek v tabulce s nalezenou původní
	 *         příponou nebo null, pokud v tabulce taková původní přípona není..
	 */
	public final AbstractTableValue getValueWithOriginalExtension(final String originalExtension) {
		try {
			return valuesList.stream().filter(v -> v.getOriginalValue().equalsIgnoreCase(originalExtension)).findFirst()
					.get();
		} catch (NoSuchElementException e) {
			return null;
		}
	}
	
	
	
	


	
	


	@Override
	public void setLoadedText(final Properties prop) {
		if (prop != null) {
			// Načtení textů do sloupců:			
			String[] tempColumns = CreateLocalization.getArrayFromText(prop.getProperty("CFTTM_Columns"));
			
			/*
			 * Pokud se pole nenačetlo nebo je prázdné - například jej uživatel smazal apod.
			 * Nebo nemá požadovanou délku, pak se vezme původní:
			 */
			if (tempColumns == null || tempColumns.length == 0 || tempColumns.length != 2)
				tempColumns = Constants.CHANGE_FILE_TYPE_COLUMNS;
			
			columns = tempColumns;
			
			
			// Chybove hlasky:
			txtWrongExtensionText = prop.getProperty("CFTTM_Jop_Wrong_Extension_Text", Constants.CFTTM_JOP_WRONG_EXTENSION_TEXT);
			txtWrongExtensionTitle = prop.getProperty("CFTTM_Jop_Wrong_Extension_Title", Constants.CFTTM_JOP_WRONG_EXTENSION_TITLE);
		}
		
		
		else {
			columns = Constants.CHANGE_FILE_TYPE_COLUMNS;
			

			// Chybove hlasky:
			txtWrongExtensionText = Constants.CFTTM_JOP_WRONG_EXTENSION_TEXT;
			txtWrongExtensionTitle = Constants.CFTTM_JOP_WRONG_EXTENSION_TITLE;
		}
	}
}
