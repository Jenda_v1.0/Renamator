package kruncik.jan.renamator.panels;

import java.awt.Dimension;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.util.List;
import java.util.Properties;

import javax.swing.JButton;
import javax.swing.JList;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextField;

import kruncik.jan.renamator.forms.AbstractForm;
import kruncik.jan.renamator.jListWithJRadioButtons.ListItem;
import kruncik.jan.renamator.jListWithJRadioButtons.MyJList;
import kruncik.jan.renamator.localization.Constants;
import kruncik.jan.renamator.localization.LocalizationImpl;

/**
 * Tato třída slouží jako panel, který obsahuje JList se všemi soubory, které
 * byly načteny ze zdrojového adresáře. Označené soubory buodu vynechány z
 * přejmenování nebo nahrazení textů v názvech souborů.
 * 
 * @see ActionListener
 * @see Properties
 * 
 * @author Jan Krunčík
 * @version 1.0
 * @since 1.8
 */

public class SkipFilesWithNamePanel extends JPanel implements ActionListener, LocalizationImpl {

	
	private static final long serialVersionUID = 1L;

	

	/**
	 * Textové pole, které slouží pro zadání textu, který se má vyhledat v souborech.
	 */
	private final JTextField txtSearchFileName;
	
	
	/**
	 * Tlačítko, které slouží pro vyheldání textu v názvech souborů, nebylo by
	 * potřeba, je zde KeyAdapter pro textové pole, ale kdyby náhodou.
	 */
	private final JButton btnSearchFileName;
	
	
	
	
	
	
	
	/**
	 * KeyAdapter, který slouží pro textové pole txtSearchFileName, při každém
	 * uvolnení tlačítka se vyhledá text v textovém poli v načtených souborech.
	 */
	private KeyAdapter txtSearchFileNameKeyAdapter;
	
	
	
	
	/**
	 * List, který obsahuje veškeré soubory načtené ze zdorjového adresáře. V tomto
	 * listu je lze označeit a označné soubory budou vynechány z přejmenování nebo
	 * nahrazení textů v názvech souborů.
	 */
	private final MyJList listForSkipFilesWithName;
	
	
	
	/**
	 * Tlačítko pro značení všech položek v listu listForSkipFilesWithName.
	 */
	private final JButton btnSelectAllItemsListSkipFilesWithName;
	
	/**
	 * Tlačítko pro odznačení všech položek v listu listForSkipFilesWithName.
	 */
	private final JButton btnDeselectAllItemsListSkipFilesWithName;
	
	
	
	
	/**
	 * Tento list vždy obsahuje veškeré hodnota, které se zobrazují v okně aplikace,
	 * dole v listu listForSkipFilesWithName. Je to zde potřeba kvůli vyhledávání
	 * souborů -> Je potřeba s nějakých dat filtrovat, proto se ty data uloží sem.
	 */
	private List<ListItem> allItemsForListSkipFilesWithName;
	
	
	
	
	
	
	/**
	 * Konstruktor této třídy.
	 */
	public SkipFilesWithNamePanel() {
		super(new GridBagLayout());
		
		
		createKeyAdapterTxtSearch();
		
		txtSearchFileName = new JTextField(40);
		txtSearchFileName.addKeyListener(txtSearchFileNameKeyAdapter);
						
		btnSearchFileName = new JButton();
		btnSearchFileName.addActionListener(this);		
		
		
		
		
		
		// List pro zobrazení názvu souborů (budou zobrazeny i s příponou, protože mohou
		// být například stejné názvy, jen jiné přípony - linux to tak může mít
		// nerozeznává přípony, ale obsah souboru)
		listForSkipFilesWithName = new MyJList(new ListItem[] {}, JList.VERTICAL);
		final JScrollPane jspListSkipFilesWithName = new JScrollPane(listForSkipFilesWithName);
		jspListSkipFilesWithName.setPreferredSize(new Dimension(530, 150));
		
		
		
		
		// Panel pro list s názvy souborů - jspListSkipFilesWithName, aby byl trochu odsazený od kraje:
		final GridBagConstraints gbcListSkipFilesWithName = AbstractForm.createGbc(5, 20, 5, 0);
				
		// Přidání panelu s komponentami pro vyhledávání:
		add(txtSearchFileName, gbcListSkipFilesWithName);
		
		AbstractForm.setGbc(gbcListSkipFilesWithName, 0, 1);
		add(btnSearchFileName, gbcListSkipFilesWithName);
		
		
		// Přidání ostatních komponent:
		gbcListSkipFilesWithName.gridheight = 2;
		AbstractForm.setGbc(gbcListSkipFilesWithName, 1, 0);
		add(jspListSkipFilesWithName, gbcListSkipFilesWithName);
		
		
		
		// Tlačítka pro označení nebo odznačení všech položek v listu:
		btnSelectAllItemsListSkipFilesWithName = new JButton();
		btnSelectAllItemsListSkipFilesWithName.addActionListener(Event -> listForSkipFilesWithName.markAllItems(true));
		
		btnDeselectAllItemsListSkipFilesWithName = new JButton();
		btnDeselectAllItemsListSkipFilesWithName.addActionListener(Event -> listForSkipFilesWithName.markAllItems(false));
		
		gbcListSkipFilesWithName.gridheight = 1;
		AbstractForm.setGbc(gbcListSkipFilesWithName, 1, 1);
		add(btnSelectAllItemsListSkipFilesWithName, gbcListSkipFilesWithName);

		AbstractForm.setGbc(gbcListSkipFilesWithName, 2, 1);
		add(btnDeselectAllItemsListSkipFilesWithName, gbcListSkipFilesWithName);
	}


	
	
	
	
	
	
	/**
	 * Metoda, která vytvoří Key Adapter pro vyhledávání názvu souborů.
	 * 
	 * Pokaždé, když se uvolní klávesa, otestuji, zda se v listu s načtenými soubory
	 * vyskytuje soubor (i s cestou odzdrojového adresáře), který obsahuje zadaný
	 * text v textovém poli txtSearchFileName.
	 */
	private void createKeyAdapterTxtSearch() {
		txtSearchFileNameKeyAdapter = new KeyAdapter() {
			@Override
			public void keyReleased(KeyEvent e) {
				searchText();
			}
		};
	}
	
	
	
	
	
	
	
	/**
	 * Metoda, která si z textového pole získá zadaný text a pokud není prázdné -
	 * prázdný text, tak vyhledá texnto text v seznamu načtených souborů a zobrazí
	 * jej, v podstatě soubory, obsahující ten text z textového pole se vyfiltrují v
	 * tom listu, že tam budou jen ty soubory obsahující zadaný text.
	 * 
	 * Pokud to pole txtSearchFileName pro zadání textu bude prázdné, zobrazí se v
	 * listu s načtenými soubory všechny načtené soubory jako před tím.
	 */
	private void searchText() {
		final String searchText = txtSearchFileName.getText();
		
		if (searchText.isEmpty()) {
			// Zde je vyhledávací pole prázdné, tak vložím do listu původní hodnoty:
			listForSkipFilesWithName.setListData(allItemsForListSkipFilesWithName.toArray(new ListItem[] {}));
			listForSkipFilesWithName.repaint();
			return;
		}		
		
		/*
		 * Zde je zadán nějaký text, tak filtruje data z listu s původními hodnotami 
		 * a vyfiltrovaná data vložím do toho listu, který je zobrazen v okně aplikace.
		 */
		listForSkipFilesWithName.setListData(allItemsForListSkipFilesWithName.stream()
				.filter(f -> f.getFile().toString().toUpperCase().contains(searchText.toUpperCase()))
				.toArray(ListItem[]::new));
		
		listForSkipFilesWithName.repaint();
	}
	
	
	
	
	
	
	
	/**
	 * Metoda, která si tento list list uloží do proměnné a zobrazí jej v listu, kde
	 * půjde každý soubor označit
	 * 
	 * @param list
	 *            list s načtenými soubory ze zvoleného zdrojového adresáře.
	 */
	public final void setDataToListSkipFilesWithName(final List<ListItem> list) {
		allItemsForListSkipFilesWithName = list;

		listForSkipFilesWithName.setListData(allItemsForListSkipFilesWithName.toArray(new ListItem[] {}));
	}
	
	
	

	
	
	/**
	 * Metoda, která vrátí referenci na list, který obsahuje veškeré položky v listu
	 * pro označení souborů - listForSkipFilesWithName.
	 * 
	 * @return referenci na výše popsaný list.
	 */
	public List<ListItem> getAllItemsForListSkipFilesWithName() {
		return allItemsForListSkipFilesWithName;
	}
	
	
	

	
	
	
	/**
	 * Metoda, která vrátí referenci na list, který obsahuje položky - názvy souborů
	 * s příponami, případně i jejich cestu od zdrojového adresáře.
	 * 
	 * @return výše popsanou referenci na list se soubory ve zdrjovém adresáři.
	 */
	public MyJList getListForSkipFilesWithName() {
		return listForSkipFilesWithName;
	}
	
	
	



	@Override
	public void setLoadedText(final Properties prop) {
		if (prop != null) {
			btnSearchFileName.setText(prop.getProperty("SFWNP_Btn_Search_File_Name", Constants.SFWNP_BTN_SEARCH_FILE_NAME));
			
			btnSelectAllItemsListSkipFilesWithName.setText(prop.getProperty("SFWNP_Btn_Select_All_Items_List_Skip_Files_With_Name", Constants.SFWNP_BTN_SELECT_ALL_ITEMS_LIST_SKIP_FILES_WITH_NAME));
			btnDeselectAllItemsListSkipFilesWithName.setText(prop.getProperty("SFWNP_Btn_Delect_All_Items_List_Skip_Files_With_Name", Constants.SFWNP_BTN_DELECT_ALL_ITEMS_LIST_SKIP_FILES_WITH_NAME));
		}
		
		
		else {
			btnSearchFileName.setText(Constants.SFWNP_BTN_SEARCH_FILE_NAME);
			
			btnSelectAllItemsListSkipFilesWithName.setText(Constants.SFWNP_BTN_SELECT_ALL_ITEMS_LIST_SKIP_FILES_WITH_NAME);
			btnDeselectAllItemsListSkipFilesWithName.setText(Constants.SFWNP_BTN_DELECT_ALL_ITEMS_LIST_SKIP_FILES_WITH_NAME);
		}
	}









	@Override
	public void actionPerformed(ActionEvent e) {
		if (e.getSource() instanceof JButton && e.getSource() == btnSearchFileName) searchText();
	}
}
