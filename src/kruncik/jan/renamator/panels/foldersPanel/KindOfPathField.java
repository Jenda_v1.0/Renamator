package kruncik.jan.renamator.panels.foldersPanel;

/**
 * Jedná se o výčet, dle kterého se pozná, o jaké textové pole se jedná.
 * 
 * Neboli, jaké textové pole pro zadání cesty k adresáři se má otestovat. Jedná
 * se o to, že když dojde ke změně hodnoty v textovém poli pro zadání zdrojové
 * nebo cílové cesty k adresáři, tak potřebuji nějak rozpoznat o jaké pole jde.
 * Proto tento výčet, ale neměl by být potřeba, jsou zde pouze dvě hodnoty, tak
 * stačí využít například logickou proměnnou nebo proměnnou typu byte apod. Ale
 * pro případ potenciálního rozšíření si nechci přidělávat práci, kdyby se někdy
 * v budoucnu zadávali ještě nějaké další cesty k adresáři, kdyby se jednalo
 * například o více adresářů apod.
 * 
 * @see Enum
 * 
 * @author Jan Krunčík
 * @version 1.0
 */

public enum KindOfPathField {

	/**
	 * Cesta ke zdrojovému adresáři
	 */
	SOURCE_DIR,

	/**
	 * Cesta ke cílovému adresáři
	 */
	DESTINATION_DIR
}
