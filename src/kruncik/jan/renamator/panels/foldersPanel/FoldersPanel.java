package kruncik.jan.renamator.panels.foldersPanel;

import java.awt.Color;
import java.awt.FlowLayout;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.io.File;
import java.util.Arrays;
import java.util.Objects;
import java.util.Properties;

import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;

import kruncik.jan.renamator.app.App;
import kruncik.jan.renamator.fileExplorer.FileExplorer;
import kruncik.jan.renamator.app.Menu;
import kruncik.jan.renamator.forms.AbstractForm;
import kruncik.jan.renamator.localization.Constants;
import kruncik.jan.renamator.localization.LocalizationImpl;
import kruncik.jan.renamator.panels.FileChooser;

/**
 * Tato třída slouží jako panel, který obsahuje komponenty pro výběr zdrojového
 * a cílového adresáře.
 * 
 * Zdrojový adresář je adresář, kde se budou přejmenovávat soubory a případně
 * nahrazovat texty v názvech souborů.
 * 
 * Pokud bude zadán cílový adresář, pak se nejprve zkopírují soubory ze
 * zdrojového adresáře do cílového adresáře, a pak se v tomto cílovém adresáři
 * provedou změny v názvech souborů.
 *
 * @see Properties
 * @see ActionListener
 * 
 * @author Jan Krunčík
 * @version 1.0
 * @since 1.8
 */

public class FoldersPanel extends JPanel implements LocalizationImpl, ActionListener {

	private static final long serialVersionUID = 1L;

	
	
	/**
	 * Regulární výraz pro cestu v linuxu.
	 * 
	 * Například: /xx/yy/...
	 * 
	 * Je Výraz může obsahovat velká a malá písmena s diakritikou nebo bez, číslice,
	 * podtržítka, tečky, vykřičník, pomlčka, mezery. Výrazem projde i cesta k
	 * souboru i k dresáři, tak je třeba dávat pouzor.
	 */
	private static final String REG_EX_PATH_IN_LINUX = "^\\s*(/\\s*[\\w áčďéěíňóřšťúůýžÁČĎÉĚÍŇÓŘŠŤÚŮÝŽ\\(\\)\\.!-]+)+\\s*$";
	
	/**
	 * Regulární výraz pro cestu v Windows.
	 * 
	 * Například: X:\asdf\asdf\...
	 * 
	 * Je Výraz může obsahovat velká a malá písmena s diakritikou nebo bez, číslice,
	 * podtržítka, tečky, vykřičník, pomlčka, mezery. Výrazem projde i cesta k
	 * souboru i k dresáři, tak je třeba dávat pouzor.
	 */
	private static final String REG_EX_PATH_IN_WINDOWS = "^\\s*([a-zA-Z]\\s*:)\\s*(\\\\[\\w áčďéěíňóřšťúůýžÁČĎÉĚÍŇÓŘŠŤÚŮÝŽ\\(\\)\\.!-]+)+\\s*$";
	
	
	
	
	
	/**
	 * Label, který obsahuje text "Zdrojový adresář" ve zvoleném jazyce.
	 */
	private final JLabel lblSourceDir;
	
	/**
	 * Textové pole, ve kterém je zadána cesta ke zvolenému zdrojovému adresáři. Je
	 * v textovém poli, aby jej uživatel mohl kdykoli změnit, případně napsat apod.
	 */
	private final JTextField txtSourceDir;
	
	/**
	 * Tento label obsahuje nějaký chybový text. Jendá seo o chybu, která se
	 * vyskytuje v zadané zdrojové cestě, například znak, který by se tam neměl
	 * vyskytovat apod.
	 */
	private final JLabel lblSourcePathError;
	
	/**
	 * Tlačítko,které otevře dialog, ve kterém se vybere zdrojový adresář.
	 */
	private final JButton btnSelectSourceDir;
	
	
	
	
	/**
	 * Label, který obsahuje text "Cílový adresář" ve zvoleném jazyce.
	 */
	private final JLabel lblDestinationDir;
	
	/**
	 * Textové pole, ve kterém je zadána cesta ke zvolenému cílovému adresáři. Je v
	 * textovém poli, aby jej uživatel mohl kdykoli změnit, případně napsat apod.
	 * Tato "položka" není povinná, nemusí se tento adresář nastavovat.
	 */
	private final JTextField txtDestinationDir;
	
	/**
	 * Tento label obsahuje nějaký chybový text. Jendá seo o chybu, která se
	 * vyskytuje v zadané cílové cestě, například znak, který by se tam neměl
	 * vyskytovat apod.
	 */
	private final JLabel lblDestinationPathError;
	
	/**
	 * Tlačítko,které otevře dialog, ve kterém se vybere cílový adresář.
	 */
	private final JButton btnSelectDestinationDir;
	
	
	
	
	
	
	
	
	/**
	 * Talčítko, které otevře dialog coby průzkmník souborů, který obsahuje
	 * stromovou strukturu uživatelova disku a informace o souborech v něm.
	 */
	private final JButton btnOpenFileExplorer;	 
	
	
	
	/**
	 * txtSelectSourceDir - Proměnná pro text do dialogu pro výběr zdrojového
	 * adresáře
	 * 
	 * txtSelectDestinationDir - Proměnná pro text do dialogu pro výběr cílového
	 * adresáře
	 */
	private String txtSelectSourceDir, txtSelectDestinationDir;
	
	
	
	
	/**
	 * KeyAdapter, který bude kontrolovat správnou syntaxi zadané cesty k adresáři.
	 */
	private KeyAdapter pathKeyAdapter;
	
	
	
	
	// Proměnné do chybových labelů pod textovými poli:
	private String txtMissingPath;
	private String txtWrongPath;
	private String txtDirDoesntExist;
	private String txtDirDoesntExistWillBeCreated;
	private String txtPathIsNotHeadingToDir;
	private String txtSamePaths;
	
	
	
	
	
	/**
	 * Komponenta, dle které se pozná, zda se mají vyhledávat i skryté soubory.
	 */
	private final JCheckBox chcbCountHiddenFiles;
	
	
	
	
	
	
	
	
	/**
	 * Konstruktor této třídy.
	 */
	public FoldersPanel() {
		super(new GridBagLayout());
		
		final GridBagConstraints gbc = AbstractForm.createGbc(5, 5, 5, 5);
		
		
		createPathKeyAdapter();
		
		
		
		
		chcbCountHiddenFiles = new JCheckBox();
		chcbCountHiddenFiles.setHorizontalAlignment(JCheckBox.CENTER);
		
		gbc.gridwidth = 3;
		add(chcbCountHiddenFiles, gbc);
		gbc.gridwidth = 1;
		
		
		
		
		// Lbl zdrojový adresář:
		lblSourceDir = new JLabel();
		
		AbstractForm.setGbc(gbc, 1, 0);
		add(lblSourceDir, gbc);
		
				
		// textové pole pro zdrojový adresář:
		txtSourceDir = new JTextField(20);
		txtSourceDir.addKeyListener(pathKeyAdapter);
		
		AbstractForm.setGbc(gbc, 1, 1);
		add(txtSourceDir, gbc);
		
		// Tlačítko pro zdrojový adresář:
		btnSelectSourceDir = new JButton();
		btnSelectSourceDir.addActionListener(this);
		
		AbstractForm.setGbc(gbc, 1, 2);
		add(btnSelectSourceDir, gbc);
		
		
		// Chybový label:
		lblSourcePathError = new JLabel();
		lblSourcePathError.setFont(Constants.FP_ERROR_LABEL);
		lblSourcePathError.setForeground(Color.RED);
		lblSourcePathError.setVisible(false);
		
		final JPanel pnlLblSourcePathError = new JPanel(new FlowLayout(FlowLayout.CENTER));
		pnlLblSourcePathError.add(lblSourcePathError);
		
		gbc.gridwidth = 3;
		AbstractForm.setGbc(gbc, 2, 0);
		add(pnlLblSourcePathError, gbc);
		
		
		
		
		gbc.gridwidth = 1;
		
		// lbl Cílový adresář:
		lblDestinationDir = new JLabel();		
		
		AbstractForm.setGbc(gbc, 3, 0);
		add(lblDestinationDir, gbc);
		
		// textové pole pro cílový adresář:
		txtDestinationDir = new JTextField(20);
		txtDestinationDir.addKeyListener(pathKeyAdapter);
		
		AbstractForm.setGbc(gbc, 3, 1);
		add(txtDestinationDir, gbc);		
		
		// tlačítko pro cílový adresář
		btnSelectDestinationDir = new JButton();
		btnSelectDestinationDir.addActionListener(this);
		
		AbstractForm.setGbc(gbc, 3, 2);
		add(btnSelectDestinationDir, gbc);
		
		
		// Chybový label:
		lblDestinationPathError = new JLabel();
		lblDestinationPathError.setFont(Constants.FP_ERROR_LABEL);
		lblDestinationPathError.setForeground(Color.RED);
		lblDestinationPathError.setVisible(false);
		
		final JPanel pnlLblDestinationPathError = new JPanel(new FlowLayout(FlowLayout.CENTER));
		pnlLblDestinationPathError.add(lblDestinationPathError);
		
		gbc.gridwidth = 3;
		AbstractForm.setGbc(gbc, 4, 0);
		add(pnlLblDestinationPathError, gbc);
		
		
		
		// Tlačítko pro otevření průzkumníku souborů:
		final JPanel pnlBtnExplorer = new JPanel(new FlowLayout(FlowLayout.CENTER));
		btnOpenFileExplorer = new JButton();
		btnOpenFileExplorer.addActionListener(this);
		
		pnlBtnExplorer.add(btnOpenFileExplorer);
		
		AbstractForm.setGbc(gbc, 5, 0);
		add(pnlBtnExplorer, gbc);
	}


	
	
	
	
	
	
	
	
	/**
	 * Metoda, která vytvoří instanci KeyAdapteru, který kontroluje zadanou cestu
	 * adresáře.
	 * 
	 * Kontroluje (pokud není pole prázdné):
	 * - Zda je cesta ve správné syntaxi (dle regulárního výrazu)
	 * - Zda ten soubor na zadané cestě existuje a je to adresář
	 */
	private void createPathKeyAdapter() {
		pathKeyAdapter = new KeyAdapter() {						
			@Override
			public void keyReleased(KeyEvent e) {
				if (e.getSource() == txtSourceDir)
					checkPathFields(KindOfPathField.SOURCE_DIR);

				else if (e.getSource() == txtDestinationDir) // Není třeba testovat.
					checkPathFields(KindOfPathField.DESTINATION_DIR);
			}
		};
	}
	
	
	
	
	
	
	/**
	 * Metoda, která zkontroluje text v textových polích pro zadání cesty ke
	 * zdrojovému a cílovému adresáři.
	 * 
	 * @param pathField
	 *            - Výčtový typ, který definuje o jaké textové pole se jedná, resp.
	 *            ve kterém testovém poli pro zadání cesty došlo k estisknutí
	 *            klávesy.
	 */
	private void checkPathFields(final KindOfPathField pathField) {
		// Proměnná, do které uložím zadanou cestu k adresáři z nějakého textového pole:
		final String path;

		if (pathField == KindOfPathField.SOURCE_DIR)
			path = txtSourceDir.getText();
		
		else if (pathField == KindOfPathField.DESTINATION_DIR)	// Není třeba testovat.
			path = txtDestinationDir.getText();
		
		else path = null;
				
		
		/*
		 * Cestu ke zdrojovému adresáři je třeba zadat vždy, zato cílový adresář není
		 * povinný, takže kdykoliv bude cesta pro zdrojový adresář prázdná, tak je třeba
		 * zobrazit uživateli oznámení:
		 */
		if (txtSourceDir.getText().isEmpty()) {
			lblSourcePathError.setText(txtMissingPath);

			if (!lblSourcePathError.isVisible())
				lblSourcePathError.setVisible(true);

			/*
			 * V případě, že se jedná o změnu v textovém poli pro zdrojový adresář, tak zde
			 * skončím, protože by se jinak nastavilo níže, že se jedná o nepovolenou
			 * syntaxi nebo znaky:
			 */
			if (pathField == KindOfPathField.SOURCE_DIR) {
				App.setFileSourceDir(null, false);
				return;
			}
				
		}

		
		/*
		 * Cílový adresář není povinný, tak jeslti je prázdný, není třeba jej testovat
		 * ale je třeba případně zneviditelnit lable s chybou:
		 */
		if (pathField == KindOfPathField.DESTINATION_DIR && path.isEmpty()) {
			if (lblDestinationPathError.isVisible())
				lblDestinationPathError.setVisible(false);

			App.setFileDestinationDir(null);

			return;
		}
		
		
		
		
		
		if (App.KIND_OF_OS.equalsIgnoreCase("Linux")) {
			if (!path.matches(REG_EX_PATH_IN_LINUX)) {
				setLabels(pathField, txtWrongPath);

				if (pathField == KindOfPathField.SOURCE_DIR)
					App.setFileSourceDir(null, false);
				else if (pathField == KindOfPathField.DESTINATION_DIR)
					App.setFileDestinationDir(null);

				return;
			}
		}
		
		
		else if (App.KIND_OF_OS.toUpperCase().contains("Windows".toUpperCase()) && !path.matches(REG_EX_PATH_IN_WINDOWS)) {
			setLabels(pathField, txtWrongPath);

			if (pathField == KindOfPathField.SOURCE_DIR)
				App.setFileSourceDir(null, false);
			else if (pathField == KindOfPathField.DESTINATION_DIR)
				App.setFileDestinationDir(null);

			return;
		}
		
		
		
		
		/*
		 * Zde je zadána nějaká cesta, tak otestuji, zda se jedná o existující adresář
		 */
		final File file = new File(path);
		
		
		
		if (!file.exists()) {
			if (pathField == KindOfPathField.SOURCE_DIR) {
				setLabels(pathField, txtDirDoesntExist);

				App.setFileSourceDir(null, false);
			}
				
			
			else if (pathField == KindOfPathField.DESTINATION_DIR) {
				// Zde cílový adresář neexistuje, tak bude vytvořen, proto mohu nastavit cestu k
				// zadanému adresáři:
				App.setFileDestinationDir(file);

				// Zobrazení labelu s danou infromací, že neexistuje cílový adresář a že se
				// vytvoří:
				setLabels(pathField, txtDirDoesntExistWillBeCreated);
			}
			
			return;
		}
		
		
		
		if (!file.isDirectory()) {
			setLabels(pathField, txtPathIsNotHeadingToDir);

			if (pathField == KindOfPathField.SOURCE_DIR)
				App.setFileSourceDir(null, false);
			else if (pathField == KindOfPathField.DESTINATION_DIR)
				App.setFileDestinationDir(null);

			return;
		}
		
		
		
		/*
		 * Zde poslední test, aby se nerovnaly adresáře, nesmí být zadány cesty do stejného adresáře,
		 * pokud budou, je to špatně, jinak by se kopírovaly soubory do z jednoho adresáře do toho samého.
		 * 
		 * Ale ještě potřebuji otestovat, zda se jedná o Windows nebo linux, protože Windowsy nerozeznávají velikost písmen
		 * v názvech souborů, kdežto linuxy ano.
		 */
		
		if (!txtDestinationDir.getText().isEmpty() && !txtSourceDir.getText().isEmpty()) {
			// Zde jsou obě pole zaplněna, tak potřebuji otestovat cesty:
			if (App.KIND_OF_OS.equalsIgnoreCase("Linux")) {
				if (txtSourceDir.getText().equals(txtDestinationDir.getText())) {
					setLabels(pathField, txtSamePaths);

					if (pathField == KindOfPathField.SOURCE_DIR)
						App.setFileSourceDir(null, false);

					else if (pathField == KindOfPathField.DESTINATION_DIR)
						App.setFileDestinationDir(null);

					return;
				}
			}


			else if (App.KIND_OF_OS.toUpperCase().contains("Windows".toUpperCase()) && txtSourceDir.getText().equalsIgnoreCase(txtDestinationDir.getText())) {
				setLabels(pathField, txtSamePaths);

				if (pathField == KindOfPathField.SOURCE_DIR)
					App.setFileSourceDir(null, false);

				else if (pathField == KindOfPathField.DESTINATION_DIR)
					App.setFileDestinationDir(null);

				return;
			}
		}
		
		
		
		
		// Zde je vše v pořádku, tak otestuji, zda jsou chybové labely schované:{
		if (lblSourcePathError.isVisible())
			lblSourcePathError.setVisible(false);
		
		if (lblDestinationPathError.isVisible())
			lblDestinationPathError.setVisible(false);	
		
		
		
		
		/*
		 * Cestu ke zdrojovému adresáři chci nastavit jen v případě, že se jedná právě o
		 * psanív poli pro zdrojový adresář, jinak i když bych psal v poli pro cílový
		 * adresář, tak by se mi pokaždé nastavila cesta ke zdrojovému adresáři a
		 * přenačetli se tak již dříve označené položky vrůzných Jlistech, což necici,
		 * protože by to pak mohl uživatel pořád opakovat.
		 */
		if (!txtSourceDir.getText().isEmpty() && pathField == KindOfPathField.SOURCE_DIR)
			App.setFileSourceDir(new File(txtSourceDir.getText()), containsDirSubDirs(txtSourceDir.getText()));

		if (!txtDestinationDir.getText().isEmpty())
			App.setFileDestinationDir(new File(txtDestinationDir.getText()));
	}
	
	
	
	
	
	
	
	/**
	 * Metoda, která zjistí, zda adresář na cestě path obsahuje nějaké subadresáře.
	 * 
	 * @param path
	 *            - cesta k nějakému adresáři, u kterého se má zjistit, zda obsahuje
	 *            podadresáře nebo ne.
	 * 
	 * @return true pokud adresář na cestě path obsahuje další adresáře, jinak
	 *         false.
	 */
	public static boolean containsDirSubDirs(final String path) {
		final File file = new File(path);

		return Arrays.stream(Objects.requireNonNull(file.listFiles())).anyMatch(File::isDirectory);
	}
	
	
	
	
	
	
	
	
	
	
	/**
	 * Metoda, která otestuje, zda došlo k uvolnení tlačítka v textovém poly pro
	 * zdrojový nebo cílový adresář a dle toho nastaví lbelu s chybovými hláškami
	 * text a zobrazí jej.
	 * 
	 * @param pathField
	 *            - dle toho poznám, o jaké textové pole se jedná.
	 * 
	 * @param text
	 *            - text, který se má zobrazit v příslušném dialogu
	 */
	private void setLabels(final KindOfPathField pathField, final String text) {
		if (pathField == KindOfPathField.SOURCE_DIR)
			setShowOnOffLabel(lblSourcePathError, text, true);

		else if (pathField == KindOfPathField.DESTINATION_DIR)	// Není třeba testovat.
			setShowOnOffLabel(lblDestinationPathError, text, true);
	} 
	
	
	
	
	
	
	
	
	
	
	/**
	 * Metoda, která nastaví do JLabelu lbl text (text) a pokud se má label
	 * zobrazit, tj. proměnná show je true, pak se otestuje, zda je daný lbl
	 * zobrazený a pokud není, tak se zobrazí.
	 * 
	 * @param lbl
	 *            - JLabel, kterému se má nastavit text text a dle proměnné show
	 *            zobrazit.
	 * @param text
	 *            - text, který se má nastavit do Jlabelu lbl
	 * @param show
	 *            - Proměnná bude true pouze v případě, že se má lbl zobrazit, když
	 *            bude false, nic se s ním nemá dělat ať už je zobrazen nebo ne.
	 */
	private static void setShowOnOffLabel(final JLabel lbl, final String text, final boolean show) {
		lbl.setText(text);
				
		if (show && !lbl.isVisible())
			lbl.setVisible(true);
	}
	
	
	
	
	
	
	
	
	

	@Override
	public void setLoadedText(final Properties prop) {
		if (prop != null) {
			lblSourceDir.setText(prop.getProperty("FP_Lbl_Source_Dir", Constants.FP_LBL_SOURCE_DIR) + ": ");
			lblDestinationDir.setText(prop.getProperty("FP_Lbl_Destination_Dir", Constants.FP_LBL_DESTINATION_DIR) + ": ");			
			
			btnSelectSourceDir.setText(prop.getProperty("FP_Btn_Source_Dir_Text", Constants.FP_BTN_SELECT_SOURCE_DIR_TEXT));
			btnSelectDestinationDir.setText(prop.getProperty("FP_Btn_Destination_Dir_Text", Constants.FP_BTN_SELECT_DESTINATION_DIR_TEXT));
			
			btnSelectSourceDir.setToolTipText(prop.getProperty("FP_Btn_Select_Source_Dir_TT", Constants.FP_BTN_SELECT_SOURCE_DIR_TT));
			btnSelectDestinationDir.setToolTipText(prop.getProperty("FP_Btn_Select_Destination_Dir_TT", Constants.FP_BTN_SELECT_DESTINATION_DIR_TT));
			
			btnOpenFileExplorer.setText(prop.getProperty("FP_Btn_Open_File_Explorer_Text", Constants.FP_BTN_OPEN_FILE_EXPLORER_TEXT));
			btnOpenFileExplorer.setToolTipText(prop.getProperty("FP_Btn_Open_File_Explorer_TT", Constants.FP_BTN_OPEN_FILE_EXPLORER_TT));
			
			txtSelectSourceDir = prop.getProperty("FP_Txt_Select_Source_Dir", Constants.FP_TXT_SELECT_SOURCE_DIR);
			txtSelectDestinationDir = prop.getProperty("FP_Txt_Select_Destination_Dir", Constants.FP_TXT_SELECT_DESTINATION_DIR);
			
			txtDestinationDir.setToolTipText(prop.getProperty("FP_Txt_Destination_Dir_TT", Constants.FP_TXT_DESTINATION_DIR_TT));			
			
			chcbCountHiddenFiles.setText("? " + prop.getProperty("FP_Chcb_Count_Hidden_Files_Text", Constants.FP_CHCB_COUNT_HIDDEN_FILES_TEXT));
			chcbCountHiddenFiles.setToolTipText(prop.getProperty("FP_Chcb_Count_Hidden_Files_TT", Constants.FP_CHCB_COUNT_HIDDEN_FILES_TT));
			
			// Proměnné do chybových labelů pod textovými poli:
			txtMissingPath = prop.getProperty("FP_Txt_Missing_Path", Constants.FP_TXT_MISSING_PATH);
			txtWrongPath = prop.getProperty("FP_Txt_Wrong_Path", Constants.FP_TXT_WRONG_PATH);
			txtDirDoesntExist = prop.getProperty("FP_Txt_Dir_Doesnt_Exist", Constants.FP_TXT_DIR_DOESNT_EXIST);
			txtDirDoesntExistWillBeCreated = prop.getProperty("FP_Txt_Dir_Doesnt_Exist_Will_Be_Created", Constants.FP_TXT_DIR_DOESNT_EXIST_WILL_BE_CREATED);
			txtPathIsNotHeadingToDir = prop.getProperty("FP_Txt_Path_Is_Not_Heading_To_Dir", Constants.FP_TXT_PATH_IS_NOT_HEADING_TO_DIR);
			txtSamePaths = prop.getProperty("FP_Txt_Same_Paths", Constants.FP_TXT_SAME_PATHS);
		}
		
		
		else {
			lblSourceDir.setText(Constants.FP_LBL_SOURCE_DIR + ": ");
			lblDestinationDir.setText(Constants.FP_LBL_DESTINATION_DIR + ": ");
			
			btnSelectSourceDir.setText(Constants.FP_BTN_SELECT_SOURCE_DIR_TEXT);
			btnSelectDestinationDir.setText(Constants.FP_BTN_SELECT_DESTINATION_DIR_TEXT);
			
			btnSelectSourceDir.setToolTipText(Constants.FP_BTN_SELECT_SOURCE_DIR_TT);
			btnSelectDestinationDir.setToolTipText(Constants.FP_BTN_SELECT_DESTINATION_DIR_TT);
			
			btnOpenFileExplorer.setText(Constants.FP_BTN_OPEN_FILE_EXPLORER_TEXT);
			btnOpenFileExplorer.setToolTipText(Constants.FP_BTN_OPEN_FILE_EXPLORER_TT);
			
			txtSelectSourceDir = Constants.FP_TXT_SELECT_SOURCE_DIR;
			txtSelectDestinationDir = Constants.FP_TXT_SELECT_DESTINATION_DIR;
						
			txtDestinationDir.setToolTipText(Constants.FP_TXT_DESTINATION_DIR_TT);
			
			chcbCountHiddenFiles.setText("? " + Constants.FP_CHCB_COUNT_HIDDEN_FILES_TEXT);
			chcbCountHiddenFiles.setToolTipText(Constants.FP_CHCB_COUNT_HIDDEN_FILES_TT);
			
			// Proměnné do chybových labelů pod textovými poli:
			txtMissingPath = Constants.FP_TXT_MISSING_PATH;
			txtWrongPath = Constants.FP_TXT_WRONG_PATH;
			txtDirDoesntExist = Constants.FP_TXT_DIR_DOESNT_EXIST;
			txtDirDoesntExistWillBeCreated = Constants.FP_TXT_DIR_DOESNT_EXIST_WILL_BE_CREATED;
			txtPathIsNotHeadingToDir = Constants.FP_TXT_PATH_IS_NOT_HEADING_TO_DIR;
			txtSamePaths = Constants.FP_TXT_SAME_PATHS;
		}
	}


	
	
	
	
	
	/**
	 * Metoda, která nastaví do textového pole pro cestu ke zdrojovému adresáři
	 * cestu path.
	 * 
	 * @param path
	 *            - cesta ke zdrojovému adresáři.
	 */
	public final void setPathToSourceTxtField(final String path) {
		txtSourceDir.setText(path);
	}

	/**
	 * Metoda, která nastaví do textového pole pro cestu k cílovému adresáři cestu
	 * path.
	 * 
	 * @param path
	 *            - cesta k cílovému adresáři.
	 */
	public final void setPathToDestinationTxtField(final String path) {
		txtDestinationDir.setText(path);
	}
	
	

	
	
	



	@Override
	public void actionPerformed(ActionEvent e) {
		if (e.getSource() instanceof JButton) {
			if (e.getSource() == btnSelectSourceDir) {
				final File file = new FileChooser().getPathToDir(txtSelectSourceDir);
				
				if (file != null) {
					// Nastavím cestu do textového pole:
					txtSourceDir.setText(file.getAbsolutePath());
					// Otestuji zadanou cestu a případně se nastaví i cesta v App:
					checkPathFields(KindOfPathField.SOURCE_DIR);
				}				
			}
			
			
			if (e.getSource() == btnSelectDestinationDir) {
				final File file = new FileChooser().getPathToDir(txtSelectDestinationDir);
				
				if (file != null) {
					// Nastavím cestu do textového pole:
					txtDestinationDir.setText(file.getAbsolutePath());
					// Otestuji zadanou cestu a případně se nastaví i cesta v App:
					checkPathFields(KindOfPathField.DESTINATION_DIR);
				}					
			}
			
			
			else if (e.getSource() == btnOpenFileExplorer) {
				final FileExplorer explorer = new FileExplorer(this, Menu.isShowHiddenFilesInJtree());
				explorer.setLoadedText(App.getLanguage());
				explorer.showDialog();
			}
		}
	}
	
	
	
	
	
	/**
	 * Metoda, která vrátí referenci na JCheckBox, který dle toho, zda je označen
	 * nebo ne určuje, zda se mají započítat i skryté soubory nebo ne.
	 * 
	 * @return reference ny výše popsaný JCheckBox.
	 */
	public JCheckBox getChcbCountHiddenFiles() {
		return chcbCountHiddenFiles;
	}
}
