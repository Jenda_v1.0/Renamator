package kruncik.jan.renamator.panels.renamePanel;

import java.io.File;
import java.util.*;

/**
 * Tato třída obsahuje pouze nějaké metody, které slouží pro naplnění mapy
 * souborami a adresáři ze zvoleného zdrojového adresáře.
 * 
 * Mapa je typu <File, List<File>>, jako klíč je dána cesta k adresáři a jako
 * jeho hodnota je list, který obsahuje všechny soubory v daném adresáři coby
 * klíč, ale pouze soubory, ne adresáře.
 * 
 * Tímto způsobem jsem to rozdělil, protože jsem dal v aplikaci uživateli
 * možnost zvolit, že se mohou soubory v vždy novém adresáři indexovat od
 * začátku, tak musím nějak rozlišit, soubory v daných adresářích, takto mí
 * stačí pouze projít vždy daný list u klíče coby nějakého adresáře a všechny je
 * mohu bez problému přejmenovat dle potřeba, akorát otestuji, zda se nemají
 * nějaké soubory vynechat apod. ale to je již jiná část.
 * 
 * @see Map
 * 
 * @author Jan Krunčík
 * @version 1.0
 * @since 1.8
 */

class FilesToMap {

	
	/**
	 * Mapa, která obsahuje jako klíče adresáře nacházející se ve zdrojovém adresáři
	 * a jako hodnoty ke každému klíči je list souborů, které se nachází v daném
	 * adresáři coby klíč.
	 */
	private static final Map<File, List<File>> MAP = new HashMap<>();
	
	
	
	
	/**
	 * Metoda, která načte všechny soubory a adresáře do mapy a tu vrátí.
	 * 
	 * Mapa bude obsahovat jako klíče veškeré adresáře, včetně toho předaného -
	 * sourceDir. A jako hodnoty ke každému klíči bude obsahovat list všech souborů,
	 * které se nacházejí v daném adresáři coby klíč v mapě.
	 * 
	 * @param sourceDir
	 *            - zdrojový adresář, odkud se má začit vyhledávat.
	 * 
	 * @param loadAllDirs
	 *            - logická proměnná o tom, zda se mají načist do mapy všechny
	 *            adresáře, které se nachází ve zvoleném adresáři sourceDir nebo ne.
	 *            True, načtou se všechny podadresáře sourceDir, false načtou se do
	 *            mapy pouze ty soubory, které se nachází v sourceDir, žádné jiné
	 *            adresáře v mapě nebudou. Takže v té mapě bude jeden klíč -
	 *            sourceDir a jako jeho hodnota všechny soubory, které se v tom
	 *            adresáři nachází.
	 * 
	 * @param countHiddenFiles
	 *            - logická proměnná o tom, zda se mají započítat i skryté soubory
	 *            nebo ne.
	 * 
	 * @return - výše popsaou mapu, která jako kíče obsahuje adresáře a jako hodnoty
	 *         všechny soubory v daném adresáři coby klč.
	 */
	static Map<File, List<File>> getLoadedDirs(final File sourceDir, final boolean loadAllDirs,
			final boolean countHiddenFiles) {
		// Vymažu mapu, kdyby v ní byly nějaké hodnoty.
		MAP.clear();
		
		
		if (loadAllDirs)
			// Zde si načtu všechny adresáře ve zvoleném zdrojovém adresáři (včetně)
			loadAllDirsToMap(sourceDir, countHiddenFiles);
		
		/*
		 * Zde se mají načíst soubory pouze ze zdrojového adresáře, protože uživatel
		 * zvolil, že nechce přejmenovat soubory v podadresářích, tak do mapy vložím
		 * pouze ten zdrojový adresář a zavolám metodu, která načte všechny soubory,
		 * které se v tom zdrojovém adresáři nachází, apk bude v pamě pouze ta jedna
		 * hodnota - všechny soubory ve zdrojovém adresáři.
		 */
		else MAP.put(sourceDir, new ArrayList<>());
		
		

		// zde si načtu všechny soubory ke každému adresáři v mapě, který se načetl
		// výše:
		loadAllFilesToMap(countHiddenFiles);
		

		// vrátím načtenou mapu:
		return MAP;		
	}
	
	
	
	
	
	
	/**
	 * Metoda, která začne vyhledávat od zdrojového adresáře a hledá všechny
	 * adresáře - resp. všechny podadresáře přdaného zdrojového adresáře.
	 * 
	 * Metoda načte všechny adresáře, které senachází v předaném zdrojovém adresáři,
	 * včechny tyto adresáře budou načtené jako klíče do mapy.
	 * 
	 * @param file
	 *            - zdrojový adresář, odkud se má začít hledat všechny podadresáře.
	 * 
	 * @param countHiddenFiles
	 *            - logická proměnná o tom, zda se mají započítat skryté soubory
	 *            nebo ne.
	 */
	private static void loadAllDirsToMap(final File file, final boolean countHiddenFiles) {
		if (file.isDirectory()) {
			// Pokud se nemají počíat skryté soubory a soubor je skrytý, tak zde skončím
			if (!countHiddenFiles && file.isHidden())
				return;

			MAP.put(file, new ArrayList<>());

			Arrays.asList(Objects.requireNonNull(file.listFiles())).forEach(f -> loadAllDirsToMap(f, countHiddenFiles));
		}
	}
	
	
	
	
	
	
	
	/**
	 * Metoda, která projde mapu, která v tuto chvíli obsahuje načtené všechny
	 * adresáře. Při každém cyklu si načte všechny soubory každého adresáře a vloží
	 * jej do mapy - do listu k danému klíči coby adresáři, kde se ten soubor
	 * nachází.
	 * 
	 * @param countHiddenFiles
	 *            - logická proměnná o tom, zda se mají počítat skryté soubory
	 *            nebone.
	 */
	private static void loadAllFilesToMap(final boolean countHiddenFiles) {
		for (final Map.Entry<File, List<File>> it : MAP.entrySet()) {
			if (!countHiddenFiles)
				Arrays.stream(Objects.requireNonNull(it.getKey().listFiles())).filter(f -> !f.isDirectory() && !f.isHidden())
						.forEach(f -> it.getValue().add(f));

			else
				Arrays.stream(Objects.requireNonNull(it.getKey().listFiles())).filter(f -> !f.isDirectory())
						.forEach(f -> it.getValue().add(f));
		}
	}
}
