package kruncik.jan.renamator.panels.renamePanel;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.Font;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.io.File;
import java.util.*;

import javax.swing.BorderFactory;
import javax.swing.ButtonGroup;
import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JComboBox;
import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.JTextField;
import javax.swing.JViewport;
import javax.swing.ListModel;
import javax.swing.table.JTableHeader;
import javax.swing.table.TableColumnModel;

import org.apache.commons.io.FilenameUtils;

import kruncik.jan.renamator.app.App;
import kruncik.jan.renamator.forms.AbstractForm;
import kruncik.jan.renamator.jListWithJRadioButtons.ListItem;
import kruncik.jan.renamator.jListWithJRadioButtons.MyJList;
import kruncik.jan.renamator.localization.Constants;
import kruncik.jan.renamator.localization.CreateLocalization;
import kruncik.jan.renamator.localization.LocalizationImpl;
import kruncik.jan.renamator.panels.AbstractTableValue;
import kruncik.jan.renamator.panels.ChangeFileType;
import kruncik.jan.renamator.panels.ChangeFileTypeTableModel;
import kruncik.jan.renamator.panels.SkipFilesWithNamePanel;
import kruncik.jan.renamator.panels.foldersPanel.FoldersPanel;

/**
 * Tato třída slouží jako panel, který obsahuje komponenty pro nastavení
 * parametrů pro přejmenování souborů.
 *
 * @see Properties
 * @see ActionListener
 * @see FilenameUtils
 * 
 * @author Jan Krunčík
 * @version 1.0
 * @since 1.8
 */

public class RenamePanel extends JPanel implements LocalizationImpl, ActionListener {

	
	private static final long serialVersionUID = 1L;


	/**
	 * Font, který slouží "zvýraznění" některých nadpisů - labelů.
	 */
	private static final Font FONT_FOR_SUBTITLE = new Font("font for subtitles", Font.BOLD, 16);
	
	/**
	 * Font pro label, který obsahuje nějakou chybovou zprávu, že třeba v nějakém
	 * poli je chyba apod.
	 */
	private static final Font FONT_ERROR_LABEL = new Font("Font for error labels.", Font.BOLD, 13);
	
	/**
	 * Font pro textové pole pro zadání regulárního výrazu v případě, že je prázdné,
	 * tj. obsahuje chybu.
	 */
	private static final Font BOLD_FONT_ERROR_TXT_FIELD = new Font("Bold font for text field with error.", Font.BOLD, 12);
	
	/**
	 * Font pro textové pole pro zadání regulárního výrazu, když neobsahuje chybu,
	 * tj. výchozí hodnoty.
	 */
	private static final Font DEFAULT_FONT_ERROR_TXT_FIELD = new Font("Font for text field with error.", Font.PLAIN, 12);
	
	
	
	// Lebely s popisky:
	private final JLabel lblNewName, lblName, lblIndexation, lblStyleNumbering, lblStyleOfIndexationByLetters,
			lblNewNamesExample, lblRenameFilesInSubDirs, lblFileForRename, lblRegEx, lblTestRegExText, lblSkipFiles,
			lblErrorInfo;
	
	
	
	// JCheckBoxy:
	private final JCheckBox chcbIndexByNumber, chcbIndexInFrontOfText, chcbIndexBehingOfText, chcbIndexByLetters,
			chcbUseBigLetters, chcbUseSmallLetters, chcbLettersInFrontOfText, chcbLettersBehindOfText,
			chcbRenameFilesInSubDirs, chcbStartIndexingFromZeroInSubDirs, chcbLeaveFileType, chcbChangeFileType,
			chcbRenameAllFiles, chcbRenameFileContaintsText, chcbIgnoreLetterSize, chcbRenameFilesByRegEx,
			chcbRenameFileWithExtension, chcbSkipFilesWithExtension,
			chcbSkipFilesWithName, chcbIncluseExtension;
	
	
	
	/**
	 * Pole pro název souborů.
	 */
	private final JTextField txtName;
	
	/**
	 * Textové pole pro zadání názvu souborů. Soubory, které tento název obsahujů se
	 * přejmenují dle nastavených parametrů.
	 */
	private final JTextField txtFileNameForRename;
	
	
	/**
	 * Toto pole slouží pro zadání regulárního výrazu, který určuji názvy souborů
	 * nebo typy apod. (dle regEx), které se mají přejmenovat. Takže dle výrazu se
	 * testují názvy souborů a když název projde regulárním výrazem, tak se
	 * přejmenuje.
	 * 
	 * Toto pole nebudu nijak testovat ne regulární výraz, akorát, zda není pole prázdné,
	 * protože nevím, jaký výraz tam chce uživatel zadat.
	 */
	private final JTextField txtRegExOriginalFileName;
	
	
	
	/**
	 * Textové pole, které slouží pro zadání textu, který se má otestovat na
	 * regulární výraz, který je v textovém poli txtRegExOriginalFileName.
	 */
	private final JTextField txtRegExText;
	
	
	
	/**
	 * Tlačítko, které otestuje zadaný text v poli txtRegExText na regulární výraz,
	 * který je v poli txtRegExOriginalFileName.
	 */
	private final JButton btnTestTextForRegEx;
	
	
	
	
	/**
	 * Panel, který obsahuje komponenty pro vyhledávní a zobrazení načtencýh souborů
	 * ve zdrojovém adresáři.
	 */
	private final SkipFilesWithNamePanel pnlSkipFilesWithName;
	
	
	
	
	
	/**
	 * Tato komponenta bude obsahovat veškeré přípony souborů, které se vyskytují ve
	 * zvoleném adresáři. Uživatel si pak vybere jen ty, přípony souborů, které se
	 * mají přejmenovat, resp. pak se přejmenují pouze ty soubory. které mají
	 * zvolenou příponu.
	 */
	private final MyJList listFileExtensions;
	
	
	
	
	/**
	 * Tato komponenta slouží pro zobrazení souborů ve zvoleném adresáři. Každý
	 * soubor buded jako JRadioButton nebo JCheckBox, a každý soubor bude
	 * reprezentován jako přípona, nebo celý název apod. - Dle aktuální potřeby.
	 */
	private final MyJList listSkipFileWithExtension;
	
	
	
	
	// Tlačítka pro označení a odznačení všech položek v listu s příponymi:
	private final JButton btnSelectAllItemsWithExtensions, btnDeselectAllItemsWithExtensions; 
	
	
	
	// Tlačítka pro označení a odznačení všech položek v listu pro označení přípon
	// souborů, které se mají přejmenovat.
	private final JButton btnSelectAllItemsListFileExtensions, btnDeselectAllItemsListFileExtensions;
	
	
	
	
	/**
	 * Panel, který obsahuje pole pro přípony souborů ve zvooleném adresáři, kde si
	 * může uživatel vybrat, jaké soubory se mají vynechat a nepřejmenovat a jaké
	 * ne. Dále tento panel obsahuje tlačítka pro označení a odznačení všech přípon.
	 */
	private final JPanel pnlListWithExtensions;
	
	
	
	/**
	 * Panel, který obsahuje komponenty: MyJlist s příponami a tlačítka pro označení
	 * nebo odznačení všech jeho položek
	 */
	private final JPanel pnlListFileExtension;
	
	
	
	
	
	/**
	 * Jednorozměrné pole, které slouží jako model pro komponentu JComboBox
	 * (cmbKindOfIndexing).
	 */
	private static final KindOfIndexing[] KIND_OF_INDEXING_MODEL = {
			new KindOfIndexing("1, 2", IndexingEnum.NUMBERS_WITHOUT_ZEROS),
			new KindOfIndexing("01, 01", IndexingEnum.NUMBERS_1_ZERO),
			new KindOfIndexing("001, 002", IndexingEnum.NUMBERS_2_ZEROS),
			new KindOfIndexing("0001, 0002", IndexingEnum.NUMBERS_3_ZEROS),
			new KindOfIndexing("00001, 00002", IndexingEnum.NUMBERS_4_ZEROS),
			new KindOfIndexing("000001, 000002", IndexingEnum.NUMBERS_5_ZEROS),
			new KindOfIndexing("0000001, 0000002", IndexingEnum.NUMBERS_6_ZEROS),
			new KindOfIndexing("00000001, 00000002", IndexingEnum.NUMBERS_7_ZEROS),
			new KindOfIndexing("000000001, 000000002", IndexingEnum.NUMBERS_8_ZEROS),
			new KindOfIndexing("0000000001, 0000000002", IndexingEnum.NUMBERS_9_ZEROS),
			new KindOfIndexing("00000000001, 00000000002", IndexingEnum.NUMBERS_10_ZEROS),

			new KindOfIndexing("1-1, 1-2", IndexingEnum.NUMBERS_DASH_WITHOUT_ZEROS),
			new KindOfIndexing("01-01, 01-02", IndexingEnum.NUMBERS_DASH_1_ZERO),
			new KindOfIndexing("001-001, 001-002", IndexingEnum.NUMBERS_DASH_2_ZEROS),
			new KindOfIndexing("0001-0001, 0001-0002", IndexingEnum.NUMBERS_DASH_3_ZEROS),

			new KindOfIndexing("1_1, 1_2", IndexingEnum.NUMBERS_UNDERSCORE_WITHOUT_ZEROS),
			new KindOfIndexing("01_01, 01_02", IndexingEnum.NUMBERS_UNDERSCORE_1_ZERO),
			new KindOfIndexing("001_001, 001_002", IndexingEnum.NUMBERS_UNDERSCORE_2_ZEROS),
			new KindOfIndexing("0001_0001, 0001_0002", IndexingEnum.NUMBERS_UNDERSCORE_3_ZEROS), };
	
	/**
	 * Kompnenta, ve které si uživatel vybere způsob indexování souborů při
	 * přejmenování.
	 */
	private final JComboBox<KindOfIndexing> cmbKindOfIndexing;
	
	
	
	
	
	/**
	 * Jednorozměrné pole coby model pro komponentu cmbKindOfIndexingByLetters.
	 */
	private static final KindOfIndexing[] KIND_OF_INDEXING_BY_LETTERS_MODEL = {
			new KindOfIndexing("A, B", IndexingEnum.LETTERS), new KindOfIndexing("AA, AB", IndexingEnum.LETTERS_PAIR),
			new KindOfIndexing("AAA, AAB", IndexingEnum.LETTERS_TRINITY),

			new KindOfIndexing("A-A, A-B", IndexingEnum.LETTERS_DASH),
			new KindOfIndexing("AA-AA, AA-AB", IndexingEnum.LETTERS_PAIRS_DASH),
			new KindOfIndexing("AAA-AAA, AAA-AAB", IndexingEnum.LETTERS_TRINITY_DASH),

			new KindOfIndexing("A_A, A_B", IndexingEnum.LETTERS_UNDERSCORE),
			new KindOfIndexing("AA_AA, AA_AB", IndexingEnum.LETTERS_PAIRS_UNDERSCORE),
			new KindOfIndexing("AAA_AAA, AAA_AAB", IndexingEnum.LETTERS_TRINITY_UNDERSCORE) };

	
	/**
	 * Komponenta pro zvolení způsobu indexování dle abecedy.
	 */
	private final JComboBox<KindOfIndexing> cmbKindOfIndexingByLetters;
	
	
	
	
	
	
	// Panely s komponentami, které se zpřístupní nebo znepřístupně dle označených možností.
	
	/**
	 * Panel, který obsahuje komponenty pro nastavení parametrů pro indexování dle
	 * čísel.
	 */
	private final JPanel pnlIndexByNumberComponents;
	
	/**
	 * Panel, který obsahuje komponenty pro nastavení parametrů pro indexování dle
	 * písmen.
	 */
	private final JPanel pnlIndexByLettersComponents;
	
	
	/**
	 * Panel, který obsahuje JCheckBoxy pro nastavení parametrů ohledně toho, zda se
	 * mají přejmenovat i soubory, které jsou v podadresářích - jsou li nějaké
	 * podadresáře.
	 */
	private final JPanel pnlRenameSubDirs;
	
	
	
	/**
	 * Panel, který obsahuje komponenty pro nastavení a otestvání regulárního výrazu
	 * - pro přejmenování souborů, které splňují ten reguálární výraz.
	 */
	private final JPanel pnlComponentsForRegEx;
	
	
	
	
	
	
	/**
	 * Toto je model pro tabulku, která slouží pro změnu připon souborů.
	 */
	private final ChangeFileTypeTableModel tableModelChangeFileType;
	
	
	
	/**
	 * Tabulka pro zadání dat ohledně změn přípon souborů.
	 */
	private final JTable tableChangeFileType;
	
	
	/**
	 * Tlačítka pro manipulaci dat v tabule s příponami souborů pro přejmenování (u
	 * tabulky změnit přípony.)
	 */
	private final JButton btnCreateExtension, btnDeleteExtension, btnLoadExtensions, btnSwapExtensions;
	
	
	/**
	 * Panel, který obsahuje tabulku s daty pro změnu přípon a tlačítka pro přidání
	 * nové přípony a smazání existující.
	 */
	private final JPanel pnlChangeFileTypeTableData;
	
	
	
	
	
	// Proměnné pro texty do chybových hlášek:
	private String txtRowIsNotSelectedText, txtRowIsNotSelectedTitle;
	
	
	
	
	
	
	/**
	 * Tlačítko pro potvrzení. Po kliknutí na toto tlačítko se zkontrolují zadaná
	 * data a přejmenují se soubory dle zadaných parametrů.
	 */
	private final JButton btnConfirm;
	
	
	
	/**
	 * Adaptér, který se bude hlídat, zda je v textovém poli pro zadání nového názvu
	 * souboru (pro indexování apod.)
	 */
	private KeyAdapter keyAdapterTxtName;
	
	
	
	/**
	 * Tento adaptér slouží puze na to, aby se nastavilo do textových polí pro
	 * regulární výraz a do pole, kam se zadává text aby se otestoval, zda odpovídá
	 * regulárnímu výrazu normální barvu pozadí. Jedná se o textová pole:
	 * txtRegExOriginalFileName a txtRegExText
	 */
	private KeyAdapter keyAdapterDefaultBackground;
	
	
	
	
	/**
	 * Proměnná, do které se vloží text ve zvoleném jazyce "Ukázka" a tento text
	 * ve zvoleném jazyce se bude vkládat do labelu, který ukazuje příklad jak budou 
	 * vypadat soubory po přejmenování (pouze jejich názvy ne přípony)
	 */
	private String txtLblNewNameExampleStart;
	
	
	
	
	/**
	 * Proměnná, která slouží pro nastavení textu "Neuvedeno" ve zvoleném jazyce.
	 * Tento text je využit v případě, že se jedná o zobrazení ukázky nového názvu
	 * souborů s nastavenými parametry. Pokud nebude zadán název souborů, pak se
	 * využije tento text "Neuvedeno".
	 */
	private String txtNotSpecified;
	
	
	
	
	
	/**
	 * Reference na panel, který obsahuje komponenty pro zadání zdrojového a
	 * cílového adresáře a zda se mají vynechat nebo započítat skryté soubory a
	 * adresáře. Právě kvůli tomuto JCheckBoxu to potřebuji, protože při
	 * přejmenování si potřebuji znovu načíst osubory a znovu otestovat, zda se mají
	 * načíst i skryté soubory a adresáře nebo ne.
	 */
	private final FoldersPanel folderPanel;
	
	
	
	
	/**
	 * Proměnná, která slouží pro výpočet počtu souborů, které se nachází ve
	 * zvoleném adresáři.
	 */
	private static long count = 0;
	
	
	
	/**
	 * Kolekce, do které se budou vkládat vygenerované indexy pro názvy souborů,
	 * například při zvolení indexování dle čísel zde budou například 1, 2, 3, ...
	 * indexy, případně u písmen: AA_AA, AA_AB, ... apod. Tozáleží na uživatelem
	 * zvolenémindexování v okně aplikace.
	 */
	private static List<String> allIndexesList;
	
	/**
	 * Tato proměnná slouží pro indexování prvků v listu allIndexesList, je to zde
	 * napsáno jako statická / globální proměnná, protože, v případě, že uživatel
	 * zvolí, že chce indexovat položky v podadresářích postupně, tj. že chce
	 * indexovat všechny položky od zdrojového adresáře stejným indexem a nechce
	 * indexovat podadresáře od začátku, pak je třeba tento index nastavit jednou na
	 * nulu, pak se budou pouze inkrementovat, dokud se všechny soubory
	 * nepřejmenují.
	 * 
	 * Pokud uživatel zvolí, že chce indexovat podadresáře od začátku, pak se pří
	 * každém zavolání metody renameFile nastaví před cyklem pro přejmenování
	 * souborů tato proměnná na nulu a budou se brát vždy indexy od začátku pro
	 * každý adresář.
	 */
	private static int indexForIndexes;
	
	
	
	
	
	
	
	
	
	/**
	 * Proměnné pro texty do chybvých hlášek apod.
	 */
	private String txtEmptyTableText, txtEmptyTableTitle, txtUnselectedTableRowText, txtUnselectedTableRowTitle,
			txtCopyDirErrorText, txtCopyDirErrorTitle, txtErrorOrEmptyFieldText, txtErrorOrEmptyFieldTitle,
			txtEmptyTableChangeTypeText, txtEmptyTableChangeTypeTitle, txtEmptyFieldInTableText,
			txtEmptyFieldInTableTitle, txtEmptyFieldFileForRenameText, txtEmptyFieldFileForRenameTitle,
			txtRegExEmptyFieldText, txtRegExEmptyFieldTitle, txtSourceDirisEmptyText, txtSourceDirisEmptyTitle,
			txtUnselectedItemsInListText, txtUnselectedItemsInListTitle,

			txtRenameFileErrorText_1, txtRenameFileErrorText_2, txtRenameFileErrorText_3, txtRenameFileErrorTitle,

			txtRenameFileNameErrorText_1, txtRenameFileNameErrorText_2, txtRenameFileNameErrorText_3,
			txtRenameFileNameErrorTitle;
	
	
	
	
	
	
	
	
	
	/**
	 * Konstruktor této třídy.
	 * 
	 * @param prop
	 *            - Načtené texty pro aplikaci, zda je potřeba to předat kvůli
	 *            nastavení textu sloupců v tabulce.
	 * 
	 * @param folderPanel
	 *            - Reference na panel, který obsahuje komponeny pro zadání
	 *            zdrojového a cílového adresáře, zde je potřeba, protože si
	 *            potřebuji zjistit, zda se mají započítat skryté soubory nebo ne.
	 */
	public RenamePanel(final Properties prop, final FoldersPanel folderPanel) {
		super(new GridBagLayout());
		
		this.folderPanel = folderPanel;
		
		final GridBagConstraints gbc = AbstractForm.createGbc(6, 10, 10, 0);
		
		// Proměnná pro index řádku:
		int rowIndex = 1;

		
		createKeAdapterDefaultBackground();	
		
		
		
		
		// Nadpis - Nový název
		lblNewName = new JLabel();
		lblNewName.setFont(Constants.FONT_FOR_MAIN_TITLE);
		add(lblNewName, gbc);
		
		
		// Label pro "Název: " a popisek s informací: 
		lblName = new JLabel();		
		// Panel pro lblName, aby byl trochu posunuty:
		final JPanel pnlLblName = new JPanel(new GridBagLayout());
		final GridBagConstraints gbcPnlLblname = AbstractForm.createGbc(5, 20, 5, 0);
		pnlLblName.add(lblName, gbcPnlLblname);
		
		AbstractForm.setGbc(gbc, rowIndex, 0);
		add(pnlLblName, gbc);
		
		
		
		
		
		createKeyAdapterTxtName();
		
		// Pole pro zadání názvu souborů:
		txtName = new JTextField(20);
		txtName.addKeyListener(keyAdapterTxtName);
		
		AbstractForm.setGbc(gbc, rowIndex, 1);
		add(txtName, gbc);
		
		
		
		// Label pro info, když uživatel zadá neplatné znaky:
		lblErrorInfo = new JLabel();
		lblErrorInfo.setFont(FONT_ERROR_LABEL);
		lblErrorInfo.setForeground(Color.RED);
		// Výchozí stav - skrytý:
		lblErrorInfo.setVisible(false);
		
		AbstractForm.setGbc(gbc, ++rowIndex, 1);
		add(lblErrorInfo, gbc);
		
		
		
		
		
		
		
		
		
		
		// Nadpis Indexace:
		lblIndexation = new JLabel();
		lblIndexation.setFont(FONT_FOR_SUBTITLE);
		
		gbc.gridwidth = 2;
		AbstractForm.setGbc(gbc, ++rowIndex, 0);
		add(lblIndexation, gbc);
		
		
		
		// chcb pro označení, zda se má indexovat dle čísel:
		chcbIndexByNumber = new JCheckBox();
		chcbIndexByNumber.addActionListener(this);
		chcbIndexByNumber.setSelected(true);
		
		AbstractForm.setGbc(gbc, ++rowIndex, 0);
		add(chcbIndexByNumber, gbc);
		
		
		
		
		
		// Panel, který obsahuje komponenty pro nastavení indexování dle čísel:
		pnlIndexByNumberComponents = new JPanel(new GridBagLayout());
		final GridBagConstraints gbcPnlNumbers = AbstractForm.createGbc(5, 20, 5, 0);
		
		// Label pro "Styl číslování: "
		lblStyleNumbering = new JLabel();
		
		AbstractForm.setGbc(gbcPnlNumbers, 0, 0);
		pnlIndexByNumberComponents.add(lblStyleNumbering, gbcPnlNumbers);
		
		
		// JComboBox pro výběr stylu číslování:
		cmbKindOfIndexing = new JComboBox<>(KIND_OF_INDEXING_MODEL);
		cmbKindOfIndexing.addActionListener(this);
		
		AbstractForm.setGbc(gbcPnlNumbers, 0, 1);
		pnlIndexByNumberComponents.add(cmbKindOfIndexing, gbcPnlNumbers);
		
		
		// chcb pro přidat čísla na začátek zadaného názvu:
		chcbIndexInFrontOfText = new JCheckBox();
		chcbIndexInFrontOfText.addActionListener(this);

		gbcPnlNumbers.gridwidth = 2;
		AbstractForm.setGbc(gbcPnlNumbers, 1, 0);
		pnlIndexByNumberComponents.add(chcbIndexInFrontOfText, gbcPnlNumbers);
		
		// chcb pro přidat čísla na konec zadaného názvu:
		chcbIndexBehingOfText = new JCheckBox();
		chcbIndexBehingOfText.addActionListener(this);
		chcbIndexBehingOfText.setSelected(true);

		createButtongGroup(chcbIndexInFrontOfText, chcbIndexBehingOfText);
		
		AbstractForm.setGbc(gbcPnlNumbers, 2, 0);
		pnlIndexByNumberComponents.add(chcbIndexBehingOfText, gbcPnlNumbers);		
		
		
		AbstractForm.setGbc(gbc, ++rowIndex, 0);
		add(pnlIndexByNumberComponents, gbc);
		

		
		
			
		
		
		
		
		
		
		
		
		

		chcbIndexByLetters = new JCheckBox();
		chcbIndexByLetters.addActionListener(this);
		
		AbstractForm.setGbc(gbc, ++rowIndex, 0);
		add(chcbIndexByLetters, gbc);
		
		createButtongGroup(chcbIndexByNumber, chcbIndexByLetters);
		
		
		
		// Panel, který obsahuj ekomponenty pro nastavení indexování dle abecedy:
		pnlIndexByLettersComponents = new JPanel(new GridBagLayout());
		final GridBagConstraints gbcPnlLetters = AbstractForm.createGbc(5, 20, 5, 0);
		
		// Lebel s zpusob indexování:
		lblStyleOfIndexationByLetters = new JLabel();
		
		AbstractForm.setGbc(gbcPnlLetters, 1, 0);
		pnlIndexByLettersComponents.add(lblStyleOfIndexationByLetters, gbcPnlLetters);
		
		
		// JComboBox pro výběr způsobu indexování dle abecedy:
		cmbKindOfIndexingByLetters = new JComboBox<>(KIND_OF_INDEXING_BY_LETTERS_MODEL);
		cmbKindOfIndexingByLetters.addActionListener(this);
		
		AbstractForm.setGbc(gbcPnlLetters, 1, 1);
		pnlIndexByLettersComponents.add(cmbKindOfIndexingByLetters, gbcPnlLetters);
		
		
		// chcb pro použít velká písmena:
		chcbUseBigLetters = new JCheckBox();
		chcbUseBigLetters.addActionListener(this);
		chcbUseBigLetters.setSelected(true);
		
		AbstractForm.setGbc(gbcPnlLetters, 2, 0);
		pnlIndexByLettersComponents.add(chcbUseBigLetters, gbcPnlLetters);
		
		// chcb pro pužít malá písmena
		chcbUseSmallLetters = new JCheckBox();
		chcbUseSmallLetters.addActionListener(this);
		
		AbstractForm.setGbc(gbcPnlLetters, 3, 0);
		pnlIndexByLettersComponents.add(chcbUseSmallLetters, gbcPnlLetters);
		
		createButtongGroup(chcbUseBigLetters, chcbUseSmallLetters);
		
		
		// chcb pro přidat písmena před název souboru
		chcbLettersInFrontOfText = new JCheckBox();
		chcbLettersInFrontOfText.addActionListener(this);
		
		AbstractForm.setGbc(gbcPnlLetters, 4, 0);
		pnlIndexByLettersComponents.add(chcbLettersInFrontOfText, gbcPnlLetters);
		
		// chcb pro přidat písmena za název souboru
		chcbLettersBehindOfText = new JCheckBox();
		chcbLettersBehindOfText.addActionListener(this);
		chcbLettersBehindOfText.setSelected(true);
		
		AbstractForm.setGbc(gbcPnlLetters, 5, 0);
		pnlIndexByLettersComponents.add(chcbLettersBehindOfText, gbcPnlLetters);
		
		createButtongGroup(chcbLettersInFrontOfText, chcbLettersBehindOfText);
		
		
		AbstractForm.setGbc(gbc, ++rowIndex, 0);
		add(pnlIndexByLettersComponents, gbc);		
		
		// Rovnou ten panel znepřístupním:
		enabledAllComponents(pnlIndexByLettersComponents.getComponents(), false);
		
		
		
		
		
		// Label, který ukazuje jak budou vypadat nové názvy souborů dle
		// zvolenýchparametrů:
		lblNewNamesExample = new JLabel();
		
		AbstractForm.setGbc(gbc, ++rowIndex, 0);
		add(lblNewNamesExample, gbc);
		
		
		
		
		
		
		
		
		
		
		
		// Sem vložím panelu pouze za účelem odsazení:
		AbstractForm.setGbc(gbc, ++rowIndex, 0);
		add(getPanelForSpace(), gbc);
		
		
		
		
		
		
		
		
		

		// Label - nadpis, zda se mají přejmenovat i soubory v podadresárich:
		lblRenameFilesInSubDirs = new JLabel();
		lblRenameFilesInSubDirs.setFont(FONT_FOR_SUBTITLE);
		
		AbstractForm.setGbc(gbc, ++rowIndex, 0);
		add(lblRenameFilesInSubDirs, gbc);
		
		
		/*
		 * Panel pro přejmenování souboru i v podadresářích - jsou li. Tento panel se
		 * zpřístupní puze v případě, že se ve zvoleném adresáři nachází i podadresáře.
		 */
		pnlRenameSubDirs = new JPanel(new GridBagLayout());
		final GridBagConstraints gbcPnlRenameSubDirs = AbstractForm.createGbc(5, 20, 5, 0);
		
		
		// chcb, zda se mají přejmenovat i soubory v podadresářích:
		chcbRenameFilesInSubDirs = new JCheckBox();
		
		AbstractForm.setGbc(gbcPnlRenameSubDirs, 1, 0);
		pnlRenameSubDirs.add(chcbRenameFilesInSubDirs, gbcPnlRenameSubDirs);
		
		
		// chcb, zda se má začínat indexování znovu v podadresářch:
		chcbStartIndexingFromZeroInSubDirs = new JCheckBox();
		// Výchozí hodnota:
		chcbStartIndexingFromZeroInSubDirs.setSelected(true);
		
		AbstractForm.setGbc(gbcPnlRenameSubDirs, 2, 0);
		pnlRenameSubDirs.add(chcbStartIndexingFromZeroInSubDirs, gbcPnlRenameSubDirs);		
		
		AbstractForm.setGbc(gbc, ++rowIndex, 0);
		add(pnlRenameSubDirs, gbc);
		
		enableSubDirValues(false);
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		// chcb pro ponechat typ souboru:
		chcbLeaveFileType = new JCheckBox();
		chcbLeaveFileType.addActionListener(this);
		chcbLeaveFileType.setSelected(true);		
		
		AbstractForm.setGbc(gbc, ++rowIndex, 0);
		add(chcbLeaveFileType, gbc);
		
		
		// chcb pro změnit typ souboru:
		chcbChangeFileType = new JCheckBox();
		chcbChangeFileType.addActionListener(this);
		
		AbstractForm.setGbc(gbc, ++rowIndex, 0);
		add(chcbChangeFileType, gbc);
		
		createButtongGroup(chcbLeaveFileType, chcbChangeFileType);
		
		
		
		// Tabulka pro zadání typů souborů - na jaké typy se mají na najeké změnit:
		tableModelChangeFileType = new ChangeFileTypeTableModel(prop);
		tableModelChangeFileType.setValuesList(new ArrayList<>(Collections.singletonList(new ChangeFileType(".jpg", ".png"))));
		
		tableChangeFileType = new JTable(tableModelChangeFileType);
		tableChangeFileType.setAutoCreateColumnsFromModel(false);
		
		final JScrollPane jspTableChangeFileType = new JScrollPane(tableChangeFileType);
		jspTableChangeFileType.setPreferredSize(new Dimension(0, 150));
		
		
		
		// Panel s tlačítky pro smazání existující přípony a přidání nové:
		final JPanel pnlEditButtonsFileTypeTable = new JPanel(new FlowLayout(FlowLayout.CENTER, 15, 0));
		
		btnCreateExtension = new JButton();
		btnCreateExtension.addActionListener(this);		
		
		btnDeleteExtension = new JButton();
		btnDeleteExtension.addActionListener(this);
		
		btnLoadExtensions = new JButton();
		btnLoadExtensions.addActionListener(this);
		
		btnSwapExtensions = new JButton();
		btnSwapExtensions.addActionListener(this);
		
		pnlEditButtonsFileTypeTable.add(btnCreateExtension);
		pnlEditButtonsFileTypeTable.add(btnDeleteExtension);
		pnlEditButtonsFileTypeTable.add(btnLoadExtensions);
		pnlEditButtonsFileTypeTable.add(btnSwapExtensions);
		
		
		
		// Panel, který bude obsahovat tabulku pro změnu přípon a tlačítka pro přidání
		// nové přípony a smazání existující:
		pnlChangeFileTypeTableData = new JPanel(new BorderLayout());
		pnlChangeFileTypeTableData.add(jspTableChangeFileType, BorderLayout.CENTER);
		pnlChangeFileTypeTableData.add(pnlEditButtonsFileTypeTable, BorderLayout.SOUTH);
		
		AbstractForm.setGbc(gbc, ++rowIndex, 0);
		add(pnlChangeFileTypeTableData, gbc);
		
		// Výchozí stav:
		enabledAllComponents(pnlChangeFileTypeTableData.getComponents(), false);
		
		
		
		
		
		
		
		// Přidám prazdný panel coby mezera_
		AbstractForm.setGbc(gbc, ++rowIndex, 0);
		add(getPanelForSpace(), gbc);
		
		
		
		
		
		
		
		
		// Label pro nadpis - soubory - původní názvy
		lblFileForRename = new JLabel();
		lblFileForRename.setFont(Constants.FONT_FOR_MAIN_TITLE);
		
		AbstractForm.setGbc(gbc, ++rowIndex, 0);
		add(lblFileForRename, gbc);
		
		
		/*
		 * Panel, který bude obsahovat komponenty pro nastavení souborů / názvů souborů,
		 * které se mají přejmenovat.
		 */
		final JPanel pnlFileForRename = new JPanel(new GridBagLayout());
		final GridBagConstraints gbcPnlFileForRename = AbstractForm.createGbc(5, 20, 5, 0);
		gbcPnlFileForRename.gridwidth = 2;
		
		// chcb pro prejmenování vsech souborů:
		chcbRenameAllFiles = new JCheckBox();
		chcbRenameAllFiles.addActionListener(this);
		chcbRenameAllFiles.setSelected(true);
		
		pnlFileForRename.add(chcbRenameAllFiles, gbcPnlFileForRename);
		
		
		
		// chcb pro prejmenování vsech souborů, které obsahuji zadaný text
		chcbRenameFileContaintsText = new JCheckBox();
		chcbRenameFileContaintsText.addActionListener(this);
		
		gbcPnlFileForRename.gridwidth = 1;
		AbstractForm.setGbc(gbcPnlFileForRename, 1, 0);
		pnlFileForRename.add(chcbRenameFileContaintsText, gbcPnlFileForRename);
		
		
		
		// Textové pole pro zadání názvu souborů. Soubory, které tento text obsahují se
		// dle nastavených parametrů přejmenují:
		txtFileNameForRename = new JTextField(20);
		
		
		AbstractForm.setGbc(gbcPnlFileForRename, 1, 1);
		pnlFileForRename.add(txtFileNameForRename, gbcPnlFileForRename);
				
		gbcPnlFileForRename.gridwidth = 2;
		
		// Chcb pro nastavení, zda se má testovat název souborů s ignorováním velikosti písmen nebo ne.
		// (Umístím jej do dalšího panelu, protože ho chci ještě posunout - jakoby odražení od levého kraje:)
		chcbIgnoreLetterSize = new JCheckBox();
		
		final JPanel pnlChcbIgnoreLetterSize = new JPanel(new GridBagLayout());
		final GridBagConstraints gbcPnlChcbIgnoreLetterSize = AbstractForm.createGbc(5, 20, 5, 0);		
		pnlChcbIgnoreLetterSize.add(chcbIgnoreLetterSize, gbcPnlChcbIgnoreLetterSize);
		
		AbstractForm.setGbc(gbcPnlFileForRename, 2, 0);
		pnlFileForRename.add(pnlChcbIgnoreLetterSize, gbcPnlFileForRename);
		
		
		// Ve výchozím nastavení jsou komponenty pro zadání text, který mají názvy
		// souborů obsahovat, aby byly přejmenovány zněpřístupněny:
		enableRenameFileContainsTextComponents(false);
		
		
		
		
		// chcb pro přejmenování souborů dle reguálního výrazu:
		chcbRenameFilesByRegEx = new JCheckBox();
		chcbRenameFilesByRegEx.addActionListener(this);
		
		AbstractForm.setGbc(gbcPnlFileForRename, 3, 0);
		pnlFileForRename.add(chcbRenameFilesByRegEx, gbcPnlFileForRename);
		
					
		/*
		 * panel, který obsahuje label s textem regularní vyraz, pod nim textové ple pro
		 * zadání regulárního výrazu a pod tím je label s textem, vedle něj pole pro
		 * zadání textu -> pro otestování regulárního výrazu a tlačítko pro otestování
		 * textu.
		 * 
		 * Po kliknutí na to tlačítko se otestuje zadaný text na regulární výraz a dle
		 * toho se obarví pzadí textového pole s textem na červeno nebo zeleno - dle
		 * toho, zda text prošel regulárním výrazem nebo ne.
		 */
		pnlComponentsForRegEx = new JPanel(new GridBagLayout());
		final GridBagConstraints gbcPnlComponentsForRegEx = AbstractForm.createGbc(5, 20, 5, 0);
		
		// Regulární výraz pro to, zda se mají názvy souborů testovat včetně přípony
		// nebo ne.
		chcbIncluseExtension = new JCheckBox();		
		gbcPnlComponentsForRegEx.gridwidth = 2;
		pnlComponentsForRegEx.add(chcbIncluseExtension, gbcPnlComponentsForRegEx);
		
		lblRegEx = new JLabel();
		AbstractForm.setGbc(gbcPnlComponentsForRegEx, 1, 0);
		pnlComponentsForRegEx.add(lblRegEx, gbcPnlComponentsForRegEx);
		
		
		txtRegExOriginalFileName = new JTextField("^$");
		txtRegExOriginalFileName.addKeyListener(keyAdapterDefaultBackground);		
		// Panel, do kterého vložím pole pro regulární výraz, abych jej trochu posunul od kraje:
		final JPanel pnlTxtRegExOriginalFileName = new JPanel(new GridBagLayout());
		final GridBagConstraints gbcPnlTxtRegExOriginalFileName = AbstractForm.createGbc(5, 20, 5, 0);
		pnlTxtRegExOriginalFileName.add(txtRegExOriginalFileName, gbcPnlTxtRegExOriginalFileName);
		
		AbstractForm.setGbc(gbcPnlComponentsForRegEx, 2, 0);
		pnlComponentsForRegEx.add(pnlTxtRegExOriginalFileName, gbcPnlComponentsForRegEx);
		
		
		// label s textem pro otestování regulárního vyrazu:
		lblTestRegExText = new JLabel();
		
		AbstractForm.setGbc(gbcPnlComponentsForRegEx, 3, 0);
		pnlComponentsForRegEx.add(lblTestRegExText, gbcPnlComponentsForRegEx);
		
		
		// Panel, který obsahuje textové pole a tlačítko pro otestování zadaného textu
		// na regulární výraz:
		final JPanel pnlTestRegExComponents = new JPanel(new GridBagLayout());
		final GridBagConstraints gbcPnlTestRegExComponents = AbstractForm.createGbc(5, 20, 5, 0);
		
		// Textové pole pro text, který se otestuje na regulární výraz::
		txtRegExText = new JTextField(44);
		txtRegExText.addKeyListener(keyAdapterDefaultBackground);
		pnlTestRegExComponents.add(txtRegExText, gbcPnlTestRegExComponents);
		
		// Tlačítko, které otestuje výše zadaný text na regulrání výraz:
		btnTestTextForRegEx = new JButton();
		btnTestTextForRegEx.addActionListener(this);
		
		AbstractForm.setGbc(gbcPnlTestRegExComponents, 0, 1);
		pnlTestRegExComponents.add(btnTestTextForRegEx, gbcPnlTestRegExComponents);
		
		
		// Přidání panelu, které obsahuje textové pole a tlačítko pro otestování
		// zadaného textu na regulární výraz:
		AbstractForm.setGbc(gbcPnlComponentsForRegEx, 4, 0);
		pnlComponentsForRegEx.add(pnlTestRegExComponents, gbcPnlComponentsForRegEx);
		
		
		// Přidání panelu s komponentami pro regulární výraz a jeho testování:
		AbstractForm.setGbc(gbcPnlFileForRename, 4, 0);
		pnlFileForRename.add(pnlComponentsForRegEx, gbcPnlFileForRename);
		
		
		// Ve výchozím nastavení jsou komponenty pro zadání a testování regulárního
		// výrazu, který musí názvy souborů obsahovat, aby byly přejmenovány
		// znepřístupněné:
		enablePnlFileNameByRegEx(false);
		
		
		
		
		// chcb pro přejmenování souborů typu
		chcbRenameFileWithExtension = new JCheckBox();
		chcbRenameFileWithExtension.addActionListener(this);
		
		AbstractForm.setGbc(gbcPnlFileForRename, 5, 0);
		pnlFileForRename.add(chcbRenameFileWithExtension, gbcPnlFileForRename);
		
		// Tento list musím vytvořit již zde, abych mohl referenci na něj předat do akce
		// pro tlačítko, aby se neoznčily některé pložky.
		listSkipFileWithExtension = new MyJList(new ListItem[] {}, JList.HORIZONTAL_WRAP);
		
		// MyJList pro zobrazení přípon souborů ve zvoleném zdrojovém adresáři:	
		listFileExtensions = new MyJList(new ListItem[] {}, JList.HORIZONTAL_WRAP);	
		
		// Nasetuji si proměnné pro list - kvůli referencím při označování hodnot:
		listSkipFileWithExtension.setAnotherList(listFileExtensions);
		listFileExtensions.setAnotherList(listSkipFileWithExtension);
		
		// Ve výchozím nastavení není zpřístupněná:
		final JScrollPane jspListFileExtension = new JScrollPane(listFileExtensions);		
		jspListFileExtension.setPreferredSize(new Dimension(530, 150));
		
		// Tlačítka pro označení a odznačení všech položek:
		btnSelectAllItemsListFileExtensions = new JButton();
		btnSelectAllItemsListFileExtensions.addActionListener(Event -> listFileExtensions.markAllItems(true, true));
		
		btnDeselectAllItemsListFileExtensions = new JButton();
		btnDeselectAllItemsListFileExtensions.addActionListener(Event -> listFileExtensions.markAllItems(false, false));
		
		// Panel pro odsazení textArey pro přípony:
		pnlListFileExtension = new JPanel(new GridBagLayout());
		final GridBagConstraints gbcPnlListFileExtension = AbstractForm.createGbc(5, 20, 5, 0);
		gbcPnlListFileExtension.gridheight = 2;
		pnlListFileExtension.add(jspListFileExtension, gbcPnlListFileExtension);
		
		gbcPnlListFileExtension.gridheight = 1;
		AbstractForm.setGbc(gbcPnlListFileExtension, 0, 1);
		pnlListFileExtension.add(btnSelectAllItemsListFileExtensions, gbcPnlListFileExtension);
		
		AbstractForm.setGbc(gbcPnlListFileExtension, 1, 1);
		pnlListFileExtension.add(btnDeselectAllItemsListFileExtensions, gbcPnlListFileExtension);
		
		AbstractForm.setGbc(gbcPnlFileForRename, 6, 0);
		pnlFileForRename.add(pnlListFileExtension, gbcPnlFileForRename);
		
		// Výchozí stav:
		enabledAllComponents(pnlListFileExtension.getComponents(), false);
		
		
		// Vytvořím skupinu pro JCheckBoxy, aby bylo možné označit puze jeden:
		createButtongGroup(chcbRenameAllFiles, chcbRenameFileContaintsText, chcbRenameFilesByRegEx,
				chcbRenameFileWithExtension);
		
		
		// Přiidání panelu s komponentami pro nastavení souborů pro přejmenování:
		AbstractForm.setGbc(gbc, ++rowIndex, 0);
		add(pnlFileForRename, gbc);
		
		
		
		
		
		
		
		// Přidám prazdný panel coby mezera_
		AbstractForm.setGbc(gbc, ++rowIndex, 0);
		add(getPanelForSpace(), gbc);
		
		
		
		
		
		
		// Label pro "Vynechat soubory":
		lblSkipFiles = new JLabel();
		lblSkipFiles.setFont(FONT_FOR_SUBTITLE);
		
		AbstractForm.setGbc(gbc, ++rowIndex, 0);
		add(lblSkipFiles, gbc);
		
		
		// Panel obsahující komponenty pro zadání názvů, textů nebo typů souborů, které
		// se vynechají - nebudou přejmenovány: 
		final JPanel pnlSkipFilesComponents = new JPanel(new GridBagLayout());
		final GridBagConstraints gbcPnlSkipFilesComponents = AbstractForm.createGbc(5, 20, 5, 0);
		
		gbcPnlSkipFilesComponents.gridwidth = 2;
		
		// chcb pro přeskočení souborů, které mají danou příponu, toto bude přístupně
		// pouze v případě, že není označeno přejmenování souborů typu.
		chcbSkipFilesWithExtension = new JCheckBox();	
		chcbSkipFilesWithExtension.addActionListener(this);
		pnlSkipFilesComponents.add(chcbSkipFilesWithExtension, gbcPnlSkipFilesComponents);
		
		// Panel, aby byla komponenta pro přípony ještě více posunutá:
		pnlListWithExtensions = new JPanel(new GridBagLayout());
		final GridBagConstraints gbcListWithFileExtensions = AbstractForm.createGbc(5, 20, 5, 0);
				
		final JScrollPane jspMyList = new JScrollPane(listSkipFileWithExtension);
		jspMyList.setPreferredSize(new Dimension(530, 150));		
		
		gbcListWithFileExtensions.gridheight = 2;
		pnlListWithExtensions.add(jspMyList, gbcListWithFileExtensions);
		
		// Tlačítka pro označení a odznačení:
		btnSelectAllItemsWithExtensions = new JButton();
		btnSelectAllItemsWithExtensions.addActionListener(Event -> listSkipFileWithExtension.markAllItems(true, true));
		
		btnDeselectAllItemsWithExtensions = new JButton();
		btnDeselectAllItemsWithExtensions.addActionListener(Event -> listSkipFileWithExtension.markAllItems(false, false));
		
		gbcListWithFileExtensions.gridheight = 1;
		AbstractForm.setGbc(gbcListWithFileExtensions, 0, 1);
		pnlListWithExtensions.add(btnSelectAllItemsWithExtensions, gbcListWithFileExtensions);
		
		AbstractForm.setGbc(gbcListWithFileExtensions, 1, 1);
		pnlListWithExtensions.add(btnDeselectAllItemsWithExtensions, gbcListWithFileExtensions);
		
		
		AbstractForm.setGbc(gbcPnlSkipFilesComponents, 1, 0);
		pnlSkipFilesComponents.add(pnlListWithExtensions, gbcPnlSkipFilesComponents);
		
		// Výchozí stav panelu je znepřístupněno:
		enabledAllComponents(pnlListWithExtensions.getComponents(), false);
		
		
		
		
		
		
		// chcb pro přeskočení konkrétních souborů:
		chcbSkipFilesWithName = new JCheckBox();
		chcbSkipFilesWithName.addActionListener(this);
		
		AbstractForm.setGbc(gbcPnlSkipFilesComponents, 2, 0);
		pnlSkipFilesComponents.add(chcbSkipFilesWithName, gbcPnlSkipFilesComponents);
		
		
		
		
		// Panel, který obsahuje komponenty pro zobrazení listu se všemi načtemými
		// soubory ve zvoleném zdrojovém adresáři a jejich vyhledávání.
		pnlSkipFilesWithName = new SkipFilesWithNamePanel();
		pnlSkipFilesWithName.setLoadedText(prop);
		
		// Výchozí stav:
		enabledAllComponents(pnlSkipFilesWithName.getComponents(), false);
		
		AbstractForm.setGbc(gbcPnlSkipFilesComponents, 3, 0);
		pnlSkipFilesComponents.add(pnlSkipFilesWithName, gbcPnlSkipFilesComponents);
		
		
		
		
		
		
		// Přidání panelu s komponentami pro přeskočení / vynechání souborů v
		// přejmenování:
		AbstractForm.setGbc(gbc, ++rowIndex, 0);
		add(pnlSkipFilesComponents, gbc);
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		// Tlačítko pro potvrzení:
		btnConfirm = new JButton();
		btnConfirm.addActionListener(this);
		
		// Panel pro tlačítko pro potvrzení, aby bylo na středu s "normální" velikosti:
		final JPanel pnlBtnConfirm = new JPanel(new FlowLayout(FlowLayout.CENTER));
		pnlBtnConfirm.add(btnConfirm);
		
		AbstractForm.setGbc(gbc, ++rowIndex, 0);
		add(pnlBtnConfirm, gbc);
	}
	
	
	
	
	
	
	
	
	
	/**
	 * Metoda, která vytvoří panel a vloží do nej prázdný JLabel, aby se komponenty
	 * před a za tímto panelem patříčně odsadily.
	 * 
	 * @return Jpanel, který obsahuje prázdný Jlabel.
	 */
	public static JPanel getPanelForSpace() {
		final JPanel pnlSpace = new JPanel(new FlowLayout());
		pnlSpace.add(new JLabel());
		
		return pnlSpace;
	}
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	/**
	 * Metoda, která vytvoří instanci Key adapteru, který bude reagovat na puštění
	 * klávesy v textovém poli txtName - pole pro zadání nového názvu pro soubory.
	 * 
	 * Tento keyAdapter bude pouze testovat, zda je nový název v povolené syntaxi,
	 * povolené znaky jsou pouze velká a malá písmena s diakritikou, číslice,
	 * podtržítka, tečky, vykřičník a pomlčka. (to za tečkou se nebere jako přípona,
	 * jedna se pouze o název souboru.)
	 */
	private void createKeyAdapterTxtName() {
		keyAdapterTxtName = new KeyAdapter() {
			@Override
			public void keyReleased(KeyEvent e) {
				if (!txtName.getText().isEmpty() && !txtName.getText().trim().matches(Constants.REG_EX_FILE_NAME)) {
					// Zde název neodpovídá regulárnímu výrazu, tak zobrazím label s informací o
					// chybě:
					if (!lblErrorInfo.isVisible())
						lblErrorInfo.setVisible(true);
				}

				// Zde je text v pohodě, tak se ujistím, že není vidět label s informací o
				// chybě:
				else {
					if (lblErrorInfo.isVisible())
						lblErrorInfo.setVisible(false);

					// Dále nastavím vzor nového názvu dle zadaných hodnot:
					setExampleLabel();
				}
			}
		};
	}
	
	
	
	
	
	
	
	
	/**
	 * Metoda, která nastaví do labelu lblNewNamesExample ukázku zadaného názvu pro
	 * přejmenování souborů. Metoda si získá text z textového pole pro nové názvy
	 * souborů a dle označených parametrů pro nový název souborů se vytvoří ukázka a
	 * vloží do uvedeného labelu.
	 */
	private void setExampleLabel() {
		final String example = getTextForExample();
		setTextToLblExample(example);
	}
	
	
	
	
	
	
	
	
	/**
	 * Metoda, která nastaví text labelu, který zobrazuje ukázku nastaveného názvu nových souborů s indexováním.
	 * Metoda nastaví text labelu na text v syntaxi: ? Ukázka: "example"
	 * 
	 * @param example - získaný text dle nastavených parametrů.
	 */
	private void setTextToLblExample(final String example) {
		lblNewNamesExample.setText(txtLblNewNameExampleStart + ": " + example);
	}
	
	
	
	
	
	
	
	
	
	/**
	 * Metoda, která dle zadaných parametrů v aplikaci vytvoří text, který bude
	 * sloužit jako ukázka zvoleného nastavení nového názvu pro soubory.
	 * 
	 * @return ukázka nového názvu sobour, jak se bude indexovat - jak se budou
	 *         indexovat zvolené soubory
	 */
	private String getTextForExample() {
		// Pokud je pole prázdné, vrátím prázdný text:
		if (txtName.getText().replace("\\s*", "").isEmpty())
			return txtNotSpecified;

		// Text z textového pole:
		final String textFromTextField = txtName.getText();
		
		
		
		
		// Proměnná, do které vložím výsledný text:
		String var = "";
		
		
		if (chcbIndexByNumber.isSelected()) {
			// Zde je označeno indexování dle číslic.
			final KindOfIndexing selectedKind = (KindOfIndexing) cmbKindOfIndexing.getSelectedItem();

			// V poli budou ty dva "texty", co jsou vidět v cmb, tak je rozdělím dle
			// destinné čárky,
			// a pak je jen přidám do textu pro ukázku.
			final String[] array = selectedKind != null ? selectedKind.getKind().split(",") : new String[0];

			if (chcbIndexInFrontOfText.isSelected())
				// Zde si složím text, nejprve dám text z textového pole a za něj přidám zvolený
				// index, pak
				// desetinnou čárku a to samé, akorát s druhým indexem:
				var = array[0].trim() + textFromTextField + ", " + array[1].trim() + textFromTextField;

			else if (chcbIndexBehingOfText.isSelected()) // Není třeba testovat, jen pro potenciální rozšíření.
				var = textFromTextField + array[0].trim() + ", " + textFromTextField + array[1].trim();
		}
		
		
		else if (chcbIndexByLetters.isSelected()) {	// Není třeba testovat, pro potenciální rozšíření.
			// Zde je zvoleno indexování dle písmen.
			final KindOfIndexing selectdeKind = (KindOfIndexing) cmbKindOfIndexingByLetters.getSelectedItem();
			
			final String[] array = selectdeKind != null ? selectdeKind.getKind().split(",") : new String[0];
			
			
			if (chcbLettersInFrontOfText.isSelected()) {
				if (chcbUseBigLetters.isSelected())
					var = array[0].toUpperCase().trim() + textFromTextField + ", " + array[1].toUpperCase().trim()
							+ textFromTextField;

				else if (chcbUseSmallLetters.isSelected())// Není třeba testovat, jen pro potenciální rozšiřování.
					var = array[0].toLowerCase().trim() + textFromTextField + ", " + array[1].toLowerCase().trim()
							+ textFromTextField;
			}

			else if (chcbLettersBehindOfText.isSelected()) {// Není třeba testovat, pouze pro potenciální rozšiřování.
				if (chcbUseBigLetters.isSelected())
					var = textFromTextField + array[0].toUpperCase().trim() + ", " + textFromTextField
							+ array[1].toUpperCase().trim();

				else if (chcbUseSmallLetters.isSelected())// Není třeba testovat, jen pro potenciální rozšiřování.
					var = textFromTextField + array[0].toLowerCase().trim() + ", " + textFromTextField
							+ array[1].toLowerCase().trim();
			}
		}
		
		return var;
	}
	
	
	
	
	
	
	
	
	
	
	/**
	 * Metoda, která vytvoří KeyAdapter, který pouze otestuje, zda textové pole, ve kterém byla stisknuta klávesa
	 * obsahuje "normální" - bílou barvu pozadí nebo ne, pokud ne, pak ji nastaví, pokud ano, pak nic nedělá.
	 * 
	 * Jde o to, že když uživatel napíše regulrání výraz a text, na kterém chce ten výraz otestovat, tak se podbarví pole
	 * zelenou nebo červenou barvu, dle úspěchu n ebo neúspěchu, ale už by se nevrátila barva pozadí těch polí zpět nabílou,
	 * tak proto tento KeyAdapter.
	 */
	private void createKeAdapterDefaultBackground() {
		keyAdapterDefaultBackground = new KeyAdapter() {
			@Override
			public void keyReleased(KeyEvent e) {
				if (e.getSource() == txtRegExOriginalFileName) {
					if (txtRegExOriginalFileName.getBackground().getRGB() != Color.WHITE.getRGB())
						txtRegExOriginalFileName.setBackground(Color.WHITE);
				}

				else // Tato podmínka je zbytečná.
					if (e.getSource() == txtRegExText && txtRegExText.getBackground().getRGB() != Color.WHITE.getRGB())
						txtRegExText.setBackground(Color.WHITE);
			}
		};
	}
	
	
	
	
	
	
	
	
	
	
	/**
	 * Metoda, která zne / přístupní komponenty pro zadání textu / názvu, které musí
	 * obsahovat názvy souborů any se přejmenovali a chcb pro ignorování velikostí
	 * písmen pro dané názvy.
	 * 
	 * @param enable
	 *            - true, výše popsné komponenty se zpřístupní, false - komponenty
	 *            se znepřístupní.
	 */
	private void enableRenameFileContainsTextComponents(final boolean enable) {
		enabledAllComponents(new Component[] { txtFileNameForRename }, enable);
		enabledAllComponents(new Component[] { chcbIgnoreLetterSize }, enable);
	}
	
	
	
	
	
	/**
	 * Metoda, která zne / přístunpní panel s komponentami pro napsání a otestování
	 * regulárního výrazu u přejmenování souborů - dole pod tabulkou, pod chcb
	 * přejmenovat soubory dle regulárního výrazu.
	 * 
	 * @param enable
	 *            - true, panel se zpřístupní, false, panel se zněpřístupní.
	 */
	private void enablePnlFileNameByRegEx(final boolean enable) {
		enabledAllComponents(pnlComponentsForRegEx.getComponents(), enable);
	}
	
	
	
	
	
	
	
	
	

	/**
	 * Metoda, která vytvoří skupinu ButtongGroup, která bude obsahovat komponenty v
	 * parametru této metody.
	 * 
	 * @param chcbs
	 *            - V podstatě "Nemezený" počet parametrů - objektů JCheckBox z nich
	 *            půjde vždy označit pouze jeden.
	 * @return vytvořenou ButtongGroup, která bude obsahovat výše popsané komponenty
	 *         (chcbs).
	 */
	private static ButtonGroup createButtongGroup(final JCheckBox... chcbs) {
		final ButtonGroup bg = new ButtonGroup();
		
		for (final JCheckBox c : chcbs)
			bg.add(c);
		
		return bg;
	}
	
	
	
	
	
	
	
	
	
	/**
	 * Metoda, která zpřístupní nebo znepřístupní panel pro nastavení parametrů pro
	 * přejmenování i souborů v podadresářích. Tato metoda se zavolá pokaždé, když
	 * se označí nějaký zdrojový adresář, pak se zjistí, zda obsahuje nějaké
	 * podadresáře nebo ne a dle toho se zavolá tato metoda s potřebným parametrem
	 * enable.
	 * 
	 * @param enable
	 *            - logická proměnná o tom, zda se mají výše popsané komponenty
	 *            zpřístupnit nebo ne.
	 */
	public final void enableSubDirValues(final boolean enable) {
		enabledAllComponents(pnlRenameSubDirs.getComponents(), enable);		
	}
	
	
	
	
	
	
	
	
	
	
	@Override
	public void setLoadedText(final Properties prop) {
		if (prop != null) {
			tableModelChangeFileType.setLoadedText(prop);
			pnlSkipFilesWithName.setLoadedText(prop);
			
			lblNewName.setText(prop.getProperty("RP_2_Lbl_New_Name", Constants.RP_LBL_NEW_NAME));
			
			lblName.setText("? " + prop.getProperty("RP_2_Lbl_Name_Text", Constants.RP_LBL_NAME_TEXT) + ": ");
			lblName.setToolTipText(prop.getProperty("RP_2_Lbl_Name_TT", Constants.RP_LBL_NAME_TT));
			
			lblErrorInfo.setText("? " + prop.getProperty("RP_2_Lbl_Error_Info_Text", Constants.RP_LBL_ERROR_INFO_TEXT));
			lblErrorInfo.setToolTipText(prop.getProperty("RP_2_Lbl_Error_Info_TT", Constants.RP_LBL_ERROR_INFO_TT));
			
			txtNotSpecified = prop.getProperty("RP_2_Txt_Not_Specified", Constants.RP_TXT_NOT_SPECIFIED);
			
			lblIndexation.setText("? " + prop.getProperty("RP_2_Lbl_Indexation_Text", Constants.RP_LBL_INDEXATION_TEXT));
			lblIndexation.setToolTipText(prop.getProperty("RP_2_Lbl_Indexation_TT", Constants.RP_LBL_INDEXATION_TT));
			
			
			chcbIndexByNumber.setText(prop.getProperty("RP_2_Chcb_Index_By_Number", Constants.RP_CHCB_INDEX_BY_NUMBER));
			chcbIndexByLetters.setText("? " + prop.getProperty("RP_2_Chcb_Index_By_Letters_Text", Constants.RP_CHCB_INDEX_BY_LETTERS_TEXT));
			chcbIndexByLetters.setToolTipText(prop.getProperty("RP_2_Chcb_Index_By_Letters_TT", Constants.RP_CHCB_INDEX_BY_LETTERS_TT));
			
			lblStyleNumbering.setText(prop.getProperty("RP_2_Lbl_Style_Numbering", Constants.RP_LBL_STYLE_NUMBERING) + ": ");
			
			chcbIndexInFrontOfText.setText(prop.getProperty("RP_2_Chcb_Index_In_Front_Of_Text", Constants.RP_CHCB_INDEX_IN_FRONT_OF_TEXT));					
			chcbIndexBehingOfText.setText(prop.getProperty("RP_2_Chcb_Index_Behind_Of_Text", Constants.RP_CHCB_INDEX_BEHIND_OF_TEXT));
			
			chcbUseBigLetters.setText(prop.getProperty("RP_2_Chcb_Use_Big_Letters", Constants.RP_CHCB_USE_BIG_LETTERS));
			chcbUseSmallLetters.setText(prop.getProperty("RP_2_Chcb_Use_Small_Letters", Constants.RP_CHCB_USE_SMALL_LETTERS));
			chcbLettersInFrontOfText.setText(prop.getProperty("RP_2_Chcb_Letters_In_Front_Of_Text", Constants.CHCB_LETTERS_IN_FRONT_OF_TEXT));
			chcbLettersBehindOfText.setText(prop.getProperty("RP_2_Chcb_Letters_Behind_Of_Text", Constants.RP_CHCB_LETTERS_BEHIND_OF_TEXT));
			
			lblStyleOfIndexationByLetters.setText(prop.getProperty("RP_2_Lbl_Style_Of_Indexation_By_Letters", Constants.RP_LBL_STYLE_OF_INDEXATION_BY_LETTERS) + ": ");
			
			
			lblNewNamesExample.setText("? " + prop.getProperty("RP_2_Lbl_New_Name_Example_Text", Constants.RP_LBL_NEW_NAME_EXAMPLE_TEXT) + ": ");// Toto jako výchozí text při spušění
			txtLblNewNameExampleStart = "? "+ prop.getProperty("RP_2_Txt_Lbl_New_Name_Example_Start", Constants.RP_TXT_LBL_NEW_NAME_EXAMPLE_START);
			lblNewNamesExample.setToolTipText(prop.getProperty("RP_2_Lbl_New_Name_Example_TT", Constants.RP_LBL_NEW_NAME_EXAMPLE_TT));
			
			lblRenameFilesInSubDirs.setText(prop.getProperty("RP_2_Lbl_Rename_File_In_Subdirs", Constants.RP_LBL_RENAME_FILE_IN_SUBDIRS));
			chcbRenameFilesInSubDirs.setText(prop.getProperty("RP_2_Chcb_Rename_Files_In_Sub_Dirs", Constants.RP_CHCB_RENAME_FILES_IN_SUB_DIRS));
			
			chcbStartIndexingFromZeroInSubDirs.setText("? " + prop.getProperty("RP_2_Chcb_Start_Indexing_From_Zero_In_Subdirs_Text", Constants.RP_CHCB_START_INDEXING_FROM_ZERO_IN_SUBDIRS_TEXT));
			chcbStartIndexingFromZeroInSubDirs.setToolTipText(prop.getProperty("RP_2_Chcb_Start_Indexing_From_Zero_In_Subdirs_TT", Constants.RP_CHCB_START_INDEXING_FROM_ZERO_IN_SUBDIRS_TT));
			
			
			chcbLeaveFileType.setText(prop.getProperty("RP_2_Chcbc_Leave_File_Type", Constants.RP_CHCB_LEAVE_FILE_TYPE));
			
			chcbChangeFileType.setText("? " + prop.getProperty("RP_2_Chcb_Change_File_Type_Text", Constants.RP_CHCB_CHANGE_FILE_TYPE_TEXT));
			chcbChangeFileType.setToolTipText(prop.getProperty("RP_2_Chcb_Change_File_Type_TT", Constants.RP_CHCB_CHANGE_FILE_TYPE_TT));
			
			pnlChangeFileTypeTableData.setBorder(BorderFactory.createTitledBorder(prop.getProperty("RP_2_Pnl_Change_File_Type_Table_Data", Constants.RP_PNL_CHANGE_FILE_TYPE_TABLE_DATA)));
			
			
			btnCreateExtension.setText(prop.getProperty("RP_2_Btn_Create_Extension_Text", Constants.RP_BTN_CREATE_EXTENSION_TEXT));
			btnCreateExtension.setToolTipText(prop.getProperty("RP_2_Btn_Create_Extension_TT", Constants.RP_BTN_CREATE_EXTENSION_TT));
			btnDeleteExtension.setText(prop.getProperty("RP_2_Btn_Delete_Extension_Text", Constants.RP_BTN_DELETE_EXTENSION_TEXT));
			btnDeleteExtension.setToolTipText(prop.getProperty("RP_2_Btn_Delete_Extension_TT", Constants.RP_BTN_DELETE_EXTENSION_TT));
			btnSwapExtensions.setText(prop.getProperty("RP_2_Btn_Swap_Extensions_Text", Constants.RP_BTN_SWAP_EXTENSIONS_TEXT));
			btnSwapExtensions.setToolTipText(prop.getProperty("RP_2_Btn_Swap_Extensions_TT", Constants.RP_BTN_SWAP_EXTENSIONS_TT));			
			btnLoadExtensions.setText(prop.getProperty("RP_2_Btn_Load_Extensions_Text", Constants.RP_BTN_LOAD_EXTENSIONS_TEXT));
			btnLoadExtensions.setToolTipText(prop.getProperty("RP_2_Btn_Load_Extensions_TT", Constants.RP_BTN_LOAD_EXTENSIONS_TT));
			
			
			
			
			// Chybové hlášky:
			txtRowIsNotSelectedText = prop.getProperty("RP_2_Txt_Row_Is_Not_Selected_Text", Constants.RP_TXT_ROW_IS_NOT_SELECTED_TEXT);
			txtRowIsNotSelectedTitle = prop.getProperty("RP_2_Txt_Row_Is_Not_Selected_Title", Constants.RP_TXT_ROW_IS_NOT_SELECTED_TITLE);
			
			
			
			lblFileForRename.setText("? " + prop.getProperty("RP_2_Lbl_File_For_Rename_Text", Constants.RP_LBL_FILE_FOR_RENAME_TEXT));
			lblFileForRename.setToolTipText(prop.getProperty("RP_2_Lbl_File_For_Rename_TT", Constants.RP_LBL_FILE_FOR_RENAME_TT));
			
			
			chcbRenameAllFiles.setText(prop.getProperty("RP_2_Chcb_Rename_All_Files", Constants.RP_CHCB_RENAME_ALL_FILES));
			chcbRenameFileContaintsText.setText("? " + prop.getProperty("RP_2_Chcb_Rename_File_Contains_Text_Text", Constants.RP_CHCB_RENAME_FILE_CONTAINS_TEXT_TEXT) + ": ");
			chcbRenameFileContaintsText.setToolTipText(prop.getProperty("RP_2_Chcb_Rename_File_Contains_Text_TT", Constants.RP_CHCB_RENAME_FILE_CONTAINS_TEXT_TT));
			
			chcbIgnoreLetterSize.setText(prop.getProperty("RP_2_Chcb_Ignore_Letter_Size", Constants.RP_CHCB_IGNORE_LETTER_SIZE));
			
			chcbRenameFilesByRegEx.setText(prop.getProperty("RP_2_Chcb_Rename_Files_By_Reg_Ex", Constants.RP_CHCB_RENAME_FILES_BY_REG_EX));
			
			
			
			chcbIncluseExtension.setText("? " + prop.getProperty("RP_2_Chcb_Include_Extension_Text", Constants.RP_CHCB_INCLUDE_EXTENSION_TEXT));			
			chcbIncluseExtension.setToolTipText(prop.getProperty("RP_2_Chcb_Include_Extension_TT", Constants.RP_CHCB_INCLUDE_EXTENSION_TT));
			
			lblRegEx.setText("? " + prop.getProperty("RP_2_Lbl_Reg_Ex_Text", Constants.RP_LBL_REG_EX_TEXT) + ":");
			lblRegEx.setToolTipText(prop.getProperty("RP_2_Lbl_Reg_Ex_TT", Constants.RP_LBL_REG_EX_TT));
			
			lblTestRegExText.setText(prop.getProperty("RP_2_Lbl_Test_Reg_Ex_Text", Constants.RP_LBL_TEST_REG_EX_TEXT) + ":");
			
			btnTestTextForRegEx.setText(prop.getProperty("RP_2_Btn_Test_Text_For_Reg_Ex", Constants.RP_BTN_TEST_TEXT_FOR_REG_EX));
			
			
			
			chcbRenameFileWithExtension.setText("? " + prop.getProperty("RP_2_Chcb_Rename_File_With_Extension_Text", Constants.RP_CHCB_RENAME_FILE_WITH_EXTENSION_TEXT) + ":");
			chcbRenameFileWithExtension.setToolTipText(prop.getProperty("RP_2_Chcb_Rename_File_With_Extensions_TT", Constants.RP_CHCB_RENAME_FILE_WITH_EXTENSIONS_TT));
			
						
			lblSkipFiles.setText("? " + prop.getProperty("RP_2_Lbl_Skip_Files_Text", Constants.RP_LBL_SKIP_FILES_TEXT));
			lblSkipFiles.setToolTipText(prop.getProperty("RP_2_Lbl_Skip_Files_TT", Constants.RP_LBL_SKIP_FILES_TT));
			
			chcbSkipFilesWithExtension.setText(prop.getProperty("RP_2_Chcb_Skip_Files_With_Extension", Constants.RP_CHCB_SKIP_FILES_WITH_EXTENSION) + ":");
			chcbSkipFilesWithName.setText(prop.getProperty("RP_2_Chcb_Skip_Files_With_Name", Constants.RP_CHCB_SKIP_FILES_WITH_NAME) + ":");
			
			btnSelectAllItemsListFileExtensions.setText(prop.getProperty("RP_2_Btn_Select_All_Items_List_File_Extensions", Constants.RP_BTN_SELECT_ALL_ITEMS_LIST_FILE_EXTENSIONS));
			btnDeselectAllItemsListFileExtensions.setText(prop.getProperty("RP_2_Btn_Deselect_All_Items_List_File_Extensions", Constants.RP_BTN_DESELECT_ALL_ITEMS_LIST_FILE_EXTENSIONS));
			
			
			btnSelectAllItemsWithExtensions.setText(prop.getProperty("RP_2_Btn_Select_All_Items_With_Extensions", Constants.RP_BTN_SELECT_ALL_ITEMS_WITH_EXTENSIONS));
			btnDeselectAllItemsWithExtensions.setText(prop.getProperty("RP_2_Btn_Deselect_All_Items_With_Extensions", Constants.RP_BTN_DESELECT_ALL_ITEMS_WITH_EXTENSIONS));
			
			
			btnConfirm.setText(prop.getProperty("RP_2_Btn_Confirm", Constants.RP_2_BTN_CONFIRM));
			
			
			// Texty do chybových hlášek:
			txtEmptyTableText = prop.getProperty("RP_2_Txt_Empty_Table_Text", Constants.RP_2_TXT_EMPTY_TABLE_TEXT);
			txtEmptyTableTitle = prop.getProperty("RP_2_Txt_Empty_Table_Title", Constants.RP_2_TXT_EMPTY_TABLE_TITLE);
			
			txtUnselectedTableRowText = prop.getProperty("RP_2_Txt_Unselected_Table_Row_Text", Constants.RP_TXT_UNSELECTED_TABLE_ROW_TEXT);
			txtUnselectedTableRowTitle = prop.getProperty("RP_2_Txt_Unselected_Table_Row_Title", Constants.RP_TXT_UNSELECTED_TABLE_ROW_TITLE);
			
			txtCopyDirErrorText = prop.getProperty("RP_2_Txt_Copy_Dir_Error_Text", Constants.RP_TXT_COPY_DIR_ERROR_TEXT);
			txtCopyDirErrorTitle = prop.getProperty("RP_2_Txt_Copy_Dir_Error_Title", Constants.RP_TXT_COPY_DIR_ERROR_TITLE);
			
			txtErrorOrEmptyFieldText = prop.getProperty("RP_2_Txt_Error_Or_Empty_Field_Text", Constants.RP_TXT_ERROR_OR_EMPTY_FIELD_TEXT);
			txtErrorOrEmptyFieldTitle = prop.getProperty("RP_2_Txt_Error_Or_Empty_Field_Title", Constants.RP_TXT_ERROR_OR_EMPTY_FIELD_TITLE);
			
			txtEmptyTableChangeTypeText = prop.getProperty("RP_2_Txt_Empty_Table_Change_Type_Text", Constants.RP_TXT_EMPTY_TABLE_CHANGE_TYPE_TEXT);
			txtEmptyTableChangeTypeTitle = prop.getProperty("RP_2_Txt_Empty_Table_Change_Type_Title", Constants.RP_TXT_EMPTY_TABLE_CHANGE_TYPE_TITLE);
			
			txtEmptyFieldInTableText = prop.getProperty("RP_2_Txt_Empty_Field_In_Table_Text", Constants.RP_2_TXT_EMPTY_FIELD_IN_TABLE_TEXT);
			txtEmptyFieldInTableTitle = prop.getProperty("RP_2_Txt_Empty_Field_In_Table_Title", Constants.RP_2_TXT_EMPTY_FIELD_IN_TABLE_TITLE);
			
			txtEmptyFieldFileForRenameText = prop.getProperty("RP_2_Txt_Empty_Field_File_For_Rename_Text", Constants.RP_TXT_EMPTY_FIELD_FILE_FOR_RENAME_TEXT);
			txtEmptyFieldFileForRenameTitle = prop.getProperty("RP_2_Txt_Empty_Field_File_For_Rename_Title", Constants.RP_TXT_EMPTY_FIELD_FILE_FOR_RENAME_TITLE);
			
			txtRegExEmptyFieldText = prop.getProperty("RP_2_Txt_Reg_Ex_Empty_Field_Text", Constants.RP_TXT_REG_EX_EMPTY_FIELD_TEXT);
			txtRegExEmptyFieldTitle = prop.getProperty("RP_2_Txt_Reg_Ex_Empty_Field_Title", Constants.RP_TXT_REG_EX_EMPTY_FIELD_TITLE);
			
			txtSourceDirisEmptyText = prop.getProperty("RP_2_Txt_Source_Dir_Is_Empty_Text", Constants.RP_TXT_SOURCE_DIR_IS_EMPTY_TEXT);
			txtSourceDirisEmptyTitle = prop.getProperty("RP_2_Txt_Source_Dir_Is_Empty_Title", Constants.RP_TXT_SOURCE_DIR_IS_EMPTY_TITLE);
			
			txtUnselectedItemsInListText = prop.getProperty("RP_2_Txt_Unselected_Items_In_List_Text", Constants.RP_TXT_UNSELECTED_ITEMS_IN_LIST_TEXT);
			txtUnselectedItemsInListTitle = prop.getProperty("RP_2_Txt_Unselected_Items_In_List_Title", Constants.RP_TXT_UNSELECTED_ITEMS_IN_LIST_TITLE);
			
			
			txtRenameFileErrorText_1 = prop.getProperty("RP_2_Txt_Rename_File_Error_Text_1", Constants.RP_2_TXT_RENAME_FILE_ERROR_TEXT_1);
			txtRenameFileErrorText_2 = prop.getProperty("RP_2_Txt_Rename_File_Error_Text_2", Constants.RP_2_TXT_RENAME_FILE_ERROR_TEXT_2);
			txtRenameFileErrorText_3 = prop.getProperty("RP_2_Txt_Rename_File_Error_Text_3", Constants.RP_TXT_RENAME_FILE_ERROR_TEXT_3); 
			txtRenameFileErrorTitle = prop.getProperty("RP_2_Txt_Rename_File_Error_Title", Constants.RP_2_TXT_RENAME_FILE_ERROR_TITLE); 
					
			txtRenameFileNameErrorText_1 = prop.getProperty("RP_2_Txt_Rename_File_Name_Error_Text_1", Constants.RP_TXT_RENAME_FILE_NAME_ERROR_TEXT_1);
			txtRenameFileNameErrorText_2 = prop.getProperty("RP_2_Txt_Rename_File_Name_Error_Text_2", Constants.RP_TXT_RENAME_FILE_NAME_ERROR_TEXT_2);
			txtRenameFileNameErrorText_3 = prop.getProperty("RP_2_Txt_Rename_File_Name_Error_Text_3", Constants.RP_TXT_RENAME_FILE_NAME_ERROR_TEXT_3);
			txtRenameFileNameErrorTitle = prop.getProperty("RP_2_Txt_Rename_File_Name_Error_Title", Constants.RP_TXT_RENAME_FILE_NAME_ERROR_TITLE);
			
			
			
			
			// Sloupce do tabulky:
			// Načtení textů do sloupců:			
			String[] tempColumns = CreateLocalization.getArrayFromText(prop.getProperty("CFTTM_Columns"));
			
			/*
			 * Pokud se pole nenačetlo nebo je prázdné - například jej uživatel smazal apod.
			 * Nebo nemá požadovanou délku, pak se vezme původní:
			 */
			if (tempColumns == null || tempColumns.length == 0 || tempColumns.length != 2)
				tempColumns = Constants.CHANGE_FILE_TYPE_COLUMNS;
			
			
			setTableHeaders(tableChangeFileType, tempColumns);
		}
		
		
		else {			
			lblNewName.setText(Constants.RP_LBL_NEW_NAME);
			
			lblName.setText("? " + Constants.RP_LBL_NAME_TEXT + ": ");
			lblName.setToolTipText(Constants.RP_LBL_NAME_TT);
			
			lblErrorInfo.setText("? " + Constants.RP_LBL_ERROR_INFO_TEXT);
			lblErrorInfo.setToolTipText(Constants.RP_LBL_ERROR_INFO_TT);
			
			txtNotSpecified = Constants.RP_TXT_NOT_SPECIFIED;
			
			lblIndexation.setText("? " + Constants.RP_LBL_INDEXATION_TEXT);
			lblIndexation.setToolTipText(Constants.RP_LBL_INDEXATION_TT);
			
			
			chcbIndexByNumber.setText(Constants.RP_CHCB_INDEX_BY_NUMBER);
			chcbIndexByLetters.setText("? " + Constants.RP_CHCB_INDEX_BY_LETTERS_TEXT);
			chcbIndexByLetters.setToolTipText(Constants.RP_CHCB_INDEX_BY_LETTERS_TT);
			
			lblStyleNumbering.setText(Constants.RP_LBL_STYLE_NUMBERING + ": ");
			
			chcbIndexInFrontOfText.setText(Constants.RP_CHCB_INDEX_IN_FRONT_OF_TEXT);					
			chcbIndexBehingOfText.setText(Constants.RP_CHCB_INDEX_BEHIND_OF_TEXT);
			
			chcbUseBigLetters.setText(Constants.RP_CHCB_USE_BIG_LETTERS);
			chcbUseSmallLetters.setText(Constants.RP_CHCB_USE_SMALL_LETTERS);
			chcbLettersInFrontOfText.setText(Constants.CHCB_LETTERS_IN_FRONT_OF_TEXT);
			chcbLettersBehindOfText.setText(Constants.RP_CHCB_LETTERS_BEHIND_OF_TEXT);
			
			lblStyleOfIndexationByLetters.setText(Constants.RP_LBL_STYLE_OF_INDEXATION_BY_LETTERS + ": ");
			
			
			lblNewNamesExample.setText("? " + Constants.RP_LBL_NEW_NAME_EXAMPLE_TEXT + ": ");// Toto jako výchozí text při spušění
			txtLblNewNameExampleStart = "? "+ Constants.RP_TXT_LBL_NEW_NAME_EXAMPLE_START;
			lblNewNamesExample.setToolTipText(Constants.RP_LBL_NEW_NAME_EXAMPLE_TT);
			
			lblRenameFilesInSubDirs.setText(Constants.RP_LBL_RENAME_FILE_IN_SUBDIRS);
			chcbRenameFilesInSubDirs.setText(Constants.RP_CHCB_RENAME_FILES_IN_SUB_DIRS);
			
			chcbStartIndexingFromZeroInSubDirs.setText("? " + Constants.RP_CHCB_START_INDEXING_FROM_ZERO_IN_SUBDIRS_TEXT);
			chcbStartIndexingFromZeroInSubDirs.setToolTipText(Constants.RP_CHCB_START_INDEXING_FROM_ZERO_IN_SUBDIRS_TT);
			
			
			chcbLeaveFileType.setText(Constants.RP_CHCB_LEAVE_FILE_TYPE);
			
			chcbChangeFileType.setText("? " + Constants.RP_CHCB_CHANGE_FILE_TYPE_TEXT);
			chcbChangeFileType.setToolTipText(Constants.RP_CHCB_CHANGE_FILE_TYPE_TT);
			
			pnlChangeFileTypeTableData.setBorder(BorderFactory.createTitledBorder(Constants.RP_PNL_CHANGE_FILE_TYPE_TABLE_DATA));
			
			
			btnCreateExtension.setText(Constants.RP_BTN_CREATE_EXTENSION_TEXT);
			btnCreateExtension.setToolTipText(Constants.RP_BTN_CREATE_EXTENSION_TT);
			btnDeleteExtension.setText(Constants.RP_BTN_DELETE_EXTENSION_TEXT);
			btnDeleteExtension.setToolTipText(Constants.RP_BTN_DELETE_EXTENSION_TT);
			btnSwapExtensions.setText(Constants.RP_BTN_SWAP_EXTENSIONS_TEXT);
			btnSwapExtensions.setToolTipText(Constants.RP_BTN_SWAP_EXTENSIONS_TT);			
			btnLoadExtensions.setText(Constants.RP_BTN_LOAD_EXTENSIONS_TEXT);
			btnLoadExtensions.setToolTipText(Constants.RP_BTN_LOAD_EXTENSIONS_TT);
			
			
			
			
			// Chybové hlášky:
			txtRowIsNotSelectedText = Constants.RP_TXT_ROW_IS_NOT_SELECTED_TEXT;
			txtRowIsNotSelectedTitle = Constants.RP_TXT_ROW_IS_NOT_SELECTED_TITLE;
			
			
			
			lblFileForRename.setText("? " + Constants.RP_LBL_FILE_FOR_RENAME_TEXT);
			lblFileForRename.setToolTipText(Constants.RP_LBL_FILE_FOR_RENAME_TT);
			
			
			chcbRenameAllFiles.setText(Constants.RP_CHCB_RENAME_ALL_FILES);
			chcbRenameFileContaintsText.setText("? " + Constants.RP_CHCB_RENAME_FILE_CONTAINS_TEXT_TEXT + ": ");
			chcbRenameFileContaintsText.setToolTipText(Constants.RP_CHCB_RENAME_FILE_CONTAINS_TEXT_TT);
			
			chcbIgnoreLetterSize.setText(Constants.RP_CHCB_IGNORE_LETTER_SIZE);
			
			chcbRenameFilesByRegEx.setText(Constants.RP_CHCB_RENAME_FILES_BY_REG_EX);
			
			
			
			chcbIncluseExtension.setText("? " + Constants.RP_CHCB_INCLUDE_EXTENSION_TEXT);			
			chcbIncluseExtension.setToolTipText(Constants.RP_CHCB_INCLUDE_EXTENSION_TT);
			
			lblRegEx.setText("? " + Constants.RP_LBL_REG_EX_TEXT + ":");
			lblRegEx.setToolTipText(Constants.RP_LBL_REG_EX_TT);
			
			lblTestRegExText.setText(Constants.RP_LBL_TEST_REG_EX_TEXT + ":");
			
			btnTestTextForRegEx.setText(Constants.RP_BTN_TEST_TEXT_FOR_REG_EX);
			
			
			
			chcbRenameFileWithExtension.setText("? " + Constants.RP_CHCB_RENAME_FILE_WITH_EXTENSION_TEXT + ":");
			chcbRenameFileWithExtension.setToolTipText(Constants.RP_CHCB_RENAME_FILE_WITH_EXTENSIONS_TT);
			
						
			lblSkipFiles.setText("? " + Constants.RP_LBL_SKIP_FILES_TEXT);
			lblSkipFiles.setToolTipText(Constants.RP_LBL_SKIP_FILES_TT);
			
			chcbSkipFilesWithExtension.setText(Constants.RP_CHCB_SKIP_FILES_WITH_EXTENSION + ":");
			chcbSkipFilesWithName.setText(Constants.RP_CHCB_SKIP_FILES_WITH_NAME + ":");
			
			btnSelectAllItemsListFileExtensions.setText(Constants.RP_BTN_SELECT_ALL_ITEMS_LIST_FILE_EXTENSIONS);
			btnDeselectAllItemsListFileExtensions.setText(Constants.RP_BTN_DESELECT_ALL_ITEMS_LIST_FILE_EXTENSIONS);
			
			
			btnSelectAllItemsWithExtensions.setText(Constants.RP_BTN_SELECT_ALL_ITEMS_WITH_EXTENSIONS);
			btnDeselectAllItemsWithExtensions.setText(Constants.RP_BTN_DESELECT_ALL_ITEMS_WITH_EXTENSIONS);
			
			
			btnConfirm.setText(Constants.RP_2_BTN_CONFIRM);
			
			
			// Texty do chybových hlášek:
			txtEmptyTableText = Constants.RP_2_TXT_EMPTY_TABLE_TEXT;
			txtEmptyTableTitle = Constants.RP_2_TXT_EMPTY_TABLE_TITLE;
			
			txtUnselectedTableRowText = Constants.RP_TXT_UNSELECTED_TABLE_ROW_TEXT;
			txtUnselectedTableRowTitle = Constants.RP_TXT_UNSELECTED_TABLE_ROW_TITLE;
			
			txtCopyDirErrorText = Constants.RP_TXT_COPY_DIR_ERROR_TEXT;
			txtCopyDirErrorTitle = Constants.RP_TXT_COPY_DIR_ERROR_TITLE;
			
			txtErrorOrEmptyFieldText = Constants.RP_TXT_ERROR_OR_EMPTY_FIELD_TEXT;
			txtErrorOrEmptyFieldTitle = Constants.RP_TXT_ERROR_OR_EMPTY_FIELD_TITLE;
			
			txtEmptyTableChangeTypeText = Constants.RP_TXT_EMPTY_TABLE_CHANGE_TYPE_TEXT;
			txtEmptyTableChangeTypeTitle = Constants.RP_TXT_EMPTY_TABLE_CHANGE_TYPE_TITLE;
			
			txtEmptyFieldInTableText = Constants.RP_2_TXT_EMPTY_FIELD_IN_TABLE_TEXT;
			txtEmptyFieldInTableTitle = Constants.RP_2_TXT_EMPTY_FIELD_IN_TABLE_TITLE;
			
			txtEmptyFieldFileForRenameText = Constants.RP_TXT_EMPTY_FIELD_FILE_FOR_RENAME_TEXT;
			txtEmptyFieldFileForRenameTitle = Constants.RP_TXT_EMPTY_FIELD_FILE_FOR_RENAME_TITLE;
			
			txtRegExEmptyFieldText = Constants.RP_TXT_REG_EX_EMPTY_FIELD_TEXT;
			txtRegExEmptyFieldTitle = Constants.RP_TXT_REG_EX_EMPTY_FIELD_TITLE;
			
			txtSourceDirisEmptyText = Constants.RP_TXT_SOURCE_DIR_IS_EMPTY_TEXT;
			txtSourceDirisEmptyTitle = Constants.RP_TXT_SOURCE_DIR_IS_EMPTY_TITLE;
			
			txtUnselectedItemsInListText = Constants.RP_TXT_UNSELECTED_ITEMS_IN_LIST_TEXT;
			txtUnselectedItemsInListTitle = Constants.RP_TXT_UNSELECTED_ITEMS_IN_LIST_TITLE;
			
			
			txtRenameFileErrorText_1 = Constants.RP_2_TXT_RENAME_FILE_ERROR_TEXT_1;
			txtRenameFileErrorText_2 = Constants.RP_2_TXT_RENAME_FILE_ERROR_TEXT_2;
			txtRenameFileErrorText_3 = Constants.RP_TXT_RENAME_FILE_ERROR_TEXT_3; 
			txtRenameFileErrorTitle = Constants.RP_2_TXT_RENAME_FILE_ERROR_TITLE; 
					
			txtRenameFileNameErrorText_1 = Constants.RP_TXT_RENAME_FILE_NAME_ERROR_TEXT_1;
			txtRenameFileNameErrorText_2 = Constants.RP_TXT_RENAME_FILE_NAME_ERROR_TEXT_2;
			txtRenameFileNameErrorText_3 = Constants.RP_TXT_RENAME_FILE_NAME_ERROR_TEXT_3;
			txtRenameFileNameErrorTitle = Constants.RP_TXT_RENAME_FILE_NAME_ERROR_TITLE;
			
			
			
			
			
			// Sloupce do tabulky:									
			setTableHeaders(tableChangeFileType, Constants.CHANGE_FILE_TYPE_COLUMNS);
		}
	}


	
	
	
	/**
	 * Metoda, která přejmenuje sloupce v tabulce.
	 * 
	 * @param table
	 *            - tabulka, ve které se mají prejmenovat sloupce.
	 * @param columns
	 *            - Jednorozměrné pole, které obsahuje nové názvy pro sloupce v
	 *            tabulce.
	 */
	public static void setTableHeaders(final JTable table, final String[] columns) {
		final JTableHeader th = table.getTableHeader();
		final TableColumnModel tcm = th.getColumnModel();
		
		tcm.getColumn(0).setHeaderValue(columns[0]);
		tcm.getColumn(1).setHeaderValue(columns[1]);		
		
		th.repaint();
	}
	
	






	@Override
	public void actionPerformed(ActionEvent e) {
		if (e.getSource() instanceof JButton) {
			if (e.getSource() == btnCreateExtension) {
				/*
				 * Přidám nový řádek do tabulky a do obou proměnných coby přípony (původní a
				 * nová) vložím puze desetinné tečky.
				 * 
				 * Nejprve ale otestuji, zda již není přidán nějaký řádek, kde jsou pouze tečky,
				 * tj. nebyl už jednou přidán ale není vyplněn, pokud tomu tak je, pak není
				 * důvod přidávat další, dodku se nevyplní.
				 * 
				 * Ještě si budu pamatovat označený řádek - pokud byl a znovu jej označím:
				 */
				
				if (tableModelChangeFileType.containsSomePoint())
					return;
				
				
				final int selectedRow = tableChangeFileType.getSelectedRow();
				
				tableModelChangeFileType.addValueToList(new ChangeFileType(".", "."));
				
				if (selectedRow > -1)
					tableChangeFileType.setRowSelectionInterval(selectedRow, selectedRow);
			}
			
			else if (e.getSource() == btnDeleteExtension) {
				/*
				 * Zjistím si, jaká přípona (na jakém řádku) je označena a smažu jej, pokud není
				 * žádná označena, oznámím to uživateli coby chyba.
				 */				
				final int selectedRow = tableChangeFileType.getSelectedRow();
				
				if (selectedRow == -1) {
					JOptionPane.showMessageDialog(this, txtRowIsNotSelectedText, txtRowIsNotSelectedTitle,
							JOptionPane.ERROR_MESSAGE);
					return;
				}				
				
				tableModelChangeFileType.deleteValueAtIndex(selectedRow);				
				
				// Zkusím znovu označit řádek na stejném indexu - pokud to nebyl poslední:
				if (selectedRow < tableChangeFileType.getRowCount())
					tableChangeFileType.setRowSelectionInterval(selectedRow, selectedRow);
				
				// Označím poslední řádek, ale jen jestli tam nějaký je:
				else if (tableChangeFileType.getRowCount() > 0)
					tableChangeFileType.setRowSelectionInterval(tableChangeFileType.getRowCount() - 1,
							tableChangeFileType.getRowCount() - 1);
			}
			
			else if (e.getSource() == btnLoadExtensions) {
				/*
				 * Pouze se do tabulky přenačtou data z list listFileExtensions, který obsahuje
				 * zvolené přípony. Bylo by možné znovu načíst data ze zvoleného adresáře, ale
				 * to již je při jeho změně. Zde stačí pouze přenačíst data z uvedenho listu,
				 * který obsahuje přípony souborů (případně i skrytých souborů)
				 */
				// Vezmu si hodnoty z listu s příponami:
				final ListModel<ListItem> model = listFileExtensions.getModel();
				final List<ListItem> temp = new ArrayList<>();				
				
				for (int i = 0; i < model.getSize(); i++)
					temp.add(model.getElementAt(i));
				
				

				// Vytvořím si nový list, do kterého vložím hodnoty z listu s příponami výše:
				final List<AbstractTableValue> tableValues = new ArrayList<>();

				// Vložím do listu tableValues přípony z listu s příponami, které jsou v
				// defaultModel listu:
				temp.forEach(v -> tableValues.add(new ChangeFileType("." + v.getExtensionFromFile(), ".")));

				// vložím hodnoty do tabulky:
				tableModelChangeFileType.setValuesList(tableValues);
			}
			
			else if (e.getSource() == btnSwapExtensions) {
				/*
				 * Toto tlačítko slouží pro prohození přípon z levého sloupce do pravého a
				 * naopak.
				 * 
				 * Zjistí se, zda není tabulka prázdná (žádné položky - přípony) a zda je
				 * označen nějaký řádek v tabulce, pak se data prohodí, ale jen pokud nebudou
				 * žádné duplicity (nemělo by nastat).
				 */
				if (tableModelChangeFileType.getValuesList().isEmpty()) {
					JOptionPane.showMessageDialog(this, txtEmptyTableText, txtEmptyTableTitle,
							JOptionPane.ERROR_MESSAGE);
					return;
				}
				
				
				final int selectedRow = tableChangeFileType.getSelectedRow();
				
				if (selectedRow == -1) {
					JOptionPane.showMessageDialog(this, txtUnselectedTableRowText, txtUnselectedTableRowTitle,
							JOptionPane.ERROR_MESSAGE);
					return;
				}
				
				
				/*
				 * Zde je vše v pořádku, pak mohu prohodit přípony v označeném řádku a otestovat
				 * duplicity, případně vrátit změny.
				 * 
				 * A označit zpět řádek, který byl označen - kvůli přenačtení tabulky.
				 */
				if (tableModelChangeFileType.swapColumns(selectedRow))
					tableChangeFileType.setRowSelectionInterval(selectedRow, selectedRow);
			}
			
			
			
			
			
			
			
			else if (e.getSource() == btnTestTextForRegEx) {
				/*
				 * Zde otestuji text v textovém poli: txtRegExText na regulární výraz, který je v poli txtRegExText.
				 * 
				 * Zde mě nenapadlo, jak bych testoval samotné zadané texty na regulární výrazy, abych zjistil, co uživatel zadal,
				 * akorát, zda není prázdné pole pro regulární výraz, ale to je asi tak všechno, pak už jen vezmu ten regulární výraz a otestuji jej na
				 * zadaný text coby nýzev souboru.
				 * 
				 * Postup:
				 * - Otestuji, zda není prázdný regulární výraz, případně podsvítím pole.
				 * - Pak si vezmu texty, odeberu bílé mezery pred a za textem a otestuji zadanž text na výraz, dle toho obarvím pole s textem na zeleno nebo řerveno.
				 */
				if (txtRegExOriginalFileName.getText().trim().isEmpty()) {
					// Zde je prázdné pole pro regulární výraz, tak jej obarvím - pokud není:					
					if (txtRegExOriginalFileName.getBackground().getRGB() != Color.RED.getRGB()) {
						txtRegExOriginalFileName.setBackground(Color.RED);
						txtRegExOriginalFileName.setFont(BOLD_FONT_ERROR_TXT_FIELD);
						return;
					}
				}
				
				// Zde v tom poli pro regulární výraz něco je, tak obarvím pole na bílo - pokud
				// není:
				else if (txtRegExOriginalFileName.getBackground().getRGB() != Color.WHITE.getRGB()) {
					txtRegExOriginalFileName.setBackground(Color.WHITE);
					txtRegExOriginalFileName.setFont(DEFAULT_FONT_ERROR_TXT_FIELD);
				}
					
				
				// Zde je text v poli pro regulární výrazu asi dobrý, alespoň není prázdný. Tak
				// mohu pokračovat v testování textu z druhého pole pro zadání řetězce, který se
				// má otestovat, zda splňuje podmínky pro zadaný regulární výraz.
				
				if (txtRegExText.getText().matches(txtRegExOriginalFileName.getText())) {
					// Zde tex splňuje podmínky pro regulární výraz, tak jej obarvím zelené:
					if (txtRegExText.getBackground().getRGB() != Color.GREEN.getRGB()) {
						txtRegExText.setBackground(Color.GREEN);
						txtRegExText.setFont(BOLD_FONT_ERROR_TXT_FIELD);						
					}
				}
				
				// Zde text nesplňuje podmínky pro regulární výraz, tak jej obarvím červeně.
				else if (txtRegExText.getBackground().getRGB() != Color.RED.getRGB()) {
					txtRegExText.setBackground(Color.RED);
					txtRegExText.setFont(BOLD_FONT_ERROR_TXT_FIELD);
				}		
			}
			
			
			
			
			
			
			
			
			
			
			else if (e.getSource() == btnConfirm) {
				// Otestuji zadané adresáře, případně vytvořím cílový adresář - pokud je zadán:
				if (!App.checkDirs())
					return;
				
				/*
				 * Sem se dostaqnu, pokud existuje zdrojový adresář, a cílový pokud byl zadán, byl
				 * vytvořen pokud neexistval, tak jako tak, cílový adresář již existuje, pokud byl zadán.
				 * 
				 * Tak to otestuji a případně zkopíruji soubory do cílového adresáře:
				 */
				if (App.getFileDestinationDir() != null && !App.copySourceDirToDestinationDir()) {
					JOptionPane.showMessageDialog(this, txtCopyDirErrorText, txtCopyDirErrorTitle,
							JOptionPane.ERROR_MESSAGE);
					return;
				}
				
				
				
				// Nyní mohu pokračovat v nahrazení návzů souborů:				
				/*
				 * Nejprve otestuji, zda je zadán validní název pro přejmenování souborů - jedná se o název,
				 * ke kterému se přípojí indexování souborů.
				 */

				final String newName = txtName.getText();
				
				if (newName.replaceAll("\\s*", "").isEmpty() || !newName.matches(Constants.REG_EX_FILE_NAME)) {
					JOptionPane.showMessageDialog(this, txtErrorOrEmptyFieldText, txtErrorOrEmptyFieldTitle,
							JOptionPane.ERROR_MESSAGE);
					return;
				}
				

				
				/*
				 * Nyní zjistím, zda je označeno změnit typ souboru - přípony souborů, pokud
				 * ano, tak zjistím, zda tabulka není prázdná a zda jsou všechna pole vyplněné.
				 * tj. ani jeden řádek či sloupec, či jednotlivé políčko není prázdné, prostě
				 * všechna políčka v tabulce jsou vyplněná.
				 * 
				 * Pokud jedna z těch podmínek nění splněna, tj. tak zde skončím a oznámím to
				 * uživateli, protože bych nemohl pokračovat v přejmenování souborů.
				 */
				if (chcbChangeFileType.isSelected()) {
					if (tableModelChangeFileType.getValuesList().isEmpty()) {
						JOptionPane.showMessageDialog(this, txtEmptyTableChangeTypeText, txtEmptyTableChangeTypeTitle,
								JOptionPane.ERROR_MESSAGE);
						return;
					}

					else if (tableModelChangeFileType.containsSomePoint()) {
						JOptionPane.showMessageDialog(this, txtEmptyFieldInTableText, txtEmptyFieldInTableTitle,
								JOptionPane.ERROR_MESSAGE);
						return;
					}
				}
				
				
				
				
				
				
				/*
				 * Nyní zjistím, zda je označeno "Přejmenovat soubory obsahující text" v části,
				 * kde se "definuje", jaké soubory se mají přejmenovat. Pokud je to označneo,
				 * tak otestuji, zda to pole není prázdné a to je vše, ještě bych mohl zadaný text otestovat na regulární výraz, ale
				 * v podstatě může být zadáno témšř cokoli, tak si to nebudu níjak komplikovat.
				 */
				if (chcbRenameFileContaintsText.isSelected()
						&& txtFileNameForRename.getText().replaceAll("\\s*", "").isEmpty()) {
					JOptionPane.showMessageDialog(this, txtEmptyFieldFileForRenameText, txtEmptyFieldFileForRenameTitle,
							JOptionPane.ERROR_MESSAGE);
					return;
				}
				
				
				
				
				
				
				
				/*
				 * Zde se otestuje, zda je označeno, že semají přejmenovat soubory, které splňují zadaný regulární výraz, 
				 * pokud tomu tak, je, tak zjistím, zda je nějaký text vůbec zadán, resp. nějaký regulární výraz.
				 * Zde už netestuji i regulární výraz na zadaný regulární výraz, stačí, když nebude prázdné pole,
				 * nevíc uživatel má možnost si zadaný výraz otestovat, tak jestli tam alespoň něco zadá, uvidí sám.
				 */
				if (chcbRenameFilesByRegEx.isSelected()
						&& txtRegExOriginalFileName.getText().replaceAll("\\s*", "").isEmpty()) {
					JOptionPane.showMessageDialog(this, txtRegExEmptyFieldText, txtRegExEmptyFieldTitle,
							JOptionPane.ERROR_MESSAGE);
					return;
				}
				
				
				
				
				
				
				
				/*
				 * Zde otestuji, zda je označené, že se mají přejmenovat pouze soubory typu,
				 * resp. soubory s danou příponou. Pokud ano, tak otestuji, zda je alespoň jedna
				 * přípona označena, jinak by nemělo žádný smysl přejmenovávat soubory, protože
				 * by se žádný nepřejmenoval.
				 */
				if (chcbRenameFileWithExtension.isSelected()) {
					if (listFileExtensions.isListEmpty()) {
						JOptionPane.showMessageDialog(this, txtSourceDirisEmptyText, txtSourceDirisEmptyTitle,
								JOptionPane.ERROR_MESSAGE);
						return;
					}

					else if (!listFileExtensions.isAtLeastOneItemSelected()) {
						JOptionPane.showMessageDialog(this, txtUnselectedItemsInListText, txtUnselectedItemsInListTitle,
								JOptionPane.ERROR_MESSAGE);
						return;
					}
				}
				
				
				
				/*
				 * V tuto chvíli vím, že existuje minimálne zdrojový adresář, tak pouze
				 * otestuji, zda existuje cílový adresář, když ano, tak budu pracovat se soubory
				 * v něm, pokud cílový adresář neexistuje, pak jej uživatel nezadal a chce
				 * pracovat pouze se soubory ve zdrojovém adresáří.
				 * 
				 * Note:
				 * Toto bych si mohl zjistit z toho, zda se výše zkopírovaly soubory nebo ne,
				 */
				final File file;
				
				if (App.getFileDestinationDir() != null && App.getFileDestinationDir().exists())
					file = App.getFileDestinationDir();
				else
					file = App.getFileSourceDir();
				
				
				
				
				final Map<File, List<File>> map = FilesToMap.getLoadedDirs(file, chcbRenameFilesInSubDirs.isSelected(),
						folderPanel.getChcbCountHiddenFiles().isSelected());
				
				
				
				/*
				 * Pokud není označen chcb pro to, že semají indexovat soubory v každém
				 * podadresáři od začátku, tak se zde musí vytvořit list s indexy, který se pak
				 * aplikuje na všechny nové názvy souborů,které se mají přejmenovat. Pokud bude
				 * označen, pak se má pro každý podadresář vytvořit nový seznam indexů,
				 * začínající od začátku, tak to budu řešit až pri každém přejmenování, ne zde.
				 * 
				 * Zde to řeším, protože zde mám k dispozici všechny soubory, i když se pak
				 * nějaké mohou vynechat, tak není třeba generovat takový počet, ale kdybych to
				 * řešil později, tak bych to měl trochu složitejší, to by musel stejně vyřešit
				 * zde, a pak jen nějaké indexy například odebrat z této kolekce, pokud se
				 * nějaký počet souborů odebere - že se nebude přejmenovávat, tak tento počet
				 * prvků odebrat
				 * 
				 */
				if (!chcbStartIndexingFromZeroInSubDirs.isSelected()) {
					/*
					 * Zjistím počet souborů ve všech adresářích v mapě, abych věděl, kolik mám
					 * vytvořit indexů pro nové názvy souborů:
					 */
					count = 0;

					/*
					 * Cyklem projdu celou mapu a do výše uvedené proměnnou count vypočítám celkový
					 * počet soubourů ve všech adresářích v proměnných file v mapě jako klíče - tam
					 * jsou všchny adresáře, tak celkový počet jejich souborů vložím do proměnné
					 * count.
					 */
					map.forEach((key, value) -> findCountOfFiles(key, folderPanel.getChcbCountHiddenFiles().isSelected()));

					/*
					 * Nyní mám v proměnné count celkový počet souborů ve včech adresářích v mapě v
					 * klíčích mapy. Postupně se sice budou volat metody, které nějaké soubory mohou
					 * ale nemusí vynechat z přejmenování - to záleží na uživatelem označených
					 * parametrech v okně aplikace.
					 */

					/*
					 * Zde si zjistím, jaký typ indexování uživatel zvolil a dle toho vytvořím
					 * příslušné indexy dle výše získaného počtu (count).
					 */
					/*
					 * Proměnná, do které si vložím typ indexování zvolený uživatelem v aplikaci.
					 */
					final KindOfIndexing kind = getKindOfIndexing();

					/*
					 * Do listu allIndexesList si uložím indexy, který se vytvoří dle výše zvoleného
					 * typu a zjištěného počtu.
					 */
					allIndexesList = Alg.getIndexes(kind != null ? kind.getIndexingEnum() : null, count);
				}
				
				
				
				/*
				 * Zde potřebuji nastavit index pro braní indexů z kolekce (abych věděl jaké
				 * indexy v kolekci - na jaké pozici), tak tento index zde nastavím na nulu,
				 * protože pokud se jedná například o opakované přejmenování souborů, pak se v
				 * případě, že je nastaveno, že se nemá indexovat v podadresářích od začátku,
				 * tak by se tato proměnné pořád inkrementovala a pokud by se jednalo o
				 * opakované přejmenování, pak by byl index vždy mimo velikost kolekce, tak je
				 * třeba v tomto případě vždy nastavit index na nulu.
				 */
				indexForIndexes = 0;
				
				
				
				
				/*
				 * Zde již mám namapované adresáře i s jejich soubory, které se nachází ve
				 * zvoleném zdrojovém adresáři. tak mohu pro všechny soubory volat metody, které
				 * vyfiltrují pouze soubory, které se mají přejmenovat, na názvy, na které
				 * semají přejmenovat.
				 */
				map.forEach((key, value) -> checkFileForRename(value));
				
				
				
				/*
				 * Na konec otestuji, zda se jedná o změnu dat ve zdrojovém adresáři, pokud ano,
				 * pak znovu načtu data, protože byly změněny data, pak musím znovu načíst data
				 * do aplikace, protože ted už se ve zdrojovém adresáři nachází jiná data než
				 * při jejich původním načtení do aplikace.
				 */
				if (file == App.getFileSourceDir())
					/*
					 * Zde by bylo vhodné, dvakrát zavolat metodu checkPathFields, která se nachází
					 * ve třídě FoldersPanel, slouží pro přenačtení položek v aplikaci - položky
					 * jako jsou načtené přípony ze zvoleného zdrojového adresáře, soubory v něm pro
					 * vynechání apod. Tu metodu checkPathFields by stačilo zavolat jednou s
					 * parametrem výčtového typu pro textové pole pro zadání zdrojového adresáře,
					 * pro jistotu by bylo třeba dobré i zavolat to dvakrát i druhým výčtovým typem
					 * pro textové pole pro cílový adresář, ani jedno není třeba, protože v tuto
					 * chvíli vím, že zdrojový adresář byl zadán, a dle této podmínky vím, že byl
					 * zvolen i jako cílový - soubory se nikam neměly zkopírovat, takže se všechny
					 * soubory přejmenovaly v tom zdrojovém adresáři, tak znovu přenačtu data, aby
					 * uživatel mohl případně označit znovu a přejmenovat to ještě nějak jinak apod.
					 * - Aby již měl k dispozici aktuální soubory, aby nemusel znovu nastavovat
					 * zdrojový adresář na ten samý, jen kvůli přenačtění dat.
					 */
					App.setFileSourceDir(file, FoldersPanel.containsDirSubDirs(file.getAbsolutePath()));
			}
		}
		
		
		
		
		
		
		
		
		else if (e.getSource() instanceof JCheckBox) {
			if (e.getSource() == chcbIndexByNumber) {
				/*
				 * Zde je označeno indexování dle čísel, tak znepřístupním panel pro indxování
				 * dle písmen:
				 */
				enabledAllComponents(pnlIndexByNumberComponents.getComponents(), true);
				enabledAllComponents(pnlIndexByLettersComponents.getComponents(), false);
				
				// Dále nastavím text pro ukázku výsledného názvu zvolených komponent:
				setExampleLabel();
			}

			
			else if (e.getSource() == chcbIndexByLetters) {
				/*
				 * Zde je označeno indexování dle písmen, tak znepřístupním panel pro indxování
				 * dle čísel:
				 */
				enabledAllComponents(pnlIndexByNumberComponents.getComponents(), false);
				enabledAllComponents(pnlIndexByLettersComponents.getComponents(), true);
				
				// Dále nastavím text pro ukázku výsledného názvu zvolených komponent:
				setExampleLabel();
			}
			
			
			
			
			else if (e.getSource() == chcbLeaveFileType)
				/*
				 * Zde se má datový typ souborů ponechat, tak znepřístupním editaci tabulky (i s
				 * tlačítky) - celý panel
				 */
				enabledAllComponents(pnlChangeFileTypeTableData.getComponents(), false);
				
			else if (e.getSource() == chcbChangeFileType)
				/*
				 * Zde se má změnit datový typ, tak zpřístupním tabulku, aby uživatel mohl zadat
				 * nové datové typy (včetně tlačítek).
				 */
				enabledAllComponents(pnlChangeFileTypeTableData.getComponents(), true);
			
			
			
			
			// Část pro zadání parametrů pro výběry souborů, které se mají prejmenovat -
			// zpřístupnění a znepřístupnění komponent:
			else if (e.getSource() == chcbRenameAllFiles) {
				/*
				 * Zde zněpřístupním komponenty pro zadání textu, který mají názvy souborů
				 * obsahovat a panel pro zadání a testování regulárního výrazu dle kterého se
				 * testují pak názvy souborů pro přejmenování a text areu pro zadání přípon:
				 */
				enableRenameFileContainsTextComponents(false);
				enablePnlFileNameByRegEx(false);
				enabledAllComponents(pnlListFileExtension.getComponents(), false);
			}
			
			else if (e.getSource() == chcbRenameFileContaintsText) {
				/*
				 * Zde Znepřístupním panel, který osabuje komponenty pro zadání a testování
				 * regulárního výrazu, dle které se testují názvy souborů, které se mají
				 * přejmenovat. A Zpřístupním panel pro zadání textu, který musí názvy souborů
				 * obsahovat, aby se přejmenovaly. A ještě znepřístupním komponentu text area
				 * pro zadání přípon.
				 */
				enableRenameFileContainsTextComponents(true);
				enablePnlFileNameByRegEx(false);
				enabledAllComponents(pnlListFileExtension.getComponents(), false);
			}
			
			else if (e.getSource() == chcbRenameFilesByRegEx) {
				/*
				 * Zde znepřístupním panel pro zadání textu, který musí názvysouborů obshovat,
				 * aby se přejmenovaly a zpřístupním panel pro zadání a testování regulárního
				 * výrazu, dle které se testují názvy souborů, které se mají přejmenovat. A
				 * ještě znepřístupním komponentu Textarea pro zadání přípon.
				 */
				enableRenameFileContainsTextComponents(false);
				enablePnlFileNameByRegEx(true);
				enabledAllComponents(pnlListFileExtension.getComponents(), false);
			}
			
			else if (e.getSource() == chcbRenameFileWithExtension) {
				/*
				 * Zde zpřístupním pouze komponentu JTextArea pro zadání přípon souborů, které
				 * se mají přejmenovat, zbytek znepřístupním (znepřítupním: pole pro zadání
				 * textu, který mají názvy souborů obsahovat a panel s komponentami pro zadání a
				 * testování regulárního výrazu dle kterého se testují názvy souborů pro
				 * přejmenování).
				 */
				enableRenameFileContainsTextComponents(false);
				enablePnlFileNameByRegEx(false);
				enabledAllComponents(pnlListFileExtension.getComponents(), true);
			}
			
			
			
			
			
			
			
			else if (e.getSource() == chcbSkipFilesWithExtension) {
				/*
				 * Zde otestuji, zda je označena tento chcb, pokud je označen, pak zpřístupním
				 * pole pro označení přípon souborů, které se mají vynechat. Pokud není označen
				 * chcb, tak toto pole znepřístupním (i s tlačítky pro označení / odznačení
				 * všech komponent.
				 */
				if (chcbSkipFilesWithExtension.isSelected())
					enabledAllComponents(pnlListWithExtensions.getComponents(), true);
				else
					enabledAllComponents(pnlListWithExtensions.getComponents(), false);
			}

			else if (e.getSource() == chcbSkipFilesWithName) {
				/*
				 * Zde otestuji, zda je označen tento chcb, pokud ano, pak zpřístupním pole pro
				 * označení konkrétních souborů, které se mají vynechat, pokud není chcb
				 * označen, pak toto pole znepřístupním (včetně tlačítek pro označení a
				 * odznačení všech položek v poli.
				 */
				if (chcbSkipFilesWithName.isSelected())
					enabledAllComponents(pnlSkipFilesWithName.getComponents(), true);				
				else
					enabledAllComponents(pnlSkipFilesWithName.getComponents(), false);
			}
			
			
			
			
			
			
			
			// Nastavení labelu s ukázkami nového názvu pro soubory:
			else if (e.getSource() == chcbIndexInFrontOfText || e.getSource() == chcbIndexBehingOfText
					|| e.getSource() == chcbUseBigLetters || e.getSource() == chcbUseSmallLetters
					|| e.getSource() == chcbLettersInFrontOfText || e.getSource() == chcbLettersBehindOfText)
				setExampleLabel();
		}
		
		
		
		
		// Pokud se označí nějaký jiný typ indexování, chci aby se změny ihned projevily
		// v labelu, který ukazuje nastavné názvy - tedy jejich příklad.
		else if (e.getSource() instanceof JComboBox<?> && (e.getSource() == cmbKindOfIndexingByLetters || e.getSource() == cmbKindOfIndexing))
			setExampleLabel();
	}
	
	
	
	
	
	
	
	
	
	/**
	 * Metoda slouží pro filtrování souborů, které se mají přejmenovat.
	 * 
	 * Metoda, která otestuje všechny souboru v převzatém listu a zjistí, zda se
	 * soubory v tom listu mohou přejmenovat nebo ne.
	 * 
	 * Nejprve se zjistí, zda uživatel v aplikaci označil, že se mají přejmenovat
	 * všechny soubory nebo ne. Pokud ano, je to OK, a pokračuje se dále metodou
	 * checkForSkipFile.
	 * 
	 * Ale pokud uživatel označil, že se mají pořejmenovat pouze soubory obsahující
	 * zadaný text, či splňující regulární výraz nebo mají označeou příponu apod.
	 * tak se vytvoří nový list a do nej se vloží pouze soubory, které se mohou z
	 * tohoto hlediska přejmenovat (bylo by možné pouze mazat soubory z listu v
	 * parametru metody), list se pak předá do metody checkForSkipFile, kterou se
	 * pokračuje.
	 * 
	 * @param list
	 *            - list se soubory v nějakém adresáři, které se mají přejmenovat.
	 */
	private void checkFileForRename(final List<File> list) {
		if (list.isEmpty())
			// nemá smysl pokračovat:
			return;
		
		/*
		 * List, do kterého se vloží pouze soubory, které lze přejmenovat, resp.
		 * soubory, které splňují zadané podmínky uživatelem v aplikaci, aby bylo možné
		 * soubor přejmenovat.
		 */
		final List<File> tempList = new ArrayList<>();
		
		// Zde je označeno, že se mají přejmenovat všechny soubory, tak není třeba
		// nějaké vynechat:
		if (chcbRenameAllFiles.isSelected()) {
			checkForSkipFile(list);
			return;
		}
			
			
		
		
		/*
		 * Zde je označeno, že se mají přejmenovat pouze soubory, které obsahují zadaný
		 * text, tak projdu převzaty list v parametru metody a pro každý list otestuji,
		 * zda obsahuje zadaný text, ale ještě s tím, že se budou nebo nebudou ignorovat
		 * velikosti písmen.
		 */
		else if (chcbRenameFileContaintsText.isSelected()) {
			// Text z textvého pole, které obsahuje text, který má obsahovat název souboru,
			// aby byl přejmenován
			final String text = txtFileNameForRename.getText().trim();

			list.forEach(f -> {
				// Název souboru bez přípony:
				final String fileName = FilenameUtils.removeExtension(f.getName());

				// Podmínka, zda mám testovat zda název souboru obsahuje zadaný text s
				// ignorováním velikosti písmen nebo ne:
				if (chcbIgnoreLetterSize.isSelected()) {
					// Zde se mají ignorovat velikost písmen:
					if (fileName.toUpperCase().contains(text.toUpperCase()))
						tempList.add(f);
				}

				// Zde se neignorují velikost písmen:
				else if (fileName.contains(text))
					tempList.add(f);

			});
		}
		
		
		
		
		/*
		 * Zde je označeno, že se mají přejmenovat pouze soubory, které splňují
		 * regulární výraz, tak zjistím, zda se má výraz testovat s příponou nebo bez a
		 * dle toho sestavím název souboru a pak jej otestuji na regulární výraz, pokud
		 * jím daný soubor projde, přidám jej do listu.
		 */
		else if (chcbRenameFilesByRegEx.isSelected()) {
			list.forEach(f -> {
				/*
				 * Název souboru, který se bude testovat na regulární výraz, akorát do něj
				 * vložím název souboru s příponou nebo bez:
				 */
				final String fileName;
				
				if (chcbIncluseExtension.isSelected())
					fileName = f.getName();
				else
					fileName = FilenameUtils.removeExtension(f.getName());
				
				
				// Nyní otestuji výše získaný název na regulární výraz:
				if (fileName.matches(txtRegExOriginalFileName.getText().trim()))
					tempList.add(f);
			});
		}
		
		
		
		
		/*
		 * Zde je označeno, že se mají přejmenovat puze soubory, které mají označenou
		 * příponu. Tak do listu tempList vložím puze soubory, které mají jednu z
		 * označených přípon:
		 */
		else if (chcbRenameFileWithExtension.isSelected()) {
			list.forEach(f -> {
				/*
				 * Získám si příponu iterovaného souboru. Musím k ní přidat tečku, protože se v
				 * listu jsou přípony zobrazeny s tečkou a v metodě isItemWithTextSelected nad
				 * listem se testuje zobrazený text u položky. Kdybych tam tu tečku nedal, nikdy
				 * by se nic nerovnalo.
				 */
				final String fileExtension = "." + FilenameUtils.getExtension(f.getName());

				/*
				 * Nyní zjístím, zda je výše získaná přípona označena v listu:
				 */
				if (listFileExtensions.isItemWithTextSelected(fileExtension))
					tempList.add(f);
			});
		}
		
		
		
		
		/*
		 * Nyní se v listu tempList nachází puze soubory, které je možné přejmenovat,
		 * vložily se do něj pouze soubory, které prošly jednou výše označených
		 * podmínek, tak nyní musím otestovat, zda se nějaké soubory mají vynechat:
		 */
		checkForSkipFile(tempList);
	}
	
	
	
	
	
	
	
	
	/**
	 * Metoda slouží pro filtrování souborů, které se mají vynechat.
	 * 
	 * Pouze se otestují soubory v listu list na to, zda se vyskytují jejich přípony
	 * nebo jsou soubory přímo označené, že se mají vynechat. Jedná se o dva listy
	 * dole v okně aplikace, listy Vynechat soubory typu a Vynechat soubory.
	 * 
	 * V metodě se využívá převzatý list v parametru metody a pouze se z něj
	 * odebírají soubory, které se mají vynechat.
	 * 
	 * @param list
	 *            - List se soubory, které se otestují, zda se mají vynechat nebo
	 *            ne.
	 */
	private void checkForSkipFile(final List<File> list) {
		// nemá smysli pokračovat:
		if (list.isEmpty())
			return;
		
		
		
		/*
		 * Zde se otestuje, zda je označen chcb pro vynechání souboru s označenou
		 * příponou. Pokud je nějaká přípony označena, pak se otestuje, zda se v listu
		 * nachází nějaký soubor, který obsahuje danou příponu, pokud ano, odebere se z
		 * toho listu
		 */

		if (chcbSkipFilesWithExtension.isSelected() && listSkipFileWithExtension.isAtLeastOneItemSelected()) {
			/*
			 * Zde je nějaká přípona označena, tak má smysl testovat přípony souborů:
			 */
			for (final Iterator<File> it = list.iterator(); it.hasNext(); ) {
				// Získám si iterovaný soubor:
				final File file = it.next();

				// Získám si příponu:
				final String extension = "." + FilenameUtils.getExtension(file.getName());

				/*
				 * Zde otestuji, zda je přípona právě iterovaného souboru označena v aplikaci
				 * jako přípona souboru, který se má vynechat, pokud ano, pak jej odstraním z
				 * listu:
				 */
				if (listSkipFileWithExtension.isItemWithTextSelected(extension))
					it.remove();
			}
		}
		
		
		
		


		
		
		/*
		 * Zde se ještě otestuje, zda je označen nějaký konkrétní soubor, který se má
		 * vynechat. Pokud ano, pak takový soubor bude z listu vymazán.
		 */
		/*
		 * Zde otestuji, zda je nějaký soubor označen, že se má vynechat, pokud ano, pak
		 * má smysl procházet list a odebrat z něj soubory, které se mají vynechat:
		 */
		if (chcbSkipFilesWithName.isSelected() && pnlSkipFilesWithName.getListForSkipFilesWithName().isAtLeastOneItemSelected()) {
			for (final Iterator<File> it = list.iterator(); it.hasNext(); ) {
				final File file = it.next();

				/*
				 * Zde je označen nějaký soubor, tak musím otestovat, zda se jedná o ten
				 * konkrétní označený soubor nebo ne. Ale musím zde hlídat cestu k tomu souboru,
				 * protože v aplikaci jsou načteny ty soubory ze zdrojového adresáře, ale v tuto
				 * chvíli se v listu list mohou nacházet ty samé soubory, ale z cílového
				 * adresáře, tak musím při porovnávání hlídat cesty.
				 */
				if (App.getFileDestinationDir() != null && App.getFileDestinationDir().exists()) {
					/*
					 * Zde eixstuje cílový adresář, tak beru cestu z toho cílového adresáře a přidám
					 * k ní soubory z listu v okně aplikace:
					 */

					if (pnlSkipFilesWithName.getAllItemsForListSkipFilesWithName().stream()
							.anyMatch(v -> (App.getFileDestinationDir().getAbsolutePath() + "/" + v.toString())
									.equals(file.getAbsolutePath()) && v.isSelected()))
						// Zde je v okně aplikace označen daný soubor, tak jej odeberu z listu:
						it.remove();
				}

				/*
				 * Zde se nachází soubory pořád ve zdrojovém adresáři, tak není třeba měnit
				 * cesty, soubory v aplikaci jsou stejné jako načtené soubory v listu (mělo by),
				 * tak otestuji, zda jsou stejné cesty a pokud se najde soubor v aplikaci v
				 * Jlistu a není označen, pak jej přidám do tempListu, protože se má
				 * přejmenovat, kdyby byl označen, tak se má vynechat, tak nebudu nic dělat -
				 * přidávat ho do listu se soubory pro přejmenování.
				 */
				else if (pnlSkipFilesWithName.getAllItemsForListSkipFilesWithName().stream().anyMatch(
						v -> v.getFile().getAbsolutePath().equals(file.getAbsolutePath()) && v.isSelected()))
					// Zde je v okně aplikace označen právě iterovaný soubor, tak se má vynechat,
					// proto jej odeberu z listu:
					it.remove();
			}
		}
		
		
		
		/*
		 * Předám zbývající soubory, které se nemají vynechat do metody pro
		 * přejmenování:
		 */
		renameFile(list);
	}
	
	
	
	
	
	
	
	/**
	 * Metoda, která slouží pro přejmenování souborů.
	 * 
	 * Nejprve se zjistí, zda se mají přepsat nějaké přípony souborů, pokud ano, pak
	 * se rovnou přepíšou.
	 * 
	 * Pak se zjistí počet souborů v daném adresáři, kde jso uvšechny soubory, k
	 * tomu si stačí vzít libovolný soubor z listu a vzít se jeho rodiče, pak mám
	 * adresář, ve kterém se nachízí souory, které se mají přemenovat - soubory v
	 * listu list.
	 * 
	 * Pak se pro příslušný počet souborů vytvoří list, který obsahuje indexy pro
	 * nové názvy souborů.
	 * 
	 * A pak se přejmenuí soubory dle výše vygenerovaných názvů. Projde se list
	 * files a každý soubor dostane nový index.
	 * 
	 * @param list
	 *            - list souborů, které se mají přejmenovat.
	 */
	private void renameFile(final List<File> list) {
		if (list.isEmpty())
			return;
		
		
		if (chcbChangeFileType.isSelected()) {
			// Zde je označeno, že se mají změnit typy souborů, pak je změním:
			for (final ListIterator<File> it = list.listIterator(); it.hasNext();) {
				// Načtu si soubor s listu - právě iterovaný:
				final File f = it.next();

				// Půvédní přípona pro testování, zda se má změnit - je v tabulce:
				final String extension = "." + FilenameUtils.getExtension(f.getName());

				// tableValue se naplní pouze v případě, že se v tabulce nachází původní přípona
				// - extension
				final AbstractTableValue tableValue = tableModelChangeFileType.getValueWithOriginalExtension(extension);

				// Zde tabulka obsahuje příponu souboru a má se změnit, tak jej změním:
				if (tableValue != null) {
					// Zde byla nazelena v tabulce původní přípona, tak příponu u daného souboru
					// změním na novou:
					final File newExtension = new File(f.getParent() + "/" + FilenameUtils.removeExtension(f.getName())
							+ tableValue.getNewValue());

					// Přejmenuji soubor a otestuji, zda se to povedlo, pokud ne, vyhodím chybové
					// oznámení uživateli:
					if (f.renameTo(newExtension))
						// Nastavím do listu ten změněný soubor - s tou změněnou příponou, aby se
						// přepsal i jeho název:
						it.set(newExtension);
					else
						JOptionPane.showMessageDialog(this,
								txtRenameFileErrorText_1 + "\n" + txtRenameFileErrorText_2 + ": " + f.getAbsolutePath()
										+ "\n" + txtRenameFileErrorText_3 + ": " + newExtension.getAbsolutePath(),
								txtRenameFileErrorTitle, JOptionPane.ERROR_MESSAGE);
				}
			}
		}
				
		
		
		
		
		
		
		/*
		 * Zde je třeba znovu otestovat, zda se mají indexovat soubory v podadresářích od začátku nebo ne, pokud ne,
		 * pak není třeba nic dělat, v metodě po kliknutí na tlačítko Přejmenovat se vše již nastavilo, ale pokud to je
		 * označeno, pak zde musím vygenerovat nový počet indexů pro aktuální soubory a nastavit index na nulu
		 */
		if (chcbStartIndexingFromZeroInSubDirs.isSelected()) {
			/*
			 * Proměnná, do které si vložím typ indexování zvolený uživatelem v aplikaci.
			 */
			final KindOfIndexing kind = getKindOfIndexing();
			
			
			/*
			 * Zjistím počet souborů v adresáři file, abych věděl, kolik mám vytvořit indexů
			 * pro nové názvy souborů:
			 */
			count = 0;
			findCountOfFiles(list.get(0).getParentFile(), folderPanel.getChcbCountHiddenFiles().isSelected());		
			
			/*
			 * List, do kterého si načtu index pro soubory dle zvoleného nastavení.
			 */
			allIndexesList = Alg.getIndexes(kind != null ? kind.getIndexingEnum() : null, count);
		}
		
		
		
		/*
		 * List, do kterého budu vkládat soubory, které se najdou v adresáří - listu
		 * list.
		 * 
		 * Resp. Soubory, kterým se vygenruje nový název dle zadaného indexu, ale soubor
		 * s takovým názvem - indexem již v tom adresáři existuje (v listu - obsahuje
		 * načtené soubory z daného adresáře), proto je přidávám do tohoto listu a
		 * později se akorát otestuje, zda se ten název může nechat, nebo se přejmenuje
		 * na nový - "doplňující" název, pokud nypříklad byl právě z důvodu duplicity
		 * nějaký soubor vynechán a přidán do této kolekce, tak se pak znovu projde a ty
		 * indexy, které byly vynechány budou doplněny.
		 */
		final List<File> tempList = new ArrayList<>();
		
		
		
		/*
		 * Index pro braní indexů z kolekce s indexy.
		 * 
		 * V případě, že je tento chcb označen, pak semají indexovat soubory v
		 * podadresářích od začátku, pak musím přslušný index nastavit na nulu, pokud
		 * ale není tento chcb označen, pak se nemají soubory v podadresářích indexovat
		 * od začátku, pak ten index nechám, tak jak byl nastaven u poslení hodnoty ana
		 * tu navážu:
		 * 
		 * Note: je možné nastavit index v podmínce výše, tento důvod je pouze pro
		 * zpřehlednění, že ten index - proměnná indexForIndexes patří k tomuto cyklu:
		 */
		if (chcbStartIndexingFromZeroInSubDirs.isSelected())
			indexForIndexes = 0;
		
		/*
		 * Proměnná, která obsahuje nový text souboůr pro indexování - k tomuto názvu se přidá zvolený index.
		 */
		final String newName = txtName.getText();
		
		for (final File f : list) {
			/*
			 * Proměnná pro nový soubor, na který se přejmenuje ten původní:
			 */
			final File newFile = getNewFileName(f, newName, allIndexesList.get(indexForIndexes++));
						
			
			
			/*
			 * Zde otestuji, zda se ten soubor newFile již nachází v listu, protože pokud
			 * ano, tak je třeba dále zjistit, zda se ten soubor může nechat tak jak je -
			 * jeho název je v pořádku a ve správné syntaxi, nebo se má přejmenovat na nový
			 * název se "správným indexováním" Protože se ten soubor mohl nějak jmenovat,
			 * třeba name_AA a v na tuto syntaxi se mají přejmenovat všechny soubory v daném
			 * adresáři, tak jen zjistím, zda ten soubor mám přejmenovat na novou syntaxi,
			 * (toto v případě, že jsem třeba o pár iterací dříve v tomto cyklu již daný
			 * název využil a tento soubor již existuje) nebo zda se ten soubor má později
			 * přejmenovat na nějaký "doplňující" index, například byl nějaký index
			 * přskočen, právě kvůli tomu, že nějaký soubor již existuje.
			 * 
			 * Proto soubory, které projdou podmínkou přidám do listu, abych je mohl
			 * přejmenovat na ty indexy, které byly vynechány, protože dané soubory již
			 * existují.
			 */
			if (containstListFileWithPath(list, newFile.getAbsolutePath())) {
				tempList.add(f);
				continue;
			}
			
			
			
			/*
			 * Zde mmohu přejmenovat soubor, ale jen, když se už nejmenuje tak, jak se má
			 * jmenovat, tj. pokud ten soubor již obsahuje název, který má, pak nemá smysl
			 * jej přejmenovat:
			 */
			if (!f.getAbsolutePath().equals(newFile.getAbsolutePath()) && !f.renameTo(newFile))
				JOptionPane.showMessageDialog(this,
						txtRenameFileNameErrorText_1 + "\n" + txtRenameFileNameErrorText_2 + ": " + f.getAbsolutePath()
								+ "\n" + txtRenameFileNameErrorText_3 + ": " + newFile.getAbsolutePath(),
						txtRenameFileNameErrorTitle, JOptionPane.ERROR_MESSAGE);
		}
		
		
		
		
		
		/*
		 * Zde projdu tempList a u všech souborů otestuji, zda je možné jim přiřadit
		 * nový index pro "srovnání pořadí" nebo to není třeba, protože již mají správný
		 * index.
		 */
		for (final File f : tempList) {
			/*
			 * Zde si potřebuji vytvořit nový index, protože potřebuji itestovat idnexy v
			 * kolekci od začátku, ne využít ten indexForIndexes, protože obsahuje vždy
			 * aktuální index pro přejmenování souborů, kdezto tento index slouží pro
			 * "opakované" načítání indexů pro otestování vynechaných indexů.
			 */
			int index = 0;

			/*
			 * Získám si nový název pro tento soubor tak, že projdu postupně všechny získané
			 * indexy a pro každý zjistím, zda je již využit nebo ne, pokud ne, přejmenuji
			 * na něj daný soubor f, pokud projdu všechny indexy, pak je soubor již
			 * pojmenován správně, již není třeba brát nové indexy.
			 */
//			File newFile = getNewFileName(f, newName, allIndexesList.get(indexForIndexes++));
			File newFile = getNewFileName(f, newName, allIndexesList.get(index++));

			/*
			 * Tento cyklus testuje všechny využité indexy v názvech souborů ve zvoleném
			 * adresáři, tak zjistím, zda je možné doplnit nějaký index pro název souboru,
			 * který byl přeskočen nebo je to již v pořádku:
			 */
			while (newFile != null
					&& isFileWithIndexNameInDir(FilenameUtils.removeExtension(newFile.getName()), f.getParentFile())) {
				if (index < allIndexesList.size())
					newFile = getNewFileName(f, newName, allIndexesList.get(index++));
				/*
				 * Pokud se dostanu sem, tak jsem vyčerpal / otestovat veškeré indexy, to
				 * znamená, že se soubor v podstatě jmenuje tak, "jak má", tj. již má jeden z
				 * požadovaných indexů, tj. soubor s požadovaným indexem již existuje, a je
				 * využit "v pořádku - správně" - ten název, tak zde není třeba nic dělat,
				 * nebudu ten soubor přejmenovávat, tak nastavím danou proměnnou na null, aby se
				 * ukončil cyklus a přešlo k další iteraci - k dalšímu souboru pro otestování.
				 */
				else
					newFile = null;
			}
			
			
			
			
			/*
			 * Zde mohu přejmenovat soubor, ale musím otestovat, zda se má přejmenovat, tj.,
			 * zda už nemá správny název (pokud není proměnná newFile null), pak, zde má
			 * smysl jej přejmenovat, zda se už nejmenuje tak, jak se jmenovat má, pokud
			 * toto projde, pak se může přejmenovat soubor a pokud dojde k chybě při pokusu
			 * o přejmenování, vypíše se chyba uživateli:
			 */
			if (newFile != null && !f.getAbsolutePath().equals(newFile.getAbsolutePath()) && !f.renameTo(newFile))
				// Stejný text, jako výše:
				JOptionPane.showMessageDialog(this,
						txtRenameFileNameErrorText_1 + "\n" + txtRenameFileNameErrorText_2 + ": " + f.getAbsolutePath()
								+ "\n" + txtRenameFileNameErrorText_3 + ": " + newFile.getAbsolutePath(),
						txtRenameFileNameErrorTitle, JOptionPane.ERROR_MESSAGE);
		}
	}

	
	
	
	
	
	
	
	
	
	/**
	 * Metoda, která zjistí a vrátí výčet, který značí / definuje zvolený typ
	 * indexování souborů.
	 * 
	 * Jedná se o hodnotu z komponent JComboBox, ve kterých se vybírá "typ
	 * indexování" souborů. Například A, B, nebo AA, AB, nebo čísla apod.
	 * 
	 * @return výše popsaný výčtový typ indexování zvolený uživatelem, který
	 *         definuje typ indexvání souborů.
	 */
	private KindOfIndexing getKindOfIndexing() {
		if (chcbIndexByNumber.isSelected())
			return (KindOfIndexing) cmbKindOfIndexing.getSelectedItem();

		else if (chcbIndexByLetters.isSelected()) // Není třeba testovat, stačí else
			return (KindOfIndexing) cmbKindOfIndexingByLetters.getSelectedItem();

		return null; // Jen protože je proměnná kind final
	}
	
	
	
	
	
	
	
	
	
	

	
	/**
	 * Metoda, která zjistí, zda je soubor s názvem fileName v adresáři fileDir,
	 * pokud ano, vrátí se true, jinak false.
	 * 
	 * V metodě se zjistí, zda se jedná o skrytý soubor nebo ne, pokud ano, pak se
	 * ze začátku názvu souboru ještě odeberou desetinné tečky a podorvnávají se
	 * názvy bez této tečky, jinak by se nerovnaly, i kdyby byly názvy stejné, ale v
	 * jednom bybyla na jeho začátku desetinná tečka.
	 * 
	 * @param fileName
	 *            - název souboru (s desetinnou tečkou na začátku, pokud se jedná o
	 *            skrytý soubor)
	 * 
	 * @param fileDir
	 *            - Adresář, ve kterém se má zjistit, zda se již vyskytuje nějaký
	 *            soubor, který jako název má index - fileName.
	 * 
	 * @return true, pokud se v adresáři fileDir vyskytuje nějaký soubor, který jako
	 *         název obsahuje index fileName, jinak false.
	 */
	private static boolean isFileWithIndexNameInDir(final String fileName, final File fileDir) {
		final String fileNameWithoutPoint;
		
		if (fileName.startsWith("."))
			fileNameWithoutPoint = fileName.substring(1);
		else
			fileNameWithoutPoint = fileName;
		
		/*
		 * Projdu všechny soubory v daném adresáři fileDir a zjistím, zda je tam soubor
		 * s názvem fileName nebo ne:
		 */
		return Arrays.stream(Objects.requireNonNull(fileDir.listFiles())).anyMatch(f -> {
			/*
			 * Proměnná, do které si uložím název souboru bez jeho přípony.
			 */
			String name = FilenameUtils.removeExtension(f.getName());

			if (name.startsWith("."))
				name = name.substring(1);

			return !f.isDirectory() && name.equals(fileNameWithoutPoint);
		});
	}
	
	
	
	
	
	
	
	
	
	/**
	 * Metoda, která "vytvoří nový soubor pro přejmenování", resp. vytvoří proměnnou
	 * typu File, která obsahuje "nový soubor", na který se má přejmenovat soubor v
	 * proměnné f.
	 * 
	 * Metoda si zjistí označené parametry v aplikaci, například, zda je označeno
	 * indexování dle písmen nebo čísel, zda se má přidat index před nebo za zadaný
	 * texto coby název k indexování souboru, u písmen, zda mají být velká nbeo
	 * malá, způsob indexování apod.
	 * 
	 * Metoda pak vytvoří zmíněný "nový soubor" - proměnnou typu File, kterou metoda
	 * vrátí. Tato proměnná se vytvoří tak, že se zjistí adresář, ve kterém se
	 * nachází soubor f a jeho přípona a přidá se k tomu / nahradí se jím původní
	 * název - v proměnné newName je získaný nový název pro indexování od uživatele.
	 * 
	 * Vysledný název je: parent / newName + indexInlist . extension
	 * 
	 * parent - adresář, ve kterém se nachází soubor f. newName - název získaný z
	 * aplikace od uživatele - pro nový název souborů indexInlist - vygenerovaný
	 * index (čísla nebo písmena), občas budou před nebo za newName extension -
	 * přípona souboru f.
	 * 
	 * @param f
	 *            - soubor, který se má přejmenovat.
	 * 
	 * @param newName
	 *            - nový název souboru, ke kterému se přidá indexování.
	 * 
	 * @param indexInList
	 *            - Vygenerovaný index (čísla nebo písmena), který se přidá k
	 *            newName - před nabo za něj (dle zvolené možnosti)
	 * 
	 * @return výše popsaný "nový soubor", na který se přejmenuje f.
	 */
	private File getNewFileName(final File f, final String newName, final String indexInList) {
		/*
		 * Proměnná pro nový název souboru. Sem se uloží nový název souboru s indexy i
		 * samotným názvem.
		 */
		String newFileName = null;
		
		/*
		 * Zjistím, zda semá indexovat dle písmen nebo číslic:
		 */
		if (chcbIndexByNumber.isSelected()) {
			/*
			 * Zde je označeno, že se mají využit číslicové indexy, tak ještě otestuji, zda
			 * se mají indexy přidávat před nebo za název a dle toho vytvořím nový název pro
			 * soubor
			 */
			if (chcbIndexInFrontOfText.isSelected())
				// Zde je označeno, že se má přidat index před text:
				newFileName = indexInList + newName;

			else if (chcbIndexBehingOfText.isSelected()) // Není třeba testovat
				// Zde je označeno, že se má přidat index za zadaný text:
				newFileName = newName + indexInList;
		}
		
		
		else if (chcbIndexByLetters.isSelected()) { // Není třeba testovat, pouze pro potenciální rozšíření
			/*
			 * Zde se má indexovat dle číslic, tak zjistím, zda se má přidat index před nebo
			 * za nový název a mají se využít velká nebo malá písmena:
			 */
			/*
			 * Proměnná, do které uložím index pro název souboru s velkými nebo malými
			 * písmeny - dle toho, co je označeno
			 */
			final String fileNameIndex;

			if (chcbUseBigLetters.isSelected())
				fileNameIndex = indexInList.toUpperCase();

			else if (chcbUseSmallLetters.isSelected()) // není třeba testovat
				fileNameIndex = indexInList.toLowerCase();

			// Nemělo by nastat:
			else
				fileNameIndex = indexInList;
			
			
			/*
			 * Nyní zjistím, zda se má index přidat před nebo za zadaný text coby název
			 * souboru:
			 */
			if (chcbLettersInFrontOfText.isSelected())
				newFileName = fileNameIndex + newName;

			else if (chcbLettersBehindOfText.isSelected()) // Není třeba testovat
				newFileName = newName + fileNameIndex;
		}
		
		
		/*
		 * Zde již mám v proměnné newFileName připravený název soubory, tak stačí pouze
		 * získat cestu k adresáři, kde se soubor nachází a získat příponu, pak jen
		 * nahradím název souboru a je to hotovo:
		 */
		final String parent = f.getParent();
		final String extension = FilenameUtils.getExtension(f.getName());
		
		/*
		 * Proměnná pro nový soubor, na který se přejmenuje ten původní:
		 */
		final File newFile;

		/*
		 * Nyní pouze otestuji, zda je soubor skrytý nebo ne. Pokud ano, tak ho sice
		 * přejmenuji, ale pořád ho zachovám jako skrytý soubor, takže před název vložím
		 * desetinnou tečku:
		 */
		if (f.isHidden())
			newFile = new File(parent + "/." + newFileName + "." + extension);
		else
			newFile = new File(parent + File.separator + newFileName + "." + extension);

		return newFile;
	}
	
	
	
	
	
	
	

	
	
	
	
	/**
	 * Metoda, která zjistí, zda nějaký file v listu list obsahuje cestu k nějakému
	 * souboru na cestě path, resp. zda nějaký file v listu list obsahuje cestu
	 * path. Pokud ano, vrátí se true, jinak false.
	 * 
	 * @param list
	 *            - List typu File, který obsahuje cesty k souborům
	 * @param path
	 *            - cesta k nějakému souboru, o kterém se má zjistit, zda se ty samá
	 *            cesty vyskytuje v listu list v nějakém file.
	 * 
	 * @return true, pokud se cesta path vyskytuje v nějakém file v listu list.
	 *         Jinak false.
	 */
	private static boolean containstListFileWithPath(final List<File> list, final String path) {
		return list.stream().anyMatch(f -> f.getAbsolutePath().equals(path));
	}
	
	
	
	
	
	
	
	
	
	
	
	/**
	 * Metoda, která zjistí počet souborů v adresáři file. Metoda bude / nebude
	 * počítat skryté soubory dle proměnné countHiddenFiles. A metody počítá pouze
	 * soubory, ne adresáře.
	 * 
	 * Info: Vím, že stačí zjistit pouze dolké pole souborů, které se získá metodou
	 * listFiles (například), ale nechci do toho počítat i adresáře a skryté soubory
	 * jen dle proměnné countHiddenFiles.
	 * 
	 * @param file
	 *            - Adresář, ve kterém se má zjistit počet souborů
	 * 
	 * @param countHiddenFiles
	 *            - logická proměnná o tom, zda semají počítat skryté soubory nebo
	 *            ne. True - budou započítány skryté soubory, false nebudou
	 *            započítány skryté soubory.
	 */
	private static void findCountOfFiles(final File file, final boolean countHiddenFiles) {
		if (!countHiddenFiles) {
			// isDirectory není třeba, jen pro jistotu, kdybych někde něco přehlédl a file
			// nebyl adresář.
			if (file.isDirectory() && !file.isHidden()) {
				// Zde stačí zjistit, zda se nejedná o adresář (je to soubor) a není skrytý:
				count += Arrays.stream(Objects.requireNonNull(file.listFiles())).filter(f -> !f.isDirectory() && !f.isHidden())
						.count();
			}
		}

		// Zde stačí else, jen pro jistotu, zda jsem někde něco nepřehlédl a file nebyl
		// adresář:
		else if (file.isDirectory())
			// Zde stačí pouze zjistit, zda se jedná o soubor, resp. nejedná se o adresář:
			count += Arrays.stream(Objects.requireNonNull(file.listFiles())).filter(f -> !f.isDirectory()).count();
	}
	
	
	
	
	
	
	
	
	
	
	
	/**
	 * Metoda, která si convertuje parametr components do listu, který cyklem projde
	 * a všechny komponenty znepřístupní / zpřístupní - dle proměnné enabled.
	 * 
	 * "Metoda, která zne / přístupní všechny komponenty - components".
	 * 
	 * @param components
	 *            - Komponenty, které se mají zne / přístupnit.
	 * @param enabled
	 *            - logická proměnná o tom, zda se mají komponenty components
	 *            zpřístupnit nebo ne, true - zpřístupnit, false - znepřístupnit.
	 */
	public static void enabledAllComponents(final Component[] components, final boolean enabled) {
		/*
		 * Projdu pole a znepřístupním všechny komponenty, v případě, že se jedná o
		 * panel, tak pomocí rekurze zne / přístupním i jeho komponenty.
		 */
		Arrays.asList(components).forEach(c -> {
			if (c instanceof JPanel)
				enabledAllComponents(((JPanel) c).getComponents(), enabled);

			else if (c instanceof JScrollPane) {
				// Pokud se jedná o komponentu JScrollpane, tak zne / přístupním, co je
				// "uvnitř":
				final JViewport viewport = ((JScrollPane) c).getViewport();
				viewport.getView().setEnabled(enabled);
			}

			c.setEnabled(enabled);
		});
	}
	
	
	
	
	
	
	
	
	
	/**
	 * Metoda, která vloží data do pole pro označení přípon souborů, které se mají
	 * přejmenovat.
	 * 
	 * Takže se pak přejmenují pouze soubory, které budou mít označenou příponu v
	 * tomto listu.
	 * 
	 * @param list
	 *            - list s příponami souborů.
	 */
	public final void setDataToListForFileExtension(final List<ListItem> list) {
		listFileExtensions.setListData(list.toArray(new ListItem[] {}));
	}
	
	
	
	/**
	 * Metoda, která nastaví data do listu, který zobrazuje přípony souborů.
	 * 
	 * Soubory, které mají příponu, který je označena v tomto listu nebude přejmenována - souibor bude vynechán.
	 * 
	 * @param list - list s příponami souborů, které se nachází ve zvoleném adresáři.
	 */
	public final void setDataToListForSkipFileWithExtension(final List<ListItem> list) {
		listSkipFileWithExtension.setListData(list.toArray(new ListItem[] {}));
	}
	
	
	
	/**
	 * Metoda, která nastaví data do listu, ve kterém jsou zobrazeny veškeré soubory
	 * ve zvoleném zdrjovém adresáři (včetně jeho podadresářů) Označené soubory
	 * nebudou přejmenovány.
	 * 
	 * @param list
	 *            - list se všemi soubory, které se nachází ve zvoleném zdrojovém
	 *            adresáři (včetně podadresářů).
	 */
	public final void setDataToListSkipFilesWithName(final List<ListItem> list) {
		pnlSkipFilesWithName.setDataToListSkipFilesWithName(list);
	}
}