package kruncik.jan.renamator.panels.renamePanel;

/**
 * Tento výčet obsahuje hodnoty pro určení typu indexování, například indexování
 * dle čísel s určitým počtem na začátku, indexování dle čísel s nějakým
 * symbolem mezi čísly apod. Dále podobně pro písmena.
 * 
 * @see Enum
 * 
 * @author Jan Krunčík
 * @version 1.0
 */

public enum IndexingEnum {

	/**
	 * Indexování dle čísel v následující syntaxi, jako parametr je uveden počet nul
	 * před samotným indexováním.
	 * 
	 * Syntaxe: 1, 2, 3, ...
	 */
	NUMBERS_WITHOUT_ZEROS(0),

	/**
	 * Indexování dle čísel v následující syntaxi, jako parametr je uveden počet nul
	 * před samotným indexováním.
	 * 
	 * Syntaxe: 01, 02, 03, ...
	 */
	NUMBERS_1_ZERO(1),

	/**
	 * Indexování dle čísel v následující syntaxi, jako parametr je uveden počet nul
	 * před samotným indexováním.
	 * 
	 * Syntaxe: 001, 002, 003, ...
	 */
	NUMBERS_2_ZEROS(2),

	/**
	 * Indexování dle čísel v následující syntaxi, jako parametr je uveden počet nul
	 * před samotným indexováním.
	 * 
	 * Syntaxe: 0001, 0002, 0003, ...
	 */
	NUMBERS_3_ZEROS(3),

	/**
	 * Indexování dle čísel v následující syntaxi, jako parametr je uveden počet nul
	 * před samotným indexováním.
	 * 
	 * Syntaxe: 00001, 00002, 00003, ...
	 */
	NUMBERS_4_ZEROS(4),

	/**
	 * Indexování dle čísel v následující syntaxi, jako parametr je uveden počet nul
	 * před samotným indexováním.
	 * 
	 * Syntaxe: 000001, 000002, 000003, ...
	 */
	NUMBERS_5_ZEROS(5),

	/**
	 * Indexování dle čísel v následující syntaxi, jako parametr je uveden počet nul
	 * před samotným indexováním.
	 * 
	 * Syntaxe: 0000001, 0000002, 0000003, ...
	 */
	NUMBERS_6_ZEROS(6),

	/**
	 * Indexování dle čísel v následující syntaxi, jako parametr je uveden počet nul
	 * před samotným indexováním.
	 * 
	 * Syntaxe: 00000001, 00000002, 00000003, ...
	 */
	NUMBERS_7_ZEROS(7),

	/**
	 * Indexování dle čísel v následující syntaxi, jako parametr je uveden počet nul
	 * před samotným indexováním.
	 * 
	 * Syntaxe: 000000001, 000000002, 000000003, ...
	 */
	NUMBERS_8_ZEROS(8),

	/**
	 * Indexování dle čísel v následující syntaxi, jako parametr je uveden počet nul
	 * před samotným indexováním.
	 * 
	 * Syntaxe: 0000000001, 0000000002, 0000000003, ...
	 */
	NUMBERS_9_ZEROS(9),

	/**
	 * Indexování dle čísel v následující syntaxi, jako parametr je uveden počet nul
	 * před samotným indexováním.
	 * 
	 * Syntaxe: 00000000001, 00000000002, 00000000003, ...
	 */
	NUMBERS_10_ZEROS(10),
	
	
	
	
	
	
	
	/**
	 * Indexování dle čísel v následující syntaxi, jako parametr je uveden počet nul
	 * před samotným indexováním.
	 * 
	 * Syntaxe: 1-1, 1-2
	 */
	NUMBERS_DASH_WITHOUT_ZEROS(0),

	/**
	 * Indexování dle čísel v následující syntaxi, jako parametr je uveden počet nul
	 * před samotným indexováním.
	 * 
	 * Syntaxe: 01-01, 01-02
	 */
	NUMBERS_DASH_1_ZERO(1),
	
	/**
	 * Indexování dle čísel v následující syntaxi, jako parametr je uveden počet nul
	 * před samotným indexováním.
	 * 
	 * Syntaxe: 001-001, 001-002
	 */
	NUMBERS_DASH_2_ZEROS(2),
	
	/**
	 * Indexování dle čísel v následující syntaxi, jako parametr je uveden počet nul
	 * před samotným indexováním.
	 * 
	 * Syntaxe: 0001-0001, 0001-0002
	 */
	NUMBERS_DASH_3_ZEROS(3),
	
	
	
	
	
	
	/**
	 * Indexování dle čísel v následující syntaxi, jako parametr je uveden počet nul
	 * před samotným indexováním.
	 * 
	 * Syntaxe: 1_1, 1_2
	 */
	NUMBERS_UNDERSCORE_WITHOUT_ZEROS(0),
	
	/**
	 * Indexování dle čísel v následující syntaxi, jako parametr je uveden počet nul
	 * před samotným indexováním.
	 * 
	 * Syntaxe: 01_01, 01_02
	 */
	NUMBERS_UNDERSCORE_1_ZERO(1),
	
	/**
	 * Indexování dle čísel v následující syntaxi, jako parametr je uveden počet nul
	 * před samotným indexováním.
	 * 
	 * Syntaxe: 001_001, 001_002
	 */
	NUMBERS_UNDERSCORE_2_ZEROS(2),
	
	/**
	 * Indexování dle čísel v následující syntaxi, jako parametr je uveden počet nul
	 * před samotným indexováním.
	 * 
	 * Syntaxe: 0001_0001, 0001_0002
	 */
	NUMBERS_UNDERSCORE_3_ZEROS(3),
	
	
	
	
	
	
	/**
	 * Indexování dle písmen v následující syntaxi.
	 * 
	 * Syntaxe: A, B
	 */
	LETTERS(1),
	
	/**
	 * Indexování dle písmen v následující syntaxi.
	 * 
	 * Syntaxe: AA, AB
	 */
	LETTERS_PAIR(2),
	
	/**
	 * Indexování dle písmen v následující syntaxi.
	 * 
	 * Syntaxe: AAA, AAB
	 */
	LETTERS_TRINITY(3),
	
	/**
	 * Indexování dle písmen v následující syntaxi.
	 * 
	 * Syntaxe: A-A, A-B
	 */
	LETTERS_DASH(1),
	
	/**
	 * Indexování dle písmen v následující syntaxi.
	 * 
	 * Syntaxe: AA-AA, AA-AB
	 */
	LETTERS_PAIRS_DASH(2),
	
	/**
	 * Indexování dle písmen v následující syntaxi.
	 * 
	 * Syntaxe: AAA-AAA, AAA-AAB
	 */
	LETTERS_TRINITY_DASH(3),
	
	/**
	 * Indexování dle písmen v následující syntaxi.
	 * 
	 * Syntaxe: A_A, A_B
	 */
	LETTERS_UNDERSCORE(1),
	
	/**
	 * Indexování dle písmen v následující syntaxi.
	 * 
	 * Syntaxe: AA_AA, AA_AB
	 */
	LETTERS_PAIRS_UNDERSCORE(2),
	
	/**
	 * Indexování dle písmen v následující syntaxi.
	 * 
	 * Syntaxe: AAA_AAA, AAA_AAB
	 */
	LETTERS_TRINITY_UNDERSCORE(3);
	
	
	
	
	
	
	
	/**
	 * Proměnná, která obsahuje počet nul před číslem. Tato proměnná je využita v
	 * případě, že se jedná o indexování v syntaxi: 001, 002 apod. A tato proměnná
	 * konkrétně obsahuje počet uvedených nul před samotným indexováním.
	 * 
	 * Dále tato proměnná je využita v případě, že se jedná o syntaxi: A nebo AA,
	 * nebo AAA apod. Je využita pro určení počtu písmen, se kterým se má začinat.
	 * Stejně tak je to mu i pro syntaxe A-A a A_A
	 */
	private final int count;


	/**
	 * Konstruktor této třídy, který slouží pro naplnění proměnné count.
	 * 
	 * @param count
	 *            - int - ová proměnná, která definuje počet nul před samotným
	 *            indexováním neo počet začínajících písmen - A nebo AA apod..
	 */
	IndexingEnum(final int count) {
		this.count = count;
	}
	
	
	/**
	 * Getr na proměnnou count,která obsahuje počet nul před samotným indexováním v
	 * případě zvoleného indexování v syntaxi: 001, 002, ...
	 * 
	 * Nebo obsahuje počet písmen, se kterými se má začinat, například A, nebo AA
	 * nebo AAA apod. - Tento počet začínajících písmen.
	 * 
	 * @return výše popsanou hodnotu - počet nul přede indexováním nebo počet písmen
	 *         pro začátek.
	 */
	public int getCount() {
		return count;
	}
}
