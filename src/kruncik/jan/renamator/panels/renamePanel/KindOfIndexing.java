package kruncik.jan.renamator.panels.renamePanel;

/**
 * Tato třída slouží pouze pro definování způsobu indexování souborů, například:
 * 01, 001, 01_01, 01-01, ...
 * 
 * Podobně je to pro písmena - A, AA, AA_AA, ...
 * 
 * @author Jan Krunčík
 * @version 1.0
 */

public class KindOfIndexing {

	/**
	 * Proměnná, která drží typ indexování, například 01, 001, 01_01, 01-01, ...
	 * Podobně pro písmena.
	 */
	private final String kind;

	
	/**
	 * Proměnná výčtového typu, která definuje jaký typ indexování se má využít.
	 */
	private final IndexingEnum indexingEnum;
	
	
	/**
	 * Konstruktor této třídy.
	 * 
	 * @param kind
	 *            - Způsob indexování.
	 */
	KindOfIndexing(final String kind, final IndexingEnum indexingEnum) {
		super();

		this.kind = kind;
		this.indexingEnum = indexingEnum;		
	}

	public String getKind() {
		return kind;
	}
	
	IndexingEnum getIndexingEnum() {
		return indexingEnum;
	}

	@Override
	public String toString() {
		return kind;
	}
}
