package kruncik.jan.renamator.panels.renamePanel;

import java.util.ArrayList;
import java.util.List;

/**
 * Třída obsahuje metody pro získání indexů pro názvy souborů.
 * 
 * @author Jan Krunčík
 * @version 1.0
 */

class Alg {


	/**
	 * Jednorozměrné pole, které obsahuje písmena abecedy bez diakritiky. Pole
	 * obsahuje pouze ta písmena abecedy, terá lze využít k indexování.
	 */
	private static final String[] ALPHABET = { "A", "B", "C", "D", "E", "F", "G", "H", "I", "J", "K", "L", "M", "N",
			"O", "P", "Q", "R", "S", "T", "U", "V", "W", "X", "Y", "Z" };
	
	
	
	
	
	/**
	 * Metoda, která slouží pro vytvoření indexů dle zadaných parametrů. Paraetry
	 * pro typ indexování - čísla, písmena apod. a počet souborů, resp. kolik indexů
	 * se má vytvořit.
	 * 
	 * @param indexingEnum
	 *            - Výčtová hodnota, která obsahuje zvolený typ indexování.
	 * 
	 * @param countOfFiles
	 *            - Jedná se o počet souborů ve zvoleném adresáři. tento počet je
	 *            důležitý, protože značí, kolik indexů se má vytvořit, abych
	 *            nevytvářel indexy třeba do Long.MaxValue, tak si spočítám celkový
	 *            počet souborů ve zvoleném adresáři a pak už jen testuji, zda se má
	 *            pro každý adresář počítat znovu nebo ne. Ale to je jiná část, zde
	 *            potřebuji pouze tu maximální hondotou pro počet indexů, které se
	 *            mají vytvořit.
	 * 
	 * @return - list typu String, který obsahuje vytvořené indexy převedené na
	 *         datový typ String.
	 */
	static List<String> getIndexes(final IndexingEnum indexingEnum, final long countOfFiles) {
		
		
		// Čísla v syntaxi 01, 02 apod. s nulami nebo nebo bez nul
		if (indexingEnum == IndexingEnum.NUMBERS_WITHOUT_ZEROS || indexingEnum == IndexingEnum.NUMBERS_1_ZERO
				|| indexingEnum == IndexingEnum.NUMBERS_2_ZEROS || indexingEnum == IndexingEnum.NUMBERS_3_ZEROS
				|| indexingEnum == IndexingEnum.NUMBERS_4_ZEROS || indexingEnum == IndexingEnum.NUMBERS_5_ZEROS
				|| indexingEnum == IndexingEnum.NUMBERS_6_ZEROS || indexingEnum == IndexingEnum.NUMBERS_7_ZEROS
				|| indexingEnum == IndexingEnum.NUMBERS_8_ZEROS || indexingEnum == IndexingEnum.NUMBERS_9_ZEROS
				|| indexingEnum == IndexingEnum.NUMBERS_10_ZEROS) {
			return getNumbersWithZeros(indexingEnum.getCount(), countOfFiles);
		}

		
		// Čísla s pomlčkou a případně s nulami v sytnaxi 1-1, 01-02, ...
		else if (indexingEnum == IndexingEnum.NUMBERS_DASH_WITHOUT_ZEROS
				|| indexingEnum == IndexingEnum.NUMBERS_DASH_1_ZERO || indexingEnum == IndexingEnum.NUMBERS_DASH_2_ZEROS
				|| indexingEnum == IndexingEnum.NUMBERS_DASH_3_ZEROS) {
			return getNumbersWithSymbol(indexingEnum.getCount(), countOfFiles, "-");
		}

		
		// Čísla s podtržítkem s syntaxi: 1_1, 01_02, ...
		else if (indexingEnum == IndexingEnum.NUMBERS_UNDERSCORE_WITHOUT_ZEROS
				|| indexingEnum == IndexingEnum.NUMBERS_UNDERSCORE_1_ZERO
				|| indexingEnum == IndexingEnum.NUMBERS_UNDERSCORE_2_ZEROS
				|| indexingEnum == IndexingEnum.NUMBERS_UNDERSCORE_3_ZEROS) {
			return getNumbersWithSymbol(indexingEnum.getCount(), countOfFiles, "_");
		}

		
		
		
			// Písmena:

		else if (indexingEnum == IndexingEnum.LETTERS || indexingEnum == IndexingEnum.LETTERS_PAIR
				|| indexingEnum == IndexingEnum.LETTERS_TRINITY)
			return getLettersWithoutSymbol(indexingEnum.getCount(), countOfFiles);

		
		
		else if (indexingEnum == IndexingEnum.LETTERS_DASH || indexingEnum == IndexingEnum.LETTERS_PAIRS_DASH
				|| indexingEnum == IndexingEnum.LETTERS_TRINITY_DASH)
			return getLettersWithSymbol(indexingEnum.getCount(), countOfFiles, "-");

		
		
		else if (indexingEnum == IndexingEnum.LETTERS_UNDERSCORE
				|| indexingEnum == IndexingEnum.LETTERS_PAIRS_UNDERSCORE
				|| indexingEnum == IndexingEnum.LETTERS_TRINITY_UNDERSCORE)
			return getLettersWithSymbol(indexingEnum.getCount(), countOfFiles, "_");
		
		
		
		// Nemělo by nastat:
		return new ArrayList<>();
	}
	
	
	
	
	
	
	
	/**
	 * Metoda, která vygeneruje list typu String, který bude obsahovat veškeré
	 * indexy. Vygeneruje pouze takový počet indexů, jako je počet souborů
	 * (přibližné).
	 * 
	 * Indexy budou v syntaxi: 1, 2 nebo 01, 02, ... Zálšží na proměné zeros coby
	 * počet nul před indexem (číslem).
	 * 
	 * @param zeros
	 *            - počet nul před samotným indexem.
	 * 
	 * @param countOfFiles
	 *            - počet souborů, resp. počet indexů, který semá vygenerovat.
	 * 
	 * @return list typu String, který obsahuje výše popsané indexy.
	 */
	private static List<String> getNumbersWithZeros(final int zeros, final long countOfFiles) {
		/*
		 * List, do kterého vložím vygenerované indexy.
		 */
		final List<String> indexes = new ArrayList<>();

		
		for (long i = 0; i < countOfFiles; i++)
			// Do kolekce vložím počet null a k tomu aktuálně iterovaný index
			indexes.add(getZerosInText(zeros, i) + String.valueOf(i));

		
		// Vrátím kolekce s vygenrovanými indexy:
		return indexes;
	}
	
	
	
	
	
	
	/**
	 * Metoda, která vrátí počet nul v textu.
	 * 
	 * Metoda si vypočítá počet nul, který má být v textu a od toho odečte počet
	 * znaků aktuálního čísla,
	 * 
	 * například když má být syntaxe 0001, 0002, ..
	 * Pak jsou to 3 nuly a index 1, 2, ... 
	 * pak 3 - 1 + 1 = 3 nuly
	 * nebo 0021 (21 je index): 3 - 2 + 1 = 2 nuly
	 * 
	 * @param countOfZeros
	 *            - počet nul, který má být v texte před indexem (samotným číslem)
	 * 
	 * @param index
	 *            - Index je aktuální číslo, ke kterému se má přidat nejaký počet
	 *            nul, tak vypočítám jaký.
	 * 
	 * @return text, který obsahuje určitý počet nul a k tomu index (popsáno výše)
	 */
	private static String getZerosInText(final int countOfZeros, final long index) {
		final int tempForZeros = countOfZeros - String.valueOf(index).length() + 1;
		
		String text = "";
		
		for (int i = 0; i < tempForZeros; i++)
			text += "0";	
		
		return text;
	}
	
	
	
	
	
	
	
	
	
	
	
	
	
	/**
	 * Metoda, která vygeneruje určitý počet indexů v definované syntaxi, všechny
	 * vygenerované indexy vloží do listu typu String, který bude vrácen.
	 * 
	 * @param zeros
	 *            - počet nul před samotným indexem.
	 * 
	 * @param countOfFiles
	 *            - počet souborů, resp. počet indexů, který semá vygenerovat.
	 * 
	 * @param symbol
	 *            - symbol, který se vloží mezi čísla - x symbol y
	 * 
	 * @return list typu String, který obsahuje vygenerované indexy
	 */
	private static List<String> getNumbersWithSymbol(final int zeros, final long countOfFiles,
			final String symbol) {
		/*
		 * List, do kterého vložím vytvořené indexy.
		 */
		final List<String> indexes = new ArrayList<>();
		
		/*
		 * index_1 - před symbolem
		 * index_2- za symbolem
		 */
		long index_1 = 0, index_2 = 0;
		
		
		
		/*
		 * Začátek dle počtu nul (dle počtu nul se nastaví například 10 nebo 100 apod. -
		 * kdy se má indkremetovat index_1 a index_2 má začít od nuly)
		 * 
		 * Toto je třeba provést jako první, je zde třeba nastavit (dle počtu nul), kdy
		 * se má inkrementovat index_1, a znovu iterovat index_2 - od nuly.
		 * 
		 * Pozná se to dle počtu nul, pokud se nemá nastavit žádná nula, pak se jedná o
		 * inkrmentaci index_1 při každém dosažení hodnoty 10 v index_2. Pokud se jedná
		 * o větší početnul, pak při 100 nebo 1000, ... v index_2.
		 */
		String var = "10";

		for (int i = 0; i < zeros; i++)
			var += "0";
		
		/*
		 * Tato hodnota se vytvoří parsováním hodnoty v proměnné var a tato hodnota
		 * slouží pro porovnání, zda se má inkrementovat hodnota před zvoleným symbolem
		 * pro odsazení čísel, konkrétně index_1.
		 */
		long varForCompare = Long.parseLong(var);	
		
		
		
		for (long i = 0; i < countOfFiles; i++) {
			final String text_1 = getZerosInText(zeros, index_1) + index_1;

			final String text_2 = getZerosInText(zeros, index_2) + index_2;

			indexes.add(text_1 + symbol + text_2);

			index_2++;
			
			/*
			 * Tato podmínka je zde potřeba pro určení, kdy semá vynulovat index_2 a
			 * inkrementovat index_1.
			 */
			if (index_2 == varForCompare) {
				index_2 = 0;
				index_1++;

				/*
				 * Tato podmínka znovu naplní proměnou varForCompare příslušnou hodnotou. Je to
				 * potřeba proto, že když se nastaví třeba 2 nuly, pak se jedná o dosažení 100 a
				 * pak se inkrementuje index_1 apod. Ale když už bude hodně souborů, pak chci,
				 * aby v index_1 a 2 byl stejný počet čísel, vždy v obou byl 100 nebo 1000 apod.
				 * Proto musím inkrementovat dle dosažení určité dekadické hodnoty a vždy tu
				 * proměnnou varForCompare zvýšit.
				 * 
				 * Například při nastevení indexování bez nul by zde byla jako první hodnota 10
				 * v varForCompare, až dosáhnu hodnoty 10, pak musím i zvýšit hodnotu
				 * varForCompare, aby se rovnala 100, pak bude hodnota v index_1 inkrementována
				 * při dosažení 1000 v index_2 a tak dále, aby byly hodnoty v index_1 a 2 vždy
				 * stejně "velké" - v obou 100 nebo 1000 apod.
				 */
//				if (index_1 >= varForCompare) {
				if (index_1 == varForCompare) {
					String tempVar = "1";

					for (int q = 0; q < String.valueOf(index_1).length(); q++)
						tempVar += "0";

					varForCompare = Long.parseLong(tempVar);
				}
			}
		}
		
		return indexes;
	}
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	/**
	 * Metoda, která vygeneruje indexy od počátečního počtu písmen (například A nebo
	 * AA nebo AAA apod.) až po celkový počet souborů, algoritmus si automaticky
	 * vygeneruje potřebný počet sloupců - písmen vedle sebe tak, aby do dalo do
	 * celkového počtu souborů, například se začne jedním písmenem - A a skončí to
	 * čtyřmi písmeny ZZBX apod.
	 * 
	 * @param countOfLetters
	 *            - počet znaků, resp. počet A - ček, s kolika se má začít
	 *            indexovat, například se má začít s jedním A, pak se zadá počet 1.
	 *            Pokud se má začít s dvěma A - čky, pak se zadá počet 2 (AA) apod.
	 *            TATO HODNOTA MUSÍ BÝT VĚTŠÍ ROVNO 1 (countOfLetters >= 1) !!!
	 * 
	 * @param countOfFiles
	 *            - počet souborů, resp. počet indexů, který se má vygenerovat.
	 * 
	 * @return list, který obsahuje veškeré indexy v zadaném počtu a s počtátečním
	 *         písmenem / písmeny, vrátí se například list s položkami v syntaxi A,
	 *         B, C, ... AAB, AAC - až po celkový počet souborů, který se má
	 *         vygenerovat.
	 */
	private static List<String> getLettersWithoutSymbol(final int countOfLetters, final long countOfFiles) {
		/*
		 * Kolekce, do které se budou vkládat vygenerované indexy, tuto kolekci pak
		 * metoda vrátí, až se dosáhne výsledného počtu pro vygenerování indexů pro
		 * přejmenování, resp. indexování souborů.
		 */
		final List<String> finalIndexes = new ArrayList<>();

		
		/*
		 * Kolekce, do které se budou vkládat indexy pro "vytváření indexů".
		 * 
		 * Kolekce bude obsahovat vždy indexy, odkud se mají brát indexy, dle kterých se
		 * berou písmena z abecedy.
		 */
		final List<Integer> indexes = new ArrayList<>();

		
		
		/*
		 * Na začátku do indexů přidám takový počet indexů, kolik chce uživatel písmen.
		 */
		for (int i = 0; i < countOfLetters; i++)
			indexes.add(0);

		
		
		/*
		 * Cyklus pro vytvoření indexů:
		 */
		for (long i = 0; i < countOfFiles; i++) {
			/*
			 * Proměnná, do které se vloží písmena z abecedy na požadovaných indexích.
			 */
			String text = "";

			
			/*
			 * Stačí projít kolekci si indexy, která vždy obsahuje indexy, které se
			 * využívají a značí indexy písmen v abecedě, které se mají vzít.
			 */
			for (final Integer in : indexes)
				text += ALPHABET[in];

			
			
			/*
			 * Přidánm do výsledné klekce s indexy, které se využijí pro indexování souborů
			 * nově vygenerovaný index s písmeny:
			 */
			finalIndexes.add(text);

			
			/*
			 * Je třeba inkrementovat vždy poslední index v kolekci a další cyklus pak
			 * testuje, zda se ty předchozí indexy mají také inkrementovat nebo ne.
			 */
			int index = indexes.get(indexes.size() - 1);
			indexes.set(indexes.size() - 1, ++index);

			
			/*
			 * Cyklus, který prochází od konce kolekci s indexy a vždy testuje, zda je právě
			 * iterovaná hodnota rovna velikosti pole s písmeny abecedy, pokud ano, pak jej
			 * nastaví na nulu, protože se musí brát písmena od začátku a v tomto případě se
			 * dále musí inkrementovat index před právě inkrementovaným. Pokud je ten právě
			 * inkrementovaný již první prvek v kolekce, a cyklus nadále pokračuje pro
			 * generování indexů (je jich třeba více), pak stačí na začátek koelce přidat
			 * další - nový prvek, který bude značit další sloupec s písmeny a generování
			 * pokračuje.
			 */
			for (int q = indexes.size() - 1; q >= 0; q--) {
				if (indexes.get(q) >= ALPHABET.length) {
					indexes.set(q, 0);

					// Zjistím, zda je ještě nějaký "předchozí" index - zda se nejedná o první prvek
					// v listu:
					if (q > 0) {
						// Zde je ještě nějaký prvek v listu před tím právě iterovaným, tak jej také
						// musím inkrementovat,
						// protože jsem v právě iterovaném prvku dosáhl maximální hodnoty (rovny poctu
						// prvku v poli - pocetp ismen v abecede vyuzit pro indexování)
						int temp = indexes.get(q - 1);

						indexes.set(q - 1, ++temp);
					}

					/*
					 * Zde je právě iterovaný prvek jako první v listu, ale má hodnotu velikosti
					 * pole, takže "došli písmena", tak je potřeba přidat další písmeno jako další
					 * sloupec pro generování:
					 */
					else
						indexes.add(0, 0);
				}
			}
		}

		// Vrátím kolekci s vygenerovanými indexy pro přejmenoání souborů:
		return finalIndexes;
	}
	
	
	
	
	
	
	
	
	
	
	
	
	
	/**
	 * Metoda, která vygeneruje indexy od počátečního počtu písmen (například A-A
	 * nebo AA-AA nebo AAA-AAA apod.) až po celkový počet souborů, algoritmus si
	 * automaticky vygeneruje potřebný počet sloupců - písmen vedle sebe tak, aby do
	 * dalo do celkového počtu souborů, například se začne jedním písmenem A- A a
	 * skončí to čtyřmi písmeny ZZBX - AABB apod.
	 * 
	 * @param countOfLetters
	 *            - počet znaků, resp. počet A - ček, s kolika se má začít indexovat
	 *            (na obou stranách), například se má začít s jedním A, pak se zadá
	 *            počet 1. Pokud se má začít s dvěma A - čky, pak se zadá počet 2
	 *            (AA) apod. TATO HODNOTA MUSÍ BÝT VĚTŠÍ ROVNO 1 (countOfLetters >=
	 *            1) !!!
	 * 
	 * @param countOfFiles
	 *            - počet souborů, resp. počet indexů, který se má vygenerovat.
	 * 
	 * @param symbol
	 *            - jedná se o hodnotu, která se bude vkládat mezi indexy - mezi obě
	 *            strany indexů, například s pomlčkou: AAA - AAA, apod.
	 * 
	 * @return list, který obsahuje veškeré indexy v zadaném počtu a s počtátečním
	 *         písmenem / písmeny, vrátí se například list s položkami v syntaxi
	 *         A-A, A-B, A-C, ... AAB - AAC, CCC - AAC - až po celkový počet
	 *         souborů, který se má vygenerovat.
	 */
	private static List<String> getLettersWithSymbol(final int countOfLetters, final long countOfFiles,
			final String symbol) {
		/*
		 * List, do kterého budu vkládat veškeré vygenerované indexy a který pak vrátím,
		 * po vygenerování.
		 */
		final List<String> indexes = new ArrayList<>();
		
		
		/*
		 * Kolekce, do které se budou vkládat indexy pro "vytváření indexů".
		 * 
		 * Kolekce bude obsahovat vždy indexy, odkud se mají brát indexy, dle kterých se
		 * berou písmena z abecedy.
		 * 
		 * Kolekce obsahuje indexy pro písmena abecedy, které se nachází na levé straně
		 * - před oddělovacím symbolem
		 */
		final List<Integer> indexesLeftSide = new ArrayList<>();
		
		/*
		 * Tato kolekce slouží pro to samé, jako kolekce indexesLeftSide, akorát tato
		 * kolekce obsahuje indexy, které jsou pro pravou stranu.
		 * 
		 * Kolekce obsahuje indexy pro písmena abecedy, které jsou na pravé straně, tj.
		 * za oddělovacím symbolem.
		 */
		final List<Integer> indexesRightSide = new ArrayList<>();
		
		/*
		 * Na začátku do indexů přidám takový počet indexů, kolik chce uživatel písmen.
		 */
		for (int i = 0; i < countOfLetters; i++) {
			indexesLeftSide.add(0);
			indexesRightSide.add(0);
		}
			

		/*
		 * Cyklus pro vytvoření indexů:
		 */
		for (long i = 0; i < countOfFiles; i++) {
			/*
			 * Proměnná, do které se vloží písmena z abecedy na požadovaných indexích.
			 */
			String text = getTextFromAlphabet(indexesLeftSide);

			// Přidám oddělovací symbol:
			text += symbol;

			/*
			 * To samé potřebuji odelat pro druhou stranu, akorát s indexy pro pravou
			 * stranu:
			 */
			text += getTextFromAlphabet(indexesRightSide);

			// Přidám do kolekce nový index pro název souboru:
			indexes.add(text);
			
			
			
			
			
			/*
			 * Je třeba inkrementovat vždy poslední index v kolekci a další cyklus pak
			 * testuje, zda se ty předchozí indexy mají také inkrementovat nebo ne.
			 */
			int index = indexesRightSide.get(indexesRightSide.size() - 1);
			indexesRightSide.set(indexesRightSide.size() - 1, ++index);

			/*
			 * Cyklus, který prochází od konce kolekci s indexy pro levou stranu a vždy
			 * testuje, zda je právě iterovaná hodnota rovna velikosti pole s písmeny
			 * abecedy, pokud ano, pak jej nastaví na nulu, protože se musí brát písmena od
			 * začátku a v tomto případě se dále musí inkrementovat i ten před právě
			 * inkrementovaným. Pokud je ten právě inkrementovaný již první prvek v kolekce,
			 * a cyklus nadále pokračuje pro generování indexů (je jich třeba více), pak je
			 * třeba projít i druhou kolekci - s indexy pro levou stranu a provést to samé
			 * akorát s druhou kolekcí.
			 */
			for (int q = indexesRightSide.size() - 1; q >= 0; q--) {
				if (indexesRightSide.get(q) >= ALPHABET.length) {
					indexesRightSide.set(q, 0);

					if (q > 0) {
						// Nejedná se o první index, tak ten předchozí inkrementuji:
						int temp = indexesRightSide.get(q - 1);
						indexesRightSide.set(q - 1, ++temp);
					}

					else {
						/*
						 * Nejprve musím inkrementovat poslední index v levé kolekci, jinak by se nic
						 * nestalo, pořád by tam byly nuly:
						 */
						indexesLeftSide.set(indexesLeftSide.size() - 1,
								indexesLeftSide.get(indexesLeftSide.size() - 1) + 1);

						/*
						 * V tuto chvíli jsem na první pozici v iteraci cyklu, který prochází kolekci s
						 * pravou stranou, tak vím, že již "předchozí" prvek žádný není, tak musím
						 * projít tu druhou kolekci - s levou stranou a udělat v podstatě to samé, jako
						 * v cyku výše.
						 * 
						 * Prostě projdu kolekci s indexy pro levou stranu a pokud je nějaký index >=
						 * velikost pole s písmeny abecedy, pak jej nastavím na nulu a ten předchozí
						 * index inkrementuji, ale v případě, že dojdu na začátek té kolekce a mám jej
						 * také nastavit na nulu, tj. vyčerpaly se všechny písmenka, pak musím přidat
						 * indexy do obou kolekcí. Tj. do obou kolekcí přidám na začátek kolekce vždy
						 * index nula, pro další sloupec. Do obou kolekcí protože chci, by byly indexy -
						 * počet písmen, resp. sloupce s písmeny vždy stejné.
						 */
						for (int w = indexesLeftSide.size() - 1; w >= 0; w--) {
							if (indexesLeftSide.get(w) >= ALPHABET.length) {
								indexesLeftSide.set(w, 0);

								if (w > 0) {
									// Nejedná se o první index, tak ten předchozí inkrementuji:
									int temp = indexesLeftSide.get(w - 1);
									indexesLeftSide.set(w - 1, ++temp);
								}

								else {
									/*
									 * Zde jsem došel na první index (w = 0, pak musím přidat další sloupec - pro
									 * další písmeno, ale do obou kolekcí, protože chci, aby se vždy indexovaly se
									 * stejným počtem:
									 */
									indexesLeftSide.add(0, 0);
									indexesRightSide.add(0, 0);
								}
							}
						}
					}
				}
			}
		}
		
		// Vrátím list, který nyní obsahuje vygenerované indexy:
		return indexes;
	}
	
	
	
	
	
	
	
	
	
	/**
	 * Metoda, která vytvoří text, který obsahuje písmena z abecedy na indexech,
	 * které obsahuje kolekce v parametru metody (indexes).
	 * 
	 * Metoda projde list indexes a v každé iteraci si vezme index na iterované
	 * pozici v listu, pak se dle té hodnoty vezme písmeno z jednorozměrného pole s
	 * abecedou a přidá se k výslednému textu, který se vráti.
	 * 
	 * @param indexes
	 *            - list s indexy písmen, které se mají vzít z pole s abecedou.
	 * 
	 * @return výše popsaný text, který obsahuje písmena abecedy dle indexů z listu
	 *         indexes.
	 */
	private static String getTextFromAlphabet(final List<Integer> indexes) {
		/*
		 * Proměnná, do které vložím veškerá písmena dle indexů v listu indexes. Indexy
		 * jsou pouzice písmen v poli s písmeny abecedy.
		 */
		String text = "";

		/*
		 * Stačí projít kolekci s indexy, která vždy obsahuje indexy, které se využívají
		 * a značí indexy písmen v abecedě (v daném poli), které se mají vzít.
		 */

		for (final Integer i : indexes)
			text += ALPHABET[i];

		return text;
	}
}
