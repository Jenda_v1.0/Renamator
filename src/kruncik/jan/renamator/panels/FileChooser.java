package kruncik.jan.renamator.panels;

import java.io.File;

import javax.swing.JFileChooser;
import javax.swing.JOptionPane;
import javax.swing.filechooser.FileNameExtensionFilter;

/**
 * Tato třída slouží pouze coby dialog pro výběr souborů a adresářů.
 * 
 * @see JFileChooser
 * @see FileNameExtensionFilter
 * 
 * @author Jan Krunčík
 * @version 1.0
 */

public class FileChooser extends JFileChooser {

	
	private static final long serialVersionUID = 1L;




	public FileChooser() {
		super(System.getProperty("home.dir"));		
	}
	
	
	
	
	/**
	 * Metoda, která slouží pro označení adresáře.
	 * 
	 * @return cestu ke zvolenému adresáři nebo null.
	 */
	public final File getPathToDir(final String title) {
		setDialogTitle(title);

		resetChoosableFileFilters();

		setFileSelectionMode(JFileChooser.DIRECTORIES_ONLY);

		if (showOpenDialog(this) == JFileChooser.APPROVE_OPTION)
			return getSelectedFile();

		return null;
	}
	
	
	
	
	
	
	
	/**
	 * Metoda, která otevři dialogové okno pro výběr souboru typu .properties.
	 * 
	 * @param title
	 *            - titulek dialogu
	 * 
	 * @param txtWrongFileText
	 *            - Text pro chybovou hlášku ve zvoleném jazyce.
	 * 
	 * @param txtWrongFileTitle
	 *            - text pro titulek chybové hlášku ve zvoleném jazyce.
	 * 
	 * @return označený soubor typu .properties nebo null, pokud se nevybere soubor,
	 *         ale například zavře dialog apod.
	 */
	public final File getPathToProperties(final String title, final String txtWrongFileText,
			final String txtWrongFileTitle) {
		setDialogTitle(title);

		resetChoosableFileFilters();

		final FileNameExtensionFilter filter = new FileNameExtensionFilter("Properties", "properties");
		addChoosableFileFilter(filter);

		if (showOpenDialog(this) == JFileChooser.APPROVE_OPTION) {
			if (getSelectedFile().getAbsolutePath().endsWith(".properties"))
				return getSelectedFile();
			else
				JOptionPane.showMessageDialog(this, txtWrongFileText, txtWrongFileTitle, JOptionPane.ERROR_MESSAGE);
		}
			

		return null;
	}
	
	
	
	
	/**
	 * Metoda, která otevře dialogové okno pro výběr umístění a názvu souboru typu
	 * .properties.
	 * 
	 * Pokud se nevybere soubor s výše uvedenou příponou, pak bude přidána na konec
	 * názevu označeného souboru.
	 * 
	 * @param title
	 *            - Titulek dialogu.
	 * 
	 * @param wrongFileText
	 *            - proměnná, která obsahuje text pro chybovou hlášku, že nebyl
	 *            vybrán soubor s příponou .properties.
	 * 
	 * @param wrongFileTitle
	 *            - proměnná, která obsauje titulek pro chybovou hlášku, že nebyl
	 *            označen soubor s příponou .properties.
	 * 
	 * @return Cestu k označenému souboru nebo null, pokud uživatel dialog zavře
	 *         nebo nebude vybrán soubor typu .properties.
	 */
	public final File getPathToPropertiesToSave(final String title, final String wrongFileText,
			final String wrongFileTitle) {
		setDialogTitle(title);

		resetChoosableFileFilters();

		final FileNameExtensionFilter filter = new FileNameExtensionFilter("Properties", "properties");
		addChoosableFileFilter(filter);

		if (showSaveDialog(this) == JFileChooser.APPROVE_OPTION) {
			if (!getSelectedFile().getAbsolutePath().endsWith(".properties"))
				JOptionPane.showMessageDialog(this, wrongFileText, wrongFileTitle, JOptionPane.ERROR_MESSAGE);
			
			else return getSelectedFile();
		}

		return null;
	}
}
