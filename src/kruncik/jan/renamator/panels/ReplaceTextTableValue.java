package kruncik.jan.renamator.panels;

/**
 * Tato komponenta slouží pouze pro "rozpoznání a lepší orientaci" úplně není
 * potřeba.
 * 
 * Tato třída reprezentuje objekt, který se vkládá do tabulky pro nahrazení
 * textů v názvech souborů a adresářů.
 * 
 * @author Jan Krunčík
 * @version 1.0
 */

public class ReplaceTextTableValue extends AbstractTableValue {

	
	/**
	 * Konstruktor této třídy.
	 * 
	 * @param originalText
	 *            - původní text v názvu souboru nebo adresáře, který se má nahradit
	 *            za newText
	 * @param newText
	 *            - Nový text, který má nahradit originalText v názvu souboru nebo
	 *            adresáře.
	 */
	public ReplaceTextTableValue(final String originalText, final String newText) {
		super(originalText, newText);
	}
}
