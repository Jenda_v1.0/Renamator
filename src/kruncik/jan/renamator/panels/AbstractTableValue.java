package kruncik.jan.renamator.panels;

/**
 * Tato třída slouží pouze jako předek pro hodnoty, které se vkládají do tabulek
 * - jak pro přípony souborů, které se mají nahradit / změnit, tak pro texty v
 * názvech souborů, které se mají nahradit.
 * 
 * Třídy, které z této dědí mají stejné hodnoty, takže by mohl být jeden
 * neutrální, ale kdybych se někdy rozhodl něco doplnit jen k jednomu, tak ať si
 * nepřidělávám práci.
 * 
 * @author Jan Krunčík
 * @version 1.0
 */

public abstract class AbstractTableValue {

	/**
	 * Do této proměnné se vkládají původní hodnoty, buď to jsou přípony, které
	 * existují a mají se změnit na novou příponu (newValue) nebo jsou to texty v
	 * názvech souborů, které se mají zaměnit za nové texty - změnit název souboru v
	 * newValue
	 */
	private String originalValue;
	
	/**
	 * Proměnná, která obsahuje "novou" hodnotu, resp. hodnotu, která má nahradit tu
	 * původní (oridinalValue). Buď to bude nová přípona nebo text v názvu souboru,
	 * který má nahradit ten původní.
	 */
	private String newValue;
	
	
	/**
	 * Konstruktor třídy, je vždy potřeba naplnit tento konstruktor - daty.
	 * 
	 * @param originalValue
	 *            - původní hodnota (přípona nebo text v názvu souboru / souborů)
	 * @param newValue
	 *            - nová hodnota (nová přípona nebo nový text do názvu souboru /
	 *            souborů)
	 */
	AbstractTableValue(final String originalValue, final String newValue) {
		super();
		
		this.originalValue = originalValue;
		this.newValue = newValue;
	}
	
	
	
	public String getOriginalValue() {
		return originalValue;
	}

	void setOriginalValue(String originalValue) {
		this.originalValue = originalValue;
	}

	public String getNewValue() {
		return newValue;
	}

	void setNewValue(String newValue) {
		this.newValue = newValue;
	}
}
