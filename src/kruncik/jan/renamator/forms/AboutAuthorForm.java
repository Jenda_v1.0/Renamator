package kruncik.jan.renamator.forms;

import java.awt.Dimension;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.util.Properties;

import javax.swing.ImageIcon;
import javax.swing.JLabel;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;

import kruncik.jan.renamator.loadPictures.KindOfPicture;
import kruncik.jan.renamator.loadPictures.Pictures;
import kruncik.jan.renamator.localization.Constants;
import kruncik.jan.renamator.localization.LocalizationImpl;

/**
 * Tato třída slouží pouze jako dialog, který obsahuje základní informace o
 * autorovi této aplikace.
 * 
 * @see Properties
 * 
 * @author Jan Krunčík
 * @version 1.0
 */

public class AboutAuthorForm extends AbstractForm implements LocalizationImpl {

	private static final long serialVersionUID = 1L;


	private static final String FIRST_NAME = "Jan", LAST_NAME = "Krunčík", EMAIL = "kruncikjan95@sezam.cz",
			TELEPHONE = "--- --- ---";
	
	
	
	
	// lblImage - Label pro umístění obrázku
	// lblFirstName - Label pro mé jméno
	// lblLastName - Label pro mé příjmení
	// lblEmail - Label pro email
	// lblTelephone - Label pro telefonní číslo
	private final JLabel lblImage, lblFirstName, lblLastName, lblEmail, lblTelephone;
	
	
	
	
	
	/**
	 * Oblast pro nějaké základní inforamce o mě případně o této aplikaci "ode mě".
	 */
	private final JTextArea txaInfo;
	
	
	public AboutAuthorForm() {
		super();
		
		initGui(500, 500, new GridBagLayout(), "/appMenu/AuthorIcon.png");
		
		final GridBagConstraints gbc = createGbc(5, 5, 5, 5);
		
		
		
		final ImageIcon img = Pictures.loadImage("/AutorImage.png", KindOfPicture.AUTHOR_ICON);
		
		lblImage = new JLabel();
		
		if (img != null)
		lblImage.setIcon(img);
		
		gbc.gridheight = 4;
		add(lblImage, gbc);
		
		
		
		
		
		lblFirstName = new JLabel();		
		
		gbc.gridheight = 1;
		setGbc(gbc, 0, 1);
		add(lblFirstName, gbc);
		
		
		
		lblLastName = new JLabel();
		
		setGbc(gbc, 1, 1);
		add(lblLastName, gbc);
		
		
		
		lblTelephone = new JLabel();
		
		setGbc(gbc, 2, 1);
		add(lblTelephone, gbc);
		
		
		
		lblEmail = new JLabel();
		
		setGbc(gbc, 3, 1);
		add(lblEmail, gbc);
		
		
		
		
		txaInfo = new JTextArea();
		
		txaInfo.setEditable(false);
		txaInfo.setLineWrap(true);
		
		final JScrollPane jspInfo = new JScrollPane(txaInfo);
		jspInfo.setPreferredSize(new Dimension(0, 150));
		
		gbc.gridwidth = 2;
		setGbc(gbc, 4, 0);
		add(jspInfo, gbc);
	}
	
	
	
	
	@Override
	public void setLoadedText(final Properties prop) {
		/*
		 * Proměnná pro text pro potenciální rozšíření.
		 */
		final String textForPotencialExnted;
		
		if (prop != null) {
			setTitle(prop.getProperty("About_Author_Form_Author", Constants.ABOUT_AUTHOR_FORM_DIALOG_TITLE));
			
			if (lblImage.getIcon() == null)
				lblImage.setText(prop.getProperty("About_Author_Form_Lbl_Image", Constants.ABOUT_AUTHOR_FORM_LBL_IMAGE));
			
			lblFirstName.setText(prop.getProperty("About_Author_Form_Lbl_First_Name", Constants.ABOUT_AUTHOR_FORM_LBL_FIRST_NAME) + ": " + FIRST_NAME);
			lblLastName.setText(prop.getProperty("About_Author_Form_Lbl_Last_Name", Constants.ABOUT_AUTHOR_FORM_LBL_LAST_NAME) + ": " + LAST_NAME);
			lblEmail.setText(prop.getProperty("About_Author_Form_Lbl_Email", Constants.ABOUT_AUTHOR_FORM_LBL_EMAIL) + ": " + EMAIL);
			lblTelephone.setText(prop.getProperty("About_Author_Form_Lbl_Telephone", Constants.ABOUT_AUTHOR_FORM_LBL_TELEPHONE) + ": " + TELEPHONE);
			
			
			textForPotencialExnted = "  - " + prop.getProperty("AAF_Text_1", Constants.AAF_TEXT_1) + "\n\n" + "   "
					+ prop.getProperty("AAF_Text_2", Constants.AAF_TEXT_2) + "\n" + " - "
					+ prop.getProperty("AAF_Text_3", Constants.AAF_TEXT_3) + "\n" + " - "
					+ prop.getProperty("AAF_Text_4", Constants.AAF_TEXT_4) + "\n\n" + " - "
					+ prop.getProperty("AAF_Text_5", Constants.AAF_TEXT_5) + "\n" + " - "
					+ prop.getProperty("AAF_Text_6", Constants.AAF_TEXT_6) + "\n" + " - "
					+ prop.getProperty("AAF_Text_7", Constants.AAF_TEXT_7) + "\n" + " - "
					+ prop.getProperty("AAF_Text_8", Constants.AAF_TEXT_8);
		}
		
		
		else {
			setTitle(Constants.ABOUT_AUTHOR_FORM_DIALOG_TITLE);
			
			if (lblImage.getIcon() == null)
				lblImage.setText(Constants.ABOUT_AUTHOR_FORM_LBL_IMAGE);
			
			lblFirstName.setText(Constants.ABOUT_AUTHOR_FORM_LBL_FIRST_NAME + ": " + FIRST_NAME);
			lblLastName.setText(Constants.ABOUT_AUTHOR_FORM_LBL_LAST_NAME + ": " + LAST_NAME);
			lblEmail.setText(Constants.ABOUT_AUTHOR_FORM_LBL_EMAIL + ": " + EMAIL);
			lblTelephone.setText(Constants.ABOUT_AUTHOR_FORM_LBL_TELEPHONE + ": " + TELEPHONE);
			
						
			textForPotencialExnted = "  - " + Constants.AAF_TEXT_1 + "\n\n" + "   "
					+ Constants.AAF_TEXT_2 + "\n" + " - "
					+ Constants.AAF_TEXT_3 + "\n" + " - "
					+ Constants.AAF_TEXT_4 + "\n\n" + " - "
					+ Constants.AAF_TEXT_5 + "\n" + " - "
					+ Constants.AAF_TEXT_6 + "\n" + " - "
					+ Constants.AAF_TEXT_7 + "\n" + " - "
					+ Constants.AAF_TEXT_8;
		}
		
		txaInfo.setText(textForPotencialExnted);
		
		// Okno musím zobrazit až tady, jinak by se nenastavil text, resp. nastavil, ale
		// díky tomu, že je okno modální, už se neprojeví.
		showWindow();
	}
}
