package kruncik.jan.renamator.forms;

import java.awt.BorderLayout;
import java.util.Properties;

import javax.swing.JScrollPane;
import javax.swing.JTextArea;

import kruncik.jan.renamator.localization.Constants;
import kruncik.jan.renamator.localization.LocalizationImpl;

/**
 * Tato třída slouží pouze jako formulář, který obsahuje informace s ohledně
 * této aplikace.
 * 
 * @see Properties
 * 
 * @author Jan Krunčík
 * @version 1.0
 */

public class AboutAppForm extends AbstractForm implements LocalizationImpl {

	
	private static final long serialVersionUID = 1L;


	private final JTextArea txaInfo;
	
	

	/**
	 * Konstruktor této třídy.
	 */
	public AboutAppForm() {
		super();
		
		initGui(500, 500, new BorderLayout(), "/appMenu/InfoIcon.png");
		
		
		
		txaInfo = new JTextArea();
		
		txaInfo.setEditable(false);
		
		txaInfo.setLineWrap(true);
		
		add(new JScrollPane(txaInfo), BorderLayout.CENTER);		
	}
	
	
	
	
	
	
	@Override
	public void setLoadedText(final Properties prop) {
		/*
		 * Proměnná pro text pro vložení do text arey s informacemi o aplikaci.
		 */
		final String textForTxtArea;
		
		if (prop != null) {
			setTitle(prop.getProperty("About_App_Form_Dialog_Title", Constants.ABOUT_APP_FORM_DIALOG_TITLE));
		
			
			textForTxtArea = " - " + prop.getProperty("AAF_Info_Text_1", Constants.AAF_INFO_TEXT_1) + "\n\n" + " - "
					+ prop.getProperty("AAF_Info_Text_2", Constants.AAF_INFO_TEXT_2) + "\n" + " - "
					+ prop.getProperty("AAF_Info_Text_3", Constants.AAF_INFO_TEXT_3) + "\n" + " - "
					+ prop.getProperty("AAF_Info_Text_4", Constants.AAF_INFO_TEXT_4) + "\n\n"

					+ "    " + prop.getProperty("AAF_Info_Text_5", Constants.AAF_INFO_TEXT_5) + ":" + "\n" + " - "
					+ prop.getProperty("AAF_Info_Text_6", Constants.AAF_INFO_TEXT_6) + "\n\n"

					+ " - " + prop.getProperty("AAF_Info_Text_7", Constants.AAF_INFO_TEXT_7) + ":" + "\n" + " - "
					+ prop.getProperty("AAF_Info_Text_8", Constants.AAF_INFO_TEXT_8) + "\n" + " - "
					+ prop.getProperty("AAF_Info_Text_9", Constants.AAF_INFO_TEXT_9) + "\n" + " - "
					+ prop.getProperty("AAF_Info_Text_10", Constants.AAF_INFO_TEXT_10) + "\n\n"

					+ "    " + prop.getProperty("AAF_Info_Text_11", Constants.AAF_INFO_TEXT_11) + "\n" + " - "
					+ prop.getProperty("AAF_Info_Text_12", Constants.AAF_INFO_TEXT_12) + "\n" + " - "
					+ prop.getProperty("AAF_Info_Text_13", Constants.AAF_INFO_TEXT_13);
		}
		
		
		else {
			setTitle(Constants.ABOUT_APP_FORM_DIALOG_TITLE);
			
			textForTxtArea = " - " + Constants.AAF_INFO_TEXT_1 + "\n\n" 
					+ " - "	+ Constants.AAF_INFO_TEXT_2 + "\n"
					+ " - " + Constants.AAF_INFO_TEXT_3 + "\n"
					+ " - " + Constants.AAF_INFO_TEXT_4 + "\n\n"
					
					+ "    " + Constants.AAF_INFO_TEXT_5 + ":" + "\n"
					+ " - " + Constants.AAF_INFO_TEXT_6 + "\n\n"
									
					+ " - " + Constants.AAF_INFO_TEXT_7 + ":" + "\n"
					+ " - " + Constants.AAF_INFO_TEXT_8 + "\n"
					+ " - " + Constants.AAF_INFO_TEXT_9 + "\n"
					+ " - " + Constants.AAF_INFO_TEXT_10 + "\n\n"
																	
					+ "    " + Constants.AAF_INFO_TEXT_11 + "\n"
					+ " - " + Constants.AAF_INFO_TEXT_12 + "\n"
					+ " - " + Constants.AAF_INFO_TEXT_13;
		}
		
		txaInfo.setText(textForTxtArea);
		
		// Okno musím zobrazit až tady, jinak by se nenastavil text, resp. nastavil, ale
		// díky tomu, že je okno modální, už se neprojeví.
		showWindow();
	}
}
