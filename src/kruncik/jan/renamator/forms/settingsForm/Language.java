package kruncik.jan.renamator.forms.settingsForm;

import kruncik.jan.renamator.localization.LocalizationLanguage;

/**
 * Tato třída slouží pouze jako objekt do komponenty JComboBox pro zvolení
 * jazyka ve třídě LanguagePanel.
 * 
 * Obsahuje pouze proměnné pro text - jazyk, aby uživatel věděl, který jazyk
 * zvolil a ta druhá hodnota je hodnota pro to, aby aplikace věděla, který jazyk
 * má načíst.
 * 
 * @author Jan Krunčík
 * @version 1.0
 */

public class Language {

	/**
	 * Proměnná, která slouží pro to, aby uživatel veděl, který jazyk volí, tj.
	 * obsahuje název jazyka.
	 */
	private final String language;

	/**
	 * Tato proměnná obsahuje název souboru XXX(.properties) který se má načist, tj.
	 * název souboru .properties, který obsahuje texty pro aplikaci ve zvoleném
	 * jazyce.
	 */
	private final LocalizationLanguage fileLanguage;

	
	
	
	/**
	 * Konstruktor této třídy.
	 * 
	 * @param language
	 *            - název jazyka.
	 * 
	 * @param fileLanguage
	 *            - název souboru, který obsahuje texty v požadovanémjazyce.
	 */
	public Language(String language, LocalizationLanguage fileLanguage) {
		
		super();
		this.language = language;
		this.fileLanguage = fileLanguage;
	}

	public String getLanguage() {
		return language;
	}

	LocalizationLanguage getFileLanguage() {
		return fileLanguage;
	}
	
	
	@Override
	public String toString() {
		return language;
	}
}
