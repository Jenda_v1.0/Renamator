package kruncik.jan.renamator.forms.settingsForm;

import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.util.Objects;
import java.util.Properties;

import javax.swing.JCheckBox;
import javax.swing.JComboBox;
import javax.swing.JLabel;

import kruncik.jan.renamator.app.App;
import kruncik.jan.renamator.app.Menu;
import kruncik.jan.renamator.fileExplorer.SizeCmbValue;
import kruncik.jan.renamator.fileExplorer.SizeEnum;
import kruncik.jan.renamator.forms.AbstractForm;
import kruncik.jan.renamator.localization.Constants;
import kruncik.jan.renamator.localization.LocalizationImpl;

import static java.util.Arrays.asList;

/**
 * Tato třída slouží jako dialog pro nastavení aplikace.
 * 
 * Nenapadá mě moc možností, nějaké jsou uvedeny v dialogu o autorovi aplikace v
 * části pro potenciální rozšíření, tak jsem zde uvedl alespoň možnosti pro
 * nastavení jazyka, ale dále je možné doplnit například nějaké vzhledové
 * funkce, třeba nastavení velikosti písem, textů, barva pozadí, barva písma
 * apod.
 * 
 * Note:
 * Mohl bych zde dát například komponentu JTabbedPane a jen do jednotlivých
 * panelů v jeho záložkách mít konkrétí položky pro nastaveí "součástí", které
 * se týkají nastavení nějaké funkcionality, kterou "reprezentuje" daná záložka.
 * 
 * @see Properties
 * 
 * @author Jan Krunčík
 * @version 1.0
 * @since 1.8
 */

public class SettingsForm extends AbstractForm implements LocalizationImpl {

	
	private static final long serialVersionUID = 1L;



	/**
	 * Komponenta, která slouží pro označení, resp. nastavení toho, zda se v
	 * průzkumníku souborů mají vlevo ve stromové struktuře souborů na disku
	 * zobrzovat skryté soubory nebo ne.
	 */
	private final JCheckBox chcbShowHiddenFilesInTree;
	

	
	
	
	/**
	 * Panel, který obsahuje komponenty pro nastavení jazyka pro aplikaci.
	 */
	private final LanguagePanel languagePanel;
	
	
	
	
	/**
	 * Label, který obsahuje pouze text s informací, že se jedná pouze o nastavení
	 * pro aktuální běh aplikace, po vypnutí se vše zase vymaže.
	 */
	private final JLabel lblInfo;
	
	
	/**
	 * Label, který nese název formát pro zobrazení velikosti souborů. Tj. aby
	 * uživatel věděl, že si zde vybírá, v jakých "typech" velikostí se budou
	 * zobrazovat velikosti souboru v tabulce v průzkumníku projěktů (v kb, MB, ...)
	 */
	private final JLabel lblSizeFormat;
	
	
	
	
	/**
	 * Proměnná, která slouží jako model pro komponentu JComboBox cmbSize, tato
	 * proměnná obsahuje hodnoty, které si lze v cmbSize zvolit.
	 */
	private static final SizeCmbValue[] CMB_SIZE_MODEL = { new SizeCmbValue(SizeEnum.B), new SizeCmbValue(SizeEnum.KB),
			new SizeCmbValue(SizeEnum.MB), new SizeCmbValue(SizeEnum.GB), new SizeCmbValue(SizeEnum.TB),
			new SizeCmbValue(SizeEnum.PB), new SizeCmbValue(SizeEnum.EB), new SizeCmbValue(SizeEnum.ZB),
			new SizeCmbValue(SizeEnum.YB) };
	
	/**
	 * Komponenta, ve které si uživatel může vybrat, v jakém velikostím formátu se
	 * mají zobrazovat v tabulce velikosti souborů, tj. například v KB, MB, ...
	 */
	private final JComboBox<SizeCmbValue> cmbSize;
	
	
	


	/**
	 * Konstruktor této třídy.
	 * 
	 * @param app
	 *            - reference na instanci třídy App pro předání do panelu pro
	 *            nastavení jazyka, aby se zavolala příslušná metoda ve třídě App.
	 */
	public SettingsForm(final App app) {
		super();
		
		initGui(850, 315, new GridBagLayout(), "/appMenu/SettingsIcon.png");
		
		final GridBagConstraints gbc = createGbc(5, 5, 5, 5);
		
		int rowIndex = 0;
		
		
		
		
		gbc.gridwidth = 2;
		lblInfo = new JLabel();
		lblInfo.setHorizontalAlignment(JLabel.CENTER);
		add(lblInfo, gbc);
		
		
		
		
		
		languagePanel = new LanguagePanel(app);
		setGbc(gbc, ++rowIndex, 0);
		add(languagePanel, gbc);
		
		
		
		
		
		chcbShowHiddenFilesInTree = new JCheckBox();
		chcbShowHiddenFilesInTree.setSelected(Menu.isShowHiddenFilesInJtree());
		chcbShowHiddenFilesInTree
				.addActionListener(Event -> Menu.setShowHiddenFilesInJtree(chcbShowHiddenFilesInTree.isSelected()));

		setGbc(gbc, ++rowIndex, 0);
		add(chcbShowHiddenFilesInTree, gbc);
		
		
		
		
		
		lblSizeFormat = new JLabel();
		
		gbc.gridwidth = 1;
		setGbc(gbc, ++rowIndex, 0);
		add(lblSizeFormat, gbc);
		
		
		
		
		
		
		cmbSize = new JComboBox<>(CMB_SIZE_MODEL);
		cmbSize.addActionListener(Event -> {
			final SizeEnum size = ((SizeCmbValue) Objects.requireNonNull(cmbSize.getSelectedItem())).getSizeEnum();
			Menu.setShowSizeInJTable(size);
		});
		
		/*
		 * Nastavím si jako označenou položku tu, která je v menu v proměnné
		 * showSizeInJTable.
		 * 
		 * To zjistím tak, že prohledám pole CMB_SIZE_MODEL a vrátím položku, která
		 * obsahuje výčtový typ stejný, jako v uvedené proměnné v menu, ten pak nastavím
		 * v cmbSize jako označenou položku.
		 */
		cmbSize.setSelectedItem(asList(CMB_SIZE_MODEL).stream()
				.filter(v -> v.getSizeEnum() == Menu.getShowSizeInJTable()).findFirst().get());
		
		
		setGbc(gbc, rowIndex, 1);
		add(cmbSize, gbc);		
	}
	
	
	
	
	
	
	
	@Override
	public void setLoadedText(final Properties prop) {
		if (prop != null) {
			setTitle(prop.getProperty("SF_Dialog_Title", Constants.SF_DIALOG_TITLE));
			
			lblInfo.setText(prop.getProperty("SF_Lbl_Info", Constants.SF_LBL_INFO));
			
			chcbShowHiddenFilesInTree.setText(prop.getProperty("SF_Chcb_Show_Hidden_Files_In_Tree", Constants.SF_CHCB_SHOW_HIDDEN_FILES_IN_TREE));
			
			languagePanel.setLoadedText(prop);
			
			lblSizeFormat.setText("? " + prop.getProperty("SF_Lbl_Size_Format_Text", Constants.SF_LBL_SIZE_FORMAT_TEXT) + ":");
			lblSizeFormat.setToolTipText(prop.getProperty("SF_Lbl_Size_Format_TT", Constants.SF_LBL_SIZE_FORMAT_TT));
		}
		
		else {
			setTitle(Constants.SF_DIALOG_TITLE);
			
			lblInfo.setText(Constants.SF_LBL_INFO);
			
			chcbShowHiddenFilesInTree.setText(Constants.SF_CHCB_SHOW_HIDDEN_FILES_IN_TREE);
			
			lblSizeFormat.setText(Constants.SF_LBL_SIZE_FORMAT_TEXT + ":");
			lblSizeFormat.setToolTipText(Constants.SF_LBL_SIZE_FORMAT_TT);
		}
		
		
		showWindow();
	}
}
