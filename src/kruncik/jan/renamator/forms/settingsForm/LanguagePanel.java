package kruncik.jan.renamator.forms.settingsForm;

import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import java.util.Objects;
import java.util.Properties;

import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;

import kruncik.jan.renamator.app.App;
import kruncik.jan.renamator.forms.AbstractForm;
import kruncik.jan.renamator.localization.Constants;
import kruncik.jan.renamator.localization.Localization;
import kruncik.jan.renamator.localization.LocalizationImpl;
import kruncik.jan.renamator.localization.LocalizationLanguage;
import kruncik.jan.renamator.panels.FileChooser;

/**
 * Tento panel obsahuje komponenty pro nastavení jazyka pro aplikaci.
 * 
 * @see Properties,
 * @see ActionListener
 * @see JPanel
 * 
 * @author Jan Krunčík
 * @version 1.0
 */

public class LanguagePanel extends JPanel implements LocalizationImpl, ActionListener {

	
	private static final long serialVersionUID = 1L;

	
	
	
	/**
	 * Komponenta, ve které je zobrazen text: Jazyk.
	 */
	private final JLabel lblLanguage;
	
	
	
	

	/**
	 * Jednorozměrné pole, které slouží jako model pro komponentu cmbLanguage.
	 * 
	 * Toto pole obsahuje možné jazyky, které lze nastavit v aplikaci.
	 */
	private static final Language[] LANGUAGE_MODEL = { new Language("Czech", LocalizationLanguage.CZ),
			new Language("English", LocalizationLanguage.EN) };

	/**
	 * Proměnná, která slouží pro nastavení jazyka, které aplikace nabízí.
	 */
	private final JComboBox<Language> cmbLanguage;

	
	/**
	 * Tlačítko, které slouží pro nastavení jazyka do aplikace.
	 */
	private final JButton btnChooseLanguage;
	
	
	
	
	
	
	/**
	 * Label, který obsahuje text pro zvolení jiného jazyka, než, který nabízí
	 * aplikace.
	 */
	private final JLabel lblChooseLanguage;
	
	/**
	 * Tlačítko, které slouží pro zvolení vlastního jazyka, někde v PC uživatele.
	 * 
	 * Jedná se o zvolení souboru typu .properties, který obsahuje požadované texty.
	 */
	private final JButton btnChooseDifferentLanguage;
	
	
	
	/**
	 * Proměnná pro text ve zvoleném jazyce pro titulek dialogu pro vybrání souboru
	 * .properties s texty pro aplikaci a pro chybové hlášky pokud se nevybere
	 * soubor typu .properties.
	 */
	private String txtTitle, txtWrongFileText, txtWrongFileTitle;
	
	
	
	/**
	 * Proměnné pro text do dialogového okna pro uložení jazyku do zvoleného souboru.
	 */
	private String txtSaveLanguageTitle, txtJopSaveLanguageText, txtJopSaveLanguageTitle; 
	
	
	
	
	
	/**
	 * Texty do chybových hlášek.
	 */
	private String txtFailedLoadPropertiesFileText_1, txtFailedLoadPropertiesFileText_2, txtFailedLoadPropertiesFileTitle;
	
	
	
	
	
	
	
	
	/**
	 * Label, ve kterém bude text pro uložení jazyka na zvolené umístění pro
	 * přepsání apod.
	 */
	private final JLabel lblSaveLanguge;
	
	/**
	 * V této komponentě budou na výběr stejné jazyky jako v cmbLanguage, akorát
	 * onačený jazyk zde bude uložen za zadané umítění.
	 */
	private final JComboBox<Language> cmbSaveLanguage;
	
	/**
	 * Po kliknutí na toto tlačítko se otevře dialogové okno pro zadání názvu a
	 * umístění jazyka (souboru typu .properties)
	 */
	private final JButton btnSaveLanguage;
	
	
	
	
	
	
	
	
	
	
	/**
	 * Reference na třídu App, která obsahuje metodu pro nastavení jazyka do
	 * ostatních částí aplikace.
	 */
	private final App app;
	
	

	
	
	

	
	/**
	 * Konstruktor této třídy.
	 */
	LanguagePanel(final App app) {
		super(new GridBagLayout());
		
		this.app = app;
		
		final GridBagConstraints gbc = AbstractForm.createGbc(5, 5, 5, 5);
		
		
		
		
		
		lblLanguage = new JLabel();
		add(lblLanguage, gbc);
		
		
		
		
		
		
		cmbLanguage = new JComboBox<>(LANGUAGE_MODEL);
		AbstractForm.setGbc(gbc, 0, 1);
		add(cmbLanguage, gbc);
		
		
		
		
		
		btnChooseLanguage = new JButton();
		btnChooseLanguage.addActionListener(this);
		
		AbstractForm.setGbc(gbc, 0, 2);
		add(btnChooseLanguage, gbc);
		
		
		
		
		
		lblChooseLanguage = new JLabel();
		AbstractForm.setGbc(gbc, 1, 0);
		add(lblChooseLanguage, gbc);		
		
		
		
		btnChooseDifferentLanguage = new JButton();
		btnChooseDifferentLanguage.addActionListener(this);
		
		gbc.gridwidth = 2;
		AbstractForm.setGbc(gbc, 1, 2);
		add(btnChooseDifferentLanguage, gbc);		
		
		
		
		
		
		
		
		
		lblSaveLanguge = new JLabel();

		gbc.gridwidth = 1;
		AbstractForm.setGbc(gbc, 2, 0);
		add(lblSaveLanguge, gbc);
		
		
		
		cmbSaveLanguage = new JComboBox<>(LANGUAGE_MODEL);
		
		AbstractForm.setGbc(gbc, 2, 1);
		add(cmbSaveLanguage, gbc);
		
		
		
		
		btnSaveLanguage = new JButton();
		btnSaveLanguage.addActionListener(this);
		
		AbstractForm.setGbc(gbc, 2, 2);
		add(btnSaveLanguage, gbc);
	}

	
	
	
	@Override
	public void setLoadedText(final Properties prop) {
		if (prop != null) {
			lblLanguage.setText(prop.getProperty("LP_Lbl_Language", Constants.LP_LBL_LANGUAGE) + ": ");
			
			btnChooseLanguage.setText(prop.getProperty("LP_Btn_Choose_Language_Text", Constants.LP_BTN_CHOOSE_LANGUAGE_TEXT));
			btnChooseLanguage.setToolTipText(prop.getProperty("LP_Btn_Choose_Language_TT", Constants.LP_BTN_CHOOSE_LANGUAGE_TT));
			
			lblChooseLanguage.setText(prop.getProperty("LP_Lbl_Choose_Language", Constants.LP_LBL_CHOOSE_LANGUAGE) + ": ");
			
			btnChooseDifferentLanguage.setText(prop.getProperty("LP_Btn_Choose_Different_Language_Text", Constants.LP_BTN_CHOOSE_DIFFERENT_LANGUAGE_TEXT));
			btnChooseDifferentLanguage.setToolTipText(prop.getProperty("LP_Btn_Choose_Different_Language_TT", Constants.LP_BTN_CHOOSE_DIFFERENT_LANGUAGE_TT));

			lblSaveLanguge.setText(prop.getProperty("LP_Lbl_Save_Language", Constants.LP_LBL_SAVE_LANGUAGE) + ": ");
			
			btnSaveLanguage.setText(prop.getProperty("LP_Btn_Save_Language_Text", Constants.LP_BTN_SAVE_LANGUAGE_TEXT));
			btnSaveLanguage.setToolTipText(prop.getProperty("LP_Btn_Save_Language_Title", Constants.LP_BTN_SAVE_LANGUAGE_TITLE));
			
			
			txtTitle = prop.getProperty("LP_Txt_Title", Constants.LP_TXT_TITLE);
			txtWrongFileText = prop.getProperty("LP_Txt_Wrong_File_Text", Constants.LP_TXT_WRONG_FILE_TEXT);
			txtWrongFileTitle = prop.getProperty("LP_Txt_Wrong_File_Title", Constants.LP_TXT_WRONG_FILE_TITLE);
			
			txtFailedLoadPropertiesFileText_1 = prop.getProperty("LP_Txt_Failed_Load_Properties_File_Text_1", Constants.LP_TXT_FAILED_LOAD_PROPERTIES_FILE_TEXT_1);
			txtFailedLoadPropertiesFileText_2 = prop.getProperty("LP_Txt_Failed_Load_Properties_File_Text_2", Constants.LP_TXT_FAILED_LOAD_PROPERTIES_FILE_TEXT_2);
			txtFailedLoadPropertiesFileTitle = prop.getProperty("LP_Txt_Failed_Load_Properties_File_Title", Constants.LP_TXT_FAILED_LOAD_PROPERTIES_FILE_TITLE);
			
			txtSaveLanguageTitle = prop.getProperty("LP_Txt_Save_Language_Title", Constants.LP_TXT_SAVE_LANGUAGE_TITLE);
			txtJopSaveLanguageText = prop.getProperty("LP_Txt_Jop_Save_Language_Text", Constants.LP_TXT_JOP_SAVE_LANGUAGE_TEXT);
			txtJopSaveLanguageTitle = prop.getProperty("LP_Txt_Jop_Save_Language_Title", Constants.LP_TXT_JOP_SAVE_LANGUAGE_TITLE);
		}
		
		
		else {
			lblLanguage.setText(Constants.LP_LBL_LANGUAGE + ": ");
			
			btnChooseLanguage.setText(Constants.LP_BTN_CHOOSE_LANGUAGE_TEXT);
			btnChooseLanguage.setToolTipText(Constants.LP_BTN_CHOOSE_LANGUAGE_TT);
			
			lblChooseLanguage.setText(Constants.LP_LBL_CHOOSE_LANGUAGE + ": ");
			
			btnChooseDifferentLanguage.setText(Constants.LP_BTN_CHOOSE_DIFFERENT_LANGUAGE_TEXT);
			btnChooseDifferentLanguage.setToolTipText(Constants.LP_BTN_CHOOSE_DIFFERENT_LANGUAGE_TT);
			
			lblSaveLanguge.setText(Constants.LP_LBL_SAVE_LANGUAGE + ": ");
			
			btnSaveLanguage.setText(Constants.LP_BTN_SAVE_LANGUAGE_TEXT);
			btnSaveLanguage.setToolTipText(Constants.LP_BTN_SAVE_LANGUAGE_TITLE);
			
			
			txtTitle = Constants.LP_TXT_TITLE;
			txtWrongFileText = Constants.LP_TXT_WRONG_FILE_TEXT;
			txtWrongFileTitle = Constants.LP_TXT_WRONG_FILE_TITLE;
			
			txtFailedLoadPropertiesFileText_1 = Constants.LP_TXT_FAILED_LOAD_PROPERTIES_FILE_TEXT_1;
			txtFailedLoadPropertiesFileText_2 = Constants.LP_TXT_FAILED_LOAD_PROPERTIES_FILE_TEXT_2;
			txtFailedLoadPropertiesFileTitle = Constants.LP_TXT_FAILED_LOAD_PROPERTIES_FILE_TITLE;	
			
			txtSaveLanguageTitle = Constants.LP_TXT_SAVE_LANGUAGE_TITLE;
			txtJopSaveLanguageText = Constants.LP_TXT_JOP_SAVE_LANGUAGE_TEXT;
			txtJopSaveLanguageTitle = Constants.LP_TXT_JOP_SAVE_LANGUAGE_TITLE;
		}
	}




	@Override
	public void actionPerformed(ActionEvent e) {
		if (e.getSource() instanceof JButton) {// Zbytečná podmínka.
			if (e.getSource() == btnChooseLanguage) {
				final Language language = (Language) cmbLanguage.getSelectedItem();
				final Properties properties = Localization.loadDefaultLocalizationFile(Objects.requireNonNull(language).getFileLanguage().getPropertiesName());
				
				if (properties != null)
					app.setLoadedText(properties);
				// else - nemělo by nastat
			}
			
			
			else if (e.getSource() == btnChooseDifferentLanguage) {
				final File file = new FileChooser().getPathToProperties(txtTitle, txtWrongFileText, txtWrongFileTitle);								
				
				if (file != null) {
					final Properties language = Localization.loadLocalizationFile(file.getAbsolutePath());
					
					if (language != null)
						app.setLoadedText(language);
					else
						JOptionPane.showMessageDialog(this,
								txtFailedLoadPropertiesFileText_1 + "\n" + txtFailedLoadPropertiesFileText_2 + ": "
										+ file.getAbsolutePath(),
								txtFailedLoadPropertiesFileTitle, JOptionPane.ERROR_MESSAGE);
				}
			}
			
			
			else if (e.getSource() == btnSaveLanguage) {
				/*
				 * Otevře dialogové okno pro zadání názvu a umístění souboru s texty pro
				 * aplikaci.
				 */
				final Language language = (Language) cmbSaveLanguage.getSelectedItem();
				
				final File file = new FileChooser().getPathToPropertiesToSave(txtSaveLanguageTitle,
						txtJopSaveLanguageText, txtJopSaveLanguageTitle);

				/*
				 * Pokud byl označen nebo zadán soubor, tak jej vytvořím, případně přepíšu
				 * označený soubor:
				 */
				if (file != null)
					Localization.createLocalizationFile(file.getAbsolutePath(), Objects.requireNonNull(language).getFileLanguage());
			}
		}
	}
}
