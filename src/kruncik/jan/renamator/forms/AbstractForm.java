package kruncik.jan.renamator.forms;

import java.awt.Dimension;
import java.awt.GridBagConstraints;
import java.awt.Insets;
import java.awt.LayoutManager;
import java.util.Objects;

import javax.swing.JDialog;

import kruncik.jan.renamator.loadPictures.KindOfPicture;
import kruncik.jan.renamator.loadPictures.Pictures;

/**
 * Tato třída slouží pouze jako abstraktní třída, která obsahuje společné
 * komponenty pro dialogy, například dědí z dialogu, obsahuje přdpřipravené
 * metody pro gbc apod.
 *
 * @see JDialog
 *
 * @author Jan Krunčík
 * @version 1.0
 */

public abstract class AbstractForm extends JDialog {


	private static final long serialVersionUID = 1L;


	/**
	 * Metoda, která nastaví velikost dialogu, akce, typ / způsob zavření dialogu,
     * layout manager pro rozvržení komponent a ikonu dialou.
     *
     * @param width      - šířka okna dialogu
     * @param height     - výška okna dialogu
     * @param layout     - layout manger - manager rozvržení komponent.
     * @param pathToIcon - cesta k ikoně, která se má nastavit do dialogu.
     */
    protected final void initGui(final int width, final int height,
                                 final LayoutManager layout, final String pathToIcon) {
		setPreferredSize(new Dimension(width, height));
		setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);
		setLayout(layout);
		setIconImage(Objects.requireNonNull(Pictures.loadImage(pathToIcon, KindOfPicture.APP_ICON)).getImage());
		setModal(true);
	}



	/**
	 * Metoda, která přizpůsobí okno velikosti komponentám, vycentruje dialog na
	 * střed obrazovky a zobrazí jej.
	 */
	protected final void showWindow() {
		pack();
		setLocationRelativeTo(null);
		setVisible(true);
	}









	/**
	 * Metoda, která vytvoří komponentu / objekt GridBagConstraints pro layout
	 * GridBagLayout a vrátít jej.
	 *
	 * Následují velikost mezer mezi komponentami:
	 *
	 * @param iTop
	 *            - odsazení komponent nahoře
	 * @param iLeft
	 *            - odsazení komponent vlevo
	 * @param iBottom
	 *            - odsazení komponent dole
	 * @param iRight
	 *            - odsazení komponent vpravo
	 *
	 * @return - vytvořenou instanci komponenty GridBagConstraints.
	 */
	public static GridBagConstraints createGbc(final int iTop, final int iLeft, final int iBottom,
			final int iRight) {
		final GridBagConstraints gbc = new GridBagConstraints();

		gbc.insets = new Insets(iTop, iLeft, iBottom, iRight);

		gbc.fill = GridBagConstraints.HORIZONTAL;

		gbc.weightx = 0.2;
		gbc.weighty = 0.2;

		gbc.gridx = 0;
		gbc.gridy = 0;

		return gbc;
	}








	/**
	 * Metoda, která nastaví parametry index řádku a index sloupce pro komponentu
	 * GridBagConstraints.
	 *
	 * @param gbc
	 *            - Komponenta, kde se má nastavit index řádku a sloupce.
	 * @param rowIndex
	 *            - index řádku
	 * @param columnIndex
	 *            - index sloupce
	 */
	public static void setGbc(final GridBagConstraints gbc, final int rowIndex, final int columnIndex) {
		gbc.gridx = columnIndex;
		gbc.gridy = rowIndex;
	}
}
