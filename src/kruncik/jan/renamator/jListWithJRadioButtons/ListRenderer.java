package kruncik.jan.renamator.jListWithJRadioButtons;

import java.awt.Color;
import java.awt.Component;

import javax.swing.JList;
import javax.swing.JRadioButton;
import javax.swing.ListCellRenderer;

/**
 * Tato třída slouží jako renderer, který vykresuluje požadované JRadioButtony
 * nebo JCheckBoxy v MyJList.
 * 
 * @author Jan Krunčík
 * @version 1.0
 */

//public class ListRenderer extends JCheckBox implements ListCellRenderer<ListItem> {
public class ListRenderer extends JRadioButton implements ListCellRenderer<ListItem> {

	private static final long serialVersionUID = 1L;

	
	
	@Override
	public Component getListCellRendererComponent(JList<? extends ListItem> list, ListItem value, int index,
			boolean isSelected, boolean cellHasFocus) {
		
		setEnabled(list.isEnabled());

		setSelected(value.isSelected());

		setFont(list.getFont());
		setBackground(list.getBackground());
		setForeground(list.getForeground());

		if (!isEnabled()) {
			setOpaque(true);
			setBackground(new Color(0, 0, 0, 0));
		} else if (!isOpaque())
			setOpaque(false);
		
		setText(value.toString());

		return this;
	}
}
