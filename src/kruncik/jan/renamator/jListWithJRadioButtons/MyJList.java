package kruncik.jan.renamator.jListWithJRadioButtons;

import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;

import javax.swing.JList;
import javax.swing.ListSelectionModel;

/**
 * Tato třída slouží jako JList, který obsahuje komponenty JRadioButton (je
 * možné i JCheckBox), a každý reprezentuje nějaký soubor.
 * 
 * @see JList
 * 
 * @author Jan Krunčík
 * @version 1.0
 */

public class MyJList extends JList<ListItem> {

	private static final long serialVersionUID = 1L;

	
	/**
	 * Tato proměnná slouží pro to, že v aplikaci mám dvě pole, které obshují
	 * přípony soubor ve zvoleném adresáři, a tak potřebuji hlídat to, aby se
	 * neoznačovaly stejné položky v obou listech, tak vždy otestuji, zda v tom
	 * druhém listu není položku, na kterou uživatel klikl již označena, pokud ano,
	 * nepůjde označit.
	 */
	private MyJList anotherList;
	
	
	
	/**
	 * Konstruktor této třídy.
	 * 
	 * @param model
	 *            - položky, které se mají vloži do tohoto Jlistu.
	 * 
	 * @param layoutOrientation
	 *            - Orientace, v podstatě, jak se mají "řadit" položky v tomto listu
	 *            zda se mají řadit do sloupců verikálně nebo horizontálně, nebo jen
	 *            jeden sloupec.
	 */
	public MyJList(final ListItem[] model, final int layoutOrientation) {
		super(model);
		
		
		// Renderer, aby se vykreslovaly JradioButtony nebo JCheckBoxy apod. (viz
		// renderer):
		setCellRenderer(new ListRenderer());
		// Aby vždy šel označit jen jedna pložka, ale to je zbytečné, nic se s tím
		// nedělá, (jen tak):
		setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
		
		// Následující dva řádky je aby byly sloupce vedle sebe v případě, že je okno
		// dostatečně široké a dostatečný počet souborů:
		// (více v dokumentaci)
		setLayoutOrientation(layoutOrientation);
		setVisibleRowCount(-1);
		
		
		addMouseListener(new MouseAdapter() {		
			
			@Override
			public void mouseClicked(MouseEvent e) {
				// Pokud je tento list znepřístupněn, tak nebudu pokračovat:
		        if (!isEnabled())
		        	return;
				
		        final int index = locationToIndex(e.getPoint());// Get index of item
		                                                           // clicked
				// Zde testuji, zda je nějaká pložka označena nebo ne:
		        if (index == -1)
		        	return;
		        
		        final ListItem item = getModel().getElementAt(index);
		        
		        
		        
		        /*
				 * Nejprve potřebuji otestovat, zda není anotherList null, protože pokud ano,
				 * pak se jedná o označení položky bez testování označených hodnot v tom druhém
				 * listu. To je například v případě, že se jedná o označení pložky v listu se
				 * všemi soubory načtenými ve zvoleném zdrojovém adresáři - dole v okne
				 * aplikace, list listForSkipFilesWithName
				 */
		        if (anotherList == null)
		        	item.setSelected(!item.isSelected()); // Toggle selected state
		        
		        
		        /*
				 * Zjistím, zda v tom druhém listu ještě není označena položka ta samá položka a
				 * pokud ne, pak ji zde označím, pokud tam je označena, pak ji zde nemohu
				 * označit.
				 */
				// Pokud v tom druhém listu není položka označena, mohu změnit stav této pložky:
		        else if (!anotherList.getModel().getElementAt(index).isSelected())
					// Zde mohu změnit stav této položky - v tom druhém listu není označena:
					item.setSelected(!item.isSelected()); // Toggle selected state
				
		        
		        repaint(getCellBounds(index, index));// Repaint cell
			}
		});
	}
	
	
	
	
	
	/**
	 * Metoda, která nastaví referenci na druhý list, ze kterého se mbudou hlídat
	 * označené položky. Aby nebyly v tomto a anotherList označené stejné položky.
	 * Jedna položka může být označena pouze v jednom z těchto dvou listů.
	 * 
	 * @param anotherList
	 *            - druhý list popsaný výše.
	 */
	public final void setAnotherList(final MyJList anotherList) {
		this.anotherList = anotherList;
	}
	
	
	
	
	
	
	/**
	 * Metoda, která projde list se všemi položkami v tomto listu (v jeho konkrétní
	 * instanci) a zjistí, zda je alespoň jedna položka označena nebo ne.
	 * 
	 * Algoritmus je takový, že se hledá první položka, která je označena, pokud se
	 * projdou všechny položky a žádná nebude označena, pak se na konec vrátí false.
	 * 
	 * @return true, alespoň jedna položka je označena. False, žádná položka není
	 *         označena.
	 */
	public final boolean isAtLeastOneItemSelected() {
		final int length = getModel().getSize();

		for (int i = 0; i < length; i++) {
			final ListItem item = getModel().getElementAt(i);

			if (item.isSelected())
				return true;
		}

		return false;
	}
	
	
	
	
	
	
	/**
	 * Metoda, která zjistí, zda je tento konkrétní list prázdný nebo ne.
	 * 
	 * @return true, pokud je list prázdný, tj. neobsahuje žádnou pložku, jiank
	 *         false, list není prázdný, obsahuje alespoň jednu položku.
	 */
	public final boolean isListEmpty() {
		return getModel().getSize() == 0;
	}
	
	
	
	
	
	
	
	/**
	 * Metoda, která zjistí, zda je označena položka, která obsahuje daný text v
	 * parametru této metody.
	 * 
	 * V listu mohou být zobrazeny buď přípony nebo cesty k souborů od nějakého
	 * adresáře, případně jen názvy souborů (s příponou), a tento zobrazený text se
	 * bude testovat, zda se text zobrazené položky v listu rovná textu v parametru
	 * této metody.
	 * 
	 * Pokud ano a zároveň je ta položka označena, pak se vrátí true, jinak false.
	 * 
	 * @param text
	 *            - text, který se testuji, zda se v listu nachází položka s tímto
	 *            textem a zároveň je označena.
	 * 
	 * @return true, pokud se v listu nachází položka s textem v proměnné text a
	 *         zároveň je označena. Jinak false.
	 */
	public final boolean isItemWithTextSelected(final String text) {
		final int length = getModel().getSize();

		for (int i = 0; i < length; i++) {
			final ListItem item = getModel().getElementAt(i);

			if (item.toString().equals(text) && item.isSelected())
				return true;
		}

		return false;
	}
	
	
	
	
	
	
	
	
	
	/**
	 * Metoda, která buď označí všechny položky nebo odznačí všechny položky v listu
	 * (dle hodnoty v proměnné select ).
	 * 
	 * @param select
	 *            - Logická proměnná o tom, zda se mají všechny položky v listu
	 *            označit nebo odznačit, true - označit, false - odznačit.
	 */
	public final void markAllItems(final boolean select) {
		final int length = getModel().getSize();
		
		for (int i = 0; i < length; i++) {
			final ListItem item = getModel().getElementAt(i);
			
			if (item.isSelected() != select) {
				item.setSelected(select);
				
				// Stačí překreslit položku pouze v případě, že u ní došlo ke změně:
				repaint(getCellBounds(i, i));// Repaint cell
			}									
		}		
	}
	
	
	
	
	/**
	 * Metoda, která dle hodnoty proměnné select označí nebo odznačí položky v tomto
	 * listu. Ale pouze v případě, že ta samá položka již není označena v listu
	 * list.
	 * 
	 * Jedná se o to, že uživatel má možnost přejmenovat pouze soubory s nějakou
	 * příponou, a zároveň vynechat soubory s nějakou příponou, tak nemůže v jednom
	 * zvolit příponu souborů, které se mají přejmenovat a zároveň soubroy se
	 * stejnou příponou vynechat, tak v případě, že se jedná o označení položek
	 * nejprve otestuje, zda v tom druhém listu již nneí označena.
	 * 
	 * @param select
	 *            - Logická proměnná o tom, zda se mají všechny položky v listu
	 *            označit nebo odznačit, true - označit, false - odznačit.
	 * 
	 * @param checkAnotherList
	 *            - logická proměnná o tom, zda se má testovat, zda je v tom druhém
	 *            listu označená ta samá položka nebo ne.
	 */
	public final void markAllItems(final boolean select, final boolean checkAnotherList) {
		final int length = getModel().getSize();
		
		for (int i = 0; i < length; i++) {
			final ListItem item = getModel().getElementAt(i);
			
			if (select && checkAnotherList) {
				/*
				 * Zde se má položka označit, tak otestuji, zda ještě nění položka označena v
				 * tom druhém listu, pokud není, je to v pohodě, zde ji mohu označit, ale pokud
				 * již je položka označena v tom druhém listu, tak ji neoznačím:
				 */
				if (!anotherList.getModel().getElementAt(i).isSelected()) {
					item.setSelected(select);
					
					// Stačí překreslit položku pouze v případě, že u ní došlo ke změně:
					repaint(getCellBounds(i, i));// Repaint cell
				}					
			}
			
			// Zde se jedná o odznačení položky, tak ji pouze odznačím (pokud již není)
			else if (item.isSelected() != select) {
				item.setSelected(select);
				
				// Stačí překreslit položku pouze v případě, že u ní došlo ke změně:
				repaint(getCellBounds(i, i));// Repaint cell
			}
		}		
	}
}
