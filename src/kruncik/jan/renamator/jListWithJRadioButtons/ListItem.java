package kruncik.jan.renamator.jListWithJRadioButtons;

import java.io.File;

import org.apache.commons.io.FilenameUtils;

import kruncik.jan.renamator.app.App;

/**
 * Tato třída slouží jako položka v MyJList, instance této třídy slouží jako
 * JRadioButton případně JCheckBox, který se pomocí rendereru vykresulje v
 * MyJList.
 * 
 * @author Jan Krunčík
 * @version 1.0
 */

public class ListItem {

	/**
	 * Proměnná, která značí soubor - nějaký soubor ve zvoleném adresáři, případně
	 * subadresáří.
	 */
	private final File file;
	
	/**
	 * Logická proměnná o tom, zda je ChechBox nebo JRadioButton (tato položka v
	 * MyJlist) označena nebo ne.
	 */
	private boolean selected;
	
	
	
	
	
	/**
	 * Výčtový typ, který značí, "kolik" se toho má zobrazit o souboru file, zda se
	 * má zobrazit jeho přípona, název s příponou nebo cesta od zdrojového adresáře
	 * i s názvem příponou souboru.
	 */
	private final PathForShow pathForShow;
	
	
	
	
	
	/**
	 * Konstruktor této třídy.
	 * 
	 * @param file
	 *            - nějaký soubor na disku v PC, kde běží aplikace.
	 * 
	 * @param selected
	 *            - logická proměnná o tom, zda je tato položka v MyJList označena
	 *            nebo ne.
	 * 
	 * @param pathForShow
	 *            - výčtový typ, který definuje, "kolik" se toho má zobrazit o
	 *            souboru file. Tzn. zda se má zobrazit pouze jeho přípona, jeho
	 *            název s příponou nebo i cesta od zdrojového adresáře i s názvem a
	 *            příponou souboru.
	 */
	public ListItem(final File file, final boolean selected, final PathForShow pathForShow) {
		super();

		this.file = file;
		this.selected = selected;
		this.pathForShow = pathForShow;
	}


	public File getFile() {
		return file;
	}

	public boolean isSelected() {
		return selected;
	}

	public void setSelected(boolean selected) {
		this.selected = selected;
	}

	public PathForShow getPathForShow() {
		return pathForShow;
	}
	
	
	/**
	 * Metoda, která vrátí pouze příponu souboru file (tj. soubor, na který "ukauje"
	 * proměnná file)
	 * 
	 * @return pouze příponu souboru v syntaxi například: "jpg" (bez tečky.)
	 */
	public final String getExtensionFromFile() {
		return FilenameUtils.getExtension(file.getAbsolutePath());
	}
	
	
	
	@Override
	public String toString() {
		if (pathForShow == PathForShow.SHOW_EXTENSION)
			// Zobrazit příponu:
			return "." + getExtensionFromFile();

		
		if (pathForShow == PathForShow.SHOW_PATH_FROM_SOURCE_DIR) {
			/*
			 * Zobrazit cestu od zdrojového adresáře k adresáři, kde se nachází soubor file,
			 * zobrazit jeho název i s příponou a zmíněnou cestou od zdrojového adresáře.
			 * 
			 * Postup:
			 * Pouze vrátí testu od zdrojového adresáře a přidám název souboru s příponou.
			 * 
			 * Tu cestu zjistím tak, že vezmu text od názvu zdrojového adresáře:
			 * Např:
			 * home/xxx/ -> Zdrojový adresář označený uživatelem (xxx)
			 * home/xxx/yyy/file.xxx -> konečný soubor nalezený v podadresáři
			 * 
			 * Pouze si vezmu text od zdrojového adresáře do konce:
			 * takže zbyde: yyy/file.xxx 
			 */
			final String path = file.getAbsolutePath().substring(App.getFileSourceDir().getAbsolutePath().length());
			
			
			/*
			 * Zde se ne vždy odebírá první znak coby lomítko, někdy se zobrazují skryté
			 * soubory a ty mají nebo mohou mít jako první znak desetinnou tečku a tu nechci
			 * odebírat, aby si to uživatel uvedomil.
			 */
			
			if (App.KIND_OF_OS.equalsIgnoreCase("Linux")) {
				if (path.startsWith("/"))
					return path.substring(1);

			} else if (App.KIND_OF_OS.toUpperCase().contains("Windows".toUpperCase()) && path.startsWith("\\"))
				return path.substring(1);

			return path;	
		}
		

		// Toto by nastat nemělo, nemělo by to zde být potřeba, ale kdyby náhodou:
		return "?";
	}
}
