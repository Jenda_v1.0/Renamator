package kruncik.jan.renamator.jListWithJRadioButtons;

/**
 * Tento výčet slouží pro definování jaká cesta se má zobrazit v listech se
 * soubory, tj. zda se vůbec má zobrazit nějaká cesta k souboru (nemá pokud je
 * soubor v označeném zdrojovém adresáři), pak se má zobrazit cesta od
 * zdrojového adresáře až po ten, ve kterém se nachází daný soubor (v případě,
 * že daný soubor nachází v podadresáři / řích zdrojového adresáře). Nebo zda se
 * má zobrazit pouze přípona souboru (v případě, že se vybírají pouze typy
 * souborů pro přejmenování či vynechání)
 * 
 * @see Enum
 * 
 * @author Jan Krunčík
 * @version 1.0
 */

public enum PathForShow {

	/**
	 * Má se zobrazit pouze přípona souboru.
	 */
	SHOW_EXTENSION,

	/**
	 * Má se zobrazit název souboru i s příponou a cestou od zdrojového adresáře.
	 */
	SHOW_PATH_FROM_SOURCE_DIR
}
