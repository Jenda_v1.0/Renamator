package kruncik.jan.renamator.app;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.event.KeyEvent;
import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.*;

import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.JScrollPane;
import javax.swing.JSplitPane;
import javax.swing.JTabbedPane;

import org.apache.commons.io.FileUtils;
import org.apache.commons.io.FilenameUtils;

import kruncik.jan.renamator.jListWithJRadioButtons.ListItem;
import kruncik.jan.renamator.jListWithJRadioButtons.PathForShow;
import kruncik.jan.renamator.loadPictures.KindOfPicture;
import kruncik.jan.renamator.loadPictures.Pictures;
import kruncik.jan.renamator.localization.Constants;
import kruncik.jan.renamator.localization.Localization;
import kruncik.jan.renamator.localization.LocalizationImpl;
import kruncik.jan.renamator.panels.foldersPanel.FoldersPanel;
import kruncik.jan.renamator.panels.renamePanel.RenamePanel;
import kruncik.jan.renamator.panels.replacePanel.ReplacePanel;

/**
 * Tato třída slouží jako "hlavní" okno aplikace, což je vlastě okno samotné
 * aplikace.
 *
 * @see Properties
 * 
 * @author Jan Krunčík
 * @version 1.0
 * @since 1.8
 */

public class App extends JFrame implements LocalizationImpl {

	
	private static final long serialVersionUID = 1L;

	
	public static final String KIND_OF_OS = System.getProperty("os.name");
	
	


	
	/**
	 * Proměnná, která obsahuje texty pro aplikaci.
	 */
	private static Properties language;
	
	
	
	
	/**
	 * Menu aplikace.
	 */
	private final Menu menu;
	
	
	
	
	
	
	
	
	/**
	 * Proměnná, která obsahuje zvolený zdrojový adresář.
	 */
	private static File fileSourceDir;
	
	/**
	 * Proměnná, která obsahuje zvolený cílový adresář. Toto je ale volitelné, pokud
	 * se nenastaví, tak se nikam nebudou soubory ze zdrojového adresáře kopírovat.
	 */
	private static File fileDestinationDir;
	
	
	
	
	/**
	 * Proměnná, která je panel, který obsahuje komponenty pro nastavení zdrojového
	 * a cílového adresáře.
	 */
	private static FoldersPanel foldersPanel;
	
	
	
	
	
	
	
	/**
	 * Komponenta, která obsahuje záložky pro přejmenování a nahrazení textů v
	 * názvech souborů. Každá záložka pak obsahuje panely s komponentami pro dané
	 * přejmnování nebo nahrazení textů v názvech souborů.
	 */
	private final JTabbedPane tabbedPane;
	
	
	// Panely do komponenty tabbedPane:
	/**
	 * Proměnná coby panel s komponentami pro přejmenování souborů dle nastavených
	 * parametrů.
	 */
	private static RenamePanel renamePanel;
	
	/**
	 * Proměnná coby panel s komponentami pro nahrazení textů v názvech souborů.
	 */
	private static ReplacePanel replacePanel;
	
	
	
	/**
	 * Tento list slouží pouze pro dočasné odkládání některých hodnot při různých
	 * cyklech ohledně zjisťování souborů ve zvoleném adresáři apod.
	 */
	private static final List<ListItem> TEMP = new ArrayList<>();
	
	
	
	
	
	/**
	 * Proměnné pro texty do chybových hlášek.
	 */
	private static String txtSourceDirDoesntExistText, txtSourceDirDoesntExistTitle;
	
	
	


	/**
	 * Konstruktor této třídy.
	 * 
	 * @param title
	 *            - Titulek do okna aplikace
	 * @param width
	 *            - Přednastavená šířka okna
	 * @param height
	 *            - Přednastavená výška okna
	 */
	public App(final String title, final int width, final int height) {
		super(title);
		
		initGui(width, height);
		
		
		menu = new Menu(this);
		setJMenuBar(menu);
		
		
		
		
		foldersPanel = new FoldersPanel();		
		
		
		
		
		/**
		 * Texty pro aplikaci si musím načíst zde, abych je předal do panelů, kde jsou
		 * tabulky, protože tam potřebuji předat názvy sloupců ve zvoleném jazyce.
		 */
		final Properties properties = Localization.loadDefaultLocalizationFile(Constants.DEFAULT_LANGUAGE);
		
		
		
		
		tabbedPane = new JTabbedPane();

		renamePanel = new RenamePanel(properties, foldersPanel);
		tabbedPane.addTab("Přejmenovat",
				Pictures.loadImage("/tabbedPane/RenameIcon.png", KindOfPicture.TABBED_PANE_ICON), renamePanel);
		tabbedPane.setMnemonicAt(0, KeyEvent.VK_1);

		replacePanel = new ReplacePanel(properties, foldersPanel);
		tabbedPane.addTab("Nahradit", Pictures.loadImage("/tabbedPane/ReplaceIcon.png", KindOfPicture.TABBED_PANE_ICON),
				replacePanel);
		tabbedPane.setMnemonicAt(1, KeyEvent.VK_2);
		
		
		final JSplitPane jspPanels = new JSplitPane(JSplitPane.VERTICAL_SPLIT, true, new JScrollPane(foldersPanel),
				new JScrollPane(tabbedPane));
		jspPanels.setResizeWeight(0.26d);
		jspPanels.setOneTouchExpandable(true);
		
		add(jspPanels, BorderLayout.CENTER);







		/*
		  (Načteno výše)
		  Načtu si nastavený jazyk pro aplikaci - jelikož si neukládám žádné soubory
		  mimo aplikaci, například do nějakého adresáře apod. tak bude vždy výchozí
		  jakyz češtine, pokud nebude nastaveno jinak. Nelze brát výchozí nastavení z
		  nějakého souboru. Pro tuto aplikaci je to spíše práce navíc.
		 */
		setLoadedText(properties);
		
		
		
		pack();
		setLocationRelativeTo(null);
	}
	
	
	
	
	
	
		
	
	
	
	
	
	
	
	/**
	 * Metoda, která slouží pro nastavení GUi pro hlavní okno aplikace.
	 * 
	 * @param width
	 *            - přednastavená šířka okna
	 * @param height
	 *            - přednastavená výška okna
	 */
	private void initGui(final int width, final int height) {
		setDefaultCloseOperation(EXIT_ON_CLOSE);
		setPreferredSize(new Dimension(width, height));
		setIconImage(Pictures.loadImage("/appIcons/RenamatorAppIcon.png", KindOfPicture.APP_ICON).getImage());
		setLayout(new BorderLayout());
	}
	
	
	
	/**
	 * Getr na proměnnou, která obsahuje texty pro aplikaci ve zvoleném jazyce.
	 * 
	 * @return Properties s texty pro aplikaci.
	 */
	public static Properties getLanguage() {
		return language;
	}
	
	
	/**
	 * Metoda, která nastaví cílový adresář.
	 * 
	 * @param fileDestinationDir
	 *            - cesta k cílovému adresáři
	 */
	public static void setFileDestinationDir(final File fileDestinationDir) {
		App.fileDestinationDir = fileDestinationDir;
		
//		if (fileDestinationDir == null)
//			return;
//		
//		System.out.println("Nastavena cílová cesta: " + fileDestinationDir.getAbsolutePath());
	}
	
	/**
	 * Metoda, která nastaví zdrojový adresář.
	 * 
	 * @param fileSourceDir
	 *            - Cesta ke zdrojovému adresáři
	 * @param enable
	 *            - logická proměnná o tom, zda zdrojový adresář obsahuje nějaké
	 *            další adresáře nebo ne, true - obsahuje, false - neobsahuje
	 */
	public static void setFileSourceDir(final File fileSourceDir, final boolean enable) {
		App.fileSourceDir = fileSourceDir;
		
		/*
		 * Zpřístupním / znepřístupním komponenty pro označení manipulace s podadresáři
		 * (dle toho, zda se ve zvoleném zdrojovém adresáři (fileSourceDir nachází
		 * podadresáře nebo ne
		 */
		renamePanel.enableSubDirValues(enable);
		
		/*
		 * To samé udělám i pro omponenty v panelu pro nahrazení textu, pokud
		 * fileSourceDir neobsahuje podadresáře, tak je znepřístupním a naopak:
		 */
		replacePanel.enableChcbsForSubDirs(enable);
		
		
		// Pokud bylo nastaveno null, pak uživatel nezedal spravný adresář, správnou
		// syntaxi,
		// apod. Prostě není nastaven, tak mohu skončit zde:
		if (fileSourceDir == null) {
			// Zde potřebuji ještě vymazat již načtené soubory a přípony, protože,
			// teď "není zvolen adresář, který by existoval"
			renamePanel.setDataToListSkipFilesWithName(new ArrayList<>());			
			replacePanel.setDataToListSkipSpecificFiles(new ArrayList<>());
			
			
			renamePanel.setDataToListForFileExtension(new ArrayList<>());
			renamePanel.setDataToListForSkipFileWithExtension(new ArrayList<>());
			
			replacePanel.setDataToListSkipFilesWithExtensions(new ArrayList<>());
			return;
		}
			
		
		
		
		
		/*
		 * List, který obsahuje celé názvy souborů
		 */
		final List<ListItem> fileNamesList = getAllFilesInDir(fileSourceDir);
		
		// Smazat:
//		System.out.println("Všechny názvy:\n" + Arrays.toString(fileNamesList.toArray()));
//		System.out.println("Všechny názvy:\n" + fileNamesList.size());
		
		renamePanel.setDataToListSkipFilesWithName(copyList(fileNamesList));
		
		// Do následujícího panelu zkopíruji kolekci výše:
		replacePanel.setDataToListSkipSpecificFiles(copyList(fileNamesList));
		
		
		
		/*
		 * Zde si musím všechny přípony načíst vícekrát, protože se ve dvou listech
		 * nachází ty samé přípony ale kdybych tam rozkopírovat jeden stejný list, pak
		 * se bude jedna o stejné reference na jeden objekt, takže když bych jednu
		 * položku v listu označil, tak se označí i v tom druhém, což nechci.
		 */
		final List<ListItem> extensions = getAllExtensionsInDir(copyList(fileNamesList));
		
		// Smazat:
//		System.out.println("Přípony:\n" + Arrays.toString(extensions.toArray()));
//		System.out.println("Přípony:\n" + extensions.size());
		
		renamePanel.setDataToListForFileExtension(extensions);
		renamePanel.setDataToListForSkipFileWithExtension(copyList(extensions));
		
		replacePanel.setDataToListSkipFilesWithExtensions(copyList(extensions));
		
		
//		System.out.println("Nastavena zdrojová cesta: " + fileSourceDir.getAbsolutePath());
	}
	
	
	
	
	
	
	
	/**
	 * Metoda, která zkopíruje list list, je to potřeba, protože do více panelů v
	 * aplikaci vkládám list se stejnými daty, jenže, když se v jednom listu něco
	 * označí, pak by se to označilo i v tom druhém listu, resp. ve všech ostatních
	 * protože by byla stejná reference na daný objekt, proto potřebuji pouze
	 * zkopírovat jejich data a vytvořím tak nové objekty (pro každý list jeden).
	 * 
	 * @param list
	 *            - list s daty, který se má zkopírovat
	 * @return nový list, který obsahuje naprosto stejná data jako list list, akorát
	 *         to jsou nové objekty.
	 */
	private static List<ListItem> copyList(final List<ListItem> list) {
		final List<ListItem> copy = new ArrayList<>();

		list.forEach(v -> copy.add(new ListItem(v.getFile(), v.isSelected(), v.getPathForShow())));

		return copy;
	}
	
	
	
	
	
	
	
	/**
	 * Metoda, která projde všechny soubory v adresáři file a vloží jej do listu
	 * TEMP, který se pak vrátí.
	 * 
	 * @param file
	 *            - cesta k adresáři, z něhož se mají vložit všechny soubory do
	 *            listu TEMP.
	 * @return list TEMP, který obsahuje všechy soubory v adresáři file (včetně
	 *         souborů v jeho podadresářích).
	 */
	private static List<ListItem> getAllFilesInDir(final File file) {
		TEMP.clear();

		// (file nebude null, tak není třeba testovat)
		Arrays.asList(Objects.requireNonNull(file.listFiles()))
				.forEach(f -> addFilesToTempList(f, foldersPanel.getChcbCountHiddenFiles().isSelected()));

		return TEMP;
	}
	
	
	
	
	
	/**
	 * Metoda, která projde adresář, na který ukazuje proměnná file a načtě si z něj
	 * všechny soubory, a každý soubor přidá do listu TEMP - pokud se v něm ještě
	 * nevyskytuje (nemělo by nastat). Pokud se najde adresář, tak se pomocí rekurze
	 * zavolá metoda znovu a projdou se i soubory v tomto podadresáři.
	 * 
	 * @param file
	 *            - adresář, jehož soubory se mají vložit do listu TEMP.
	 * 
	 * @param countHiddenFiles
	 *            - Logická proměnná o tom, zda semají započítat skryté soubory nebo
	 *            ne. True - mají se započítat i skryté soubory, false - nemají se
	 *            započítat skryté soubory.
	 */
	private static void addFilesToTempList(final File file, final boolean countHiddenFiles) {
		/*
		 * Pokud se nemají započítat skryté soubory a je to skrytý soubor, pak skončím
		 * metodu.
		 */
		if (!countHiddenFiles && file.isHidden())
			return;
		
		/*
		 * Postup:
		 * - Je li to komporesni formát, tak to rovnou přidám do listu (pokud tam ještě není - nemělo by nastat).
		 * - Jestli je to adresář, tak pomocí rekurze tuto metodu volám znovu akorát se soubory z toho "dalšího" adresáře.
		 * - a pokud je to nějaký soubor, tak jej rovnou přidám do listu (pokud tam ještě není - nemělo by nastat).
		 */
		
		if (isFileCompressionFormat(file) && !containtsTempListFileWithPath(file.getAbsolutePath()))
			TEMP.add(new ListItem(file, false, PathForShow.SHOW_PATH_FROM_SOURCE_DIR));

		// (file nebude null, neni třeba testovat)
		else if (file.isDirectory())
			Arrays.asList(Objects.requireNonNull(file.listFiles())).forEach(f -> addFilesToTempList(f, countHiddenFiles));

		else if (!containtsTempListFileWithPath(file.getAbsolutePath()))
			TEMP.add(new ListItem(file, false, PathForShow.SHOW_PATH_FROM_SOURCE_DIR));
	} 
	
	
	
	
	
	/**
	 * Metoda, která zjistí, zda se jedá o soubor v kompresním formátu, tj. zda se
	 * jedná například o soubor .zip. .gz, .jar. rar apod.
	 * 
	 * @param file
	 *            - cesta k souboru o kterém se má zjistit, zda se jedná o
	 *            komporesní formát nebo ne.
	 * @return true, pokud se jedná o soubor v kompresním formátu, jinak false.
	 */
	public static boolean isFileCompressionFormat(final File file) {
		final String extension = FilenameUtils.getExtension(file.getAbsolutePath());

		return extension.endsWith("7z") || extension.endsWith("ace") || extension.endsWith("arj")
				|| extension.endsWith("axx") || extension.endsWith("bz2") || extension.endsWith("gz")
				|| extension.endsWith("jar") || extension.endsWith("lha") || extension.endsWith("r00")
				|| extension.endsWith("r01") || extension.endsWith("r03") || extension.endsWith("r04")
				|| extension.endsWith("r05") || extension.endsWith("r06") || extension.endsWith("r07")
				|| extension.endsWith("r08") || extension.endsWith("r09") || extension.endsWith("r10")
				|| extension.endsWith("rar") || extension.endsWith("tar") || extension.endsWith("z01")
				|| extension.endsWith("z02") || extension.endsWith("z03") || extension.endsWith("z04")
				|| extension.endsWith("z05") || extension.endsWith("z06") || extension.endsWith("z07")
				|| extension.endsWith("z08") || extension.endsWith("z09") || extension.endsWith("z10")
				|| extension.endsWith("zip") || extension.endsWith("zipx") || extension.endsWith("ar")
				|| extension.endsWith("cbz") || extension.endsWith("ear") || extension.endsWith("exe")
				|| extension.endsWith("lzma") || extension.endsWith("xz") || extension.endsWith("war");

	}
	
	
	
	
	
	
	
	/**
	 * Metoda, která zjistí, zda se v list TEMP nachází proměnná, která ukazuje na
	 * soubor na cestě path.
	 * 
	 * @param path
	 *            - cesta k nějakému souboru, o kterém chci vědět, zda na něj
	 *            ukazuje ještě nějaká hodnota v listu TEMP.
	 * @return true, pokud v listu TEMP je objekt, který ukazuje na soubor na cestě
	 *         path, jinak false.
	 */
	private static boolean containtsTempListFileWithPath(final String path) {
		return TEMP.stream().anyMatch(f -> f.getFile().getAbsolutePath().equals(path));
	}
	
	
	
	
	
	
	
	/**
	 * "Metoda slouží pro zjištění všech typů souborů - jejich přípon".
	 * 
	 * Metoda projde list list, který obsahuje veškeré soubory načtené ze zdvoleného
	 * zdrojového adresáře. Už by se zde neměly nacházet žádné adresáře, pouze
	 * soubory ze všech jeho podadresářů. U každého souboru se už nemusí testovat
	 * zda je skrytý nebo ne, to už bylo rozhodnuto u načtení souborů. Pak se zjistí
	 * přípona souboru a pokud není prázdná, pak se zjistí, zda se jedná o kompresní
	 * formát a pokud se v kolekci ještě nenachází, tak jej přidám. Pokud to není
	 * kompresní formát, tak se dle přípony pozná zda se daná přípona již nachází v
	 * list nebo ne a pokud ne, tak se tam přidá.
	 * 
	 * Note: To, zda se jedá o kompresní formát, bych ani testovat nemusel, ale
	 * původně jsem měl tuto metodu napsanou na rekurzi, kde jsem potřeboval
	 * testovat, zda je to kompresní formát, abych jej neprocházel jako adresář. Teď
	 * už to potřeba není, protože se zjistí přípona tak jako tak, ale pro jistotu
	 * jsem to zde nechal kdybych to v budoucnu nějak měnil apod. Přece jen je to
	 * jen jedna část podmínky a nepočítá se s miliónem souboru pro přejmenování a
	 * navíc je to jen načtení informací o souborech, tak to ani moc velké zdržení
	 * nebude.
	 * 
	 * @param list
	 *            - Kolekce, která obsahuje veškeré soubory, které byly načteny z
	 *            zdrojového adresáře.
	 * 
	 * @return list, který obsahuje všechny přípony souborů (každou příponu pouze
	 *         jednou). Pokud bude adresář prázdný, vrátí se prázdný list.
	 */
	private static List<ListItem> getAllExtensionsInDir(final List<ListItem> list) {
		TEMP.clear();

		
		list.forEach(f -> {
			// Získám si příponu souboru:
			final String extension = FilenameUtils.getExtension(f.getFile().getAbsolutePath());

			/*
			 * Některé soubory nemají příponu, jako například některé soubory v adresáři
			 * eclipsu, pak je ani nebudu přidávat do aplikace na výběr.
			 * 
			 * Adresáře také nemají příponu, takže pokud se jedná o soubor, který nemá
			 * příponu - je prázdná, pak jej nebudu přidávat.
			 */
			
			if (extension.isEmpty()) {
				// Zde se jedná o prázdnou příponu a není to adresář, tak "skončím", jedná se o
				// prázdnou příponu, kterou nechci přidat.
				return;// Přeskočím soubor - tuto iteraci
			}
			

			if (isFileCompressionFormat(f.getFile()) && !containsListFileWithExtension(TEMP, extension))
				TEMP.add(new ListItem(f.getFile(), false, PathForShow.SHOW_EXTENSION));

			
			else if (!containsListFileWithExtension(TEMP, extension))
				TEMP.add(new ListItem(f.getFile(), false, PathForShow.SHOW_EXTENSION));
		});
		
		
		return TEMP;
	}
	



	
	
	
	
	
	
	
	/**
	 * Metoda, která zjistí, zda zadaný list list obsahuje soubor, který obsahuje
	 * příponu extension.
	 * 
	 * @param list
	 *            - list, který obsahuje proměnné typu file - cesty k souborům
	 * @param extension
	 *            - přípona nějakého souboru, o kterém se má zjistit, zda se tato
	 *            přípona již nachází v listu list.
	 * @return logickou hodnotu o tom, zda se přípona extension, resp. soubor s
	 *         touto příponou nachází v listu list nebo ne. True nachází, false
	 *         nenachází.
	 */
	private static boolean containsListFileWithExtension(final List<ListItem> list, final String extension) {
		return list.stream()
				.anyMatch(f -> FilenameUtils.getExtension(f.getFile().getAbsolutePath()).equalsIgnoreCase(extension));
	}
	
	
	
	
	/**
	 * Metoda, která vrátí cestu ke zvolenému zdrojovému adresáři.
	 * 
	 * @return cesta ke zvolenému zdrojovému adresáři.
	 */
	public static File getFileSourceDir() {
		return fileSourceDir;
	}
	
	
	
	/**
	 * Metoda, která vrátí cestu ke zvolenému cílovému adresáři
	 * 
	 * @return cesta k cílovému adresáři.
	 */
	public static File getFileDestinationDir() {
		return fileDestinationDir;
	}
	
	
	
	
	
	
	
	
	
	
	
	/**
	 * Metoda slouží pro kontrolu zdrojového a cílového adresáře.
	 * 
	 * Metoda, která provede: - Zjistí, zda ještě existuje zdrojový adresář, pokud
	 * ne, vyhodí oznámení uživateli.
	 * 
	 * - Dále se zjistí, zda je zadán cílový adresář, pokud je pole prázdné, nic se
	 * neřeší, pokud je ale nějaký adresář zadán, pak musí být i ve správné syntaxi,
	 * tak zjistím, zda existuje a pokud ne, tak se vytvoří, vytvoří se všechny
	 * adresáře na zadané cestě, ale pouze adresáře.
	 * 
	 * @return true, pokud zdrojový adresář existuje a cýlový adresář, pokud byl
	 *         zadán, tak již existuje (pokud neexistoval), jinak false, pokud
	 *         zdrojový adresář neexistuje, nebo došlo k chybě při pokusu o
	 *         vytvoření cílového adresáře (/ adresářů)
	 */
	public static boolean checkDirs() {
		if (fileSourceDir != null && !fileSourceDir.exists()) {
			JOptionPane.showMessageDialog(null, txtSourceDirDoesntExistText, txtSourceDirDoesntExistTitle,
					JOptionPane.ERROR_MESSAGE);
			return false;
		}
		
		
		if (fileDestinationDir != null && !fileDestinationDir.exists()) {
			try {
				Files.createDirectories(Paths.get(fileDestinationDir.getAbsolutePath()));
			} catch (IOException e) {
				e.printStackTrace();
				return false;
			}
		}
		
		return true;
	}
	
	
	
	
	
	
	/**
	 * Metoda, která zkopíruje veškeré soubory ve zdrojovém adresáři do cílového
	 * adresáře. Pokud se v cílovém adresáři nějaké soubory nacházejí, pak budou
	 * přepsány.
	 * 
	 * Metodu je vhodné využít až po zavolání metody checkDirs, protože tyto metoda
	 * už netestuje existenci adresářů apod. Takže pokud zdrojový nebo cílový
	 * adresář neexistuje, dojde k chybě.
	 */
	public static boolean copySourceDirToDestinationDir() {
		// Tato metoda také vytvoří adresář, pokud neexistuje, ale raději jsem to
		// rozdělil pro přehlednost, i když to není třeba..
		try {
			FileUtils.copyDirectory(fileSourceDir, fileDestinationDir, true);
			
			// SOubory es v pořádku zkopírovaly, vrátím true:
			return true;
		} catch (IOException e) {
			e.printStackTrace();
			return false;
		}
	}







	
	
	/**
	 * Metoda, která nastaví jazyk do všech částí aplikace.
	 * 
	 * Tato proměnná nesmí být null, pokud bude, nic se nestane, jazyk se nenastaví.
	 * 
	 * @param prop
	 *            - proměnná typu Properties, která obsahuje texty pro aplikaci ve
	 *            zvoleném jazyce.
	 */
	@Override
	public void setLoadedText(final Properties prop) {
		if (prop != null) {
			language = prop;
			
			// Nastavení textů a popisků pro lištu:
			tabbedPane.setTitleAt(0, prop.getProperty("App_TP_Rename_Tab_text", Constants.APP_TP_RENAME_TAB_TEXT));
			tabbedPane.setTitleAt(1, prop.getProperty("App_TP_Replace_Tab_Text", Constants.APP_TP_REPLACE_TAB_TEXT));
			
			tabbedPane.setToolTipTextAt(0, prop.getProperty("App_TP_Rename_Tab_TT", Constants.APP_TP_RENAME_TAB_TT));
			tabbedPane.setToolTipTextAt(1, prop.getProperty("App_TP_Replace_Tab_TT", Constants.APP_TP_REPLACE_TAB_TT));
			
			txtSourceDirDoesntExistText = prop.getProperty("App_Jop_Source_Dir_Doesnt_Exist_Text", Constants.TXT_JOP_SOURCE_DIR_DOESNT_EXIST_TEXT);
			txtSourceDirDoesntExistTitle = prop.getProperty("App_Jop_Source_Dir_Doesnt_Exist_Title", Constants.TXT_JOP_SOURCE_DIR_DOESNT_EXIST_TITLE);
			
			menu.setLoadedText(language);
			foldersPanel.setLoadedText(language);
			
			renamePanel.setLoadedText(language);
			replacePanel.setLoadedText(language);
		}
		
		else {
			/*
			 * Zde se nenastavil text pro komponenty v aplikaci, ale musím nastavit alespoň výchozí texty do některých komponent:
			 */
			// Nastavení textů a popisků pro lištu:
			tabbedPane.setTitleAt(0, Constants.APP_TP_RENAME_TAB_TEXT);
			tabbedPane.setTitleAt(1, Constants.APP_TP_REPLACE_TAB_TEXT);
			
			tabbedPane.setToolTipTextAt(0, Constants.APP_TP_RENAME_TAB_TT);
			tabbedPane.setToolTipTextAt(1, Constants.APP_TP_REPLACE_TAB_TT);
			
			txtSourceDirDoesntExistText = Constants.TXT_JOP_SOURCE_DIR_DOESNT_EXIST_TEXT;
			txtSourceDirDoesntExistTitle = Constants.TXT_JOP_SOURCE_DIR_DOESNT_EXIST_TITLE;
		}
	}
}
