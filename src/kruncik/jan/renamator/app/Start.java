package kruncik.jan.renamator.app;

import javax.swing.SwingUtilities;

import kruncik.jan.renamator.localization.Constants;
import kruncik.jan.renamator.localization.Localization;
import kruncik.jan.renamator.localization.LocalizationLanguage;

/**
 * Tato třída slouží jako spuštěcí třída pro tuto aplikaci.
 * see Lambda Expressions
 * 
 * @author Jan Krunčík
 * @version 1.0
 */

public class Start {

	private static final int WIDTH = 760, HEIGHT = 700;

	public static void main(String[] args) {
		SwingUtilities.invokeLater(() -> new App(Constants.TITLE, WIDTH, HEIGHT).setVisible(true));
		

		
		
		// Vytvoření výchozích lokalizačních souborů		
//		final String pathToRenamatorDir = System.getProperty("user.dir");
//		
//		final String pathForCzTextFile = pathToRenamatorDir + "/localization/CZ.properties";
//		Localization.createLocalizationFile(pathForCzTextFile, LocalizationLanguage.CZ);
//		
//		
//		final String pathForEnTextFile = pathToRenamatorDir + "/localization/EN.properties";
//		Localization.createLocalizationFile(pathForEnTextFile, LocalizationLanguage.EN);
	}
}
