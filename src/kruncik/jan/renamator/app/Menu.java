package kruncik.jan.renamator.app;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.InputEvent;
import java.awt.event.KeyEvent;
import java.util.Properties;

import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.KeyStroke;

import kruncik.jan.renamator.fileExplorer.SizeEnum;
import kruncik.jan.renamator.forms.AboutAppForm;
import kruncik.jan.renamator.forms.AboutAuthorForm;
import kruncik.jan.renamator.forms.settingsForm.SettingsForm;
import kruncik.jan.renamator.loadPictures.KindOfPicture;
import kruncik.jan.renamator.loadPictures.Pictures;
import kruncik.jan.renamator.localization.Constants;
import kruncik.jan.renamator.localization.LocalizationImpl;

/**
 * Tato třída slouží jako menu pro hlavní okno aplikace.
 * 
 * Aplikace
 * 	- Nastavení (jazyk, pozadi ???)
 * 	- Zavřít
 * 
 * 
 * Info
 * 	- Informace
 * 	- Autor
 * 
 * @see JMenuBar
 * @see Properties
 * 
 * @author Jan Krunčík
 * @version 1.0
 * @since 1.8
 */

public class Menu extends JMenuBar implements ActionListener, LocalizationImpl {
	
	private static final long serialVersionUID = 1L;

	
	/**
	 * Logická proměnná, do které se ukládá hodnota true nebo false, dle toho, zda
	 * se mají v průzkumníku souborů vlevo v komponentě Jtree zobrazovat skryté
	 * soubory nebo ne.
	 */
	private static boolean showHiddenFilesInJtree = false;
	
	
	
	/**
	 * Proměnné výčtového typu, dle které se v tabulce se soubory v průzkumníku
	 * souborů zobrazí příslušný formát velikost souborů (v KB, MB, TB, ...
	 */
	private static SizeEnum showSizeInJTable = SizeEnum.KB;
	
	
	
	
	
	
	
	// Menu:
	private final JMenu menuApp, menuInfo;
	
	
	
	
	// Tlačítka do menu:
	
	// Tlačítka do menuApp:
	private final JMenuItem itemSettings, itemClose;
	
		
	// Tlačítka do menuInfo:
	private final JMenuItem itemInfoAboutApp, itemInfoAuthor;
	
	
	
	
	/**
	 * Proměnná, která obsahuje reference na instanci třídy App, aby se tam mohla
	 * zavolatmetoda pro nastavení jazyka pro aplikaci.
	 */
	private final App app;
	
	
	
	/**
	 * Konstruktor této třídy.
	 * 
	 * @param app
	 *            - reference na instanci třídy App, kvůli předání do dialogu pro
	 *            nastavení aplikace, konkrétně do panelu pro nastavení jazyka pro
	 *            aplikaci, aby se mohla zvolat metoda pro nastavení jazyka.
	 */
	Menu(final App app) {
		super();		
		
		this.app = app;
		
		menuApp = new JMenu();
		menuInfo = new JMenu();
		
		
		
		// Položky do menu menuApp
		itemSettings = new JMenuItem();
		itemClose = new JMenuItem();		
		
		itemSettings.addActionListener(this);
		itemClose.addActionListener(this);
		
		menuApp.add(itemSettings);
		menuApp.addSeparator();
		menuApp.add(itemClose);
		

		
		
		// Položky do menu menuInfo
		itemInfoAboutApp = new JMenuItem();
		itemInfoAuthor = new JMenuItem();
		
		itemInfoAboutApp.addActionListener(this);
		itemInfoAuthor.addActionListener(this);
		
		menuInfo.add(itemInfoAboutApp);
		menuInfo.addSeparator();
		menuInfo.add(itemInfoAuthor);
		
		
		
		
		setIconsToMenuItems();
		
		setKeyShortcutsToMenuItems();
		
		
		
		add(menuApp);
		add(menuInfo);
	}
	
	
	
	
	
	
	
	
	/**
	 * Metoda, která nastaví ikony tlačítkům v menu.
	 */
	private void setIconsToMenuItems() {
		// Tlačítka do menuApp:
		itemSettings.setIcon(Pictures.loadImage("/appMenu/SettingsIcon.png", KindOfPicture.MENU_ICON));
		itemClose.setIcon(Pictures.loadImage("/appMenu/CloseIcon.png", KindOfPicture.MENU_ICON));
		
		
		// Tlačítka do menuInfo:
		itemInfoAboutApp.setIcon(Pictures.loadImage("/appMenu/InfoIcon.png", KindOfPicture.MENU_ICON));
		itemInfoAuthor.setIcon(Pictures.loadImage("/appMenu/AuthorIcon.png", KindOfPicture.MENU_ICON));
	}
	
	
	
	
	
	
	
	/**
	 * Metoda, která nastaví klávesové zkratky tlačítkům v menu.
	 */
	private void setKeyShortcutsToMenuItems() {
		// Tlačítka do menuApp:
		itemSettings.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_N, InputEvent.CTRL_DOWN_MASK));
		itemClose.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_W, InputEvent.CTRL_DOWN_MASK));
		
		
		// Tlačítka do menuInfo:
		itemInfoAboutApp.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_I, InputEvent.CTRL_DOWN_MASK));
		itemInfoAuthor.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_A, InputEvent.CTRL_DOWN_MASK));
	}
	
	
	
	
	
	
	/**
	 * Metoda, která vrátí hodnotu v proměnné showHiddenFilesInJtree, která značí,
	 * zda se mají zobrazit skryté soubory v komponentě JTree - ve stromové
	 * struktuře souborů v průzkumníku souboru nebo ne.
	 * 
	 * @return showHiddenFilesInJtree
	 */
	public static boolean isShowHiddenFilesInJtree() {
		return showHiddenFilesInJtree;
	}
	
	
	
	
	/**
	 * Metoda, která nastaví hodnotu proměnné showHiddenFilesInJtree na true nebo
	 * false.
	 * 
	 * @param showHiddenFilesInJtree
	 *            - true mají se zobrazovat skryté soubory v Jtree, false nemají se
	 *            zobrazovat skryté soubory.
	 */
	public static void setShowHiddenFilesInJtree(boolean showHiddenFilesInJtree) {
		Menu.showHiddenFilesInJtree = showHiddenFilesInJtree;
	}
	
	
	
	/**
	 * Metoda, která vrátí hodnotu v proměnné showSizeInJtable, která obsahuje
	 * výčtovou hodnotu o tom, jaký formát velikosti souborů se má zobrazit v
	 * tabulce souborů v průzkumníku souborů.
	 * 
	 * @return výše popsanou hodnotu výčtového typu.
	 */
	public static SizeEnum getShowSizeInJTable() {
		return showSizeInJTable;
	}

	/**
	 * Metoda,která nastaví hodnotu showSizeInJtable, která obsahuje hodnotu
	 * výčtového typu, která definuje jaký formát velikosti souborů se má nastavit
	 * do JTable v průzkumníku souborů.
	 * 
	 * @param showSizeInJTable
	 *            - výše popsaná proměnná výčtového typu.
	 */
	public static void setShowSizeInJTable(SizeEnum showSizeInJTable) {
		Menu.showSizeInJTable = showSizeInJTable;
	}
	



	
	
	@Override
	public void actionPerformed(ActionEvent e) {
		if (e.getSource() instanceof JMenuItem) {
			// Tlačítka do menuApp:
			if (e.getSource() == itemSettings) {
				// Zobrazím dialog pro nastavení aplikace:
				final SettingsForm settingsForm = new SettingsForm(app);
				settingsForm.setLoadedText(App.getLanguage());				
			}


			else if (e.getSource() == itemClose)
				// Zavření aplikace.
				System.exit(0);
			
			
				
			// Tlačítka do menuInfo:
			else if (e.getSource() == itemInfoAboutApp)
				new AboutAppForm().setLoadedText(App.getLanguage());

			else if (e.getSource() == itemInfoAuthor)
				new AboutAuthorForm().setLoadedText(App.getLanguage());
		}
	}




	
	
	
	@Override
	public void setLoadedText(final Properties prop) {
		if (prop != null) {		
			// Menu:
			menuApp.setText(prop.getProperty("Menu_Menu_App", Constants.MENU_MENU_APP));
			menuInfo.setText(prop.getProperty("Menu_Menu_Info", Constants.MENU_MENU_INFO));						
			
			// Tlačítka do menuApp:
			itemSettings.setText(prop.getProperty("Menu_Item_Settings", Constants.MENU_ITEM_SETTINGS));
			itemClose.setText(prop.getProperty("Menu_Item_Close", Constants.MENU_ITEM_CLOSE));
			
			itemSettings.setToolTipText(prop.getProperty("Menu_TT_Item_Setting", Constants.MENU_TT_ITEM_SETTING));
			itemClose.setToolTipText(prop.getProperty("Menu_TT_Item_Close", Constants.MENU_TT_ITEM_CLOSE));
			
				
			// Tlačítka do menuInfo:
			itemInfoAboutApp.setText(prop.getProperty("Menu_Item_Info_About_App", Constants.MENU_ITEM_INFO_ABOUT_APP));
			itemInfoAuthor.setText(prop.getProperty("Menu_Item_Info_Author", Constants.MENU_ITEM_INFO_AUTHOR));
			
			itemInfoAboutApp.setToolTipText(prop.getProperty("Menu_TT_Item_Info_About_App", Constants.MENU_TT_ITEM_INFO_ABOUT_APP));
			itemInfoAuthor.setToolTipText(prop.getProperty("Menu_TT_Item_Info_Author", Constants.MENU_TT_ITEM_INFO_AUTHOR));
		}
		
		
		
		else {
			// Menu:
			menuApp.setText(Constants.MENU_MENU_APP);
			menuInfo.setText(Constants.MENU_MENU_INFO);						
			
			// Tlačítka do menuApp:
			itemSettings.setText(Constants.MENU_ITEM_SETTINGS);
			itemClose.setText(Constants.MENU_ITEM_CLOSE);
			
			itemSettings.setToolTipText(Constants.MENU_TT_ITEM_SETTING);
			itemClose.setToolTipText(Constants.MENU_TT_ITEM_CLOSE);
			
				
			// Tlačítka do menuInfo:
			itemInfoAboutApp.setText(Constants.MENU_ITEM_INFO_ABOUT_APP);
			itemInfoAuthor.setText(Constants.MENU_ITEM_INFO_AUTHOR);
			
			itemInfoAboutApp.setToolTipText(Constants.MENU_TT_ITEM_INFO_ABOUT_APP);
			itemInfoAuthor.setToolTipText(Constants.MENU_TT_ITEM_INFO_AUTHOR);
		}
	}
}
