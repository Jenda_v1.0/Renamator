package kruncik.jan.renamator.fileExplorer;

import java.awt.BorderLayout;
import java.awt.FlowLayout;
import java.awt.Point;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.event.MouseMotionAdapter;
import java.io.File;
import java.util.*;

import javax.swing.BorderFactory;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JSplitPane;
import javax.swing.JTable;
import javax.swing.JToolBar;

import org.apache.commons.io.FilenameUtils;

import kruncik.jan.renamator.app.App;
import kruncik.jan.renamator.fileExplorer.jTree.FileTreeNode;
import kruncik.jan.renamator.fileExplorer.jTree.Tree;
import kruncik.jan.renamator.forms.AbstractForm;
import kruncik.jan.renamator.loadPictures.Pictures;
import kruncik.jan.renamator.localization.Constants;
import kruncik.jan.renamator.localization.LocalizationImpl;
import kruncik.jan.renamator.panels.foldersPanel.FoldersPanel;

/**
 * Tato třída slouží jako dialog coby "průzkumník souborů", ale obsahuje pouze sloupec se stromovou strukturou
 * souborů na disku a informace o souborech ve zvoleném adresáři.
 * Dále nabízí zvolit označený adresář jako zdrojový nebo cílový.
 * 
 * 
 * nekam nahoru dodelat i asi toolbar s moznostmi pro smazani a treba presunuti souboru
 * 
 * Okno:
 * levy sloupec - korenova						pole pro zadani cesty do nejakeho adresare 
 * struktura souboru 					tady bude asi tabulka nebo neco s informacemi o souborech
 * jako v BC App						pokud se bude jednat o obrazek, tak zkusit jeho nahled,
 * 										a u ostatnich souborů doplnit ikonu!
 * 
 * Ještě bude toolbar pod polem pro zadání cesty a nad tabulkou, kde budou možnosti pro smazani oznacenych polozek,
 * Vytvoření nové položky -> otevře se pole pro zadání návzu soiuboru i s příponou a přejmenování jedne označené
 * položky -> otevre se okno pro zadaní nového nazvu včetně přípony a dat tam třeba moznost bez pripony, pak se
 * vezme původni pripona.
 * 
 * 
 * @see Properties
 * @see FileTreeNode
 * @see Tree
 * @see JTable
 * 
 * @author Jan Krunčík
 * @version 1.0
 * @since 1.8
 */

public class FileExplorer extends AbstractForm implements LocalizationImpl {
	
	
	
	private static final long serialVersionUID = 1L;

	
	
	/**
	 * Do této komponenty se vloží tabulka, která bude zobrazovat informace o
	 * položkách, které se nachází ve zvoleném adresáři.
	 * 
	 * Je zde jako instanční / globální proměná, protože k ní chci nastavit titulek (zbytečnost).
	 */
	private final JScrollPane jspTable;
	
	
	
	
	/**
	 * Tento table model slouží jako model pro tabulku, která obsahuje vždy seznam
	 * položek ve zvoleném adresáři.
	 * 
	 * Je zde potřeba, aby se vždy nastavily hodnoty do modelu / tabulky.
	 */
	private static FileTableModel tableModel;
	
	
	/**
	 * Tato proměnná slouží pouze pro "dočasné" uložení označeného adresáře v
	 * komponentě Jtree v třídě Tree.
	 * 
	 * Jedná se o to, že po kliknutí na nějaký adresář v Jtree se načtou všechny
	 * jeho položky ale uživatel může nějaké vyfiltrovat, a pak chce ten fultr
	 * zrušit, pak ty položky potřebuji nějak načist zpět.
	 * 
	 * Jedna možnost je držet si list se všemi poožkami, to by bylo úplně optimální,
	 * jenže mezi tím může uživatel například změnit název nějakého souboru či
	 * adresáře, něbo něco smazat či přidat apod. Proto je potřeba si všechny
	 * pooložky znovu načíst.
	 */
	private static File fileSelectedDirInTree;
	
	
	/**
	 * Proměnná, do které si budu ukládat zvolený typ souorů, které se mají zobrazit
	 * v tabulce, například skryté soubory, viditelné soubnory nebo všechny apod.
	 */
	private static ShowFilesEnum showFilesEnum;
	
	
	
	
	
	
	
	
	/**
	 * Proměnná, která slouží jako toolbar, který obsahuje komponenty pro filtrování
	 * položek v označeném adresáři.
	 */
	private static FilterToolBar filterToolbar;
	
	
	
	
	/**
	 * Proměnná, do které se uloží při nájezdu myší nad řádek tabulky, který
	 * reprezentuje nějaký obrázek ve zvoleném adresáři, tak se do této proměnné
	 * vloží okno, kde je obrázek coby náhled na daný obrázek a cesta k jeho
	 * rodičovskému adresáři.
	 */
	private ImageView imageView;
	
	
	
	
	
	
	
	/**
	 * Konstruktoe této třídy.
	 * 
	 * @param foldersPanel
	 *            - reference na instanci třídy FoldersPanel, kterou potřebuji před
	 *            do popupMenu, abych tam mohl nastavit cestu ke zdrjovému neo
	 *            cílovému adresáři, pokud jej uživatel zvolí.
	 * 
	 * @param showHiddenFiles
	 *            - logická proměnná o tom, zda se mají v JTree vlevo - ve stromové
	 *            struktuře souborů na disku zobrazovat skryté soubory nebo ne.
	 *            True, mají se zobrazit skryté soubor, jinak false.
	 */
	public FileExplorer(final FoldersPanel foldersPanel, final boolean showHiddenFiles) {
		super();
		
		initGui(1050, 600, new BorderLayout(), "/fileExplorerIcons/FileExplorer.png");
		
		
		
		// Struktura pro levé menu - struktura souborů v PC:
		final File[] roots = File.listRoots();
		
		final FileTreeNode rootTreeNode = new FileTreeNode(roots, showHiddenFiles);
		final Tree tree = new Tree(rootTreeNode, foldersPanel);
		tree.setLoadedText(App.getLanguage());
		
		// Jsp pro stromovou strukturu:
		final JScrollPane jspTree = new JScrollPane(tree);		
		
		
		
		
		
		
		// Tabulka s daty (instanci je třeba vytvořit zde kvůli předání reference):
		final JTable table = new JTable();
		tableModel = new FileTableModel(table);
		
		
		
		
		
		
		
		
		// Akce pro editace a mazání souborů a vytvoření souborů:
		final AbstractActions aa = new AbstractActions(tableModel);
		aa.setLoadedText(App.getLanguage());
		
		
		// Menu:
		final Menu menu = new Menu(this);
		menu.setLoadedText(App.getLanguage());
		
		setJMenuBar(menu);
		
		
		
		
		
		
		

		// Operace nad tabulkou:
		table.setModel(tableModel);		
		
		// Událost pro zobrazení náhledu obrázku:
		table.addMouseMotionListener(new MouseMotionAdapter() {
			
			@Override
			public void mouseMoved(MouseEvent e) {
				final Point point = e.getPoint();
				
				// Řádek, kde je myš:
				final int row = table.rowAtPoint(point);
				
							
				/*
				 * Zde, pokud je obrázek zobrazen, tak jej zavřu, protože se už třeba namá
				 * zobrazit obrázek, pokud ano, tak se případně znovu zobrazí.
				 */
				if (imageView != null) {
					imageView.dispose();
					imageView = null;
				}
				
				
				if (row == -1)
					return;		
										
				
				final File file = tableModel.getFileTableValueAt(row).getFile();
				
				/*
				 * Otestuji, zda byl získán soubor, zda existuje a není to adresář (zbytečné,
				 * pro urachleí) a zda je to obrázek -> u toho ještě potřebuji zvlášť testovat,
				 * zda je to soubor s příponou .png, protože ta u metoduy pro získání obrázku
				 * neprojde.
				 */
				if (file != null && file.exists() && !file.isDirectory() && (Pictures.isImageForShow(file)
						|| FilenameUtils.getExtension(file.getName()).equals("png"))) {
					/*
					 * Zde mohu zobrazit obrázek ve větší velikosti.
					 */
					imageView = new ImageView(file, (int) point.getX(), (int) point.getY());
				}	
			}
		});		
		
		/*
		 * Toto je potřeba, když se z tabulky odjede myší, tak může být zobrazen nějaký
		 * obrázek, tak jej musím zavřít, aby nebyl i tak zobrazen.
		 */
		table.addMouseListener(new MouseAdapter() {
			
			@Override
			public void mouseExited(MouseEvent e) {
				if (imageView != null) {
					imageView.dispose();
					imageView = null;
				}
			}
		});
		
		jspTable = new JScrollPane(table);	
		
		
		
		
		
		
		

		
		
		// Toolbar:
		final JToolBar toolbar = new JToolBar();
		toolbar.setLayout(new FlowLayout(FlowLayout.CENTER, 15, 0));
		
		toolbar.add(AbstractActions.getActCreateFile());
		toolbar.addSeparator();		
		toolbar.add(AbstractActions.getActCreateDir());
		toolbar.addSeparator();
		toolbar.add(AbstractActions.getActEditFile());
		toolbar.addSeparator();
		toolbar.add(AbstractActions.getActDeleteFile());
		
		
		
		
		
			
		
		
		
		
		// Toolbar pro vyhledávání v tabulce:
		filterToolbar = new FilterToolBar(tableModel);		
		filterToolbar.setLoadedText(App.getLanguage());
		
		
		
		
		
		
		
		
		// Toolbar pro označování položek a skrývání / odkrývání souborů v tabulce
		final ToolBarTableButtons tableToolbar = new ToolBarTableButtons(tableModel);
		tableToolbar.setLoadedText(App.getLanguage());
		
		
		
		
			
		
		
		
		
		
		
		// Panel pro toolbar s tlačítky pro editaci, toolbar s filtry a tabulkou:
		final JPanel pnlRightSide = new JPanel(new BorderLayout());
		
		// Panel puze pro toolbary, aby v okně nebyla mezera nad tabulkou:
		final JPanel pnlToolbars = new JPanel(new BorderLayout());
		pnlToolbars.add(toolbar, BorderLayout.NORTH);
		pnlToolbars.add(filterToolbar, BorderLayout.CENTER);
		pnlToolbars.add(tableToolbar, BorderLayout.SOUTH);
		
		pnlRightSide.add(pnlToolbars, BorderLayout.NORTH);
		pnlRightSide.add(jspTable, BorderLayout.CENTER);
		
		
		
		
		
		
		
		
		/*
		 * Komponena, do které vložím stromovou strukturu (doleva) a doprava panel,
		 * který obsahuje tabulku, toolbar a položky pro vyhledávání adresářů apod.
		 */
		final JSplitPane jspPanels = new JSplitPane(JSplitPane.HORIZONTAL_SPLIT, true, jspTree, pnlRightSide);
		jspPanels.setResizeWeight(0.3);
		jspPanels.setOneTouchExpandable(true);
		
		// Přidání v podstatě všech objektů do okna dialogu:
		add(jspPanels, BorderLayout.CENTER);
	}

	
	
	
	
	
	/**
	 * Metoda,která zobrazí tento dialog a ještě před tím přizpůsobní okno velikost
	 * komponentám a zarovná okno na střed obrazovky.
	 * 
	 * Je to zde takto uděláno, protože potřebuji nastavit text ještě před tím, než
	 * se zobrazí dialog. protože se pak stane modálním.
	 */
	public final void showDialog() {
		showWindow();
	}
	
	
	
	
	
	
	
	
	/**
	 * Metoda, která nastaví hodnoty do tabulky - table modelu.
	 * 
	 * Z parametru si převede všechny soubory do takové podoby - do nových objektů,
	 * které je možné zobrazit v tabulce, pak jej vloží do tabulky.
	 */
	public static void setDataToTable() {
		if (fileSelectedDirInTree != null) {

			/*
			 * do proměnné si uložím, jaký typ souborů se má zobrazit.
			 */
			fillSelectedKindOfFiles();

			// a zrovna vyfiltruji pouze ty, která tam být mají dle nastavených parametrů:
			tableModel.filterData(showFilesEnum, getTableValuesFromSelectedDir());
		}
	}
	
	
	
	
	
	
	
	/**
	 * Meotda, která naplní proměnnou showFilesEnum, která bude obsahovat zvolený
	 * typ souborů, které se mají zobrazit v tabulce - skryté, viditelné apod.
	 * 
	 * Jedná se o získanou hodnotu z cmb, ve kterém si uživatel tuto hodotu zvolil
	 * pouze převedenou na výčet.
	 */
	static void fillSelectedKindOfFiles() {
		showFilesEnum = filterToolbar.getSelectedKindOfFiles();
	}
	
	
	
	
	/**
	 * Metoda, která souží pouze jako getr na proměnnou výčtového typu (proměnná
	 * showFilesEnum), který obsahuje zvolený typ souborů, který se má zobrazit dle
	 * hodnoty v cmb pro "Zobrazit".
	 * 
	 * @return výše popsanou proměnnou výčetového typu.
	 */
	static ShowFilesEnum getShowFilesEnum() {
		return showFilesEnum;
	}
	
	
	
	
	
	
	/**
	 * Metoda, která převede všechny položky v označeném adresáři na objekty typu
	 * FileTableValue a list s těmito objekty vrátí.
	 * 
	 * @return výše popsaný list s objekty pro tabulku.
	 */
	static List<FileTableValue> getTableValuesFromSelectedDir() {
		final List<FileTableValue> newValuesList = new ArrayList<>();

		if (fileSelectedDirInTree != null)
			Arrays.asList(Objects.requireNonNull(fileSelectedDirInTree.listFiles())).forEach(f -> newValuesList.add(new FileTableValue(f)));

		return newValuesList;
	}
	
	
	
	
	

	


	/**
	 * Metoda, která nastaví proměnnou fileSelectedDirInTree na uživatelem označený
	 * adersář v komponentě Jtree ve třídě Tree.
	 * 
	 * @param fileSelectedDirInTree
	 *            - uživatelem označený adresář.
	 */
	public static void setFileSelectedDirInTree(File fileSelectedDirInTree) {
		FileExplorer.fileSelectedDirInTree = fileSelectedDirInTree;
	}
	
	
	
	

	
	
	
	
	@Override
	public void setLoadedText(final Properties prop) {
		if (prop != null) {
			setTitle(prop.getProperty("FE_Dialog_Title", Constants.FE_DIALOG_TITLE));
			
			jspTable.setBorder(BorderFactory
					.createTitledBorder(prop.getProperty("FE_Jsp_Table_Title", Constants.FE_JSP_TABLE_TITLE)));			
		}


		else {
			setTitle(Constants.FE_DIALOG_TITLE);
			
			jspTable.setBorder(BorderFactory.createTitledBorder(Constants.FE_JSP_TABLE_TITLE));
		}			
	}
}