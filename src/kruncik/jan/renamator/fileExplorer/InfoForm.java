package kruncik.jan.renamator.fileExplorer;

import java.awt.BorderLayout;
import java.util.Properties;

import javax.swing.JScrollPane;
import javax.swing.JTextArea;

import kruncik.jan.renamator.forms.AbstractForm;
import kruncik.jan.renamator.localization.Constants;
import kruncik.jan.renamator.localization.LocalizationImpl;

/**
 * Tato třída slouží jako dialog s informacemi o dialogu Průzkumník projektů.
 * 
 * @see Properties
 * 
 * @author Jan Krunčík
 * @version 1.0
 */

public class InfoForm extends AbstractForm implements LocalizationImpl {

	
	private static final long serialVersionUID = 1L;



	private final JTextArea txaInfo;
	
	

	/**
	 * Konstruktor této třídy.
	 */
	InfoForm() {
		super();
		
		initGui(500, 500, new BorderLayout(), "/fileExplorerIcons/menu/InfoIcon.png");
		
		
		txaInfo = new JTextArea();

		
		txaInfo.setEditable(false);
		
		txaInfo.setLineWrap(true);
		
		add(new JScrollPane(txaInfo), BorderLayout.CENTER);	
	}
	
	
	
	
	
	
	@Override
	public void setLoadedText(final Properties prop) {
		/*
		 * Proměnná, do které se v podmínce vloží text ve zvoleném jazyce a tento text
		 * se vloží do komponenty JTextArea v tomto dialogu.
		 */
		final String text;
		
		if (prop != null) {
			setTitle(prop.getProperty("IF_Dialog_Title", Constants.IF_DIALOG_TITLE));
			
			text = "   " + prop.getProperty("IF_Text_1", Constants.IF_TEXT_1) + "\n" + " - "
					+ prop.getProperty("IF_Text_2", Constants.IF_TEXT_2) + "\n" + " - "
					+ prop.getProperty("IF_Text_3", Constants.IF_TEXT_3) + "\n" + " - "
					+ prop.getProperty("IF_Text_4", Constants.IF_TEXT_4) + "\n\n"

					+ "\t" + prop.getProperty("IF_Text_5", Constants.IF_TEXT_5) + "\n" + " - "
					+ prop.getProperty("IF_Text_6", Constants.IF_TEXT_6) + "\n" + " - "
					+ prop.getProperty("IF_Text_7", Constants.IF_TEXT_7) + "\n" + " - "
					+ prop.getProperty("IF_Text_8", Constants.IF_TEXT_8) + "\n" + " - "
					+ prop.getProperty("IF_Text_9", Constants.IF_TEXT_9) + "\n" + " - "
					+ prop.getProperty("IF_Text_10", Constants.IF_TEXT_10) + "\n"

					+ "\t" + prop.getProperty("IF_Text_11", Constants.IF_TEXT_11) + "\n" + " - "
					+ prop.getProperty("IF_Text_12", Constants.IF_TEXT_12) + "\n" + " - "
					+ prop.getProperty("IF_Text_13", Constants.IF_TEXT_13) + "\n" + " - "
					+ prop.getProperty("IF_Text_14", Constants.IF_TEXT_14) + "\n" + " - "
					+ prop.getProperty("IF_Text_15", Constants.IF_TEXT_15) + "\n" + " - "
					+ prop.getProperty("IF_Text_16", Constants.IF_TEXT_16) + "\n" + " - "
					+ prop.getProperty("IF_Text_17", Constants.IF_TEXT_17) + "\n\n"

					+ "\t" + prop.getProperty("IF_Text_18", Constants.IF_TEXT_18) + "\n" + " - "
					+ prop.getProperty("IF_Text_19", Constants.IF_TEXT_19);
		}
		
		else {
			setTitle(Constants.IF_DIALOG_TITLE);
			
			text = "   " + Constants.IF_TEXT_1 + "\n"
					+ " - " + Constants.IF_TEXT_2 + "\n"
					+ " - " + Constants.IF_TEXT_3 + "\n"
					+ " - " + Constants.IF_TEXT_4 + "\n\n"
					
					+ "\t" + Constants.IF_TEXT_5 + "\n"
					+ " - " + Constants.IF_TEXT_6 + "\n"
					+ " - " + Constants.IF_TEXT_7 + "\n"
					+ " - " + Constants.IF_TEXT_8 + "\n"
					+ " - " + Constants.IF_TEXT_9 + "\n"
					+ " - " + Constants.IF_TEXT_10 + "\n"
					
					+ "\t" + Constants.IF_TEXT_11 + "\n"
					+ " - " + Constants.IF_TEXT_12 + "\n"
					+ " - " + Constants.IF_TEXT_13 + "\n"
					+ " - " + Constants.IF_TEXT_14 + "\n"
					+ " - " + Constants.IF_TEXT_15 + "\n"
					+ " - " + Constants.IF_TEXT_16 + "\n"
					+ " - " + Constants.IF_TEXT_17 + "\n\n"
															
					+ "\t"+ Constants.IF_TEXT_18 + "\n"
					+ " - " + Constants.IF_TEXT_19;
		}
		
		// Text načtený ve zvoleném jazyce:
		txaInfo.setText(text);
		
		// Zobrazím dialog:
		showWindow();
	}
}
