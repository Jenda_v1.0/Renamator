package kruncik.jan.renamator.fileExplorer;

import java.awt.event.ActionEvent;
import java.io.File;
import java.io.IOException;
import java.util.List;
import java.util.Properties;

import javax.swing.AbstractAction;
import javax.swing.Action;
import javax.swing.JOptionPane;

import org.apache.commons.io.FileUtils;
import org.apache.commons.io.FilenameUtils;

import kruncik.jan.renamator.app.App;
import kruncik.jan.renamator.loadPictures.KindOfPicture;
import kruncik.jan.renamator.loadPictures.Pictures;
import kruncik.jan.renamator.localization.Constants;
import kruncik.jan.renamator.localization.LocalizationImpl;
import kruncik.jan.renamator.panels.FileChooser;

/**
 * Tato třída slouží pro vytvoření abstraktních akci, které budou vloženy do
 * různých částí aplikace, do menu, a do popupMenu a do toolbaru.
 * 
 * Jedá se o akce pro vytvoření nového souboru, upravení názvu souboru (případně
 * i jeho přípony) a smazání souboru či adresáře. V případně smazání adresáře se
 * vymažou i všechny položky, které daný adresář obsahuje.
 * 
 * @see Properties
 * @see AbstractActions
 * 
 * @author Jan Krunčík
 * @version 1.0
 */

public class AbstractActions implements LocalizationImpl {

	/**
	 * Akce, která slouží pro vytvoření nového souboru.
	 * Po kliknutí na tuto akci se zobrazí formulář, ve kterém se zadá nový název souboru,
	 * včetně přípony a ten se pak vytvoří na zadané cestě.
	 */
	private static AbstractAction actCreateFile;
	
	/**
	 * Akce, která slouží pro vytvoření nového adresáře. Po kliknutí na tuto akce se
	 * zobrazí formulár pro označení adresáře, kde se má vytvořit nový adresář. a
	 * pak se zavolá metoda, kde se otevře okno, kam se zadá název nového adresáře a
	 * pokud se zadá v pořádku daný adresář se i vytvoří.
	 */
	private static AbstractAction actCreateDir;
	
	/**
	 * Akce, která slouží pro upravení názvu souboru nebo adresáře. Po kliknutí na
	 * tuto akci se zobrazí formulář, kde se zadá název souboru nebo adresáře
	 * (možnost i zadání přípony) a ten se přejmenuje.
	 */
	private static AbstractAction actEditFile;	
	
	/**
	 * Ackce, která souží pro smazání souboru nebo adresáře.
	 * 
	 * Akce si načte data z tabulky a postupně vymaže označené soubory.
	 */
	private static AbstractAction actDeleteFile;
		
	
	
	/**
	 * Proměnné pro texty.
	 * 
	 * (Do chybových hlášek, titlku dialogu pro výběr adresářů apod.)
	 */
	private static String txtChooseDirTitle, txtEditFileErrorText, txtEditFileErrorTitle, txtDeleteFileErrorText,
			txtDeleteFileErrorTitle, txtDeleteFileLastCheckText, txtDeleteFileLastCheckTitle,

			txtCreateFileErrorText_1, txtCreateFileErrorText_2, txtCreateFileErrorTitle, txtCreateDirErrorText_1,
			txtCreateDirErrorText_2, txtCreateDirErrorTitle, txtRenameFileErrorText, txtRenameFileErrorTitle,
			txtDeleteErrorText_1, txtDeleteErrorText_2, txtDeleteErrorTitle, txtDeleteFileErrorText_1,
			txtDeleteFileErrorText_2, txtJopDeleteFileErrorTitle;
	
	
	
	
	
	/**
	 * Konstruktor této třídy.
	 * 
	 * @param fileTableModel
	 *            - reference na table model, který obsahuje data pro tabulku se
	 *            soubory v označeném adresáři a právě z tohoto table modelu si bude
	 *            brát označené soubory pro editaci a mazání v abstraktních akcích.
	 */
	AbstractActions(final FileTableModel fileTableModel) {
		actCreateFile = new AbstractAction("", Pictures.loadImage("/fileExplorerIcons/abstractActions/CreateIcon.png",
				KindOfPicture.TOOLBAR_FILE_EXPLORER)) {

			private static final long serialVersionUID = 1L;

			@Override
			public void actionPerformed(ActionEvent e) {
				/*
				 * Zde se zobrazí filechooser pro ozhnačení adresáře, kde se má vytvořit nový
				 * soubor.
				 * 
				 * Pak se zavolá metoda, která otevre formulář, kam uživatel zadánvý název
				 * souboru (včetně přípony) a po potvrzení se vytvoří daný soubor.
				 */
				final File fileDir = new FileChooser().getPathToDir(txtChooseDirTitle);
				
				if (fileDir != null)
					createFile(fileDir.getAbsolutePath(), true);
			}
		};
		
		
		
		actCreateDir = new AbstractAction("", Pictures.loadImage(
				"/fileExplorerIcons/abstractActions/CreateDirIcon.jpeg", KindOfPicture.TOOLBAR_FILE_EXPLORER)) {

			private static final long serialVersionUID = 1L;

			@Override
			public void actionPerformed(ActionEvent e) {
				/*
				 * Otevře se fileChooser, kde si uživatel zvolí adresář, kde se má vytvořit nový
				 * adresář.
				 * 
				 * Poté se zavolá metoda, která otevře formulář, kam uživatel zadá název nového
				 * adresáře a po potvrzení validního názevu se daný adresář vytvoří.
				 */
				final File fileDir = new FileChooser().getPathToDir(txtChooseDirTitle);
				
				if (fileDir != null)
					createFile(fileDir.getAbsolutePath(), false);
			}
		};
		
		
		
		
		actEditFile = new AbstractAction("", Pictures.loadImage("/fileExplorerIcons/abstractActions/EditIcon.png",
				KindOfPicture.TOOLBAR_FILE_EXPLORER)) {

			private static final long serialVersionUID = 1L;

			@Override
			public void actionPerformed(ActionEvent e) {
				/*
				 * Získám si list typu File s hodnotami (soubory), které jsou aktuálně označené
				 * v tabulce, tent list cyklem projdu a v každé iteraci volám metodu pro editaci
				 * souboru, kdy se otevře dialog, kam se pro každý soubor zadá nový název
				 * (případně i přípona).
				 */
				
				final List<File> listFiles = fileTableModel.getSelectedFiles();

				if (listFiles.isEmpty()) {
					JOptionPane.showMessageDialog(null, txtEditFileErrorText, txtEditFileErrorTitle,
							JOptionPane.ERROR_MESSAGE);
					return;
				}
				
				// Smažu všechny získané soubory:
				listFiles.forEach(AbstractActions::editFile);
				
				/*
				 * Zde potřebuji přenačíst všechna data v tabulce, protože všechny soubory v
				 * listu mohl uživatel přejmenovat, tak je třeba do tabulky načíst aktuální
				 * soubory:
				 */
				fileTableModel.filterData(FileExplorer.getShowFilesEnum(),
						FileExplorer.getTableValuesFromSelectedDir());
			}
		};
		
		
		
		
		actDeleteFile = new AbstractAction("", Pictures.loadImage("/fileExplorerIcons/abstractActions/DeleteIcon.png",
				KindOfPicture.TOOLBAR_FILE_EXPLORER)) {

			private static final long serialVersionUID = 1L;

			@Override
			public void actionPerformed(ActionEvent e) {
				/**
				 * Nejprve si načtu všechny označené položky v tabulce, pak se zeptám uživatele, zda je chce opravdu
				 * smazat, pokud ano, pak je cyklem projdu a postupně pro každý soubor v iteraci cyklu zavolám metodu pro
				 * jeho smazání.
				 */
				
				final List<File> listFiles = fileTableModel.getSelectedFiles();

				if (listFiles.isEmpty()) {
					JOptionPane.showMessageDialog(null, txtDeleteFileErrorText, txtDeleteFileErrorTitle,
							JOptionPane.ERROR_MESSAGE);
					return;
				}

				if (JOptionPane.NO_OPTION == JOptionPane.showConfirmDialog(null, txtDeleteFileLastCheckText,
						txtDeleteFileLastCheckTitle, JOptionPane.YES_NO_OPTION))
					return;

				// Zde mohu smazat soubory:
				listFiles.forEach(AbstractActions::deleteFile);
				
				
				/*
				 * Zde potřebuji přenačíst všechna data v tabulce, protože všechny soubory v
				 * listu mohl uživatel smazat, tak je třeba do tabulky načíst aktuální soubory,
				 * které ještě nybyly smazány:
				 */
				fileTableModel.filterData(FileExplorer.getShowFilesEnum(),
						FileExplorer.getTableValuesFromSelectedDir());
			}
		};
	}
	
	
	
	
	
	
	
	
	
	
	
	/**
	 * Metoda, která otevře dialog pro zadání názvu nového adresáře nebo souboru -
	 * případně i přípony.
	 * 
	 * @param pathToParentDir
	 *            - Cesta k adresáři, kde se má vytvořit nový soubor nebo adresář -
	 *            dle proměnné createFile
	 * 
	 * @param createFile
	 *            - logická proměnná o tom, zda se má vytvořit nový soubor nebo
	 *            adresář. True - jedna se o vytvoření nového souboru, false, jedna
	 *            se o vytvoření adresáře.
	 */
	public static void createFile(final String pathToParentDir, final boolean createFile) {
		// Formulář pro získání názvu souboru nebo adresáře:
		final RenameFileForm form = new RenameFileForm(null, createFile);
		form.setLoadedText(App.getLanguage());
		
		// Zde si získám hodnoty pro název souboru nebo adresáře, a zda se jedná o příponu:
		final NewNameTemp temp = form.getNewNameInfo();
		
		if (temp != null) {
			/*
			 * Zde jde o to, že se má vytvořit soubor nebo adresář (dle proměnné
			 * createFile), tak jen dle toho získám název souboru nebo adresáře a pokud se
			 * jedná o soubor, pak k tomu přidám i příponu, pokud se jedná jen o adresář,
			 * pak ani příponu přidávat nemusím a tento "složený" soubor / adresář vytvořím.
			 */
			
			/*
			 * Proměnná, do které vložím cestu a název souboru / adresáře.
			 */
			final File file = new File(pathToParentDir + File.separator + temp.getNewName());
			
			if (createFile) {
				try {
					if (!file.createNewFile())
						JOptionPane.showMessageDialog(null, txtCreateFileErrorText_1 + "\n" + txtCreateFileErrorText_2
								+ ": " + file.getAbsolutePath(), txtCreateFileErrorTitle, JOptionPane.ERROR_MESSAGE);
				} catch (IOException e) {
					e.printStackTrace();
					// Stejné, jako výše:
					JOptionPane.showMessageDialog(null,
							txtCreateFileErrorText_1 + "\n" + txtCreateFileErrorText_2 + ": " + file.getAbsolutePath(),
							txtCreateFileErrorTitle, JOptionPane.ERROR_MESSAGE);
				}
			}

			// Zde se jedná o vytvoření adresáře:
			else if (!file.mkdir())
				JOptionPane.showMessageDialog(null,
						txtCreateDirErrorText_1 + "\n" + txtCreateDirErrorText_2 + ": " + file.getAbsolutePath(),
						txtCreateDirErrorTitle, JOptionPane.ERROR_MESSAGE);
		}
	}
	
	
	
	
	
	
	
	
	
	
	
	
	
	/**
	 * Metoda, která otevře dialogové okno pro zadání nového názvu souboru /
	 * adresáře.
	 * 
	 * Dále je v tom okně možnost zvolit, zda se má zadat i nový název přípony, pak
	 * se může změnit i přípona souboru.
	 * 
	 * @param file
	 *            - soubor, který se má přejmenovat.
	 * 
	 * @return true, pokud se soubor v pořádku přemenuje, jinak false.
	 */
	public static boolean editFile(final File file) {
		// Formulář pro získání nového názvu:
		final RenameFileForm form = new RenameFileForm(file, false);
		form.setLoadedText(App.getLanguage());
		
		// Zde si získám hodnoty pro nový název, a zda se jedná o příponu:
		final NewNameTemp temp = form.getNewNameInfo();
		
		if (temp != null) {
			/*
			 * Zjistím si, zda se má název změnit i s příponou nebo bez, dle toho sestavím
			 * nový název resp. novou hodnotu file a na tu přejmenuji původní soubor file.
			 */
			/*
			 * adresář, ve kterém se nachází soubor, který se má přejmenovat, případně null,
			 * pokud již rodiče nemá.
			 */
			final String parent = file.getParent();
			
			/*
			 * Nový soubor, na který semá přejmenovat ten původní file.
			 */
			final File newFile;
			
			if (temp.isWithExtension()) {
				if (parent != null)
					newFile = new File(parent + File.separator + temp.getNewName());
				else
					newFile = new File(temp.getNewName()); 
			}
			
			else {
				// Původní přípona:
				final String extension = FilenameUtils.getExtension(file.getName());

				/*
				 * Pokud se jedná o adresář, pak nemohu přidat příponu, protože žádnou nemá
				 */
				if (file.isDirectory()) {
					if (parent != null)
						// Zde musím navíc přidat příponu:
						newFile = new File(parent + File.separator + temp.getNewName());
					else
						newFile = new File(temp.getNewName());
				}
				
				/*
				 * Zde se nejedná o adresář, tak mohu v pořádku přidat příponu:
				 */
				else if (parent != null)
					// Zde musím navíc přidat příponu:
					newFile = new File(parent + File.separator + temp.getNewName() + "." + extension);
				else
					newFile = new File(temp.getNewName() + "." + extension);
			}
			
			
			// Zde mohu přejmenovat soubor:
			if (!file.renameTo(newFile)) {
				JOptionPane.showMessageDialog(null, txtRenameFileErrorText, txtRenameFileErrorTitle,
						JOptionPane.ERROR_MESSAGE);
				// Zde došlo k chybě, tak vrátím false:
				return false;
			}
			// Zde se soubor úspěšnš přejmenoval, mohu vrátit true.
			return true;
		}
		// Zde nebyl získán název, tak vrátím false.
		return false;
	}
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	/**
	 * Metoda, která slouží pro smazání souboru nebo adresáře.
	 * 
	 * Původně na to mělo bý tlačítko, ale je možnost smazat soubor ze stromové
	 * struktury pomocí popupMenu nebo z tabulky, ale v obou případech se musí
	 * získat různými cestami potřebný soubor, až metoda pro smazání souboru je
	 * stejná, tak zde mám pouze metodu pro smazání.
	 * 
	 * @param file
	 *            - Soubor nebo adresář, který se má smazat.
	 */
	public static void deleteFile(final File file) {
		if (file != null) {			
			if (!file.exists())
				// Zde již soubor neexistuje, tak není co mazat:
				return;
				
			
			if (file.isDirectory()) {
				try {
					FileUtils.deleteDirectory(file);
				} catch (IOException e1) {
					e1.printStackTrace();
					JOptionPane.showMessageDialog(null,
							txtDeleteErrorText_1 + "\n" + txtDeleteErrorText_2 + ": " + file.getAbsolutePath(),
							txtDeleteErrorTitle, JOptionPane.ERROR_MESSAGE);
				}
			}

			// Zde se jedná o smazání souboru, ne adresáře:
			else if (!file.delete())
				JOptionPane.showMessageDialog(null,
						txtDeleteFileErrorText_1 + "\n" + txtDeleteFileErrorText_2 + ": " + file.getAbsolutePath(),
						txtJopDeleteFileErrorTitle, JOptionPane.ERROR_MESSAGE);
		}
	}








	@Override
	public void setLoadedText(final Properties prop) {
		if (prop != null) {
			actCreateFile.putValue(Action.NAME, prop.getProperty("AA_Txt_Create_File", Constants.AA_TXT_CREATE_FILE));
			actCreateDir.putValue(Action.NAME, prop.getProperty("AA_Txt_Create_Dir", Constants.AA_TXT_CREATE_DIR));
			actEditFile.putValue(Action.NAME, prop.getProperty("AA_Txt_Edit_File", Constants.AA_TXT_EDIT_FILE));
			actDeleteFile.putValue(Action.NAME, prop.getProperty("AA_Txt_Delete_File", Constants.AA_TXT_DELETE_FILE));
											
			actCreateFile.putValue(Action.SHORT_DESCRIPTION, prop.getProperty("AA_Txt_Create_File_TT", Constants.AA_TXT_CREATE_FILE_TT));
			actCreateDir.putValue(Action.SHORT_DESCRIPTION, prop.getProperty("AA_Txt_Create_Dir_TT", Constants.AA_TXT_CREATE_DIR_TT));
			actEditFile.putValue(Action.SHORT_DESCRIPTION, prop.getProperty("AA_Txt_Edit_File_TT", Constants.AA_TXT_EDIT_FILE_TT));
			actDeleteFile.putValue(Action.SHORT_DESCRIPTION, prop.getProperty("AA_Txt_Delete_File_TT", Constants.AA_TXT_DELETE_FILE_TT));
			
			txtChooseDirTitle = prop.getProperty("AA_Txt_Choose_Dir_Title", Constants.AA_TXT_CHOOSE_DIR_TITLE);
			
			txtEditFileErrorText = prop.getProperty("AA_Txt_Edit_File_Error_Text", Constants.AA_TXT_EDIT_FILE_ERROR_TEXT);
			txtEditFileErrorTitle = prop.getProperty("AA_Txt_Edit_File_Error_Title", Constants.AA_TXT_EDIT_FILE_ERROR_TITLE);
			
			txtDeleteFileErrorText = prop.getProperty("AA_Txt_Delete_File_Error_Text", Constants.AA_TXT_DELETE_FILE_ERROR_TEXT);
			txtDeleteFileErrorTitle = prop.getProperty("AA_Txt_Delete_File_Error_Title", Constants.AA_TXT_DELETE_FILE_ERROR_TITLE);
			
			txtDeleteFileLastCheckText = prop.getProperty("AA_Txt_Delete_File_Last_Check_Text", Constants.AA_TXT_DELETE_FILE_LAST_CHECK_TEXT);
			txtDeleteFileLastCheckTitle = prop.getProperty("AA_Txt_Delete_File_Last_Check_Title", Constants.AA_TXT_DELETE_FILE_LAST_CHECK_TITLE);
			
			
			txtCreateFileErrorText_1 = prop.getProperty("AA_Txt_Create_File_Error_Text_1", Constants.AA_TXT_CREATE_FILE_ERROR_TEXT_1);
			txtCreateFileErrorText_2 = prop.getProperty("AA_Txt_Create_File_Error_Text_2", Constants.AA_TXT_CREATE_FILE_ERROR_TEXT_2);
			txtCreateFileErrorTitle = prop.getProperty("AA_Txt_Create_File_Error_Title", Constants.AA_TXT_CREATE_FILE_ERROR_TITLE);
			
			
			txtCreateDirErrorText_1 = prop.getProperty("AA_Txt_Create_Dir_Error_Text_1", Constants.AA_TXT_CREATE_DIR_ERROR_TEXT_1);
			txtCreateDirErrorText_2 = prop.getProperty("AA_Txt_Create_Dir_Error_Text_2", Constants.AA_TXT_CREATE_DIR_ERROR_TEXT_2); 
			txtCreateDirErrorTitle = prop.getProperty("AA_Txt_Create_Dir_Error_Title", Constants.AA_TXT_CREATE_DIR_ERROR_TITLE);
			
			txtRenameFileErrorText = prop.getProperty("AA_Txt_Rename_File_Error_Text", Constants.AA_TXT_RENAME_FILE_ERROR_TEXT);
			txtRenameFileErrorTitle = prop.getProperty("AA_Txt_Rename_File_Error_Title", Constants.AA_TXT_RENAME_FILE_ERROR_TITLE);
			
			
			txtDeleteErrorText_1 = prop.getProperty("AA_Txt_Delete_Error_Text_1", Constants.AA_TXT_DELETE_ERROR_TEXT_1);
			txtDeleteErrorText_2 = prop.getProperty("AA_Txt_Delete_Error_Text_2", Constants.AA_TXT_DELETE_ERROR_TEXT_2);
			txtDeleteErrorTitle = prop.getProperty("AA_Txt_Delete_Error_Title", Constants.AA_TXT_DELETE_ERROR_TITLE);	
			
			txtDeleteFileErrorText_1 = prop.getProperty("AA_Txt_Delete_File_Error_Text_1", Constants.AA_TXT_DELETE_FILE_ERROR_TEXT_1);
			txtDeleteFileErrorText_2 = prop.getProperty("AA_Txt_Delete_File_Error_Text_2", Constants.AA_TXT_DELETE_FILE_ERROR_TEXT_2);
			txtJopDeleteFileErrorTitle = prop.getProperty("AA_Txt_Jop_Delete_File_Error_Title", Constants.AA_TXT_JOP_DELETE_FILE_ERROR_TITLE);
		}
		
		
		else {
			actCreateFile.putValue(Action.NAME, Constants.AA_TXT_CREATE_FILE);
			actCreateDir.putValue(Action.NAME, Constants.AA_TXT_CREATE_DIR);
			actEditFile.putValue(Action.NAME, Constants.AA_TXT_EDIT_FILE);
			actDeleteFile.putValue(Action.NAME, Constants.AA_TXT_DELETE_FILE);
											
			actCreateFile.putValue(Action.SHORT_DESCRIPTION, Constants.AA_TXT_CREATE_FILE_TT);
			actCreateDir.putValue(Action.SHORT_DESCRIPTION, Constants.AA_TXT_CREATE_DIR_TT);
			actEditFile.putValue(Action.SHORT_DESCRIPTION, Constants.AA_TXT_EDIT_FILE_TT);
			actDeleteFile.putValue(Action.SHORT_DESCRIPTION, Constants.AA_TXT_DELETE_FILE_TT);
			
			txtChooseDirTitle = Constants.AA_TXT_CHOOSE_DIR_TITLE;
			
			txtEditFileErrorText = Constants.AA_TXT_EDIT_FILE_ERROR_TEXT;
			txtEditFileErrorTitle = Constants.AA_TXT_EDIT_FILE_ERROR_TITLE;
			
			txtDeleteFileErrorText = Constants.AA_TXT_DELETE_FILE_ERROR_TEXT;
			txtDeleteFileErrorTitle = Constants.AA_TXT_DELETE_FILE_ERROR_TITLE;
			
			txtDeleteFileLastCheckText = Constants.AA_TXT_DELETE_FILE_LAST_CHECK_TEXT;
			txtDeleteFileLastCheckTitle = Constants.AA_TXT_DELETE_FILE_LAST_CHECK_TITLE;
			
			
			txtCreateFileErrorText_1 = Constants.AA_TXT_CREATE_FILE_ERROR_TEXT_1;
			txtCreateFileErrorText_2 = Constants.AA_TXT_CREATE_FILE_ERROR_TEXT_2;
			txtCreateFileErrorTitle = Constants.AA_TXT_CREATE_FILE_ERROR_TITLE;
			
			
			txtCreateDirErrorText_1 = Constants.AA_TXT_CREATE_DIR_ERROR_TEXT_1;
			txtCreateDirErrorText_2 = Constants.AA_TXT_CREATE_DIR_ERROR_TEXT_2; 
			txtCreateDirErrorTitle = Constants.AA_TXT_CREATE_DIR_ERROR_TITLE;
			
			txtRenameFileErrorText = Constants.AA_TXT_RENAME_FILE_ERROR_TEXT;
			txtRenameFileErrorTitle = Constants.AA_TXT_RENAME_FILE_ERROR_TITLE;
			
			
			txtDeleteErrorText_1 = Constants.AA_TXT_DELETE_ERROR_TEXT_1;
			txtDeleteErrorText_2 = Constants.AA_TXT_DELETE_ERROR_TEXT_2;
			txtDeleteErrorTitle = Constants.AA_TXT_DELETE_ERROR_TITLE;	
			
			txtDeleteFileErrorText_1 = Constants.AA_TXT_DELETE_FILE_ERROR_TEXT_1;
			txtDeleteFileErrorText_2 = Constants.AA_TXT_DELETE_FILE_ERROR_TEXT_2;
			txtJopDeleteFileErrorTitle = Constants.AA_TXT_JOP_DELETE_FILE_ERROR_TITLE;
		}
	}
	
	
	
	
	/**
	 * Getr na akci, která slouží pro vytvoření souboru.
	 * 
	 * @return akce pro vytvoření souboru.
	 */
	static AbstractAction getActCreateFile() {
		return actCreateFile;
	}
	

	/**
	 * Getr na akci, která slouží pro vytvoření adresáře.
	 * 
	 * @return akce pro vytvoření adresáře.
	 */
	static AbstractAction getActCreateDir() {
		return actCreateDir;
	}
	
	/**
	 * Getr na akci, která slouží pro přejmenování souboru.
	 * 
	 * @return akce pro přejmenování souboru.
	 */
	static AbstractAction getActEditFile() {
		return actEditFile;
	}
	
	
	/**
	 * Getr na akci, která slouží pro smazání souboru.
	 * 
	 * @return akce pro smazání souboru.
	 */
	static AbstractAction getActDeleteFile() {
		return actDeleteFile;
	}
}
