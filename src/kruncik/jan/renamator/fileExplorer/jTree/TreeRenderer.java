package kruncik.jan.renamator.fileExplorer.jTree;

import java.awt.Component;
import java.io.File;
import java.util.HashMap;
import java.util.Map;

import javax.swing.ImageIcon;
import javax.swing.JLabel;
import javax.swing.JTree;
import javax.swing.filechooser.FileSystemView;
import javax.swing.tree.DefaultTreeCellRenderer;

import org.apache.commons.io.FilenameUtils;

import kruncik.jan.renamator.loadPictures.KindOfPicture;
import kruncik.jan.renamator.loadPictures.Pictures;

/**
 * Tato třídaslouží jako renderer, který vykresluje položky v Jtree ve stavu,
 * který požaduji. Tj. s ikonou na začátku a pak s názvem souboru či adresáře (v
 * případě souboru i s příponou).
 * 
 * Zdroj:
 * http://www.pushing-pixels.org/2007/07/22/showing-the-file-system-as-a-swing-jtree.html
 * 
 * Konkrétní kód, ze kterého jsem čerpal:
 * http://www.pushing-pixels.org/wp-content/uploads/2007/07/filetreepanel.txt
 * 
 * @see TreeRenderer
 * 
 * @author Jan Krunčík
 * @version 1.0
 */

public class TreeRenderer extends DefaultTreeCellRenderer {

	
	private static final long serialVersionUID = 1L;

	/**
	 * File system view.
	 */
	private static final FileSystemView fsv = FileSystemView.getFileSystemView();
	
	/**
	 * Root name cache to speed the rendering.
	 */
	private Map<File, String> rootNameCache = new HashMap<>();
	
	
	@Override
	public Component getTreeCellRendererComponent(JTree tree, Object value,
			boolean sel, boolean expanded, boolean leaf, int row, boolean hasFocus) {
		final FileTreeNode ftn = (FileTreeNode) value;
		
		final File file = ftn.getFile();
		
		String filename = "";
		if (file != null) {
			if (ftn.isFileSystemRoot()) {
				// long start = System.currentTimeMillis();
				filename = this.rootNameCache.get(file);
				
				if (filename == null) {
					filename = fsv.getSystemDisplayName(file);
					this.rootNameCache.put(file, filename);
				}
				
				// long end = System.currentTimeMillis();
				// System.out.println(filename + ":" + (end - start));
			} else
				filename = file.getName();
		}
		
		final JLabel result = (JLabel) super.getTreeCellRendererComponent(tree, filename, sel, expanded, leaf, row,
				hasFocus);
		
		if (file != null) {
			// Původní verze ikony:
//			Icon icon = this.iconCache.get(filename);
//			if (icon == null) {
//				// System.out.println("Getting icon of " + filename);
//				icon = fsv.getSystemIcon(file);
//				this.iconCache.put(filename, icon);
//			}
//			result.setIcon(icon);
			
			// Moje verze ikony:
//			final URL imageUrl = TreeRenderer.class.getResource());
//
//			if (imageUrl != null)
//				result.setIcon(new ImageIcon(
//						new ImageIcon(imageUrl).getImage().getScaledInstance(17, 17, Image.SCALE_SMOOTH)));

			final ImageIcon image = Pictures.loadImage("/fileExplorerIcons/tree/" + getIconForLabel(ftn.getFile()),
					KindOfPicture.JTREE_ICON);

			if (image != null)
				result.setIcon(image);
		}
		return result;
	}
	
	
	
	
	
	
	
	
	
	/**
	 * Metoda, která zjistí, o jaký typ souboru file se jedná, a dle toho se vrátí
	 * příslušná ikona, například když file je adresář, vrátí se název pro ikonu
	 * adresáře, pokud jde třeba o pdf soubor, pak se vrátí ikona, která
	 * reprezentuje pdf soubor apod.
	 * 
	 * @param file
	 *            - Cesta k nějakému souboru nebo adresáři, ke kterému se přiřadí
	 *            nějaká ikona.
	 * 
	 * @return - výše popsaný název ikony
	 */
	public static String getIconForLabel(final File file) {
		if (file.isDirectory())
			return "DirectoryIcon.jpeg";

		final String extension = FilenameUtils.getExtension(file.getName());

		
		if (extension.equalsIgnoreCase("3gp"))
			return "3gpIcon.png";

		else if (extension.equalsIgnoreCase("7z"))
			return "7zipIcon.png";

		else if (extension.equalsIgnoreCase("accdb"))
			return "AccdbIcon.png";

		else if (extension.equalsIgnoreCase("amr"))
			return "AmrIcon.png";

		else if (extension.equalsIgnoreCase("ani"))
			return "AniIcon.jpeg";

		else if (extension.equalsIgnoreCase("arj"))
			return "ArjIcon.png";

		else if (extension.equalsIgnoreCase("asp"))
			return "AspIcon.png";

		else if (extension.equalsIgnoreCase("aspx"))
			return "AspxIcon.png";

		else if (extension.equalsIgnoreCase("aup"))
			return "AupIcon.png";

		else if (extension.equalsIgnoreCase("avi"))
			return "AviIcon.png";

		else if (extension.equalsIgnoreCase("bak"))
			return "BakIcon.png";

		else if (extension.equalsIgnoreCase("bat"))
			return "BatIcon.png";

		else if (extension.equalsIgnoreCase("bmp"))
			return "BmpIcon.png";

		else if (extension.equalsIgnoreCase("cdr"))
			return "CdrIcon.png";

		else if (extension.equalsIgnoreCase("css"))
			return "CssIcon.png";

		else if (extension.equalsIgnoreCase("dat"))
			return "DatIcon.png";

		else if (extension.equalsIgnoreCase("dbf"))
			return "DbfIcon.png";

		else if (extension.equalsIgnoreCase("doc") || extension.equalsIgnoreCase("docx"))
			return "DocIcon.png";

		else if (extension.equalsIgnoreCase("exe"))
			return "ExeIcon.png";

		else if (extension.equalsIgnoreCase("flac"))
			return "FlacIcon.png";

		else if (extension.equalsIgnoreCase("flv"))
			return "FlvIcon.png";

		else if (extension.equalsIgnoreCase("gif"))
			return "GifIcon.png";

		else if (extension.equalsIgnoreCase("htm") || extension.equalsIgnoreCase("html"))
			return "HtmlIcon.png";

		else if (extension.equalsIgnoreCase("ico"))
			return "IconIcon.png";

		else if (extension.equalsIgnoreCase("ini"))
			return "IniIcon.png";

		else if (extension.equalsIgnoreCase("iso"))
			return "IsoIcon.png";

		else if (extension.equalsIgnoreCase("jar"))
			return "JarIcon.png";

		else if (extension.equalsIgnoreCase("jpg") || extension.equalsIgnoreCase("jpeg"))
			return "JpgIcon.png";

		else if (extension.equalsIgnoreCase("js"))
			return "JsIcon.png";

		else if (extension.equalsIgnoreCase("mp3"))
			return "Mp3Icon.png";

		else if (extension.equalsIgnoreCase("mp4"))
			return "Mp4Icon.png";

		else if (extension.equalsIgnoreCase("msi"))
			return "MsiIcon.jpeg";

		else if (extension.equalsIgnoreCase("odt"))
			return "OdtIcon.png";

		else if (extension.equalsIgnoreCase("ogg"))
			return "OggIcon.png";

		else if (extension.equalsIgnoreCase("pdf"))
			return "PdfIcon.png";

		else if (extension.equalsIgnoreCase("php"))
			return "PhpIcon.png";

		else if (extension.equalsIgnoreCase("png"))
			return "PngIcon.png";

		else if (extension.equalsIgnoreCase("potx"))
			return "PotxIcon.png";

		else if (extension.equalsIgnoreCase("psd"))
			return "PsdIcon.png";

		else if (extension.equalsIgnoreCase("psf"))
			return "PsIcon.jpeg";

		else if (extension.equalsIgnoreCase("rar"))
			return "RarIcon.png";

		else if (extension.equalsIgnoreCase("rtf"))
			return "RtfIcon.png";

		else if (extension.equalsIgnoreCase("tar"))
			return "TarIcon.png";

		else if (extension.equalsIgnoreCase("tif"))
			return "TifIcon.jpeg";

		else if (extension.equalsIgnoreCase("tmp"))
			return "TmpIcon.png";

		else if (extension.equalsIgnoreCase("torrent"))
			return "TorrentIcon.png";

		else if (extension.equalsIgnoreCase("ttf"))
			return "TtfIcon.png";

		else if (extension.equalsIgnoreCase("txt"))
			return "TxtIcon.png";

		else if (extension.equalsIgnoreCase("wav"))
			return "WavIcon.png";

		else if (extension.equalsIgnoreCase("wma"))
			return "WmaIcon.png";

		else if (extension.equalsIgnoreCase("wmv"))
			return "WmvIcon.png";

		else if (extension.equalsIgnoreCase("wps"))
			return "WpsIcon.png";

		else if (extension.equalsIgnoreCase("xls"))
			return "XlsIcon.png";

		else if (extension.equalsIgnoreCase("xlsm"))
			return "XlsmIcon.png";

		else if (extension.equalsIgnoreCase("xlsx"))
			return "XlsxIcon.png";

		else if (extension.equalsIgnoreCase("xml"))
			return "XmlIcon.png";

		else if (extension.equalsIgnoreCase("zip"))
			return "ZipIcon.png";

		else if (extension.equalsIgnoreCase("class"))
			return "ClassIcon.png";

		else if (extension.equalsIgnoreCase("java"))
			return "JavaIcon.png";

		else if (extension.equalsIgnoreCase("json"))
			return "JsonIcon.png";

		else if (extension.equalsIgnoreCase("properties"))
			return "PropertiesIcon.png";	
		

		/*
		 * Zde nebyla definována ikona pro daný typ souboru, tak využiji výchozí:
		 */
		return "DefaultFileIcon.png";
	}
}
