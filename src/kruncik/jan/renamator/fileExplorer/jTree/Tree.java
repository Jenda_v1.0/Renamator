package kruncik.jan.renamator.fileExplorer.jTree;

import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.io.File;
import java.util.Properties;

import javax.swing.JTree;
import javax.swing.SwingUtilities;
import javax.swing.ToolTipManager;
import javax.swing.tree.TreePath;

import kruncik.jan.renamator.fileExplorer.FileExplorer;
import kruncik.jan.renamator.localization.Constants;
import kruncik.jan.renamator.localization.LocalizationImpl;
import kruncik.jan.renamator.panels.foldersPanel.FoldersPanel;

/**
 * Tato třída slouží jako komponenta Jtree, která zobrazuje strukturu souborů na disku.
 * 
 * Zdroj:
 * http://www.pushing-pixels.org/2007/07/22/showing-the-file-system-as-a-swing-jtree.html
 * 
 * Konkrétní kód, ze kterého jsem čerpal:
 * http://www.pushing-pixels.org/wp-content/uploads/2007/07/filetreepanel.txt
 *
 * @see Properties
 * 
 * @author Jan Krunčík
 * @version 1.0
 */

public class Tree extends JTree implements LocalizationImpl {

	
	private static final long serialVersionUID = 1L;

	
	/**
	 * Komponenta JPopupMenu, která se zobrazí po klilknutí pravým tlačítkem myši na
	 * nějakou položku v této komponentě Jtree, například na adresář apod.
	 */
	private final PopupMenu popupMenu;
	
	
	
	
	
	/**
	 * Proměnné pro texty ve zvoleném jazyce.
	 */
	private String txtExpandAction, txtCollapseAction;
	
	
	
	
	
	/**
	 * Konstruktor této třídy.
	 * 
	 * @param rootTreeNode
	 *            - veškeré načtené kořenové adresáře v PC již připravené jako
	 *            "uzly" pro Jtree komponntu.
	 * 
	 * @param foldersPanel
	 *            - Reference na instanci třídy FoldersPanel, abych ji mohl předat
	 *            do PopupMenu, kde je tato reference potřeba pro nastavení textu do
	 *            textových polí pro zdorjovou a cílovou cestu k adresářům
	 */
	public Tree(final FileTreeNode rootTreeNode, final FoldersPanel foldersPanel) {
		super(rootTreeNode);

		// Tooltip:
	    ToolTipManager.sharedInstance().registerComponent(this);
		
		setCellRenderer(new TreeRenderer());
		
		setRootVisible(false);
		
		
		// Zde je ta čára u hlavních adresářů:k - Tree line
//		UIManager.put("Tree.line", Color.green);
//		putClientProperty("JTree.lineStyle", "Horizontal");
		putClientProperty("JTree.lineStyle", "Angled");
		
		
		
		
		// Inicializace popupMenu:
		popupMenu = new PopupMenu(this, foldersPanel);
		
		
		
		
		// Přidání události po kliknutí myší, aby se zobrazilo PopupMenu:
		mouseListenerForPopup();
	}
	
	
	
	
	
	
	
	
	/**
	 * Metoda, která přidá Mouse adapter k této komponentě Jtree tak, aby reagovala
	 * na kliknutí pravého tlačítka myši na položku v Jtree a zobrazila tak
	 * popupMenu.
	 */
	private void mouseListenerForPopup() {
		addMouseListener(new MouseAdapter() {
			
			@Override
			public void mouseReleased(MouseEvent e) {
				if (SwingUtilities.isRightMouseButton(e)) {
					final int x = e.getX();
					final int y = e.getY();
					
					final TreePath path = getPathForLocation(x, y);
					
					if (path != null) {						
						popupMenu.setSelectedPath(path);
						
						if (isExpanded(path))
							popupMenu.putValueToActCollapseExpandLeaf(txtCollapseAction);
						else
							popupMenu.putValueToActCollapseExpandLeaf(txtExpandAction);
						
						
						
						/*
						 * Nejprve zjistím, zda se jedná o adresář nebo ne, pokud to není adresář, tak
						 * taky znepřístupním tu akci, prootože otevřit nebo zavřít lze jen adresář.
						 */
						final File file = getFileFromTreePath(path);
						
						if (file != null && !file.isDirectory()) {
							popupMenu.setActCollapseExpandLeafEnabled(false);
							popupMenu.setEnabledItemSetSourceDir(false);
							popupMenu.setEnabledItemSetDestinationDir(false);
							popupMenu.setEnabledItemCreateFile(false);
							popupMenu.setEnabledItemCreateDir(false);
						}

						else {
							popupMenu.setActCollapseExpandLeafEnabled(true);
							popupMenu.setEnabledItemSetSourceDir(true);
							popupMenu.setEnabledItemSetDestinationDir(true);
							popupMenu.setEnabledItemCreateFile(true);
							popupMenu.setEnabledItemCreateDir(true);
						}	
												
						popupMenu.show(Tree.this, x, y);
					}	
				}
				
				
				
				
				/*
				 * Pokud se klikne levým tlačítkem (nepočítají se double cliknutí, ale ne vždy
				 * se to povede, pak se nastaví do tabulky položky ze zvoleného adresáře - pokud
				 * je to adresář.
				 */
				else if (SwingUtilities.isLeftMouseButton(e) && e.getClickCount() < 2) {					
					/*
					 * Zde se uvolenilo levé tlačítko myši, takže uživatel klikl 
					 * na nějakou položku ve stromu, tak otestuji, zda je to adresář nebo 
					 * běžný soubor a pokud je to adresář, načtu se jeho položky a zobrazím je v
					 * tabulce.
					 */
					
					final TreePath path = getPathForLocation(e.getX(), e.getY());
					final File file = getFileFromTreePath(path);
					
					if (file != null && file.isDirectory()) {
						FileExplorer.setFileSelectedDirInTree(file);
						FileExplorer.setDataToTable();
					}
				}
			}
		});
	}
	
	
	
	
	
	
	
	@Override
	public String getToolTipText(MouseEvent event) {
		if (event == null)
			return null;
		
		final TreePath path = getPathForLocation(event.getX(), event.getY());
		
		if (path == null)
			return null;
		
		final FileTreeNode fnode = (FileTreeNode) path.getLastPathComponent();
		
		if (fnode == null)
			return null;
		
		final File f = fnode.getFile();				
		
//		return (f == null ? null : f.getAbsolutePath());
		return (f == null ? super.getToolTipText(event) : f.getAbsolutePath());
	}




	
	
	
	/**
	 * Metoda, která rozšíří nebo zavře všechny adresáře v Jtree.
	 * 
	 * @param expand
	 *            - true - rozšíří všechny adresáře (otevře. False - zaře všechny
	 *            adresáře v Jtree.
	 */
	final void expandAll(final boolean expand) {
		if (expand)
			for (int i = 0; i < getRowCount(); i++)
				expandRow(i);

		else
			for (int i = 0; i < getRowCount(); i++)
				collapseRow(i);
	}

	
	
	
	
	
	
	/**
	 * Metoda, která vrátí cestu k souboru, který je označen v Jtree nebo null,
	 * pokud došlo k nějaké chybě, neno není označena položka v Jtree.
	 * 
	 * @param path
	 *            - cesta k označené položce v Jtree.
	 * 
	 * @return proměnnou typu File,která reprezentuje nějaký soubor na disku.
	 */
	final File getFileFromTreePath(final TreePath path) {
		if (path == null)
			return null;
		
		final FileTreeNode fnode = (FileTreeNode) path.getLastPathComponent();

		if (fnode != null)
			return fnode.getFile();

		return null;
	}
	
	
	
	
	



	@Override
	public void setLoadedText(final Properties prop) {
		popupMenu.setLoadedText(prop);
		
		if (prop != null) {
			txtExpandAction = prop.getProperty("T_Txt_Expand_Action", Constants.T_TXT_EXPAND_ACTION);
			txtCollapseAction = prop.getProperty("T_Txt_Collapse_Action", Constants.T_TXT_COLLAPSE_ACTION);
		}
		
		
		else {
			txtExpandAction = Constants.T_TXT_EXPAND_ACTION;
			txtCollapseAction = Constants.T_TXT_COLLAPSE_ACTION;
		}
	}
}
