package kruncik.jan.renamator.fileExplorer.jTree;

import java.io.File;
import java.util.Arrays;
import java.util.Enumeration;
import java.util.NoSuchElementException;

import javax.swing.tree.TreeNode;

/**
 * Tato třída slouží jako "pložka", která se vkládá do Tree a tyto položky jsou
 * vykreslovány pomocí rendereru TreeRenderer v komponentě Jtree Tree.
 * 
 * Tato pložka tedy slouží jako "soubor", který se nachází někde na disku
 * uživatele a ten se reprezentuje v tom Tree a uživatel s tím může trofhu
 * manipulovat.
 * 
 * Zdroj:
 * http://www.pushing-pixels.org/2007/07/22/showing-the-file-system-as-a-swing-jtree.html
 * 
 * Konkrétní kód, ze kterého jsem čerpal:
 * http://www.pushing-pixels.org/wp-content/uploads/2007/07/filetreepanel.txt
 * 
 * @see TreeNode
 * 
 * @author Jan Krunčík
 * @version 1.0
 */

public class FileTreeNode implements TreeNode {
	
	/**
	 * Node file.
	 */
	private final File file;

	/**
	 * Children of the node file.
	 */
	private final File[] children;

	/**
	 * Parent node.
	 */
	private final TreeNode parent;

	/**
	 * Indication whether this node corresponds to a file system root.
	 */
	private final boolean isFileSystemRoot;
	
	/**
	 * Zde je uložena logická proměnná o tom, zda se mají ignorovat skryté soubory
	 * nebo ne.
	 * 
	 * True - mají se zobrazitskryté soubory. False - skryté soubory se nemají
	 * započítat - nebudou zobrazeny.
	 */
	private final boolean showHiddenFiles;
	
	
	/**
	 * Creates a new file tree node.
	 * 
	 * @param file
	 *            Node file
	 * 
	 * @param isFileSystemRoot
	 *            Indicates whether the file is a file system root.
	 * 
	 * @param parent
	 *            Parent node.
	 * 
	 * @param showHiddenFiles
	 *            - true - mají se ignorovat skryté soubory a adresáře, false - mají
	 *            se započítat skryté soubory a adresáře.
	 */
	FileTreeNode(final File file, final boolean isFileSystemRoot, final TreeNode parent,
				 final boolean showHiddenFiles) {
		super();
		
		this.file = file;
		this.isFileSystemRoot = isFileSystemRoot;
		this.parent = parent;
		this.showHiddenFiles = showHiddenFiles;
		
		final File[] temp = this.file.listFiles();
		
		if (temp == null)
			this.children = new File[0];

		else {
			if (!showHiddenFiles)
				// Zde je třeba vynechat skryté soubory:
				children = Arrays.asList(temp).stream().filter(f -> !f.isHidden()).toArray(File[]::new);
			// Zde se nemusí skryté soubory vynachávat, tak uložím všechny soubory
			else
				children = this.file.listFiles();
		}
		
		
		// PŮVODNI:
//		public FileTreeNode(File file, boolean isFileSystemRoot, TreeNode parent) {
//			this.file = file;
//			this.isFileSystemRoot = isFileSystemRoot;
//			this.parent = parent;
//			
//			this.children = this.file.listFiles();
//			
//			if (this.children == null)
//				this.children = new File[0];
	}

	
	
	/**
	 * Creates a new file tree node.
	 * 
	 * @param children
	 *            Children files.
	 * 
	 * @param showHiddenFiles
	 *            - true - mají se ignorovat skryté soubory a adresáře, false - mají
	 *            se započítat skryté soubory a adresáře.
	 */
	public FileTreeNode(final File[] children, final boolean showHiddenFiles) {
		super();
		
		file = null;
		parent = null;
		isFileSystemRoot = false;
		this.showHiddenFiles = showHiddenFiles;

		if (!showHiddenFiles)
			// Zde je třeba vynechat skryté soubory:
			this.children = Arrays.asList(children).stream().filter(f -> !f.isHidden()).toArray(File[]::new);
		// Zde se nemusí skryté soubory vynachávat, tak uložím všechny soubory
		else
			this.children = children;
		
		
		// PŮVODNI:
//		public FileTreeNode(File[] children) {
//			this.file = null;
//			this.parent = null;
//			this.children = children;
	}
	
	
	

	@Override
	public TreeNode getChildAt(int childIndex) {
		return new FileTreeNode(this.children[childIndex], this.parent == null, this, showHiddenFiles);
	}

	@Override
	public int getChildCount() {
		return this.children.length;
	}

	@Override
	public TreeNode getParent() {
		return this.parent;
	}

	@Override
	public int getIndex(TreeNode node) {
		final FileTreeNode ftn = (FileTreeNode) node;

		for (int i = 0; i < this.children.length; i++)
			if (ftn.file.equals(this.children[i]))
				return i;

		return -1;
	}

	@Override
	public boolean getAllowsChildren() {
		return true;
	}

	@Override
	public boolean isLeaf() {
		return (this.getChildCount() == 0);
	}

	@Override
	public Enumeration<File> children() {
		final int elementCount = this.children.length;

		return new Enumeration<File>() {
			int count = 0;

			/*
			 * (non-Javadoc)
			 * 
			 * @see java.util.Enumeration#hasMoreElements()
			 */
			public boolean hasMoreElements() {
				return this.count < elementCount;
			}

			/*
			 * (non-Javadoc)
			 * 
			 * @see java.util.Enumeration#nextElement()
			 */
			public File nextElement() {
				if (this.count < elementCount)
					return FileTreeNode.this.children[this.count++];
				
				throw new NoSuchElementException("Vector Enumeration");
			}
		};
	}
	
	
	/**
	 * Getr na proměnnou file, která "směřuje" k nějakému souboru na disku -
	 * reprezentuje jej.
	 * 
	 * @return reference na proměnnou file.
	 */
	public File getFile() {
		return file;
	}
	
	
	
	/**
	 * Getr na prměnnou isFileSystemRoot.
	 *
	 * @return boolean isFileSystemRoot.
	 */
	boolean isFileSystemRoot() {
		return isFileSystemRoot;
	}
}
