package kruncik.jan.renamator.fileExplorer.jTree;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import java.util.Properties;

import javax.swing.AbstractAction;
import javax.swing.Action;
import javax.swing.JMenuItem;
import javax.swing.JOptionPane;
import javax.swing.JPopupMenu;
import javax.swing.tree.TreePath;

import kruncik.jan.renamator.app.App;
import kruncik.jan.renamator.fileExplorer.AbstractActions;
import kruncik.jan.renamator.localization.Constants;
import kruncik.jan.renamator.localization.LocalizationImpl;
import kruncik.jan.renamator.panels.foldersPanel.FoldersPanel;

/**
 * Tato třída slouží jako "menu", kteér se zobrazí po kliknutí pravým tlačítkem
 * myši na nějakou položku v komponentě Tree - ve stromové struktuře, která
 * znázorňuje disk v PC.
 * 
 * @see Properties
 * @see JPopupMenu
 * @see ActionListener
 * 
 * @author Jan Krunčík
 * @version 1.0
 */

public class PopupMenu extends JPopupMenu implements LocalizationImpl, ActionListener {

	
	private static final long serialVersionUID = 1L;


	/**
	 * Akce, která bude sloužit pro rozbalei nebo "schování" - zabalení nějakého
	 * adresáře - dle toho, zda je již rozbalený nebo ne.
	 */
	private final Action actCollapseExpandLeaf;

	/**
	 * Cesta k položce v Jtree, na kterou uživatel klikl. Je zde potřeba, abych si
	 * vždy zjistil označenou položku a dle toho mohl nabídnout rozbalení či
	 * zabalení větve, případně tuto možnost zamítnout pokud se nejedná o adresář
	 * apod.
	 */
	private TreePath selectedPath;
	
	
	
	
	/**
	 * Položky do JPopupMenu.
	 */
	private final JMenuItem itemExpandAll, itemCollapseAll, itemSetSourceDir, itemSetDestinationDir, itemCreateFile,
			itemCreateDir, itemRenameFile, itemDeleteFile, itemRepaint;
	
	
	
	
	
	
	/**
	 * Reference na instanci třídy Tree, která slouží jako komponenta Jtree kvůli
	 * zavoláním nějakých metod apod.
	 */
	private final Tree tree;
	
	
	
	
	/**
	 * Reference na instanci třídy, FoldersPanel, která obsahuje textová pole pro
	 * zadání cesty k zdrojvému a cílovému adresáři.
	 * 
	 * Je to zde potřeba, právě kvůli tomu nastavení cesty do textových polí.
	 */
	private final FoldersPanel foldersPanel;	
	
	
	
	
	
	/**
	 * Proměnné pro texty do chybových hlášek ve zvoleném jazyce.
	 */
	private String txtSameDestinationDirErrorText, txtSameDestinationDirErrorTitle, txtSameSourceDirErrorText,
			txtSameSourceDirErrorTitle, txtDeleteFileText, txtDeleteFileTitle;
	
	
	
	
	
	
	
	/**
	 * Konstruktor této třídy.
	 * 
	 * @param tree
	 *            - reference na instanci třídy Tree, která slouží jako komponenta
	 *            JTree, například pro rozvětvení zvoleného adresáře apod.
	 * 
	 * @param foldersPanel
	 *            - instance třídy FoldersPanel kvůli nastavení cesty do textových
	 *            polí.
	 */
	PopupMenu(final Tree tree, final FoldersPanel foldersPanel) {
		this.tree = tree;
		this.foldersPanel = foldersPanel;
		
		actCollapseExpandLeaf = new AbstractAction() {
			
			private static final long serialVersionUID = 1L;

			@Override
			public void actionPerformed(ActionEvent e) {
				if (selectedPath == null)
					return;

				if (tree.isExpanded(selectedPath))
					tree.collapsePath(selectedPath);
				else
					tree.expandPath(selectedPath);
			}
		};
		
		add(actCollapseExpandLeaf);
		addSeparator();
		
		
		
		itemExpandAll = new JMenuItem();
		itemCollapseAll = new JMenuItem();
		itemSetSourceDir = new JMenuItem();
		itemSetDestinationDir = new JMenuItem();
		itemCreateFile = new JMenuItem();
		itemCreateDir = new JMenuItem();
		itemRenameFile = new JMenuItem();
		itemDeleteFile = new JMenuItem();
		itemRepaint = new JMenuItem();
		
		itemExpandAll.addActionListener(this);
		itemCollapseAll.addActionListener(this);
		itemSetSourceDir.addActionListener(this);
		itemSetDestinationDir.addActionListener(this);
		itemCreateFile.addActionListener(this);
		itemCreateDir.addActionListener(this);
		itemRenameFile.addActionListener(this);
		itemDeleteFile.addActionListener(this);
		itemRepaint.addActionListener(this);
		
		add(itemExpandAll);
		add(itemCollapseAll);
		addSeparator();
		add(itemSetSourceDir);
		add(itemSetDestinationDir);
		addSeparator();
		add(itemCreateFile);
		add(itemCreateDir);
		addSeparator();
		add(itemRenameFile);
		add(itemDeleteFile);
		addSeparator();
		add(itemRepaint);
	}
	
	
	
	
	/**
	 * Metoda, která vloží do akce pro rozbalení / zabalení větve text pro
	 * příslušnou akci, dle toho se pozná, zda se má větev - adresář rozbalit nebo
	 * zabalit.
	 * 
	 * @param value
	 *            - text pro akci, který bude zobrazen v menu.
	 */
	final void putValueToActCollapseExpandLeaf(final String value) {
		actCollapseExpandLeaf.putValue(Action.NAME, value);
	}
	
	
	
	/**
	 * Metoda zpřístupní / zněpřístupní aktci pro rozvětvení nebo "zabalení" větve.
	 * 
	 * @param enable
	 *            true zpřístupní akci, false znepřístupní akci.
	 */
	final void setActCollapseExpandLeafEnabled(final boolean enable) {
		actCollapseExpandLeaf.setEnabled(enable);
	}
	
	
	
	/**
	 * Metoda, která nastavi hodnotu pro značenou položku v komponentě Jtree
	 * 
	 * @param selectedPath
	 *            - cesta k označené položce v Jtree.
	 */
	void setSelectedPath(final TreePath selectedPath) {
		this.selectedPath = selectedPath;
	}

	
	
	/**
	 * Metoda, která zpřístupní / znepřístupní položku pro vytvoření nového souboru.
	 * 
	 * @param enable
	 *            - True - zpřístupní položku pro vytvoření nového souboru. False -
	 *            znepřístupní položku pro vytvoření nového souboru.
	 */ 
	final void setEnabledItemCreateFile(final boolean enable) {
		itemCreateFile.setEnabled(enable);
	}
	
	
	/**
	 * Metoda, která zpřístupní / znepřístupní položku pro vytvoření nového
	 * adresáře.
	 * 
	 * @param enable
	 *            - True - zpřístupní položku pro vytvoření nového adresáře. False -
	 *            znepřístupní položku pro vytvoření nového adresáře.
	 */
	final void setEnabledItemCreateDir(final boolean enable) {
		itemCreateDir.setEnabled(enable);
	}
	

	/**
	 * Metoda, která zpřístupní / znepřístupní položku pro nastavení zdrojového
	 * adresáře.
	 * 
	 * @param enable
	 *            - True - zpřístupní položku pro nastavení zdrojového adresáře.
	 *            False - znepřístupní položku pro nastaveí zdrojového adresáře.
	 */
	final void setEnabledItemSetSourceDir(final boolean enable) {
		itemSetSourceDir.setEnabled(enable);
	}

	/**
	 * Metoda, která zpřístupní / znepřístupní položku pro nastavení cílového
	 * adresáře.
	 * 
	 * @param enable
	 *            - True - zpřístupní položku pro nastavení cílového adresáře. False
	 *            - znepřístupní položku pro nastaveí cílového adresáře.
	 */
	final void setEnabledItemSetDestinationDir(final boolean enable) {
		itemSetDestinationDir.setEnabled(enable);
	}
	
	
	


	@Override
	public void setLoadedText(final Properties prop) {
		if (prop != null) {
			itemExpandAll.setText(prop.getProperty("PM_Item_Expand_All", Constants.PM_ITEM_EXPAND_ALL));
			itemCollapseAll.setText(prop.getProperty("PM_Item_Collapse_All", Constants.PM_ITEM_COLLAPSE_ALL));
			itemSetSourceDir.setText(prop.getProperty("PM_Item_Set_Source_Dir", Constants.PM_ITEM_SET_SOURCE_DIR));
			itemSetDestinationDir.setText(prop.getProperty("PM_Item_Set_Destination_Dir", Constants.PM_ITEM_SET_DESTINATION_DIR));
			itemCreateFile.setText(prop.getProperty("PM_Item_Create_File", Constants.PM_ITEM_CREATE_FILE));
			itemCreateDir.setText(prop.getProperty("PM_Item_Create_Dir", Constants.PM_ITEM_CREATE_DIR));
			itemRenameFile.setText(prop.getProperty("PM_Item_Rename_File", Constants.PM_ITEM_RENAME_FILE));
			itemDeleteFile.setText(prop.getProperty("PM_Item_Delete_File", Constants.PM_ITEM_DELETE_FILE));
			itemRepaint.setText(prop.getProperty("PM_Item_Repaint", Constants.PM_ITEM_REPAINT));
			
			
			txtSameDestinationDirErrorText = prop.getProperty("PM_Txt_Same_Destination_Dir_Error_Text", Constants.PM_TXT_SAME_DESTINATION_DIR_ERROR_TEXT);
			txtSameDestinationDirErrorTitle = prop.getProperty("PM_Txt_Same_Destination_Dir_Error_Title", Constants.PM_TXT_SAME_DESTINATION_DIR_ERROR_TITLE);
			
			txtSameSourceDirErrorText = prop.getProperty("PM_Txt_Same_Source_Dir_Error_Text", Constants.PM_TXT_SAME_SOURCE_DIR_ERROR_TEXT);
			txtSameSourceDirErrorTitle = prop.getProperty("PM_Txt_Same_Source_Dir_Error_Title", Constants.PM_TXT_SAME_SOURCE_DIR_ERROR_TITLE);		
			
			txtDeleteFileText = prop.getProperty("PM_Txt_Delete_File_Text", Constants.PM_TXT_DELETE_FILE_TEXT);
			txtDeleteFileTitle = prop.getProperty("PM_Txt_Delete_File_Title", Constants.PM_TXT_DELETE_FILE_TITLE);
		}
		
		
		else {
			itemExpandAll.setText(Constants.PM_ITEM_EXPAND_ALL);
			itemCollapseAll.setText(Constants.PM_ITEM_COLLAPSE_ALL);
			itemSetSourceDir.setText(Constants.PM_ITEM_SET_SOURCE_DIR);
			itemSetDestinationDir.setText(Constants.PM_ITEM_SET_DESTINATION_DIR);
			itemCreateFile.setText(Constants.PM_ITEM_CREATE_FILE);
			itemCreateDir.setText(Constants.PM_ITEM_CREATE_DIR);
			itemRenameFile.setText(Constants.PM_ITEM_RENAME_FILE);
			itemDeleteFile.setText(Constants.PM_ITEM_DELETE_FILE);
			itemRepaint.setText(Constants.PM_ITEM_REPAINT);
			
			
			txtSameDestinationDirErrorText = Constants.PM_TXT_SAME_DESTINATION_DIR_ERROR_TEXT;
			txtSameDestinationDirErrorTitle = Constants.PM_TXT_SAME_DESTINATION_DIR_ERROR_TITLE;
			
			txtSameSourceDirErrorText = Constants.PM_TXT_SAME_SOURCE_DIR_ERROR_TEXT;
			txtSameSourceDirErrorTitle = Constants.PM_TXT_SAME_SOURCE_DIR_ERROR_TITLE;		
			
			txtDeleteFileText = Constants.PM_TXT_DELETE_FILE_TEXT;
			txtDeleteFileTitle = Constants.PM_TXT_DELETE_FILE_TITLE;
		}
	}


	
	


	@Override
	public void actionPerformed(ActionEvent e) {
		if (e.getSource() instanceof JMenuItem) {
			
			if (e.getSource() == itemExpandAll)
				tree.expandAll(true);

			else if (e.getSource() == itemCollapseAll)
				tree.expandAll(false);
			
			
			
			
			
			else if (e.getSource() == itemSetSourceDir) {
				/*
				 * Zjistím označený adresář a nastavím jej jako zdrojový adresář v aplikaci.
				 * Před tím, než nastavím zdrojový adresář ještě otestuji, zda se náhodou
				 * nerovná již zadanému cílovému adresáři
				 */
				final File file = tree.getFileFromTreePath(selectedPath);
				
				final File desFile = App.getFileDestinationDir();

				if (desFile != null && file.getAbsolutePath().equals(desFile.getAbsolutePath())) {
					JOptionPane.showMessageDialog(this, txtSameDestinationDirErrorText,
							txtSameDestinationDirErrorTitle, JOptionPane.ERROR_MESSAGE);
					return;
				}
				
				
				if (file != null) {
					App.setFileSourceDir(file, FoldersPanel.containsDirSubDirs(file.getAbsolutePath()));
					// Dále nastavím cestu do textového pole pro zdrojový adresář:
					foldersPanel.setPathToSourceTxtField(file.getAbsolutePath());
				}
					
			}
			
			else if (e.getSource() == itemSetDestinationDir) {
				/*
				 * Zjistím označený adresář a nastavím jej jako cílový adresář v aplikaci.
				 * Před tím, než zadám cílový adresář, otestuji, zda není shodný se zdrojovým adresářem.
				 * Jinak jej nenastavím.
				 */
				final File file = tree.getFileFromTreePath(selectedPath);

				final File srcFile = App.getFileSourceDir();

				if (srcFile != null && file.getAbsolutePath().equals(srcFile.getAbsolutePath())) {
					JOptionPane.showMessageDialog(this, txtSameSourceDirErrorText, txtSameSourceDirErrorTitle,
							JOptionPane.ERROR_MESSAGE);
					return;
				}
				
				if (file != null) {
					App.setFileDestinationDir(file);
					// Dále nastavím cestu do textového pole pro cílový adresář:
					foldersPanel.setPathToDestinationTxtField(file.getAbsolutePath());
				}
					
			}
			
			
			
			
			
			else if (e.getSource() == itemRenameFile) {
				/*
				 * Zjistím si označený soubor a otevřu dialog pro přejmenování souboru.
				 */
				final File file = tree.getFileFromTreePath(selectedPath);	
								
				if (AbstractActions.editFile(file))
					tree.updateUI();
			}
			
			
			
			
			
			else if (e.getSource() == itemCreateFile) {
				/*
				 * Zde mám označený adresář, tak mohu otevřít dialog, kam se zadá název a přípona nového souboru,
				 * a vytvořit jej:
				 */
				final File file = tree.getFileFromTreePath(selectedPath);
				
				AbstractActions.createFile(file.getAbsolutePath(), true);
				
				tree.updateUI();
			}
			
			
			else if (e.getSource() == itemCreateDir) {
				/*
				 * Zde mám označený adresář, kde se má vytvořit nový adresář, tak zavolám metodu, která 
				 * otevře dialog pro zadání nového názvu a rovnou ten adresář vytvořím.
				 */
				final File file = tree.getFileFromTreePath(selectedPath);
				
				AbstractActions.createFile(file.getAbsolutePath(), false);
				
				tree.updateUI();
			}
			
			
			
			else if (e.getSource() == itemDeleteFile) {
				/*
				 * Zjistím si označený soubor nebo adrsář a smažu jej, v případě adresáře i
				 * všechny jeho podadresáře.
				 */
				final File file = tree.getFileFromTreePath(selectedPath);				
				
				
				// Neprve test:
				if (JOptionPane.NO_OPTION == JOptionPane.showConfirmDialog(null, txtDeleteFileText, txtDeleteFileTitle,
						JOptionPane.YES_NO_OPTION))
					return;
				
				
				// Zavolám metodu pro smazání souboru:				
				AbstractActions.deleteFile(file);
				
				
				
				/*
				 * Metoda, která přenačte všechny položky v Jtree a ponechá všechny otevřené
				 * adresáře a označenou položku. Například pokud byl někam přidám či přejmenován
				 * nebo smazán soubor apod. Tak aby se změny projevily v Jtree.
				 */
				tree.updateUI();
			}
			
			
			
			
			else if (e.getSource() == itemRepaint)
				// Pouze zaktualizuji stromovou strukturu:
				tree.updateUI();
		}
	}
}
