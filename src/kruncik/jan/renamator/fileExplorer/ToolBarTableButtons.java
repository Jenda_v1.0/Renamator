package kruncik.jan.renamator.fileExplorer;

import java.awt.FlowLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.Properties;

import javax.swing.JButton;
import javax.swing.JToolBar;

import kruncik.jan.renamator.localization.Constants;
import kruncik.jan.renamator.localization.LocalizationImpl;

/**
 * Tato třída slouží jako Toolbar pro vložení do dialogu průzkumník projektů
 * hned nad tabulku, tento toolbar obsahuje pouze tlačítka pro označení a
 * odznačení všech položek v tabulce a skrytí všech souborů nebo "odkrytí" ->
 * zda jsou soubory skryté nebo ne, tj. dát před název souboru desetinnou tečku
 * nebo ji odebrat.
 * 
 * Původně jsem tyto 4 tlačítka chtěl dát do toolbaru úplně nahoře v dialogu,
 * tj. toolbar s abstraktními tlačítky, ale pak jsem si to rozmyslel, nechci
 * míchat ty abstraktní tlačítka s tlačítky, pro ovládání tabulky, i když
 * všechna tlačítka slouží pro ovládání tabulky, tak tyto 4 tlačítka jsou jen
 * pro označování a ne manipulaci s daty jako ty abstraktní akce.
 * 
 * @see JToolBar
 * @see Properties
 * @see ActionListener
 * 
 * @author Jan Krunčík
 * @version 1.0
 */

public class ToolBarTableButtons extends JToolBar implements LocalizationImpl, ActionListener {

	
	private static final long serialVersionUID = 1L;

	
	
	
	/**
	 * Tlačítka pro označení / odznačení a skrytí / zviditelnění souborů.
	 */
	private final JButton btnSelectAllItems, btnDeselectAllItems, btnHideAllFiles, btnShowAllFiles,
			btnHideSelectedFiles, btnShowSelectedFiles;
	
	
	
	
	/**
	 * Reference na insanci třídy FileTableModel, což je tablemodel tabulky,který
	 * obsahuje soubory ve zvoleném adresáři. Potřebuji jej zde,abych mohl volat
	 * metody pro označení a skrývání soubor jedním tlačítkem - pro zavolání metod
	 * nad daným tableModelem.
	 */
	private final FileTableModel tableModel;
	
	
	
	
	
	/**
	 * Konstruktor této třídy.
	 * 
	 * @param tableModel
	 *            - Reference na table model, který obsahuje tabulka. Je zde potřeba
	 *            kvůli zavolání metod pro označení a ozdzančení položek a skrytí či
	 *            zviditelněni souborů.
	 */
	ToolBarTableButtons(final FileTableModel tableModel) {
		super();

		this.tableModel = tableModel;
		
		setLayout(new FlowLayout(FlowLayout.CENTER, 15, 0));
		
		
		btnSelectAllItems = new JButton();
		btnDeselectAllItems = new JButton();
		btnHideAllFiles = new JButton();
		btnShowAllFiles = new JButton();
		btnHideSelectedFiles = new JButton();		
		btnShowSelectedFiles = new JButton();
		
		btnSelectAllItems.addActionListener(this);
		btnDeselectAllItems.addActionListener(this);
		btnHideAllFiles.addActionListener(this);
		btnShowAllFiles.addActionListener(this);
		btnHideSelectedFiles.addActionListener(this);
		btnShowSelectedFiles.addActionListener(this);
		
		
		add(btnSelectAllItems);
		addSeparator();
		add(btnDeselectAllItems);
		addSeparator();
		add(btnHideAllFiles);
		addSeparator();
		add(btnShowAllFiles);
		addSeparator();
		add(btnHideSelectedFiles);
		addSeparator();
		add(btnShowSelectedFiles);
	}


	
	
	

	@Override
	public void setLoadedText(final Properties prop) {
		if (prop != null) {
			btnSelectAllItems.setText(prop.getProperty("TBTB_Btn_Select_All_Items_Text", Constants.TBTB_BTN_SELECT_ALL_ITEMS_TEXT));
			btnSelectAllItems.setToolTipText(prop.getProperty("TBTB_Btn_Select_All_Items_TT", Constants.TBTB_BTN_SELECT_ALL_ITEMS_TT));
			
			btnDeselectAllItems.setText(prop.getProperty("TBTB_Btn_Deselect_All_Items_Text", Constants.TBTB_BTN_DESELECT_ALL_ITEMS_TEXT));
			btnDeselectAllItems.setToolTipText(prop.getProperty("TBTB_Btn_Deselect_All_Items_TT", Constants.TBTB_BTN_DESELECT_ALL_ITEMS_TT));
			
			btnHideAllFiles.setText(prop.getProperty("TBTB_Btn_Hide_All_Files_Text", Constants.TBTB_BTN_HIDE_ALL_FILES_TEXT));
			btnHideAllFiles.setToolTipText(prop.getProperty("TBTB_Btn_Hide_All_Files_TT", Constants.TBTB_BTN_HIDE_ALL_FILES_TT));
			
			btnShowAllFiles.setText(prop.getProperty("TBTB_Btn_Show_All_Files_Text", Constants.TBTB_BTN_SHOW_ALL_FILES_TEXT));
			btnShowAllFiles.setToolTipText(prop.getProperty("TBTB_Btn_Show_All_Files_TT", Constants.TBTB_BTN_SHOW_ALL_FILES_TT));
			
			btnHideSelectedFiles.setText(prop.getProperty("TBTB_Btn_Hide_Selected_Files_Text", Constants.TBTB_BTN_HIDE_SELECTED_FILES_TEXT));
			btnHideSelectedFiles.setToolTipText(prop.getProperty("TBTB_Btn_Hide_Selected_Files_TT", Constants.TBTB_BTN_HIDE_SELECTED_FILES_TT));
			
			btnShowSelectedFiles.setText(prop.getProperty("TBTB_Btn_Show_Selected_Files_Text", Constants.TBTB_BTN_SHOW_SELECTED_FILES_TEXT));
			btnShowSelectedFiles.setToolTipText(prop.getProperty("TBTB_Btn_Show_Selected_Files_TT", Constants.TBTB_BTN_SHOW_SELECTED_FILES_TT));
		}
		
		
		else {
			btnSelectAllItems.setText(Constants.TBTB_BTN_SELECT_ALL_ITEMS_TEXT);
			btnSelectAllItems.setToolTipText(Constants.TBTB_BTN_SELECT_ALL_ITEMS_TT);
			
			btnDeselectAllItems.setText(Constants.TBTB_BTN_DESELECT_ALL_ITEMS_TEXT);
			btnDeselectAllItems.setToolTipText(Constants.TBTB_BTN_DESELECT_ALL_ITEMS_TT);
			
			btnHideAllFiles.setText(Constants.TBTB_BTN_HIDE_ALL_FILES_TEXT);
			btnHideAllFiles.setToolTipText(Constants.TBTB_BTN_HIDE_ALL_FILES_TT);
			
			btnShowAllFiles.setText(Constants.TBTB_BTN_SHOW_ALL_FILES_TEXT);
			btnShowAllFiles.setToolTipText(Constants.TBTB_BTN_SHOW_ALL_FILES_TT);
			
			btnHideSelectedFiles.setText(Constants.TBTB_BTN_HIDE_SELECTED_FILES_TEXT);
			btnHideSelectedFiles.setToolTipText(Constants.TBTB_BTN_HIDE_SELECTED_FILES_TT);
			
			btnShowSelectedFiles.setText(Constants.TBTB_BTN_SHOW_SELECTED_FILES_TEXT);
			btnShowSelectedFiles.setToolTipText(Constants.TBTB_BTN_SHOW_SELECTED_FILES_TT);
		}
	}
	
	


	
			
					


	@Override
	public void actionPerformed(ActionEvent e) {
		if (e.getSource() instanceof JButton) { // Zbytečná podmínka.
			if (e.getSource() == btnSelectAllItems)
				tableModel.selectAllItems(true);

			else if (e.getSource() == btnDeselectAllItems)
				tableModel.selectAllItems(false);

			else if (e.getSource() == btnHideAllFiles)
				tableModel.showHideAllItems(true);

			else if (e.getSource() == btnShowAllFiles)
				tableModel.showHideAllItems(false);

			else if (e.getSource() == btnHideSelectedFiles)
				tableModel.showHideSelectedItems(true);

			else if (e.getSource() == btnShowSelectedFiles)
				tableModel.showHideSelectedItems(false);
		}
	}
}
