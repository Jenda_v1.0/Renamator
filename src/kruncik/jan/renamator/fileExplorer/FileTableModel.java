package kruncik.jan.renamator.fileExplorer;

import java.awt.Image;
import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.LinkOption;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import java.util.Properties;
import java.util.stream.Collectors;

import javax.imageio.ImageIO;
import javax.swing.ImageIcon;
import javax.swing.JOptionPane;
import javax.swing.JTable;
import javax.swing.table.AbstractTableModel;

import org.apache.commons.io.FilenameUtils;

import kruncik.jan.renamator.app.App;
import kruncik.jan.renamator.app.Menu;
import kruncik.jan.renamator.fileExplorer.jTree.TreeRenderer;
import kruncik.jan.renamator.loadPictures.KindOfPicture;
import kruncik.jan.renamator.loadPictures.Pictures;
import kruncik.jan.renamator.localization.Constants;
import kruncik.jan.renamator.localization.CreateLocalization;
import kruncik.jan.renamator.localization.LocalizationImpl;

/**
 * Tato třída slouží jako table model pro tabulku, která zobrazuje informace o
 * souborech ve zvoleném adresáři. Tabulka se nachází ve třídě FileExplorer v
 * balíčku fileExplorer.
 *
 * @see Properties
 * @see AbstractTableModel
 * 
 * @author Jan Krunčík
 * @version 1.0
 */

public class FileTableModel extends AbstractTableModel implements LocalizationImpl {

	private static final long serialVersionUID = 1L;

	
	
	/**
	 * List, který obsahuje hodnoty pro tabulku.
	 */
	private List<FileTableValue> valuesList = new ArrayList<>();
	
	
	/**
	 * Jednorozměrné pole, které obsahuje názvy sloupců pro tabulku.
	 */
	private String[] columns;
	
	
	
	/**
	 * Reference na instanci tabulky, která obsahuje tento table model, je zde
	 * potřeba kvůli nastavení rozměru sloupců.
	 */
	private final JTable table;
	
	
	
	
	
	/**
	 * Proměnné pro texty do chybových hlášek.
	 */
	private static String txtNewNameErrorText, txtNewNameErrorTitle, txtNewExtensionErrorText,
			txtNewExtensionErrorTitle, txtRenameFileErrorText_1, txtRenameFileErrorText_2, txtRenameFileErrorText_3,
			txtRenameFileErrorTitle;
	
	
	
	
	
	
	
	
	/**
	 * Konstruktor této třídy.
	 * 
	 * @param table
	 *            - tabulka, která obsahuje tento table model, je zde potřeba kvůli
	 *            nastavení rozměrů sloupců
	 */
	FileTableModel(final JTable table) {
		super();
		
		this.table = table;
		
		/*
		 * Zde potřebuji načíst texty již při do sloupců již na "začátku", aby se
		 * vložily do tabulky.
		 */
		setLoadedText(App.getLanguage());
	}
	
	
	
	
	
	
	
		
	
	
	/**
	 * Metoda, která nastaví list, která obsahuje hodnoty v tabulce a přenačte
	 * tabulku, takže seprojeví změny v tabulce.
	 * 
	 * @param valuesList
	 *            - list s hodnotami pro tabulku.
	 */
	private void setValuesList(List<FileTableValue> valuesList) {
		this.valuesList = removeDirs(valuesList);		
		
		fireTableDataChanged();
	}
	
	
	
	
	
	
	/**
	 * Metoda, která odebere všechny adresáře z listu list a vrátí jej.
	 * 
	 * @param list
	 *            - kolekce, ze které se odeberou všechny položky, které jako file
	 *            mají adresář.
	 * 
	 * @return - list bez adresářů
	 */
	private static List<FileTableValue> removeDirs(final List<FileTableValue> list) {
		list.removeIf(value -> value.getFile().isDirectory());
		
		return list;
	}
	
	
	
	
	
	/**
	 * Metoda, která vrátí hodnotu v listu valuesList (list s hodnotami v tabulce)
	 * na zadaném indexu index.
	 * 
	 * @param index
	 *            - pozice porvku v listu, která se má vrátit.
	 * 
	 * @return objekt v tabulce (FileTableValue) na zadaném indexu - pozici v listu.
	 */
	final FileTableValue getFileTableValueAt(final int index) {
		return valuesList.get(index);
	}
	
	
	
	
	
	
	
	
	
	@Override
	public int getRowCount() {
		return valuesList.size();
	}

	@Override
	public int getColumnCount() {
		return columns.length;
	}

	
	@Override
	public String getColumnName(int column) {
		return columns[column];
	}
	
	
	@Override
	public Class<?> getColumnClass(int columnIndex) {
		final Object obj = getValueAt(0, columnIndex);

		if (obj != null)
			return obj.getClass();

		return super.getColumnClass(columnIndex);
	}
	
	
	@Override
	public Object getValueAt(int rowIndex, int columnIndex) {
		final FileTableValue value = valuesList.get(rowIndex);
		
		switch (columnIndex) {
		case 0:
			table.getColumnModel().getColumn(columnIndex).setPreferredWidth(10);
			return value.isSelected();
			
			
			
		case 1:
			table.setRowHeight(rowIndex, 25);
			table.getColumnModel().getColumn(columnIndex).setPreferredWidth(25);

			/*
			 * Zde musím testovat i zda soubor ještě existuje, protože pokud například
			 * uživatel označí více souborů, které chce přejmenovat, pak se i při tom, jak
			 * je otevřen dialog pro zadání nového názvu pořád přenačítá tabulka, proto,
			 * dokud se nepřenačtou data, pak je třeba počítat s tím, že ten soubor chvíli
			 * existovat nebude, i když se přejmenuje, tak v tabulce se to projeví, až se
			 * přenačtou všechna data, tzn. že se zavolá metoda pro přenačtení, ale ta se
			 * zavolá až co se všechny soubory přejmenují, jinak bych musel do toho cyklu
			 * pro přejmenování souborů u příslušné abstraktní akce při každém přejmenování
			 * dát i přenačtení dat v tabulce, což by mohlo trochu zpomalovat, tak jsem to
			 * vynechal.
			 */
			if (value.getFile().exists() && Pictures.isImageForShow(value.getFile())) {
				try {
					final Image image = ImageIO.read(value.getFile());
					if (image != null)
						return Pictures.resizeImage(image, 25, 25);
				} catch (IOException e) {
					e.printStackTrace();
				}
			}

			else {
				final ImageIcon image = Pictures.loadImage(
						"/fileExplorerIcons/tree/" + TreeRenderer.getIconForLabel(value.getFile()),
						KindOfPicture.FILE_EXPLORER_TABLE_ICON);

				if (image != null)
					return image;
			}
			
			return new ImageIcon();
			
			
		case 2:
			return FilenameUtils.removeExtension(value.getFile().getName());
			
			
		case 3:
			if (!value.getFile().isDirectory())
				return "." + FilenameUtils.getExtension(value.getFile().getName());
			return "-";

		case 4:
			final String pathToParent = value.getFile().getParent();

			if (pathToParent != null && !pathToParent.isEmpty())
				return pathToParent;
			return "-";
			
			
			
		case 5:
			table.getColumnModel().getColumn(columnIndex).setPreferredWidth(10);
			return value.isFileHidden();
			
			
		case 6:
			return getFileSizeInFormat(value.getLength(), Menu.getShowSizeInJTable());
					
		case 7:
			return value.getCreationDate();
			
						
		case 8:
			return value.getLastAccessDate();
			

		case 9:
			return value.getLastModifiedDate();

		default:
			break;
		}
		
		return null;
	}



	
	
	@Override
	public boolean isCellEditable(int rowIndex, int columnIndex) {
		return columnIndex == 0 || columnIndex == 2 || columnIndex == 3 || columnIndex == 5;
	}
	
	
	
	
	
	
	@Override
	public void setValueAt(Object aValue, int rowIndex, int columnIndex) {
		final FileTableValue value = valuesList.get(rowIndex);		
		
		switch (columnIndex) {
		// Označení chcb, takže se jen nastaví logická proměnná
		case 0:
			value.setSelected((boolean) aValue);
			fireTableDataChanged();
			break;
			
			

		// Název souboru - Přejmenuje se soubor - pouze jeho název
		case 2:
			final String newName = (String) aValue;
			
			if (!newName.matches(Constants.REG_EX_FILE_NAME)) {
				JOptionPane.showMessageDialog(null, txtNewNameErrorText, txtNewNameErrorTitle,
						JOptionPane.ERROR_MESSAGE);
				return;
			}
			
			// Zde je nový název v pořádku, tak jej mohu přejmenovat:
			final File newFile_1 = new File(value.getFile().getParent() + File.separator + newName + "."
					+ FilenameUtils.getExtension(value.getFile().getName()));

			if (renameFile(value.getFile(), newFile_1))
				value.setFile(newFile_1);
			break;
			
			

		// Změní se pouze přípona souboru:
		case 3:
			final String newExtension = (String) aValue;

			if (!newExtension.matches(Constants.REG_EX_FOR_FILE_EXTENSION)) {
				JOptionPane.showMessageDialog(null, txtNewExtensionErrorText, txtNewExtensionErrorTitle,
						JOptionPane.ERROR_MESSAGE);
				return;
			}
			
			// Zde je nová přípona v pořádku, tak mohu přejmenovat soubor:
			final File newFile_2 = new File(value.getFile().getParent() + File.separator
					+ FilenameUtils.removeExtension(value.getFile().getName()) + newExtension);

			if (renameFile(value.getFile(), newFile_2))
				value.setFile(newFile_2);
			break;
			
			

		// Zde se přidá / odebere tečka na začátku názvu souboru:
		case 5:
			showHideValue(value);
			
			
			// Zde si uložím do proměnné ve FileExploreru výčtový typ zvoleného typu souboru
			// (dle uživatele v cmb)
			FileExplorer.fillSelectedKindOfFiles();		
			
			// Zde vyfiltruji hodnoty, které se mají zobrazit v tabulce dle výše získané
			// hodnoty výčtového typu:
			filterData(FileExplorer.getShowFilesEnum(), FileExplorer.getTableValuesFromSelectedDir());
			
			break;

		default:
			break;
		}
		
		

		// Znovu zde označím řádek, který byl (pokud to ještě jde):
		if (table.getRowCount() > 0 && table.getRowCount() > rowIndex)
			table.setRowSelectionInterval(rowIndex, rowIndex);
	}
	
	
	
	
	
	
	
	
	/**
	 * Metoda, která zviditelní / skryje položku, resp. soubor, který se nachází,
	 * resp. který reprezentuje objekt value.
	 * 
	 * Pokud je daný soubor v objektu value skrytý, pak bude zobrazen a naopak.
	 * 
	 * @param value
	 *            - objekt, který reprezentuje nějaký soubor na disku v PC.
	 */
	private void showHideValue(final FileTableValue value) {
		/*
		 * Zde potřebuji nejprve otestovat, zda aplikace běží na OS linux nebo Windows, protože na linuxu se poznají skryté soubory tak, že
		 * na začátku názvu obsahuje desetinnou tečku, kdežto na OS Windows je to "atribut", ne tečka v názvu.
		 */

		// Test pro OS Linux:
		if (App.KIND_OF_OS.equalsIgnoreCase("Linux")) {
			if (!value.isFileHidden()) {
				// Zde soubor není skrytý, tak k jeho názvu přidám tečku - na začátek názvu:
				final File newFile_3 = new File(
						value.getFile().getParent() + File.separator + "." + value.getFile().getName());

				if (renameFile(value.getFile(), newFile_3))
					value.setFile(newFile_3);
			} else {
				/*
				 * Zde je soubor skrytý, tak pouze odeberu desetinnou tečku ze začátku souboru a
				 * přejmenuji jej:
				 *
				 * I když je to zbytečné, pro jistotu otestuji, zda se na začátku souboru
				 * nachází desetinná tečka - mělo by to tak být vždy, když je soubor skrytý:
				 */
				if (value.getFile().getName().startsWith(".")) {
					final File newFile_4 = new File(
							value.getFile().getParent() + File.separator + value.getFile().getName().substring(1));

					if (renameFile(value.getFile(), newFile_4))
						value.setFile(newFile_4);
				}
				// else Zde není co dělat - neni zde tečka k odebrání.
			}
		}


		// Test pro OS Windows:
		else if (App.KIND_OF_OS.toUpperCase().contains("Windows".toUpperCase())) {

			try {
				/*
				 * Zde otestuj, zda je soubor skrytý, pokud ano, pak odkryji, pokud není skrytý, skryji jej.
				 */
				if (value.isFileHidden())
					Files.setAttribute(value.getFile().toPath(), "dos:hidden", false, LinkOption.NOFOLLOW_LINKS);

				else Files.setAttribute(value.getFile().toPath(), "dos:hidden", true, LinkOption.NOFOLLOW_LINKS);
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
	}
	
	
	
	
	
	
	
	
	
	
	/**
	 * Metoda, která vrátí označené položky v tabulce.
	 * 
	 * Metoda projde všechny položky aktuálně zobrazené v tabulce, ukaždé otestuje,
	 * zda je položka označena a pokud ano, pak její proměnnou typu File (v podstatě
	 * soubor, který položka v tabulce reprezentuje) vloží do kolekce, která se na
	 * konec vrátí.
	 * 
	 * @return list typu File, který obsahuje označené položky v tabulce.
	 */
	final List<File> getSelectedFiles() {
		final List<File> list = new ArrayList<>();

		valuesList.stream().filter(FileTableValue::isSelected).forEach(f -> list.add(f.getFile()));
		
		return list;
	}
	
	
	
	
	
	
	
	
	
	
	
	/**
	 * Metoda, která přejmnuje soubor z originalFile na newFile.
	 * 
	 * @param originalFile
	 *            - Původní název souboru.
	 * 
	 * @param newFile
	 *            - Nový název souboru
	 * 
	 * @return true, pokud se soubor přejmenuje vpořádku, jinak false.
	 */
	private static boolean renameFile(final File originalFile, final File newFile) {
		if (!originalFile.renameTo(newFile)) {
			JOptionPane.showMessageDialog(null,
					txtRenameFileErrorText_1 + "\n" + txtRenameFileErrorText_2 + ": " + originalFile.getAbsolutePath()
							+ "\n" + txtRenameFileErrorText_3 + ": " + newFile.getAbsolutePath(),
					txtRenameFileErrorTitle, JOptionPane.ERROR_MESSAGE);
			return false;
		}
		return true;
	}
	
	
	
	
	
	
	
	

					

					
	
	/**
	 * Metoda, která vyfiltruje položky, které tam mají být, jedná se o položky dle
	 * výčtového typu.
	 * 
	 * "Metoda vyfiltruje položky, které jsou jsou / nejsou skryté, nebo zobrazí
	 * všechny."
	 * 
	 * @param showFilesEnum
	 *            - výčtový typ, který určuje, jaké položky se mají zobrazit.
	 * 
	 * @param list
	 *            - list s hodnotami, který semá filtrovat.
	 */
	final void filterData(final ShowFilesEnum showFilesEnum, final List<FileTableValue> list) {
		if (showFilesEnum == ShowFilesEnum.SHOW_ALL)
			setValuesList(list);

		else if (showFilesEnum == ShowFilesEnum.SHOW_HIDDEN_FILES)
			setValuesList(list.stream().filter(v -> v.getFile().isHidden()).collect(Collectors.toList()));

		else if (showFilesEnum == ShowFilesEnum.SHOW_VISIBLE_FILES)
			setValuesList(list.stream().filter(v -> !v.getFile().isHidden()).collect(Collectors.toList()));
	}
	
	
	
	
					
					

	
	
	/**
	 * Metoda, která vyfiltruje pouze ty položky v tabulce, které obsahují
	 * požadovaný název souboru.
	 * 
	 * Metoda si načte všechny položky ze zvoleného adresáře, převede je na objekty,
	 * které se vkládají do tabulky a z tohoto listu se vyfiltrují pouze ty položky,
	 * které mají jako název souboru název v parametru name (ignorují se velikosti
	 * písmen)
	 * 
	 * @param name
	 *            název souboru, který mají položky obsahovat
	 */
	final void filterName(final String name) {
		/*
		 * Postup:
		 * Načtu si ze zvoleného adresáře, ty se převedou na objekty, které se zobrazují
		 * v tabulce a z tech vyfiltruji pouze ty, které splňují kritéria, v tomto
		 * případě odpovídá název souboru a pak se zavolá metoda filterData, kam předám
		 * list vyfiltrovaných položek a tato metoda ještě vyfiltruje ty položky, které
		 * se mají nebo nemají zobrazit (skryté soubory apod.)
		 */

		filterData(FileExplorer.getShowFilesEnum(), FileExplorer.getTableValuesFromSelectedDir().stream().filter(
				v -> FilenameUtils.removeExtension(v.getFile().getName()).toUpperCase().contains(name.toUpperCase()))
				.collect(Collectors.toList()));
	}
	
	
	
	
	
	
	/**
	 * Metoda, která vyfiltruje pouze ty položky v tabulce, které obsahují
	 * požadovanou velikost souboru.
	 * 
	 * Metoda si načte všechny položky ze zvoleného adresáře, převede je na objekty,
	 * které se vkládají do tabulky a z tohoto listu se vyfiltrují pouze ty položky,
	 * které mají velikost lenght (získanou z ple od uživatele).
	 * 
	 * Note:
	 * Bylo by možné doplnit nějaké ozmezí, například +- 5 kb k té velikosti (zda je
	 * veliokst souboru v intervalu a zároveň v kladu)
	 * 
	 * @param length
	 *            velikost souboru, kterou hledá uživatel.
	 */
	final void filterFileLength(final double length) {
		filterData(FileExplorer.getShowFilesEnum(), FileExplorer.getTableValuesFromSelectedDir().stream()
				.filter(v -> v.getLength() == length).collect(Collectors.toList()));
	}
	
	
	
	
	
	
	
	/**
	 * Metoda, která vyfiltruje pouze ty položky v tabulce, které obsahují
	 * požadovanou příponu souboru.
	 * 
	 * Metoda si načte všechny položky ze zvoleného adresáře, převede je na objekty,
	 * které se vkládají do tabulky a z tohoto listu se vyfiltrují pouze ty položky,
	 * které mají příponu extension (získanou z ple od uživatele).
	 * 
	 * extension je v syntaxi například: ".jpg" a jsou ignorovány velikosti písmen.
	 * 
	 * @param extension
	 *            - přípona souboru, kterou mají soubory obsahovat, aby byly
	 *            zobrazeny.
	 */
	final void filterFileExtension(final String extension) {
		filterData(FileExplorer.getShowFilesEnum(),
				FileExplorer.getTableValuesFromSelectedDir().stream()
						.filter(v -> ("." + FilenameUtils.getExtension(v.getFile().getName())).toUpperCase()
								.contains(extension.toUpperCase()))
						.collect(Collectors.toList()));
	}
	
	
	
	

	
	
	/**
	 * Metoda, která vyfiltruje pouze ty položky (soubory), které splňují požadované
	 * datum (kindOfDateTime), které mají přesně, dříve nebo mladší než zadané datum
	 * date, toto se testuje dle hodnoty dateTimeCompare.
	 * 
	 * "Metoda vyfiltruje puze ty soubory, které mají datum starší, mladší nebo
	 * stejné (dle hodnoty dateTimeCompare) než zadané datum od uživatele a to datum
	 * může být datum vytvoření nebo poslední přístup nebo poslední změna (dle
	 * hodnoty kindOfDateTime)"
	 * 
	 * @param date
	 *            - datum získaný od uživatele.
	 * 
	 * @param dateTimeCompare
	 *            - výčtový typ, který definuje "jak" se mají datumy porovnávat, tj.
	 *            zda je datum získané ze souboru větší, menší nebo stejné než
	 *            zadané datum (date)
	 * 
	 * @param kindOfDateTime
	 *            - výčtový typ, který značí, "jaký typ datumu se má porovnávat",
	 *            tj. může to být datum vytvoření souboru, datum poslední změny
	 *            soubory nebo datum posledního přístupu k souboru.
	 */
	final void filterDate(final Date date, final DateTimeCompare dateTimeCompare,
			final KindOfDateTime kindOfDateTime) {
		/*
		 * Nejprve otestuji, zda se má testovat datum vytvoření, datum poslední změny
		 * nebo datum posledního přístupu, pak v každé podmínce zjistím, zda má
		 * testovaný datum být stejné, starší nebo mladší a dle toho se filtrují položky
		 * do tabulky.
		 */

		/*
		 * V tomto formátu se bude porovnávat datum
		 */
		final SimpleDateFormat formatter = new SimpleDateFormat("dd.MM.yyyy");

		if (dateTimeCompare == DateTimeCompare.BEFORE) {
			filterData(FileExplorer.getShowFilesEnum(),
					FileExplorer.getTableValuesFromSelectedDir().stream().filter(v -> {
						// Získám si datum, který se má porovnat:
						final Date dateForCompare = getFormatedDate(formatter, v, kindOfDateTime);

						// Zjistím, zda datum souboru má být starší / před získaným datumem (date):
						return dateForCompare.compareTo(date) < 0;
					}).collect(Collectors.toList()));
		}

		else if (dateTimeCompare == DateTimeCompare.EQUAL) {
			filterData(FileExplorer.getShowFilesEnum(),
					FileExplorer.getTableValuesFromSelectedDir().stream().filter(v -> {
						// Získám si datum, který se má porovnat:
						final Date dateForCompare = getFormatedDate(formatter, v, kindOfDateTime);

						// Zjistím, zda jsou datumy stejné:
						return dateForCompare.compareTo(date) == 0;
					}).collect(Collectors.toList()));
		}

		else if (dateTimeCompare == DateTimeCompare.AFTER) {
			filterData(FileExplorer.getShowFilesEnum(),
					FileExplorer.getTableValuesFromSelectedDir().stream().filter(v -> {
						// Získám si datum, který se má porovnat:
						final Date dateForCompare = getFormatedDate(formatter, v, kindOfDateTime);

						// Zjistím, zda datum souboru má být mladší / po získaným datumem (date):
						return dateForCompare.compareTo(date) > 0;
					}).collect(Collectors.toList()));
		}
	}
	
	
					
	
	
	
	
	/**
	 * Metoda, která získá datum (nebo čas - dle hodnoty formatter) vytvoření,
	 * posledního přístupu nebo poslední změny souboru, zformátuje jej na pouze
	 * datum nebo pouze čas (to druhé vždy "vymaže) - dle formatteru a tento datum
	 * nebo čas vrátí.
	 * 
	 * @param formatter
	 *            - jedná se o "formát", tj. datum nebo čas vytvoření, poslední
	 *            změny či přístupu k souboru, který se má získat.
	 * 
	 * @param value
	 *            - objekt, který se vkládá do tabulky, ze kterého se berou data
	 *            ohledně souburu (výše popsané datumy a časy)
	 * 
	 * @param kindOfDateTime
	 *            - výčtový typ, dle kterého se pozná, zda se má brát datum
	 *            vytvoření, posledního přístupu nebo poslendí modifikace souboru.
	 * 
	 * @return výše popsaný datum nebo čas ve formátu dle hodnoty ve formatter.
	 */
	private static Date getFormatedDate(final SimpleDateFormat formatter, final FileTableValue value,
			final KindOfDateTime kindOfDateTime) {
		if (kindOfDateTime == KindOfDateTime.CREATION_DATE_TIME) {
			Date date = new Date(value.getAttr().creationTime().toMillis());

			try {
				date = formatter.parse(formatter.format(date));
			} catch (ParseException e) {
				e.printStackTrace();
			}

			return date;
		}

		else if (kindOfDateTime == KindOfDateTime.LAST_ACCESS_DATE_TIME) {
			Date date = new Date(value.getAttr().lastAccessTime().toMillis());

			try {
				date = formatter.parse(formatter.format(date));
			} catch (ParseException e) {
				e.printStackTrace();
			}

			return date;
		}

		else if (kindOfDateTime == KindOfDateTime.LAST_MODIFIED_DATE_TIME) {
			Date date = new Date(value.getAttr().lastModifiedTime().toMillis());

			try {
				date = formatter.parse(formatter.format(date));
			} catch (ParseException e) {
				e.printStackTrace();
			}

			return date;
		}

		// Toto by nemělo nastat:
		return new Date();
	}
	
	
	
	
	
	
	
	
	
	
	final void filterTime(final Date time, final DateTimeCompare dateTimeCompare,
			final KindOfDateTime kindOfDateTime) {
		/*
		 * Nejprve otestuji, zda se má testovat čas vytvoření, čas poslední změny nebo
		 * čas posledního přístupu, pak v každé podmínce zjistím, zda má testovaný čas
		 * být stejný, starší nebo mladší a dle toho se filtrují položky do tabulky.
		 */

		/*
		 * V tomto formátu se bude porovnávat čas
		 */
		final SimpleDateFormat formatter = new SimpleDateFormat("HH:mm:ss");

		if (dateTimeCompare == DateTimeCompare.BEFORE) {
			filterData(FileExplorer.getShowFilesEnum(),
					FileExplorer.getTableValuesFromSelectedDir().stream().filter(v -> {
						// Získám si čas, který se má porovnat:
						final Date timeForCompare = getFormatedDate(formatter, v, kindOfDateTime);

						// Zjistím, zda čas souboru je starší / před získaným časem (time):
						return timeForCompare.compareTo(time) < 0;
					}).collect(Collectors.toList()));
		}

		
		else if (dateTimeCompare == DateTimeCompare.EQUAL) {
			filterData(FileExplorer.getShowFilesEnum(),
					FileExplorer.getTableValuesFromSelectedDir().stream().filter(v -> {
						// Získám si čas, který se má porovnat:
						final Date timeForCompare = getFormatedDate(formatter, v, kindOfDateTime);

						// Zjistím, zda čas souboru je jako získaný čas (time):
						return timeForCompare.compareTo(time) == 0;
					}).collect(Collectors.toList()));
		}

		
		else if (dateTimeCompare == DateTimeCompare.AFTER) {
			filterData(FileExplorer.getShowFilesEnum(),
					FileExplorer.getTableValuesFromSelectedDir().stream().filter(v -> {
						// Získám si čas, který se má porovnat:
						final Date timeForCompare = getFormatedDate(formatter, v, kindOfDateTime);

						// Zjistím, zda čas souboru je mladší / po získaném čase (time):
						return timeForCompare.compareTo(time) > 0;
					}).collect(Collectors.toList()));
		}
	}
	
	
	
	
	
	
	
	
	
	
	
	/**
	 * Metoda, která označí / odznačí (dle hodnoty v proměnné select) veškeré
	 * položky aktuálně zobrazené v tabulce.
	 * 
	 * @param select
	 *            - logická hodnota, true - označit všchny položky, false - Odznačí
	 *            všechny položky
	 */
	final void selectAllItems(final boolean select) {
		valuesList.forEach(v -> {
			if (v.isSelected() != select)
				v.setSelected(select);
		});
		
		fireTableDataChanged();
	}
	
	
			
	
	
	
	
	
	/**
	 * Metoda, která všechny soubory v tabulce skryje nebo zobrazí, tj. buď před
	 * jejich název vloží desetinnou tečku nebo ji odebere.
	 * 
	 * Konrétně projde všechny položky aktuálně zobrazené v tabulce a u každé se
	 * otestuje, zda se to, zda je skryý rovná hodnota v proměnné hide, pokud ne,
	 * pak se nastaví naopak, tj. pokud je soubor skrytý, pak se zvoditelní a
	 * naopak.
	 * 
	 * @param hide
	 *            - logická proměnná o tom, zda se mají položky zobrazit nebo
	 *            schovat.
	 */
	final void showHideAllItems(final boolean hide) {
		valuesList.forEach(v -> {
			if (v.isFileHidden() != hide)
				showHideValue(v);
		});
		
		// Zde si uložím do proměnné ve FileExploreru výčtový typ zvoleného typu souboru
		// (dle uživatele v cmb)
		FileExplorer.fillSelectedKindOfFiles();		
		
		// Zde vyfiltruji hodnoty, které se mají zobrazit v tabulce dle výše získané
		// hodnoty výčtového typu:
		filterData(FileExplorer.getShowFilesEnum(), FileExplorer.getTableValuesFromSelectedDir());
	}
	
	
								

	
	
	
	
	
	
	
	/**
	 * Metoda, která nastaví jako viditelné nebo skryté označené soubory v tabulce.
	 * 
	 * Metoda vyfiltruje pouze označené položky v tabulce a pak pro každou položku
	 * zjistí, zda je skrytý nebo ne, pokud není a má být, pak se skryje, tj. vloží
	 * se za začátek názvu souboru desetnná tečka. Pokud je skrytý a má být
	 * viditelný, pak se desetinná tečka odebere ze začátku názvu souboru.
	 * 
	 * @param hide
	 *            - logická proměnná, která obsahuje true, pokud se mají soubory
	 *            skrýt, jinak true, pokud se mají soubory zviditelnit.
	 */
	final void showHideSelectedItems(final boolean hide) {
		valuesList.stream().filter(FileTableValue::isSelected).forEach(v -> {
			if (v.isFileHidden() != hide)
				showHideValue(v);
		});
		
		// Zde si uložím do proměnné ve FileExploreru výčtový typ zvoleného typu souboru
		// (dle uživatele v cmb)
		FileExplorer.fillSelectedKindOfFiles();		
		
		// Zde vyfiltruji hodnoty, které se mají zobrazit v tabulce dle výše získané
		// hodnoty výčtového typu:
		filterData(FileExplorer.getShowFilesEnum(), FileExplorer.getTableValuesFromSelectedDir());
	}
	
	
	
	
	
	
	
	
	
	
	
	
	/**
	 * Metoda, která dle výčtové hodnoty sizeEnum zjistí jaký formát velikost
	 * souboru (fileLength) má vrátit a dle toho vypočítá a vrátí velikost souboru v
	 * požadovaném formátu, například v KB, MB, ...
	 * 
	 * Zdroj pro výpočty: https://www.mkyong.com/java/how-to-get-file-size-in-java/
	 * 
	 * @param fileLength
	 *            - velikost soubory v bytech
	 * 
	 * @param sizeEnum
	 *            - hodnoty výčtového typu, která definuje formát velikosti souborů,
	 *            jaký se má vypočítat z fileLength a vrátit.
	 * 
	 * @return velikost souborů ve formátu sizeEnum, tj. například v KB, MB, TB atd.
	 *         Toto by nastat nemělo, ale pokud se nepozná sizeEnum, pak se vrátí
	 *         výchozí hodnota fileLength v kylobytech (KB), ale toto by nastat
	 *         nemělo.
	 */
	private static double getFileSizeInFormat(final long fileLength, final SizeEnum sizeEnum) {
		final double bytes = fileLength;
		final double kilobytes = (fileLength / 1024);
		final double megabytes = (kilobytes / 1024);
		final double gigabytes = (megabytes / 1024);
		final double terabytes = (gigabytes / 1024);
		final double petabytes = (terabytes / 1024);
		final double exabytes = (petabytes / 1024);
		final double zettabytes = (exabytes / 1024);
		final double yottabytes = (zettabytes / 1024);

		if (sizeEnum == SizeEnum.B)
			return bytes;

		else if (sizeEnum == SizeEnum.KB)
			return kilobytes;

		else if (sizeEnum == SizeEnum.MB)
			return megabytes;

		else if (sizeEnum == SizeEnum.GB)
			return gigabytes;

		else if (sizeEnum == SizeEnum.TB)
			return terabytes;

		else if (sizeEnum == SizeEnum.PB)
			return petabytes;

		else if (sizeEnum == SizeEnum.EB)
			return exabytes;

		else if (sizeEnum == SizeEnum.ZB)
			return zettabytes;

		else if (sizeEnum == SizeEnum.YB)
			return yottabytes;

		// Výchozí hodnota (toto by nastat nemělo):
		return kilobytes;
	}
	
	
	
	
									    
									    
									    
									    
									    
		

	@Override
	public void setLoadedText(final Properties prop) {
		if (prop != null) {
			String[] tempColumns = CreateLocalization.getArrayFromText(prop.getProperty("FTM_Txt_File_Columns"));
			
			if (tempColumns == null || tempColumns.length != Constants.FILE_COLUMNS.length)
				tempColumns = Constants.FILE_COLUMNS;
			
			columns = tempColumns;
			
			
			txtNewNameErrorText = prop.getProperty("FTM_Txt_New_Name_Error_Text", Constants.FTM_TXT_NEW_NAME_ERROR_TEXT);
			txtNewNameErrorTitle = prop.getProperty("FTM_Txt_New_Name_Error_Title", Constants.FTM_TXT_NEW_NAME_ERROR_TITLE);
			
			txtNewExtensionErrorText = prop.getProperty("FTM_Txt_New_Extension_Error_Text", Constants.FTM_TXT_NEW_EXTENSION_ERROR_TEXT);
			txtNewExtensionErrorTitle = prop.getProperty("FTM_Txt_New_Extension_Error_Title", Constants.FTM_TXT_NEW_EXTENSION_ERROR_TITLE);
		
			txtRenameFileErrorText_1 = prop.getProperty("FTM_Txt_Rename_File_Error_Text_1", Constants.FTM_TXT_RENAME_FILE_ERROR_TEXT_1);
			txtRenameFileErrorText_2 = prop.getProperty("FTM_Txt_Rename_File_Error_Text_2", Constants.FTM_TXT_RENAME_FILE_ERROR_TEXT_2);
			txtRenameFileErrorText_3 = prop.getProperty("FTM_Txt_Rename_File_Error_Text_3", Constants.FTM_TXT_RENAME_FILE_ERROR_TEXT_3);
			txtRenameFileErrorTitle = prop.getProperty("FTM_Txt_Rename_File_Error_Title", Constants.FTM_TXT_RENAME_FILE_ERROR_TITLE);
			
		}
		
		
		else {
			columns = Constants.FILE_COLUMNS;
			
			txtNewNameErrorText = Constants.FTM_TXT_NEW_NAME_ERROR_TEXT;
			txtNewNameErrorTitle = Constants.FTM_TXT_NEW_NAME_ERROR_TITLE;
			
			txtNewExtensionErrorText = Constants.FTM_TXT_NEW_EXTENSION_ERROR_TEXT;
			txtNewExtensionErrorTitle = Constants.FTM_TXT_NEW_EXTENSION_ERROR_TITLE;
		
			txtRenameFileErrorText_1 = Constants.FTM_TXT_RENAME_FILE_ERROR_TEXT_1;
			txtRenameFileErrorText_2 = Constants.FTM_TXT_RENAME_FILE_ERROR_TEXT_2;
			txtRenameFileErrorText_3 = Constants.FTM_TXT_RENAME_FILE_ERROR_TEXT_3;
			txtRenameFileErrorTitle = Constants.FTM_TXT_RENAME_FILE_ERROR_TITLE;
		}
		
		columns[6] = columns[6] + " (" + Menu.getShowSizeInJTable().getSizeInText() + ")";
	}
}
