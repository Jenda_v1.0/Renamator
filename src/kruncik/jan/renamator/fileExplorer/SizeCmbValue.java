package kruncik.jan.renamator.fileExplorer;

/**
 * Tato třída slouží pouze jako objekt pro vkládání do cmbSize v nastavení, tj.
 * ve třídě SettingForm.
 * 
 * @author Jan Krunčík
 * @version 1.0
 */

public class SizeCmbValue {
	
	/**
	 * Proměnná, která obsahuje formát velikost souboru v textu, který bude zobrazen
	 * v komponentě cmbSize v dialogu nastavení (SettingsForm).
	 */
	private final String sizeInText;
	
	
	/**
	 * Výčtová hodnota, který definuje požadovaný formát velikosti souboru.
	 */
	private final SizeEnum sizeEnum;


	
	/**
	 * Konstruktor této třídy.
	 * 
	 * @param sizeEnum
	 *            - výčtová hodnota, dle které se pozná v jakém velikostím formátu
	 *            se má zobrazit velikost souboru v tabulce v průzkumníku souborů.
	 */
	public SizeCmbValue(final SizeEnum sizeEnum) {
		super();
		
		this.sizeEnum = sizeEnum;
		
		this.sizeInText = sizeEnum.getSizeInText();
	}
	
	

	public SizeEnum getSizeEnum() {
		return sizeEnum;
	}
	
	

	
	@Override
	public String toString() {
		return sizeEnum.getSizeInText();
	}
}
