package kruncik.jan.renamator.fileExplorer;

/**
 * Tento výše slouží pro definování toho, v jakém formátu velikostí se mají
 * zobrazovat velikost souborů v tabulce.
 * 
 * Například v KB, MB, TB, ...
 * 
 * @see Enum
 * 
 * @author Jan Krunčík
 * @version 1.0
 */

public enum SizeEnum {

	B("B"), KB("KB"), MB("MB"), GB("GB"), TB("TB"), PB("PB"), EB("EB"), ZB("ZB"), YB("YB");

	
	
	
	
	
	/**
	 * Proměnná, která obsahuje název velikosti souboru příslušného formátu
	 * převedený na text. například KB, MB, ...
	 */
	private final String sizeInText;

	
	
	/**
	 * Konstruktor pro naplnění proměnné
	 * 
	 * @param sizeInText
	 *            - formát velikost souboru převedný na text.
	 */
	SizeEnum(final String sizeInText) {
		this.sizeInText = sizeInText;
	}

	
	
	/**
	 * Getr na proměnnou, která obsahuje název velikosti souboru převedenou na text.
	 * 
	 * @return výše popsanou proměnnou.
	 */
	public String getSizeInText() {
		return sizeInText;
	}
}
