package kruncik.jan.renamator.fileExplorer;

import java.awt.FlowLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Properties;

import javax.swing.DefaultComboBoxModel;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JTextField;
import javax.swing.JToolBar;

import kruncik.jan.renamator.localization.Constants;
import kruncik.jan.renamator.localization.CreateLocalization;
import kruncik.jan.renamator.localization.LocalizationImpl;

/**
 * Tato třída slouží jako toolbar, který obsahuje komponenty pro filtrování dat
 * v tabulce.
 * 
 * @see JToolBar
 * @see Properties
 * 
 * @author Jan Krunčík
 * @version 1.0
 */

public class FilterToolBar extends JToolBar implements LocalizationImpl, ActionListener {
	
	private static final long serialVersionUID = 1L;

	
	
	/**
	 * Regulární výraz pro celé nebo desetinné číslo v syntaxi: Buď jsou to pouze
	 * čísla, nebo čísla, pak desetinná tečka a pak zase čísla, Výraz netestuje
	 * mezery, je třeba je odebrat před otestováním textu na tento výraz.
	 */
	private static final String REG_EX_INT_DOUBLE_VALUE = "^([\\d+]+|[\\d+]+\\.[\\s\\d+]+)$";
	
	
	
	/**
	 * Regulární výraz pro datum v syntaxi: dd.MM.yyyy
	 * 
	 * Musí se před otestováním odebrat všechny mezery, výraz nepočítá s mezerami, u
	 * dnů se testoují hodnota 1 - 31, u měsíců 1 - 12 a u roku mohou být libolvná 4
	 * čísla.
	 */
//	private static final String REG_EX_DATE = "^\\d{2}\\.(0\\d|\\d0|\\d1|\\d2)\\.\\d{4}$";
	private static final String REG_EX_DATE = "^([1-9]|0[1-9]|1\\d|2\\d|30|31)\\.([1-9]|0[1-9]|10|11|12|)\\.\\d{4}$";
	
	
	
	
	/**
	 * Regulrání výraz pro čas v syntaxi: hh:mm:ss (bez mezer, ty je třeba odebrat
	 * před testováním výrazu)
	 */
	private static final String REG_EX_TIME = "^(\\d|0\\d|1\\d|20|21|22|23):(\\d|0\\d|1\\d|2\\d|3\\d|4\\d|5\\d):(\\d|0\\d|1\\d|2\\d|3\\d|4\\d|5\\d)$";
	
	
	
	
	
	/**
	 * Komponenta, kve které si uživatel vybere "paramer" souboru, dl ekterého chce
	 * vyhledávat, například název souboru, dle jeho přípny apod,
	 */
	private final JComboBox<String> cmbFilter;
	
	
	
	
	/**
	 * Label, který nese pouze text filtr - informace pr ouživatele.
	 */
	private final JLabel lblFilter;
	
	
	
	
	/**
	 * Textové pole, které slouží pro zadání textu pro vyhledání.
	 */
	private final JTextField txtSearch;
	
	
	
	
	
	
	/**
	 * Tlačítko, které slouží pro vyhledání mezi zobrazenými položkami v tabulce. Po
	 * kliknutí na toto tlačítko se vyhledají data v aktuálně zobrazených datech v
	 * tabulce dle zvolených a zadaného parametru.
	 */
	private final JButton btnSearch;	
	
	
	
	
	
	
	/**
	 * Label, který obsahuje text Zobrazit.
	 */
	private final JLabel lblShowFiles;
	
	
	/**
	 * Komponenta, ve které si uživatel může zvolit, jaké typy souborů se mají
	 * zobrazit, zda všechny soubory ve zvoleném adresáři, nebo jen skryté soubory,
	 * nebo jen viditelné soubory.
	 */
	private final JComboBox<String> cmbShowFiles;
	
	
	
	
	
	
	/**
	 * Proměnné pro texty do toolbaru - do textových polí apod.
	 */
	private String txtExampleName;
	
	
	
	
	/**
	 * Reference na table model, je zde potřeba kvůli filtrování souborů v tabulce.
	 */
	private final FileTableModel fileTableModel;
	
	
	
	
	
	/**
	 * Tento adatér slouží pro vyhledání nějakých hodnot / hodnoty v tabulce dle
	 * zadaného kritéria.
	 */
	private KeyAdapter searchKeyAdapter;
	
	
	
	
	
	/**
	 * Proměnné pro texty do chybových hlášek ve zvoleném jazyce.
	 */
	private static String txtWrongTimeText, txtWrongTimeTitle, txtWrongDateText, txtWrongDateTitle;
	
	
	
	
	
	
	
	
	/**
	 * Konstruktor této třídy.
	 */
	FilterToolBar(final FileTableModel fileTableModel) {
		super();
		
		this.fileTableModel = fileTableModel;
		
		setLayout(new FlowLayout(FlowLayout.CENTER, 10, 0));
		
		
		lblFilter = new JLabel();
		add(lblFilter);
		
		
		
		cmbFilter = new JComboBox<>();
		cmbFilter.addActionListener(this);
		add(cmbFilter);
			
		
		
		createKeyAdapter();
		
		txtSearch = new JTextField(20);
		txtSearch.addKeyListener(searchKeyAdapter);
		add(txtSearch);
		
		
		
		
		
		btnSearch = new JButton();
		btnSearch.addActionListener(this);
		add(btnSearch);
		
		
		
		
		
		addSeparator();
		
		
		lblShowFiles = new JLabel();
		add(lblShowFiles);
		
		cmbShowFiles = new JComboBox<>();
		cmbShowFiles.addActionListener(this);
		add(cmbShowFiles);
	}


	
	
	
	
	
	/**
	 * Metoda, která vytvoří instanci KeyAdapteru, který při uvolnění nějaké klávesy
	 * v testovém poli pro vyhledání vyhledá nějaké hodnoty v tabulce.
	 */
	private void createKeyAdapter() {
		searchKeyAdapter = new KeyAdapter() {
			@Override
			public void keyReleased(KeyEvent e) {
				searchText();
			}
		};
	}
	
	
	
	
	
	
	
	
	/**
	 * Metoda,k terá vyhledá nějaké soubory dle zadaných kritérií.
	 * 
	 * V podstatě pouze zjistí, dle jakého kritérie se má vyhledávat, dle toho se
	 * pozná, jaká metoda pro filtrování hodnota nad table modelem se má zavolat a
	 * předá se jí text v textovém poli.
	 * 
	 * Pokud je textové pole pro vyhledání nějakých hodnot v tabulce prázdné, pak se
	 * zobrazí všechny hodnoty ve zvoleném adresáři.
	 */
	private void searchText() {
		/*
		 * Zde se vyhledá nějaké hodnoty ve zvoleném adresáři - hodnoty v tabulce
		 * soubory s požadovanými vlastnostmi.
		 * 
		 * Note:
		 * Budu testovat označenéhodnoty dle indexu, protože názvy mohou být v různých
		 * jazycích, ale jeden název v jakémkoliv jazyce reprezentuje stejnou hodnotu
		 * pro vyhledávání, tak si zjistím index té hodnoty a dle toho vyfiltruji
		 * požadované hodnoty.
		 */
		
		// Označený index položky v cmb:
		final int index = cmbFilter.getSelectedIndex();
		
		/*
		 * Do proměnné text si uložím text z textového pole, mohl bych testovat, zda
		 * není text prázdný, případně obsahuje požadovanou syntaxi apod.
		 */
		final String text = txtSearch.getText();
		
		if (text.replaceAll("\\s", "").isEmpty())
			// Zde je pole prázdné, pak zobrazím všechna data ve zvoleném adresáři - dle
			// možnosti pro skryté či viditelné soubory apod.
			fileTableModel.filterData(FileExplorer.getShowFilesEnum(), FileExplorer.getTableValuesFromSelectedDir());
		
		
		
		// Vyheldávání dle názvu souboru (bez přípony):
		if (index == 0)
			fileTableModel.filterName(text);
		

		// Vyhledávání dle velikost souboru:
		else if (index == 1) {
			// Vezmu si text z textového pole a odeberu všechny mezery:
			final String textWithoutSpaces = text.replaceAll("\\s", "");

			// Otestuji jej na regulární výraz a případně přetypuji na double a zavolám
			// metodu pro filtrování:
			if (textWithoutSpaces.matches(REG_EX_INT_DOUBLE_VALUE))
				fileTableModel.filterFileLength(Double.parseDouble(textWithoutSpaces));
		}
			
		
		
		// Vyhledávání dle Přípony:
		else if (index == 2) {
			/*
			 * Note:
			 * Vím, že bych zde mohl odebrat všechny mezery testovat délku apod. ale počítám
			 * s tím, že uživatel ví, jakou příponu hledá, tak tento postup přeskočím a
			 * otestuji text pouze na regulární výraz.
			 */
			
			// Otestuji příponu na regulární výraz, pokud to bude v pořádku, vyhledám ji:
			if (text.matches(Constants.REG_EX_FOR_FILE_EXTENSION))
				fileTableModel.filterFileExtension(text);
		}
		
		
		/*
		 * Ty datumy beru tak, že když je D1 a D2, pak D1 je starší, když je dříve než
		 * D2, tj. D1 je například v lednu a D2 je v únoru, a teď je třeba březen.
		 * 
		 * D1 je mladší než D2 v případě, že D1 je za / po D2, například D1 je v únoru a
		 * D2 je v lednu, tj. D1 je po D2 (únor je po lednu).
		 * 
		 * V table modelu testuji jako prvni datum získaný ze souboru, s datumem
		 * získaným od uživatele z textového pole z aplikace, takže pokud má být
		 * například Datum přístupu starší než zadané datum, tak výčet DateTimeCompare
		 * bude BEFORE
		 */
		
		
		
		// Vyhledávání dle datumu posledního přístupu:
		else if (index == 3)
			fileTableModel.filterDate(getDateFromText(text), DateTimeCompare.EQUAL,
					KindOfDateTime.LAST_ACCESS_DATE_TIME);

		// Vyhledávání dle Datumu vytvoření
		else if (index == 4)
			fileTableModel.filterDate(getDateFromText(text), DateTimeCompare.EQUAL, KindOfDateTime.CREATION_DATE_TIME);

		// Vyhledávání dle Datumu poslední změny
		else if (index == 5)
			fileTableModel.filterDate(getDateFromText(text), DateTimeCompare.EQUAL,
					KindOfDateTime.LAST_MODIFIED_DATE_TIME);

		// Vyhledávání dle Datumu posledního přístupu k souboru staršího než zadaného
		// data (zadané datum je mladší / dříve než požadované datum souboru)
		else if (index == 6)
			fileTableModel.filterDate(getDateFromText(text), DateTimeCompare.BEFORE,
					KindOfDateTime.LAST_ACCESS_DATE_TIME);

		// Vyhledávání dle Datumu posledního přístupu k souboru mladšího než zadaného
		// data (zadané datum je starší / později než požadované datum souboru)
		else if (index == 7) {
			final Date date = getDateFromText(text);
			fileTableModel.filterDate(date, DateTimeCompare.AFTER, KindOfDateTime.LAST_ACCESS_DATE_TIME);
		}

		// Vyhledávání dle Datumu vytvoření souboru starší než zadané datum (zadané
		// datum je mladší / dříve než požadované datum souboru)
		else if (index == 8)
			fileTableModel.filterDate(getDateFromText(text), DateTimeCompare.BEFORE, KindOfDateTime.CREATION_DATE_TIME);

		// Vyhledávání dle Datumu vytvoření souboru mladší než zadané datum (zadané
		// datum je starší / později než požadované datum souboru), datum vytvořeni je v
		// unoru, zadané datum je v lednu
		else if (index == 9)
			fileTableModel.filterDate(getDateFromText(text), DateTimeCompare.AFTER, KindOfDateTime.CREATION_DATE_TIME);

		// Vyhledávání dle Datumu poslední změny souboru starší než zadané datum (zadané
		// datum je mladší / dříve než požadované datum souboru), datum změny je v
		// lednu, zadané datum je v únoru
		else if (index == 10)
			fileTableModel.filterDate(getDateFromText(text), DateTimeCompare.BEFORE,
					KindOfDateTime.LAST_MODIFIED_DATE_TIME);

		// Vyhledávání dle Datumu poslední změny souboru mladší než zadané datum (zadané
		// datum je starší / později než požadované datum souboru), datum změny je v
		// únoru, zadané datum je v lednu
		else if (index == 11)
			fileTableModel.filterDate(getDateFromText(text), DateTimeCompare.AFTER,
					KindOfDateTime.LAST_MODIFIED_DATE_TIME);
		
		
		
		
		/*
		 * Časy jsou na tom podobně jako výše uvedené datumy.
		 * 
		 * Tj.:
		 * Before Čas 1 má být před Čas 2
		 * After: Čas 1 má být mladší / po Čas 2
		 * 
		 * například čas přístupu starší než zadaný čas, tak výčet DateTimeCompare bude
		 * BEFORE
		 */
		
		// Čas přístupu
		else if (index == 12)
			fileTableModel.filterTime(getTimeFromText(text), DateTimeCompare.EQUAL,
					KindOfDateTime.LAST_ACCESS_DATE_TIME);

		// Čas vytvoření
		else if (index == 13)
			fileTableModel.filterTime(getTimeFromText(text), DateTimeCompare.EQUAL, KindOfDateTime.CREATION_DATE_TIME);

		// Čas změny
		else if (index == 14)
			fileTableModel.filterTime(getTimeFromText(text), DateTimeCompare.EQUAL,
					KindOfDateTime.LAST_MODIFIED_DATE_TIME);

		// Čas přístupu starší než
		else if (index == 15)
			fileTableModel.filterTime(getTimeFromText(text), DateTimeCompare.BEFORE,
					KindOfDateTime.LAST_ACCESS_DATE_TIME);

		// Čas přístupu mladší než
		else if (index == 16)
			fileTableModel.filterTime(getTimeFromText(text), DateTimeCompare.AFTER,
					KindOfDateTime.LAST_ACCESS_DATE_TIME);

		// Čas vytvoření starší než
		else if (index == 17)
			fileTableModel.filterTime(getTimeFromText(text), DateTimeCompare.BEFORE, KindOfDateTime.CREATION_DATE_TIME);

		// Čas vytvoření mladší než
		else if (index == 18)
			fileTableModel.filterTime(getTimeFromText(text), DateTimeCompare.AFTER, KindOfDateTime.CREATION_DATE_TIME);

		// Čas změny starší než
		else if (index == 19)
			fileTableModel.filterTime(getTimeFromText(text), DateTimeCompare.BEFORE,
					KindOfDateTime.LAST_MODIFIED_DATE_TIME);

		// Čas změny mladší než
		else if (index == 20)
			fileTableModel.filterTime(getTimeFromText(text), DateTimeCompare.AFTER,
					KindOfDateTime.LAST_MODIFIED_DATE_TIME);
	}
	
	
	
	
	
	
	
	
	
	/**
	 * Metoda, která vytvoří čas ze zadaného textu a vrátí jej, pokud dojde někde k
	 * chybě, uživatel bude upozorněn a vrátí se výchozí datum (čas převedený na
	 * datum).
	 * 
	 * @param text
	 *            - text z texového pole napsaný uživatelem (čas v textu)
	 * 
	 * @return výše popsaný čas získaný z textu od uživatele nebo výchozí datum
	 *         (čas).
	 */
	private static Date getTimeFromText(final String text) {
		/*
		 * Zjistím, zda je čas - zadaný text v požadovaném formátu, pokud ano, převedu
		 * jej na čas - typu Date a předám jej do metody pro filtrování dat v tabulce.
		 */
		final String textWithoutSpaces = text.replaceAll("\\s", "");
		
		if (textWithoutSpaces.matches(REG_EX_TIME)) {
			try {
				// Mnou zvolený formát pro čas:
				final SimpleDateFormat formatter = new SimpleDateFormat("HH:mm:ss");
				
				return formatter.parse(textWithoutSpaces);
			} catch (ParseException e) {
				e.printStackTrace();
				JOptionPane.showMessageDialog(null, txtWrongTimeText, txtWrongTimeTitle, JOptionPane.ERROR_MESSAGE);
			}
		}
		
		return new Date();
	}
	
	
		
	
	
	
	
	
	
	/**
	 * Metoda, která vytvoří datum ze zadaného textu a vrátí jej, pokud dojde někde
	 * k chybě, uživatel bude upozorněn a vrátí se výchozí datum.
	 * 
	 * @param text
	 *            - text z texového pole napsaný uživatelem (datum)
	 * 
	 * @return výše popsaný datum získaný z textu od uživatele nebo výchozí datum.
	 */
	private static Date getDateFromText(final String text) {
		/*
		 * Zjistím, zda je datum - zadaný text v požadovaném formátu, pokud ano, převedu
		 * jej na datum a předám jej do metody pro filtrování dat v tabulce.
		 */
		final String textWithoutSpaces = text.replaceAll("\\s", "");
		
		if (textWithoutSpaces.matches(REG_EX_DATE)) {
			try {
				// Mnou zvolený formát pro datum:
				final SimpleDateFormat formatter = new SimpleDateFormat("dd.MM.yyyy");
				
				return formatter.parse(textWithoutSpaces);
			} catch (ParseException e) {
				e.printStackTrace();
				JOptionPane.showMessageDialog(null, txtWrongDateText, txtWrongDateTitle, JOptionPane.ERROR_MESSAGE);
			}
		}
		
		return new Date();
	}
	
	
	
	
	

	
	

	@Override
	public void setLoadedText(final Properties prop) {
		if (prop != null) {
			// Načtu si pole ze souboru:
			String[] tempModel = CreateLocalization.getArrayFromText(prop.getProperty("FT_Cmb_Model"));
			
			if (tempModel == null || tempModel.length != Constants.CMB_MODEL.length)
				tempModel = Constants.CMB_MODEL;
			
			cmbFilter.setModel(new DefaultComboBoxModel<>(tempModel));
					
			
			String[] tempFilesModel = CreateLocalization.getArrayFromText(prop.getProperty("FT_Cmb_Show_Files_Model"));
			
			if (tempFilesModel == null || tempFilesModel.length != Constants.CMB_SHOW_FILES_MODEL.length)
				tempFilesModel = Constants.CMB_SHOW_FILES_MODEL;
			
			cmbShowFiles.setModel(new DefaultComboBoxModel<>(tempFilesModel));
					
					
					
					
			lblFilter.setText("? " + prop.getProperty("FT_Lbl_Filter_Text", Constants.FT_LBL_FILTER_TEXT) + ": ");
			lblFilter.setToolTipText(prop.getProperty("FT_Lbl_Filter_TT", Constants.FT_LBL_FILTER_TT));
			
			btnSearch.setText(prop.getProperty("FT_Btn_Search", Constants.FT_BTN_SEARCH));
			
			lblShowFiles.setText(prop.getProperty("FT_Lbl_Show_Files", Constants.FT_LBL_SHOW_FILES) + ": ");
			
			txtExampleName = prop.getProperty("FT_Txt_Example_Name", Constants.FT_TXT_EXAMPLE_NAME);
			// Zrovna do pole vložím zadaný text, protože je tojako výchozí hodnota:
			txtSearch.setText(txtExampleName);
			
			
			// Texty do chybových hlášek:
			txtWrongTimeText = prop.getProperty("FT_Wrong_Time_Text", Constants.FT_TXT_WRONG_TIME_TEXT);
			txtWrongTimeTitle = prop.getProperty("FT_Txt_Wrong_Time_Title", Constants.FT_TXT_WRONG_TIME_TITLE);
			
			txtWrongDateText = prop.getProperty("FT_Txt_Wrong_Date_Text", Constants.FT_TXT_WRONG_DATE_TEXT);
			txtWrongDateTitle = prop.getProperty("FT_Txt_Wrong_Date_Title", Constants.FT_TXT_WRONG_DATE_TITLE);
			
		}
		
		
		else {
			cmbFilter.setModel(new DefaultComboBoxModel<>(Constants.CMB_MODEL));
			cmbShowFiles.setModel(new DefaultComboBoxModel<>(Constants.CMB_SHOW_FILES_MODEL));
			
			lblFilter.setText("? " + Constants.FT_LBL_FILTER_TEXT + ": ");
			lblFilter.setToolTipText(Constants.FT_LBL_FILTER_TT);
			
			btnSearch.setText(Constants.FT_BTN_SEARCH);
			
			lblShowFiles.setText(Constants.FT_LBL_SHOW_FILES + ": ");
			
			txtExampleName = Constants.FT_TXT_EXAMPLE_NAME;
			// Zrovna do pole vložím zadaný text, protože je tojako výchozí hodnota:
			txtSearch.setText(txtExampleName);
			
			
			// Texty do chybových hlášek:
			txtWrongTimeText = Constants.FT_TXT_WRONG_TIME_TEXT;
			txtWrongTimeTitle = Constants.FT_TXT_WRONG_TIME_TITLE;
			
			txtWrongDateText = Constants.FT_TXT_WRONG_DATE_TEXT;
			txtWrongDateTitle = Constants.FT_TXT_WRONG_DATE_TITLE;
		}
	}






	@Override
	public void actionPerformed(ActionEvent e) {
		if (e.getSource() == cmbFilter) {
			/*
			 * Kliknutí na tento cmb má pouze ten účel, abych uživateli "ukázal", v jaké
			 * syntaxi má uživatel vkládat texty pro vyhledávání do textového pole.
			 * 
			 * Takže zde zjistím, na jaký index se v modelu daného cmb kliklo a dle toho zobrazím
			 * "ukázkovou" hodnotu v textovém poli.
			 */
			
			final int index = cmbFilter.getSelectedIndex();
			
			// Název
			if (index == 0)
				txtSearch.setText(txtExampleName);

			// Velikost
			else if (index == 1)
				txtSearch.setText("10.5");

			// Přípona
			else if (index == 2)
				txtSearch.setText(".jpg");

			// Datum přístupu, Datum vytvoření, Datum změny, Datum přístupu staší než, Datum
			// přístupu mladší než, Datum vytvoření staší než, Datum vytvoření mladší než,
			// Datum změny staší než, Datum změny mladší než
			else if (index == 3 || index == 4 || index == 5 || index == 6 || index == 7 || index == 8 || index == 9
					|| index == 10 || index == 11)
				txtSearch.setText("dd.mm.yyyy");

			// Čas přístupu, Čas vytvoření, Čas změny, Čas přístupu starší než, Čas přístupu
			// mladší než, Čas vytvoření starší než, Čas vytvoření mladší než, Čas změny
			// starší než, Čas změny mladší než
			else if (index == 12 || index == 13 || index == 14 || index == 15 || index == 16 || index == 17
					|| index == 18 || index == 19 || index == 20)
				txtSearch.setText("hh : mm : ss");
		}
		
		
		
		
		else if (e.getSource() == btnSearch)
			searchText();
		
		
		
		else if (e.getSource() == cmbShowFiles) {
			/*
			 * Zjistí se oznčený "typ souborů" a dle toho se v tabulce zobrazi pouze ty
			 * soubory, které jsou buď skryté, nebo nejsou skryté, nebo všechny soubory v
			 * označeném adresáři.
			 */
			
			final ShowFilesEnum showFilesEnum = getSelectedKindOfFiles();
			
			fileTableModel.filterData(showFilesEnum, FileExplorer.getTableValuesFromSelectedDir());
		}
	}
	
	
	
	
	
	
	
	/**
	 * Metoda, která vrátí výčtový typ dle toho, jaký typ souborů se má zobrazit v
	 * tabulce. Vrátí se výčtový typ dle toho, zda se mají zobrazit všechny soubory
	 * ve zvoleném adresáři, nebo pouze skryté nebo neskryté soubory.
	 * 
	 * Note:
	 * Mohl jsem vytvořit již výčtové hodnoty v cmb - v mohdelu, ale kvůli jazykům
	 * bych pak musel různě řešit setry na hodnoty ve zvoleném jazyce, to už se mi
	 * nechtělo upravovat, protože jsem si na to vzpoměl trochu později, tak jsem to
	 * pouze takto doplnil, že při nastavení hodnota se zjistí pořadí položky (na
	 * jazyce nezáleží, jejich pořadí je vždy stejné) a jen se dle toho pořadí
	 * pozná, jaký typo položek se má zobrazit.
	 * 
	 * @return výše popsaný výčtový typ.
	 */
	final ShowFilesEnum getSelectedKindOfFiles() {
		final int index = cmbShowFiles.getSelectedIndex();

		if (index == 0)
			return ShowFilesEnum.SHOW_ALL;

		else if (index == 1)
			return ShowFilesEnum.SHOW_HIDDEN_FILES;

		else if (index == 2) // Není třeba testovat
			return ShowFilesEnum.SHOW_VISIBLE_FILES;

		// Výchozí hodnota:
		return ShowFilesEnum.SHOW_ALL;
	}
}
