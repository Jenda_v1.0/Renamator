package kruncik.jan.renamator.fileExplorer;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.Dialog.ModalExclusionType;
import java.io.File;
import java.io.IOException;

import javax.imageio.ImageIO;
import javax.swing.ImageIcon;
import javax.swing.JLabel;
import javax.swing.JWindow;

import kruncik.jan.renamator.loadPictures.Pictures;

/**
 * Tato třída slouží pouze jako "okno" pro zobrazení náhledu na obrázek po
 * najetí myší na něj v tabulce.
 * 
 * Toto okno obsahuje obrázek buď v původní nebo ve změnšené velikosti (pokud je
 * větší než zadaná velikost okna ) a pod obrázkem je celá cesta k rodiči -
 * adresáři, kde se nachází obrázek.
 * 
 * @see JWindow
 * 
 * @author Jan Krunčík
 * @version 1.0
 */

class ImageView extends JWindow {

	
	private static final long serialVersionUID = 1L;

	
	
	private static final int WIDTH = 350, HEIGHT = 380;
	
	
	
	
	/**
	 * Konstruktor této třídy.
	 * 
	 * @param file
	 *            - Cesta k obrázku, který se má zobrazit v tomto okně.
	 * 
	 * @param locationX
	 *            - souřadnice X, kde se má zobrazit toto okno
	 * 
	 * @param locationY
	 *            - souřadnice y, kde se má zobrazit toto okno
	 */
	ImageView(final File file, final int locationX, final int locationY) {
		super();
		
		setPreferredSize(new Dimension(WIDTH, HEIGHT));
		
		setLayout(new BorderLayout());
		
		setModalExclusionType(ModalExclusionType.TOOLKIT_EXCLUDE);
		
		
		
		
		
		/*
		 * Zde si zkusím načíst obrázek, pokud se to povede, tak zjistím, zda je jeho
		 * velikost menší, než nastavená elikost pro okno, pokud ne, pak ten obrázek
		 * změnším, pokud se obrázek nenaště, tak se do okna místo obrázku vloží label s
		 * textem "Error".
		 */
		try {
			ImageIcon image = new ImageIcon(ImageIO.read(file));

			if (image != null && (image.getIconWidth() >= WIDTH || image.getIconHeight() >= HEIGHT))
				image = Pictures.resizeImage(image, WIDTH - 5, HEIGHT - 5);

			final JLabel lblImage = new JLabel();

			if (image != null)
				lblImage.setIcon(image);
			else
				lblImage.setText("Error");

			lblImage.setHorizontalAlignment(JLabel.CENTER);

			add(lblImage, BorderLayout.CENTER);

		} catch (IOException e) {
			e.printStackTrace();
		}
		
		

		
		
		
		
		
		
		
		/*
		 * Zde zjistím, zda má obrázek rodičkovský adresář a pokud ano, tak k němu
		 * zobrzím cestu pod obrázkem:
		 */
		if (file.getParentFile() != null) {
			final JLabel lblParent = new JLabel(file.getParent());

			lblParent.setHorizontalAlignment(JLabel.CENTER);

			add(lblParent, BorderLayout.SOUTH);
		}
		
		
		pack();
		setLocation(locationX, locationY);
		setVisible(true);
	}
}
