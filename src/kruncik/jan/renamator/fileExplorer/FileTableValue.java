package kruncik.jan.renamator.fileExplorer;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.attribute.BasicFileAttributes;
import java.text.SimpleDateFormat;

/**
 * Tato třída slouží pouze jako "hodnota", která se vkládá do tabulky ve
 * fileExploreru.
 * 
 * Obsahuje pouze nutné komponenty pro zobrazení potřebných informací v tabulce.
 * 
 * Zdroj pro velikost souboru:
 * https://www.mkyong.com/java/how-to-get-file-size-in-java/
 * 
 * @see SimpleDateFormat
 * @see BasicFileAttributes
 * @see Path
 * 
 * @author Jan Krunčík
 * @version 1.0
 */

public class FileTableValue {

	
	private static final SimpleDateFormat FORMATTER = new SimpleDateFormat("dd.MM.yyyy 'at' HH:mm:ss z");
	
	
	
	/**
	 * Logická hodnota pro JCheckBox.
	 * 
	 * Dle této hodnoty (zda bude true / false resp. v tabulce bude označena nebo
	 * neoznačena), tak se pozná, zda se má například smazat více souborů apod.
	 */
	private boolean selected;
	
	
	/**
	 * Soubor, který je reprezentován "touto hodnotou" - FileTableValue. Resp. file
	 * je nějaký soubor, o kterém jsou v tabulce zobrazeny nějaká data.
	 */
	private File file;
	
	
	
	/**
	 * Co si potřebné pro zjisštění vlastností o souboru - datum vytvoření,
	 * modifikace apod.
	 */
	private BasicFileAttributes attr;
	
	
	
	/**
	 * Proměnná, která obsahuje velikost souboru v bytech.
	 */
	private final long length;
	
	
	
	
	
	/**
	 * Konstruktor této třídy.
	 * 
	 * Výchozí hodnota proměnné selected je false.
	 * 
	 * @param file
	 *            - cesta k nějakému adresáři, který je reprezentován touto
	 *            položkou.
	 */
	FileTableValue(final File file) {
		super();
		
		this.file = file;
		
		length = file.length();
		
		
		try {
			final Path path = Paths.get(file.getAbsolutePath());
			attr = Files.readAttributes(path, BasicFileAttributes.class);
		} catch (IOException e) {
			e.printStackTrace();			
		}	
	}
	
	
	
	
	public boolean isSelected() {
		return selected;
	}
	
	public void setSelected(boolean selected) {
		this.selected = selected;
	}
	
	
	public File getFile() {
		return file;
	}
	
	public void setFile(File file) {
		this.file = file;
	}
	
	public long getLength() {
		return length;
	}
	
	BasicFileAttributes getAttr() {
		return attr;
	}
	
	
	
	/**
	 * Metoda, která vrátí logickou hodnotu o tom, zda je soubor file skrytý nebo
	 * ne. True - soubor je skrytý, false - soubor není skrytý.
	 * 
	 * @return true, pokud je soubor file skrytý, jinak false.
	 */
	final boolean isFileHidden() {
		return file.isHidden();
	}
	
	
	
	
	/**
	 * Metoda, která vrátí datum vytvoření souboru v syntaxi: dd.MM.yyyy 'at'
	 * HH:mm:ss z
	 * 
	 * @return výše popsanou syntaxi datumu vytvoření souboru.
	 */
	final String getCreationDate() {
		return FORMATTER.format(attr.creationTime().toMillis());
	}
	
	
	/**
	 * Metoda, která vrátí datum posledního přístupu k souboru v syntaxi: dd.MM.yyyy
	 * 'at' HH:mm:ss z
	 * 
	 * @return výše popsanou syntaxi datumu posledního přísupu k souboru.
	 */
	final String getLastAccessDate() {
		return FORMATTER.format(attr.lastAccessTime().toMillis());
	} 
	
		
	/**
	 * Metoda, která vrátí datum poslední modifikace souboru v syntaxi: dd.MM.yyyy
	 * 'at' HH:mm:ss z
	 * 
	 * @return výše popsanou syntaxi datumu poslední modifikace souboru.
	 */
	final String getLastModifiedDate() {
		return FORMATTER.format(attr.lastModifiedTime().toMillis());
	}
}
