package kruncik.jan.renamator.fileExplorer;

/**
 * Tento výčet obsahuje pouze 3 hodnoty, které slouží pro definování jaké datumy
 * nebo časy se mají vyfiltrovat v FileTableModel.
 * 
 * Tento výčet se využívá pouze jako "proměnná", pro předání do metody jako
 * parametr a podle této hodnoty se pozná, zda se mají datumy porovnávat k tomu,
 * že jsou starší, mladší nebo stejné.
 * 
 * @see Enum
 * 
 * @author Jan Krunčík
 * @version 1.0
 */

public enum DateTimeCompare {

	/**
	 * Datum 1 má být před Datum 2 (Datum 1 je starší)
	 * 
	 * Čas 1 má být před Čas 2
	 */
	BEFORE,

	/**
	 * Datum 1 má být stejný jako Datum 2
	 * 
	 * Čas 1 má být stejný jako Čas 2
	 */
	EQUAL,

	/**
	 * Datum 1 má být mladší / po Datum 2
	 * 
	 * Čas 1 má být mladší / po Čas 2
	 */
	AFTER
}
