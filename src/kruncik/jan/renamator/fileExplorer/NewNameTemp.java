package kruncik.jan.renamator.fileExplorer;

/**
 * Tato třída slouží pouze jako "prostředník" pro předání hodnot z dialogu -
 * třídy RenameFileForm.
 * 
 * @author Jan Krunčík
 * @version 1.0
 */

class NewNameTemp {

	/**
	 * Proměnná, která obsahuje nový název nějakého souboru. (Buď sejedná pouze o
	 * název, nebo o název včetně přípony, to rozhoduje proměnná withExtension)
	 */
	private final String newName;

	/**
	 * Logická proměnná, která "říká", zda je nový název souboru (newName) včetně
	 * přípony, tj. má se změnit i typ souboru, nebo obsahuje proměnná newName pouze
	 * název nový souboru bez přípony.
	 * 
	 * True, newName obsahuje i příponu, false newName obsahuje pouze název souboru
	 * bez přípony.
	 */
	private final boolean withExtension;

	
	/**
	 * Konstruktor této třídy.
	 * 
	 * @param newName
	 *            - nový název souboru.
	 * 
	 * @param withExtension
	 *            - true - newName obsahuje novou příponu, false - newName obshauje
	 *            pouze nový název bez přípony.
	 */
	NewNameTemp(String newName, boolean withExtension) {
		super();

		this.newName = newName;
		this.withExtension = withExtension;
	}

	String getNewName() {
		return newName;
	}

	boolean isWithExtension() {
		return withExtension;
	}
}
