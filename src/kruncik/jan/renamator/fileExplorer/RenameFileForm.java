package kruncik.jan.renamator.fileExplorer;

import java.awt.Color;
import java.awt.FlowLayout;
import java.awt.Font;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.io.File;
import java.util.Properties;

import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.SwingConstants;

import org.apache.commons.io.FilenameUtils;

import kruncik.jan.renamator.forms.AbstractForm;
import kruncik.jan.renamator.localization.Constants;
import kruncik.jan.renamator.localization.LocalizationImpl;

/**
 * Tato třída slouží jako dialog pro získání nového názvu souboru nebo adresáře.
 * 
 * V podstatě se zde od uživatele získá akorát nový název pro nějaký soubor nebo
 * adresář.
 *
 * @see Properties
 * 
 * @author Jan Krunčík
 * @version 1.0
 */

public class RenameFileForm extends AbstractForm implements LocalizationImpl {

	
	private static final long serialVersionUID = 1L;



	
	/**
	 * Label, do kterého se vloží text pro informaci pro uživatele, že do textového
	 * pole má zadat nový název souboru.
	 */
	private final JLabel lblName;
	
	
	
	/**
	 * Proměnná, která se bude nacházet pod textovým polem, a bude uživateli
	 * ukazovat chyby ohledně nového názvu.
	 */
	private final JLabel lblError;
	
	
	
	
	/**
	 * Textové pole pro zadání nového názvu souboru.
	 */
	private final JTextField txtName;
	
	
	
	
	
	/**
	 * Komponenta, která slouží pro to, zda se má zadat nový název včetně přípony
	 * nebo ne.
	 */
	private final JCheckBox chcbWithExtension;
	
	
	
	
	
	private final JButton btnRename, btnClose;
	
	
	
	
	/**
	 * Proměnné pro texty do chybových hlášek a labelů s chybami.
	 */
	private String txtEmptyField, txtRegExError, txtExtensionError, txtSameNames, txtMissingExtension;
	
	
	
	/**
	 * KeyAdapter pro textové pole. Na Enter se zkusí potvrdit nový název, na zbytek
	 * se jen testuje, zda je název v pořádku, případně zobrazí label s chybovou
	 * zprávou.
	 */
	private KeyAdapter keyAdapter;
	
	
	
	/**
	 * Předaný soubor, kterému chce uživatel změnit název.
	 */
	private final File file;
	
	
	
	/**
	 * Logická proměnná, dle které se pozná, zda se má vrátit nový název nebo ne.
	 * Pokud bude nastavena tato proměnná na false, pak se vrátí hodnota null, nebo
	 * nový název.
	 */
	private boolean returnValue;
	
	
	
	/**
	 * Logická proměnná, do které se uloží true v případě, že se jedná o vytvoření
	 * souboru, takže se v dialogu zadává název nového souboru s příponou. False v
	 * případě, že se jedná o vytvoření adresáře, takže se do textového pole zadává
	 * název adresáře.
	 */
	private final boolean createFile;
	
	
	


	/**
	 * Konstruktor této třídy.
	 * 
	 * @param createFile - logická proměnná o tom, zda se má vytvořit adresář nebo soubor.
	 */
	RenameFileForm(final File file, final boolean createFile) {
		super();
		
		this.file = file;
		this.createFile = createFile;
		
		final String fileName;
		
		if (file != null) {
			if (!file.isDirectory())
				fileName = FilenameUtils.removeExtension(file.getName());
			else
				fileName = file.getName();	
		}
		else fileName = null;
		
		
		
		
		
		initGui(550, 200, new GridBagLayout(), "/fileExplorerIcons/RenameFileIcon.png");
		
		final GridBagConstraints gbc = createGbc(5, 5, 5, 5);
		
		int rowIndex = 0;
		
		
		
		lblName = new JLabel();
		add(lblName, gbc);
		
		
		
		createKeyAdapter();
		
		txtName = new JTextField(fileName, 20);
		txtName.addKeyListener(keyAdapter);
		
		setGbc(gbc, rowIndex, 1);
		add(txtName, gbc);
		
		
		
		lblError = new JLabel();
		lblError.setForeground(Color.RED);
		lblError.setFont(new Font("Font for error label", Font.BOLD, 13));
		lblError.setHorizontalAlignment(JLabel.CENTER);
		
		gbc.gridwidth = 2;
		setGbc(gbc, ++rowIndex, 0);
		add(lblError, gbc);
		
		
		
		
		
		
		/*
		 * Tuto komponentu vytvořím pokadžé, i když se nepřídá do okna, protože nechi
		 * zvlášt testovat, zda je inicializována a zda je označena. Tak ji vytvořím
		 * pokaždé, ale označena může být jen, když je přidána do okna, ale to je již
		 * detail.
		 */
		chcbWithExtension = new JCheckBox();
		
		if ((file != null && !file.isDirectory())) {
			chcbWithExtension.setHorizontalAlignment(SwingConstants.CENTER);
			
			setGbc(gbc, ++rowIndex, 0);
			add(chcbWithExtension, gbc);
		}
		
		
		
		
		
		// Panel s tlačítky:
		final JPanel pnlButtons = new JPanel(new FlowLayout(FlowLayout.RIGHT, 10, 0));
		
		btnRename = new JButton();
		btnClose= new JButton();
		
		btnRename.addActionListener(Event -> checkName());
		btnClose.addActionListener(Event -> {
			returnValue = false;			
			dispose();
		});
		
		pnlButtons.add(btnRename);
		pnlButtons.add(btnClose);
		
		
		setGbc(gbc, ++rowIndex, 0);		
		add(pnlButtons, gbc);
		
		
		
		/*
		 * Aby se nastavila proměnná returnValue na false při zavření okna dialogu.
		 */
		addWindowListener(new MyWindowsAdapter());
	}
	
	
	
	
	
	
	
	
	/**
	 * Metoda, která vytvoří keyAdapter pro textové pole.
	 * 
	 * Pokud se stiskne enter, pak se otestuje, zda je nový název v pořádku, pokud
	 * ano, pak se zrovna dialog zavře a text vrátí a soubor přejmenuje, pokud ale
	 * nový název není v počátku, pak se zobrazí text s chybovou zprávou.
	 */
	private void createKeyAdapter() {
		keyAdapter = new KeyAdapter() {
			@Override
			public void keyReleased(KeyEvent e) {
				if (e.getKeyCode() == KeyEvent.VK_ENTER)
					checkName();
				else
					checkText();
			}
		};
	}
	
	
	
	
	
	
	
	
	
	
	/**
	 * Metoda, která nastaví text do labelu lblError pod textovým polem.
	 * 
	 * Dále otestuje, zda je lable zobrazen, pokud ne, pak se zobrazí.
	 * 
	 * @param text
	 *            text, který se má nastavit do textového pole lblError.
	 */
	private void setErrorTextToLabel(final String text) {
		lblError.setText(text);

		if (!lblError.isVisible())
			lblError.setVisible(true);
	}
	
	
	
	
	
	
	
	/**
	 * Metoda, která otestuje zadaný text - nový název na nějaká data, například,
	 * zda se názvy neshodují , zda odpovídá regulárnímu výrazu pro platné znaky
	 * apod.
	 * 
	 * @return true, pokud je nový název v pořádku, jinak false.
	 */
	private boolean checkText() {
		/*
		 * Aby pole nebylo prazdne, 
		 * nazev byl v pozadovane syntaxi pro nazev souboru
		 * nebyl nový nazev stejny jako predchozi
		 * 
		 * pokud je oznaceo chcb pro priponu, tak aby byla zadana i validni pripona.
		 */
		final String newName = txtName.getText();
		
		
		/*
		 * Nejprve schován label s chybou a pokud se níže zjistí, že text obshauje chybu, pak se
		 * zase zobrazí:
		 */
		lblError.setVisible(false);
		
		
		if (newName.replaceAll("\\s*", "").isEmpty()) {
			setErrorTextToLabel(txtEmptyField);
			return false;
		}
		
		
		if (!newName.matches(Constants.REG_EX_FILE_NAME)) {
			setErrorTextToLabel(txtRegExError);
			return false;
		}
		
		
		/*
		 * Nejprve otestuji, zda je zadána validní přípona, tj. vezmu si text za
		 * poslední tečkou, pokud se v názvu nějaký nachází.
		 */
		if (chcbWithExtension.isSelected() || createFile) {
			if (newName.contains(".")) {
				if (!newName.substring(newName.lastIndexOf(".")).matches(Constants.REG_EX_FOR_FILE_EXTENSION)) {
					setErrorTextToLabel(txtExtensionError);
					return false;
				}
			}
			
			else {
				setErrorTextToLabel(txtMissingExtension);				
				return false;
			}
		}
		
		
		
		
		/*
		 * Nyní otestuji, zda nejsou původní a nový náze stejný, to se taky liší dle
		 * toho, zda je označen chcb pro zadání přípony, pokud je označen, pak musím
		 * název testovat i s příponou, jinak ne.
		 * 
		 * Původní a nový název se bude testovat jen v případě, že se jedná o
		 * přejmenování souboru, tj. proměnná file není null.
		 */
		if (chcbWithExtension.isSelected() && file != null) {
			// Zde je třeba název testovat i s příponou:
			if (newName.equals(file.getName())) {
				// Zde se názvy shodují:
				setErrorTextToLabel(txtSameNames);
				return false;
			}
		}
		else if (file != null) {
			/*
			 * Zde se testuje název bez přípony, tj. že se jedná o adresář, ale opět jen v
			 * případě, že proměnná file není null, protože pokud je, pak se jedná o
			 * vytvoření a ne o přejmenování, tak nemohu testovat původní název.
			 */
			// Zde testuji názvy bez přípony:
			final String originalFileName = FilenameUtils.removeExtension(file.getName());

			if (newName.equals(originalFileName)) {
				// Zde se názvy shodují:
				setErrorTextToLabel(txtSameNames);
				return false;
			}
		}
		
		
		return true;
	}
	
	
	
	
	
	
	/**
	 * Metoda, která otestuje zadaných text a pokud je text v pořádku, pak se zavře dialog
	 * a vrátí se tak informace
	 */
	private void checkName() {
		if (checkText()) {
			// Zde je text v pořádku, tak mohu zavřít dialog
			// Nastavím proměnnou, že je možné vrátit hodnotu nový název:
			returnValue = true;
			
			// Zavřu dialog:
			dispose();			
		}
	}
	
	
	
	
	
	
	
	
	
	/**
	 * Metoda, která zobrazí tento dialog, který se tak stane modálním a po kliknutí
	 * na potvrzovací tlačítko tato metoda vrátí objekt, který obsahuje text z
	 * textového pole pro nový název souboru a informaci, zda se jedná i o příponu
	 * nebo je celý text pouze název souboru bez přípony.
	 * 
	 * @return nový objekt, který obsahuje nový název souboru a informaci o tom, zda
	 *         se jedná o název s příponou nebo bez přípony.
	 */
	final NewNameTemp getNewNameInfo() {
		showWindow();

		if (returnValue)
			return new NewNameTemp(txtName.getText(), chcbWithExtension.isSelected());

		return null;
	}
	
	
	
	
	
	
	
	@Override
	public void setLoadedText(final Properties prop) {
		if (prop != null) {
			if (file != null)
				setTitle(prop.getProperty("RFF_Dialog_Title_1", Constants.RFF_DIALOG_TITLE_1) + " " + file.getName());
			else if (createFile)
				setTitle(prop.getProperty("RFF_Dialog_Title_2", Constants.RFF_DIALOG_TITLE_2));
			else
				setTitle(prop.getProperty("RFF_Dialog_Title_3", Constants.RFF_DIALOG_TITLE_3)); 
			
			
			lblName.setText(prop.getProperty("RFF_Lbl_Name", Constants.RFF_LBL_NAME) + ": ");
			
			chcbWithExtension.setText("? " + prop.getProperty("RFF_Chcb_With_Extension_Text", Constants.RFF_CHCB_WITH_EXTENSION_TEXT));
			chcbWithExtension.setToolTipText(prop.getProperty("RFF_Chcb_With_Extension_TT", Constants.RFF_CHCB_WITH_EXTENSION_TT));

			
			if (file != null)
				btnRename.setText(prop.getProperty("RFF_Btn_Rename_1", Constants.RFF_BTN_RENAME_1));
			else				
				btnRename.setText(prop.getProperty("RFF_Btn_Rename_2", Constants.RFF_BTN_RENAME_2));
			
			btnClose.setText(prop.getProperty("RFF_Btn_Close", Constants.RFF_BTN_CLOSE));
			
			
			txtEmptyField = prop.getProperty("RFF_Txt_Empty_Field", Constants.RFF_TXT_EMPTY_FIELD);
			txtRegExError = prop.getProperty("RFF_Txt_Reg_Ex_Error", Constants.RFF_TXT_REG_EX_ERROR);
			txtExtensionError = prop.getProperty("RFF_Txt_Extension_Error", Constants.RFF_TXT_EXTENSION_ERROR);
			txtSameNames = prop.getProperty("RFF_Txt_Same_Names", Constants.RFF_TXT_SAME_NAMES);
			txtMissingExtension = prop.getProperty("RFF_Txt_Missing_Extension", Constants.RFF_TXT_MISSING_EXTENSION);
		}
		
		
		else {
			if (file != null)
				setTitle(Constants.RFF_DIALOG_TITLE_1 + " " + file.getName());
			else if (createFile)
				setTitle(Constants.RFF_DIALOG_TITLE_2);
			else
				setTitle(Constants.RFF_DIALOG_TITLE_3); 
			
			
			lblName.setText(Constants.RFF_LBL_NAME + ": ");
			
			chcbWithExtension.setText("? " + Constants.RFF_CHCB_WITH_EXTENSION_TEXT);
			chcbWithExtension.setToolTipText(Constants.RFF_CHCB_WITH_EXTENSION_TT);

			
			if (file != null)
				btnRename.setText(Constants.RFF_BTN_RENAME_1);
			else				
				btnRename.setText(Constants.RFF_BTN_RENAME_2);
			
			btnClose.setText(Constants.RFF_BTN_CLOSE);
			
			
			txtEmptyField = Constants.RFF_TXT_EMPTY_FIELD;
			txtRegExError = Constants.RFF_TXT_REG_EX_ERROR;
			txtExtensionError = Constants.RFF_TXT_EXTENSION_ERROR;
			txtSameNames = Constants.RFF_TXT_SAME_NAMES;
			txtMissingExtension = Constants.RFF_TXT_MISSING_EXTENSION;
		}
	}
	
	
	
	
	
	
	
	
	
	
	/**
	 * Tato třída slouží pouze jako adapter, který reaguje na zavření dialogu, v
	 * takovém případě nastaví proměnnou returnValue na hodnotu false, aby se
	 * vědělo,že uživatel dialog zavřel, aniž by potvrdil nový název souboru.
	 * 
	 * @see WindowAdapter
	 * 
	 * @author Jan Krunčík
	 * @version 1.0
	 */
	private class MyWindowsAdapter extends WindowAdapter {
		@Override
		public void windowClosing(WindowEvent e) {
			returnValue = false;
		}
	}
}
