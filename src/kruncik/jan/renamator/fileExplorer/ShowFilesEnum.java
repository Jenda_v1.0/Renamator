package kruncik.jan.renamator.fileExplorer;

/**
 * Tento výčet obsahuje pouze pár hodnot -> pro filtrování souborů, které se
 * mají zobrazit v tabulce ve file exploreru v označeném adresáři.
 * 
 * Jedná se o to, zda se mají zobrazit všechny hodnoty, nebo jen ty co jsou nebo
 * nejsou skryté.
 * 
 * @see Enum
 * 
 * @author Jan Krunčík
 * @version 1.0
 */

public enum ShowFilesEnum {

	/**
	 * Mají se zobrazit všechny položky.
	 */
	SHOW_ALL, 
	
	/**
	 * Mají se zobrazit pouze skryté soubory.
	 */
	SHOW_HIDDEN_FILES, 
	
	/**
	 * Majíí se zobrazit pouze soubory, které nejsou skryté.
	 */
	SHOW_VISIBLE_FILES
}
