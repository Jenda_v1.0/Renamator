package kruncik.jan.renamator.fileExplorer;

/**
 * Tento výčet obsahuje pouze 3 hodnoty, jedná se o hodnoty pro "definování"
 * toho, zda se má testovat datum nebo čas vytvoření, poslední změny nebo
 * posledního přístupu k souboru.
 * 
 * @see Enum
 * 
 * @author Jan Krunčík
 * @version 1.0
 */

public enum KindOfDateTime {

	/**
	 * Datum nebo čas vytvoření souboru.
	 */
	CREATION_DATE_TIME,

	/**
	 * Datum nebo čas posledního přístupu k souboru.
	 */
	LAST_ACCESS_DATE_TIME,

	/**
	 * Datum nebo čas poslední změny souboru.
	 */
	LAST_MODIFIED_DATE_TIME
}
