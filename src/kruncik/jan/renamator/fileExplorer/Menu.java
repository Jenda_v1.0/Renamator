package kruncik.jan.renamator.fileExplorer;

import java.awt.event.InputEvent;
import java.awt.event.KeyEvent;
import java.util.Properties;

import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.KeyStroke;

import kruncik.jan.renamator.app.App;
import kruncik.jan.renamator.loadPictures.KindOfPicture;
import kruncik.jan.renamator.loadPictures.Pictures;
import kruncik.jan.renamator.localization.Constants;
import kruncik.jan.renamator.localization.LocalizationImpl;

/**
 * Tato třída slouží jako menu pro dialog - průzkumník projektů, obsahuje
 * následjící položky:
 * 
 * Dialog:
 * 	- Info dialog (dialog s informacemi o průzkumníku projektů)
 * 	- Zavřít (dialog)
 * 
 * Akce
 * 	- Abstraktní akce - stejné jako v toolbaru
 * 
 * @see JMenuBar
 * @see Properties
 * 
 * @author Jan Krunčík
 * @version 1.0
 */

public class Menu extends JMenuBar implements LocalizationImpl {

	
	private static final long serialVersionUID = 1L;

	
	
	/**
	 * Menu v dialogu.
	 */
	private final JMenu menuDialog, menuActions;
	
	
	
	/**
	 * Položky do menu menuDialog:
	 */
	private final JMenuItem itemInfo, itemClose;
	
	
	
	
	/**
	 * Konstruktor této třídy.
	 * 
	 * @param fileExplorer
	 *            - reference na instanci třídy FileExplorer (dialog průzkumník
	 *            projektů), protože nad ním potřebuji zavolat metodu dispose v
	 *            případě kliknutí na položku itemCLose pro zavření dialogu.
	 */
	Menu(final FileExplorer fileExplorer) {
		super();
		
		menuDialog = new JMenu();
		menuActions = new JMenu();
		
		
		
		
		// Menu Dialog:
		itemInfo = new JMenuItem();
		itemClose = new JMenuItem();
		
		itemInfo.addActionListener(Event -> {
			final InfoForm infoForm = new InfoForm();
			infoForm.setLoadedText(App.getLanguage());
		});
		itemClose.addActionListener(Event -> fileExplorer.dispose());
		
		menuDialog.add(itemInfo);
		menuDialog.addSeparator();
		menuDialog.add(itemClose);
		
		
		
		// Menu pro akce:
		menuActions.add(AbstractActions.getActCreateFile());
		menuActions.addSeparator();		
		menuActions.add(AbstractActions.getActCreateDir());
		menuActions.addSeparator();
		menuActions.add(AbstractActions.getActEditFile());
		menuActions.addSeparator();
		menuActions.add(AbstractActions.getActDeleteFile());
		
		
		
		setIconsToItems();
		
		setShortcutsToItems();
		
		
		
		add(menuDialog);
		add(menuActions);
	}



	
	
	/**
	 * Metoda, která přidá položkám v menu ikony.
	 */
	private void setIconsToItems() {
		itemInfo.setIcon(Pictures.loadImage("/fileExplorerIcons/menu/InfoIcon.png", KindOfPicture.MENU_ICON));
		itemClose.setIcon(Pictures.loadImage("/fileExplorerIcons/menu/CloseIcon.png", KindOfPicture.MENU_ICON));
	}
	
	
	
	/**
	 * Metoda, která nastaví položkám v menu klávesové zkratky.
	 */
	private void setShortcutsToItems() {
		itemInfo.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_I, InputEvent.CTRL_DOWN_MASK));
		itemClose.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_W, InputEvent.CTRL_DOWN_MASK));
	}
	
	
	
	
	
	
	
	@Override
	public void setLoadedText(final Properties prop) {
		if (prop != null) {
			menuDialog.setText(prop.getProperty("MN_MN_Dialog", Constants.MN_MN_DIALOG));
			menuActions.setText(prop.getProperty("MN_MN_Actions", Constants.MN_MN_ACTIONS));
			
			itemInfo.setText(prop.getProperty("MN_Item_Info_Text", Constants.MN_ITEM_INFO_TEXT));
			itemInfo.setToolTipText(prop.getProperty("MN_Item_Info_TT", Constants.MN_ITEM_INFO_TT));
			
			itemClose.setText(prop.getProperty("MN_Item_Close_Text", Constants.MN_ITEM_CLOSE_TEXT));
			itemClose.setToolTipText(prop.getProperty("MN_Item_Close_TT", Constants.MN_ITEM_CLOSE_TT));
		}
		
		
		else {
			menuDialog.setText(Constants.MN_MN_DIALOG);
			menuActions.setText(Constants.MN_MN_ACTIONS);
			
			itemInfo.setText(Constants.MN_ITEM_INFO_TEXT);
			itemInfo.setToolTipText(Constants.MN_ITEM_INFO_TT);
			
			itemClose.setText(Constants.MN_ITEM_CLOSE_TEXT);
			itemClose.setToolTipText(Constants.MN_ITEM_CLOSE_TT);
		}
	}
}
