package kruncik.jan.renamator.localization;

import java.util.Properties;

/**
 * Toto rozhraní obsahuje pouze jednu metodu, která slouží pro nastavení textů
 * do potřebných komponent v dané třídě.
 * 
 * @author Jan Krunčík
 * @version 1.0
 */

public interface LocalizationImpl {

	/**
	 * Tato abstraktní metoda je implementována v každé třídě, která slouží jako
	 * dialogové okno nebo nějaké grafické okno aplikace. Metoda nastaví do
	 * komponent v daném okne - dialogu apod. texty ve zvoleném jazyce. Texty se
	 * berou z objektu v parametru metody prop.
	 * 
	 * @param prop
	 *            - objekt typu Properties, který obsahuje načtené texty pro
	 *            aplikaci.
	 */
	void setLoadedText(final Properties prop);
}
