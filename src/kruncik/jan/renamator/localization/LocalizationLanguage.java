package kruncik.jan.renamator.localization;

/**
 * Tento výčet obsahuje hodnoty, které slouží pro určení jazyku lokalizace,
 * resp. soubor s texty pro aplikace v daném jazyce.
 * 
 * @see Enum
 * 
 * @author Jan Krunčík
 * @version 1.0
 */

public enum LocalizationLanguage {

	/**
	 * Jedná se o texty v jazyce českém.
	 */
	CZ("CZ"),

	/**
	 * Jedná se o texty v jazyce anglickém.
	 */
	EN("EN");
	
	
	
	
	
	
	/**
	 * Proměnná pro uložení názvu souboru .properties, který je uložen v aplikaci.
	 * Tento soubor obsahuje texty pro aplikaci v příslušném jazyce.
	 */
	private final String propertiesName;
	
	/**
	 * Konstruktor pro jednotlivé výčtové typy.
	 * 
	 * @param propertiesName
	 *            - název souboru .properties, který obsahuje texty ve zvoleném
	 *            jayzce.
	 */
	LocalizationLanguage(final String propertiesName) {
		this.propertiesName = propertiesName;
	}
	
	/**
	 * Getr na proměnnou, která obsahuje texty pro aplikaci v požadovaném jazyce.
	 * 
	 * @return název souboru .properties,k terý obsahuje texty pro aplikaci v
	 *         příslušném jazyce.
	 */
	public String getPropertiesName() {
		return propertiesName;
	}
}
