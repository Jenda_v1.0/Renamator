package kruncik.jan.renamator.localization;

import java.awt.Font;

/**
 * Tato třída obsahuje veškeré texty pro aplikaci v českém - výchozím jazyce. Je
 * zde proto, že obsahuje veškeré texty na jednom místě pro přehlednost, dále se
 * z těchto proměnné berou texty pro vytvoření daného souboru v českém jazyce a
 * navíc je to taková "pojistka" pro případ, že by se nepodařilo najít ani ty
 * hlavní soubory s texty, které jsou u aplikace, pak se vezmou tyto výchozí
 * texty.
 * 
 * @author Jan Krunčík
 * @version 1.0
 */

public class Constants {

	/**
	 * Titulek pro hlavní okno aplikace.
	 */
	public static final String TITLE = "Renamator";
	
	
	
	/**
	 * Výchozí jazyk pro aplikaci.
	 */
	public static final String DEFAULT_LANGUAGE = "CZ";
	
	
	
	public static final Font FONT_FOR_MAIN_TITLE = new Font("font for titles.", Font.BOLD, 18);
	
	
	
	/**
	 * Nemělo by se to brát doslova jako název souboru. Spíše jen jak to může
	 * vypadat, protože může obsahovat zadané znaky o maximální délce 255 znaků.
	 * 
	 * Je možné zadat velká a malá písmena s a bez diakritiky, číslice, podtržítka,
	 * tečky, vykříčník, pomlčka, kulaté závorky.
	 */
	public static final String REG_EX_FILE_NAME = "^\\s*[\\w áčďéěíňóřšťúůýžÁČĎÉĚÍŇÓŘŠŤÚŮÝŽ\\(\\)\\.!-]{1,255}\\s*$";
	
	
	

	/**
	 * Regulární výraz pro příponu souborů, můžeobsahovat minimálně 1 a maximálně 10
	 * znaků. Znaky mohou být pouze velká a malá písmena bez diakritiky a číslice
	 * (např. .7z apod.).
	 */
	public static final String REG_EX_FOR_FILE_EXTENSION = "^\\s*\\.[a-zA-Z0-9]{1,10}\\s*$";
	
	
	
	
	static final String TXT_TEXT_IN_CZ_LANGUAGE = "Texty pro aplikaci " + TITLE + " v českém jazyce.";
	static final String TXT_TEXT_IN_EN_LANGUAGE = "Texts for " + TITLE + " in english.";
	
	
	
	
	
	
	
	
	
	
	// Texty do hlavního menu v hlavním okně aplikace - menu: Menu.java v balíčku
	// app
	public static final String MENU_MENU_APP = "Aplikace", MENU_MENU_INFO = "Info", MENU_ITEM_SETTINGS = "Nastavení",
			MENU_ITEM_CLOSE = "Zavřít", MENU_ITEM_INFO_ABOUT_APP = "Aplikace", MENU_ITEM_INFO_AUTHOR = "Autor";
	
	// Popisky tlačítek v hlavním menu - v hlavním okne aplikace - menu výše:
	public static final String MENU_TT_ITEM_SETTING = "Otevře dialogové okno s nastavením aplikace.",
			MENU_TT_ITEM_CLOSE = "Ukončí aplikaci.",
			MENU_TT_ITEM_INFO_ABOUT_APP = "Otevře dialogové okno s informacemi o této aplikaci.",
			MENU_TT_ITEM_INFO_AUTHOR = "Otevře dialogové okno s informacemi o autorovi této aplikace.";
	
	
	
	
	
	// Texty do dialogu s informacemi o aplikaci - třída AboutAppForm v balíčku
	// forms:
	public static final String ABOUT_APP_FORM_DIALOG_TITLE = "Informace";
	
	
	// Texty pro info o aplikaci (dialog AboutAppForm)
	public static final String AAF_INFO_TEXT_1 = "Jedná se o aplikaci, která slouží pro přejmenování (indexování) souborů a nahrazení textů v názvech souborů.",
			AAF_INFO_TEXT_2 = "Jako zdrojový adresář je třeba označit adresář, s jehož soubory se bude pracovat.",
			AAF_INFO_TEXT_3 = "Pokud ja zadán cílový adresář (pokud neexistuje, bude vytvořen), zkopírují se do něj veškeré soubory ze zdrojového adresáře, se kterými se posléze pracuje, tj. budou přejmenovány, přetypovány apod.",
			AAF_INFO_TEXT_4 = "Je možné otevřít průzkumník projektů, kde je možné označit oba adresáře a projít soubory na disku, případně je 'po jednom' přejmenovat apod.",
			AAF_INFO_TEXT_5 = "Záložka 'Přejmenovat'",
			AAF_INFO_TEXT_6 = "Zde je třeba zadat nový název pro soubory a zvolit způsob indexování, dále je možné zvolit soubory, které se mají přejmenovat, případně, které se mají vynechat a nebudou tak přejmenovány.",
			AAF_INFO_TEXT_7 = "Záložka 'Nahradit'",
			AAF_INFO_TEXT_8 = "Zde je možné zadat texty, které obsahují názvy souborů, případně se jedná o názvy souborů, které se mají přejmenovat, nebo nahradit zadaný text v názvu souboru za nový.",
			AAF_INFO_TEXT_9 = "V tomto případě lze označit některé možnosti pro 'nalezení' souborů, u kterých se má nahradit jejich název (nebo alespoň část).",
			AAF_INFO_TEXT_10 = "Dále je možné označit nějkteré soubory, které i když jejich názvy obsahují zadaný text, budou vynechány, resp. nebudou přejmenovány.",
			AAF_INFO_TEXT_11 = "Nastavení",
			AAF_INFO_TEXT_12 = "V nastavení si lze vybrat jazyk pro aplikaci dle nabízených možností, nebo si jazyk uložit do souboru na disk, který je možné si upravit a přepsat tak texty dle vlastních potřeb a znovu jej načíst do aplikace.",
			AAF_INFO_TEXT_13 = "Dále je zde možné nastavit, že se v Průzkumníku souborů budou ve stromové struktuře zobrazovat skyrté soubory.";
	
	
	
	
	
	
	
	
	// Texty do dialogu s informacemi o autorovi aplikace - třída AboutAuthorForm v
	// balíčku forms:
	public static final String ABOUT_AUTHOR_FORM_DIALOG_TITLE = "Autor",
			ABOUT_AUTHOR_FORM_LBL_IMAGE = "Není k dispozici", ABOUT_AUTHOR_FORM_LBL_FIRST_NAME = "Jméno",
			ABOUT_AUTHOR_FORM_LBL_LAST_NAME = "Příjmení", ABOUT_AUTHOR_FORM_LBL_EMAIL = "Email",
			ABOUT_AUTHOR_FORM_LBL_TELEPHONE = "Telefonní číslo";
	
	// Texty pro popis aplikace do JTextArea v dialogu AboutAuthorForm:
	public static final String AAF_TEXT_1 = "Tuto aplikaci jsem napsal za účelem získání nových znalostí a zkušeností ohledně jazyka Java, algoritmů, postupů pri vývoji SW, využití lambda a reglárních výrazů atd.",

			AAF_TEXT_2 = "Možná rozšíření",
			AAF_TEXT_3 = "Přidání možnosti pro označení libovolného počtu zdrojových adresářů, vždy by se soubory překopírovaly do jednoho cílového nebo přejmenovaly v označených zdrojových adresářích.",
			AAF_TEXT_4 = "Další dialog, který by ukazoval výsledky přejmenování souborů, například si uživatel na kartě Přejmenovat vyplní svá kritéria pro přejmenování a po kliknutí na příslušné tlačítko 'Ukázat výsledky' by se otevřel nový dialog, kde by byl seznam adresářů s původními a novými názvy souborů, případně i adresářů.",

			AAF_TEXT_5 = "Průzkumník souborů",
			AAF_TEXT_6 = "Je možné doplnit dialog 'Nastavení', který bude obsahovat komponenty pro nastavení, v jakém formátu se mají zobrazovat datumy o souborech (datum, čas, datum a čas apod.), případně, zda se vůbec mají zobrazovat.",
			AAF_TEXT_7 = "Podobně jsou na tom ikony, tj. zda se mají zobrazovat v tabulce.",
			AAF_TEXT_8 = "Tento diaog nastavení jsem zavrhnul, protože jsem nepřidával funkci pro ukládání nastavení někam do souborů (například při spuštění aplikace apod.). Proto jsem do ani 'nenaznačoval' v aplikaci, pouze jako potenciální rozšíření.";
	
	
	
	
	
	
	
	
	
	// Texty do panelu pro zdrojový a cílový adresář v hlavním okně aplikace - třída
	// FoldersPanel v balíčku panels.foldersPanel:
	public static final String FP_LBL_SOURCE_DIR = "Zdrojový adresář", FP_LBL_DESTINATION_DIR = "Cílový adresář",
			FP_BTN_SELECT_SOURCE_DIR_TEXT = "Vybrat", FP_BTN_SELECT_DESTINATION_DIR_TEXT = "Vybrat",
			FP_BTN_SELECT_SOURCE_DIR_TT = "Otevře dialogové okno pro výběr zdrojového adresáře.",
			FP_BTN_SELECT_DESTINATION_DIR_TT = "Otevře dialogové okno pro výběr cílového adresáře.",
			FP_BTN_OPEN_FILE_EXPLORER_TEXT = "Otevřít průzkumník souborů",
			FP_BTN_OPEN_FILE_EXPLORER_TT = "Otevře dialogové okno, kde lze procházet soubory na disku (/ discích) a prohlížet informace o souborech.",
			FP_TXT_SELECT_SOURCE_DIR = "Vyberte zdrojový adresář",
			FP_TXT_SELECT_DESTINATION_DIR = "Vyberte cílový adresář",

			FP_TXT_DESTINATION_DIR_TT = "Pokud bude zadána cesta k cílovému adresáři, soubory se nejprve do tohoto adresáře překopírují a následně provedou požadované změny.",

			FP_CHCB_COUNT_HIDDEN_FILES_TEXT = "Započítat skryté soubory a adresáře.",
			FP_CHCB_COUNT_HIDDEN_FILES_TT = "Budou změněny i skryté soubory, případně soubory s jejich příponami.",

			FP_TXT_MISSING_PATH = "Není zadána cesta ke zdrojovému adresáři!",
			FP_TXT_WRONG_PATH = "Zadaná cesta obsahuje neplatné znaky nebo není v povolené syntaxi!",
			FP_TXT_DIR_DOESNT_EXIST = "Adresář na zadané cestě neexistuje!",
			FP_TXT_DIR_DOESNT_EXIST_WILL_BE_CREATED = "Adresář na zadané cestě neexistuje (bude vytvořen)!",
			FP_TXT_PATH_IS_NOT_HEADING_TO_DIR = "Zadaná cesta nesměřuje k adresáři!",
			FP_TXT_SAME_PATHS = "Cesty k adresáři se shodují!";
	
	// Font pro chybové labely v třídě FoldersPanel:
	public static final Font FP_ERROR_LABEL = new Font("Font for error labels.", Font.BOLD, 14);
	
	
	
	
	
	
	
	
	
	
	// Texty do třídy App v balíčku app:
	// Texty do komponenta JTabbedPane:
	public static final String APP_TP_RENAME_TAB_TEXT = "Přejmenovat", APP_TP_REPLACE_TAB_TEXT = "Nahradit",
			APP_TP_RENAME_TAB_TT = "Tato záložka obsahuje komponenty pro nastavení parametrů pro přejmenování souborů.",
			APP_TP_REPLACE_TAB_TT = "Tato záložka obsahuje komponenty pro nastavení parametrů pro nahrazení 'částí' názvů souborů."; 
	
	// Texty v třídě App - jako výše, ale do chybových hlášek:
	public static final String TXT_JOP_SOURCE_DIR_DOESNT_EXIST_TEXT = "Označený zdrojový adresář již neexistuje!",
			TXT_JOP_SOURCE_DIR_DOESNT_EXIST_TITLE = "Adresář neexistuje"; 
	
	
	
	
	
	/**
	 * Jedná se o jednorozměrné pole, které obsahuje názvy sloupců pro tabulku,
	 * resp. její table model pro změnu přípon (ChangeFileTypeTableModel) v balíčku
	 * panels
	 */
	public static final String[] CHANGE_FILE_TYPE_COLUMNS = {"Původní přípona", "Nová přípona"};
	
	public static final String CFTTM_JOP_WRONG_EXTENSION_TEXT = "Přípona musí být v syntaxi '.x' -> za x je možné dostadit 1 - 10 znaků (velká a malá písmena bez diakritiky).",
			CFTTM_JOP_WRONG_EXTENSION_TITLE = "Chybná syntaxe";
	
	
	
	
	
	
	/**
	 * Jednorozměrné pole, které obsahuje názvy sloupců pro nahrazení textu / písmen
	 * v názvech sobourů a případně adresářů. Texty se vkládají do tabulky ve třídě
	 * ReplacePanel v balíčku replacePanel, tyto texty se konkrétně používají v
	 * table modelu pro zmíněnou tabulku, model: ReplaceTextTableModel v balíčku
	 * panels.
	 */
	public static final String[] REPLACE_TEXT_COLUMNS = {"Původní text", "Nový text"}; 
	
	
	public static final String TXT_ORIGINAL_VALUE = "Původní text", TXT_NEW_VALUE = "Nový text";
	
	// Chybová hlášení:
	public static final String RTTM_JOP_TXT_WRONG_SYNTAX_TEXT = "Zadaný text neodpovídá požadované syntaxi (nejspíše obsahuje neplatné znaky)!",
			RTTM_JOP_TXT_WRONG_SYNTAX_TITLE = "Chybná syntaxe";
	
	
	
	
	
	
	
	
	// Texty do třídy SkipFilesWithNamePanel v balíčku panels.
	public static final String SFWNP_BTN_SEARCH_FILE_NAME = "Vyhledat",
			SFWNP_BTN_SELECT_ALL_ITEMS_LIST_SKIP_FILES_WITH_NAME = "Označit vše",
			SFWNP_BTN_DELECT_ALL_ITEMS_LIST_SKIP_FILES_WITH_NAME = "Odznačit vše"; 
	
	
	
	
	
	
	// Texy do třídy ChcbsPanels v balíčku replacePanel:
	public static final String CP_LBL_TITLE = "Nahradit texty / písmena v názvech souborů",
			CP_CHCB_IGNORE_LETTER_SIZE_TEXT = "Ignorovat velikost písmen.",
			CP_CHCB_IGNORE_LETTER_SIZE_TT = "Při porovnávání názvů souborů se zadanými texty pro nahrazení se budou / nebudou rozeznávat velikost písmen.",
			CP_CHCB_REPLACE_DIRECTORY_NAME = "Nahradit text i v názvech adresářů.",
			CP_CHCB_SEARCH_SUBDIRECTORIES = "Prohledat i podadresáře.";
	
	
	
	
	
	
	// Texty do třídy ReplacePanel v balíčku replacePanel:
	public static final String RP_PNL_TABLE_VALUES = "Texty pro nahrazení", RP_BTN_ADD_ROW_TEXT = "Přidat text",
			RP_BTN_ADD_ROW_TT = "Přidá nový řádek do tabulky.", RP_BTN_DELETE_ROW_TEXT = "Odebrat text",
			RP_BTN_DELETE_ROW_TT = "Odebere označený řádek z tabulky.",
			RP_TXT_UNSELECTED_ROW_TEXT = "Není označen řádek pro smazání!",
			RP_TXT_UNSELECTED_ROW_TITLE = "Neoznačen řádek", RP_BTN_SWAP_COLUMNS_TEXT = "Prohodit text",
			RP_BTN_SWAP_COLUMNS_TT = "Prohodí se texty ve sloupcích na označeném řádku. (Text v levém sloupci se přesune do pravého sloupce a naopak.)",
			RP_TXT_EMPTY_TABLE_TEXT = "Tabulka neobsahuje žádná data!", RP_TXT_EMPTY_TABLE_TITLE = "Prázdná tabulka",
			RP_TXT_UNSELECTED_ROW_FOR_TEXT_SWAP_TEXT = "Není označen řádek v tabulkce, kde se mají prohodit texty.",
			RP_TXT_UNSELECTED_ROW_FOR_TEXT_SWAP_TITLE = "Neoznačen řádek", RP_BTN_LOAD_ALL_NAMES_TEXT = "Načíst názvy",
			RP_BTN_LOAD_ALL_NAMES_TT = "Do tabulky se načtou názvy všech souborů ve zvoleném zdrojovém adresáři (pokud je označeno 'Prohledat i podadresáře', načtou se názvy souborů i z podadresářů). Původní data budou vymazána.",
			RP_TXT_SOURCE_DIR_DOESNT_EXIST_OR_DIDNT_SET_TEXT = "Zdrojový adresář nebyl zadán nebo neexistuje!",
			RP_TXT_SOURCE_DIR_DOESNT_EXIST_OR_DIDNT_SET_TT = "Chyba", RP_LBL_SKIP_FILES = "Vynechat soubory",
			RP_CHCB_SKIP_FILES_WITH_EXTENSIONS = "Vynechat soubory s příponou",
			RP_CHCB_SKIP_SPECIFIC_FILES = "Vynechat soubory", RP_BTN_SELECT_ALL_ITEM_EXTENSIONS = "Označit vše",
			RP_BTN_DESELECT_ALL_ITEM_EXTENSIONS = "Odznačit vše", RP_BTN_CONFIRM = "Nahradit",
			RP_TXT_COPY_ERROR_TEXT = "Došlo k chybě při pokusu o zkopírování souborů ze zdrojového adresáře do cílového adresáře, soubory nebyly zkopírovány.",
			RP_TXT_COPY_ERROR_TITLE = "Kopírování selhalo",
			RP_TXT_EMPTY_FIELD_IN_TABLE_TEXT = "Tabulka, nebo nějaké její pole je prázdné!",
			RP_TXT_EMPTY_FIELD_IN_TABLE_TITLE = "Prázdné pole",
			RP_TXT_RENAME_FILE_ERROR_TEXT_1 = "Došlo k chybě při pokusu o přejmenování souboru!\nPůvodní název",
			RP_TXT_RENAME_FILE_ERROR_TEXT_2 = "Nový název", RP_TXT_RENAME_FILE_ERROR_TITLE = "Chyba";
	
	
	
	
	
	
	
	
	// Texty do třídy FilterToolbar v balíčku fileExplorer:
	/**
	 * Model pro kompnentu JComboBox cmbFilter ve třídě FilterToolbar v balíčku
	 * fileExplorer. Tyto hodnoty se zobrazí v cmbFilter a dle toho se bude
	 * vyhledávat v tabulce - v aktuálně zobrazeých datech.
	 */
	public static String[] CMB_MODEL = { "Název", "Velikost", "Přípona", "Datum přístupu", "Datum vytvoření",
			"Datum změny", "Datum přístupu starší než", "Datum přístupu mladší než", "Datum vytvoření starší než",
			"Datum vytvoření mladší než", "Datum změny starší než", "Datum změny mladší než", "Čas přístupu",
			"Čas vytvoření", "Čas změny", "Čas přístupu starší než", "Čas přístupu mladší než",
			"Čas vytvoření starší než", "Čas vytvoření mladší než", "Čas změny starší než", "Čas změny mladší než" };
	
	/**
	 * Model pro JComboBox cmbShowFile ve třídě FilterToolbar v balíčku
	 * fileExplorers. Tyto data v tomto modelu jsou zobrazeny v cmbShowFiles a
	 * uživatel si mezi nimi může vybírat, jaká data se mají zobrazit.
	 */
	public static final String[] CMB_SHOW_FILES_MODEL = { "Vše", "Skryté soubory", "Viditelné soubory" };
	
	
	// Texty pro hlášky a texty pro komponenty v toolbaru:
	public static final String FT_LBL_FILTER_TEXT = "Filtr",
			FT_LBL_FILTER_TT = "Soubory se vyhledávají dle zadaných kritérií pouze v označeném adresáři.",
			FT_BTN_SEARCH = "Vyhledat", FT_LBL_SHOW_FILES = "Zobrazit", FT_TXT_EXAMPLE_NAME = "Název souboru",
			FT_TXT_WRONG_TIME_TEXT = "Vytvořen chybný čas!", FT_TXT_WRONG_TIME_TITLE = "Chybný čas",
			FT_TXT_WRONG_DATE_TEXT = "Vytvořeno chybné datum!", FT_TXT_WRONG_DATE_TITLE = "Chybné datum";
	
	
	
	
	
	
	
	
	
	
	// Texty do třídy FileTableModel v balíčku fileExplorer - model pro tabulku - sloupce:
	public static final String[] FILE_COLUMNS = { "Označit", "Ikona", "Název", "Přípona", "Rodičovský adresář",
			"Skrytý", "Velikost", "Datum vytvoření", "Poslední přístup", "Poslední modifikace" };
	
	// Ostatní texty pro hlášky apod.:
	public static final String FTM_TXT_NEW_NAME_ERROR_TEXT = "Nový název neodpovídá požadované syntaxi.",
			FTM_TXT_NEW_NAME_ERROR_TITLE = "Chybná syntaxe",
			FTM_TXT_NEW_EXTENSION_ERROR_TEXT = "Zadaná přípona neodpovídá požadované syntaxi (velká a malá písmena bez diakritiy a číslice).",
			FTM_TXT_NEW_EXTENSION_ERROR_TITLE = "Chybná syntaxe",
			FTM_TXT_RENAME_FILE_ERROR_TEXT_1 = "Došlo k chybě při pokusu o přemenování souboru.",
			FTM_TXT_RENAME_FILE_ERROR_TEXT_2 = "Původní název", FTM_TXT_RENAME_FILE_ERROR_TEXT_3 = "Nový název",
			FTM_TXT_RENAME_FILE_ERROR_TITLE = "Chyba přejmenování";
	
	
	
	
	
	// Texty do třídy AbstractActions v balíčku fileExplorer:
	public static final String AA_TXT_CREATE_FILE = "Vytvořit soubor", AA_TXT_CREATE_DIR = "Vytvořit adresář",
			AA_TXT_EDIT_FILE = "Přejmenovat", AA_TXT_DELETE_FILE = "Smazat",
			AA_TXT_CREATE_FILE_TT = "Otevře dialogové okno pro zadání názvu a přípony nového souboru.",
			AA_TXT_CREATE_DIR_TT = "Otevře dialogové okno pro výběr adresáře a pak otevře dialogové okno pro název nového adresáře.",
			AA_TXT_EDIT_FILE_TT = "Otevře dialogové okno pro zadání nového názvu (případně přípony) nějakého souboru (pro označené soubory).",
			AA_TXT_DELETE_FILE_TT = "Vymažou se soubory označené v tabulce.",
			AA_TXT_CHOOSE_DIR_TITLE = "Zvolte adresář",
			AA_TXT_EDIT_FILE_ERROR_TEXT = "Není označen žádný soubor pro přejmenování!",
			AA_TXT_EDIT_FILE_ERROR_TITLE = "Neoznačeno",
			AA_TXT_DELETE_FILE_ERROR_TEXT = "Není označen žádný soubor pro smazání!",
			AA_TXT_DELETE_FILE_ERROR_TITLE = "Neoznačeno",
			AA_TXT_DELETE_FILE_LAST_CHECK_TEXT = "Opravdu chcete smazat označené soubory?",
			AA_TXT_DELETE_FILE_LAST_CHECK_TITLE = "Opravdu smazat",
			AA_TXT_CREATE_FILE_ERROR_TEXT_1 = "Došlo k chybě při pokusu o vytvoření souboru. Soubor nebyl vytvořen!",
			AA_TXT_CREATE_FILE_ERROR_TEXT_2 = "Soubor", AA_TXT_CREATE_FILE_ERROR_TITLE = "Chyba",
			AA_TXT_CREATE_DIR_ERROR_TEXT_1 = "Došlo k chybě při pokusu o vytvoření adresářer. Adresář nebyl vytvořen!",
			AA_TXT_CREATE_DIR_ERROR_TEXT_2 = "Adresář", AA_TXT_CREATE_DIR_ERROR_TITLE = "Chyba",
			AA_TXT_RENAME_FILE_ERROR_TEXT = "Došlo k chybě při pokusu o přejmenování souboru. Soubor nebyl přejmenován.",
			AA_TXT_RENAME_FILE_ERROR_TITLE = "Chyba při přejmenování",
			AA_TXT_DELETE_ERROR_TEXT_1 = "Došlo k chybě při pokusu o smazání adresáře. Adresář nebyl smazán.",
			AA_TXT_DELETE_ERROR_TEXT_2 = "Soubor", AA_TXT_DELETE_ERROR_TITLE = "Chyba smazání",
			AA_TXT_DELETE_FILE_ERROR_TEXT_1 = "Došlo k chybě při pokusu o smazání souboru. Soubor nebyl smazán.",
			AA_TXT_DELETE_FILE_ERROR_TEXT_2 = "Soubor", AA_TXT_JOP_DELETE_FILE_ERROR_TITLE = "Chyba smazání";
	
	
	
	
	// Texty do třídy FileExplorer v balíčku fileExplorer:
	public static final String FE_DIALOG_TITLE = "Průzkumník souborů", FE_JSP_TABLE_TITLE = "Položky ve zvoleném adresáři";
	
	
	
	// Texty do třídy Menu v balíčku fileExplorer:
	public static final String MN_MN_DIALOG = "Dialog", MN_MN_ACTIONS = "Akce", MN_ITEM_INFO_TEXT = "Info",
			MN_ITEM_INFO_TT = "Zobrazí dialog s informacemi o průzkumníku projektů.", MN_ITEM_CLOSE_TEXT = "Zavřít",
			MN_ITEM_CLOSE_TT = "Zavře tento dialog (průzkumník souborů).";

	
	
	// Texty do třídy RenameFileForm v balíčku fileExplorer:
	public static final String RFF_DIALOG_TITLE_1 = "Přejmenovat", RFF_DIALOG_TITLE_2 = "Vytvořit soubor",
			RFF_DIALOG_TITLE_3 = "Vytvořit adresář", RFF_LBL_NAME = "Název",
			RFF_CHCB_WITH_EXTENSION_TEXT = "Zadat název včetně přípony.",
			RFF_CHCB_WITH_EXTENSION_TT = "Je třeba zadat název včetně přípony, může se od původní lišit.",
			RFF_BTN_RENAME_1 = "Přejmenovat", RFF_BTN_RENAME_2 = "Vytvořit", RFF_BTN_CLOSE = "Zavřít",
			RFF_TXT_EMPTY_FIELD = "Pole je prázdné!",
			RFF_TXT_REG_EX_ERROR = "Zadaný název neodpovídá požadované syntaxi.",
			RFF_TXT_EXTENSION_ERROR = "Přípona souboru se není uvedena ve správné syntaxi!",
			RFF_TXT_SAME_NAMES = "Nový název je stejný, jako původní. Je třeba jej změnit.",
			RFF_TXT_MISSING_EXTENSION = "Není uvedena přípona souboru.";
	
	
	
	// Texty do třídy ToolBarTableButtons v balíčku fileExplorer:
	public static final String TBTB_BTN_SELECT_ALL_ITEMS_TEXT = "Označit vše",
			TBTB_BTN_SELECT_ALL_ITEMS_TT = "Označí veškeré položky v tabulce.",
			TBTB_BTN_DESELECT_ALL_ITEMS_TEXT = "Odznačit vše",
			TBTB_BTN_DESELECT_ALL_ITEMS_TT = "Odznačí veškeré položky v tabulce.",
			TBTB_BTN_HIDE_ALL_FILES_TEXT = "Schovat vše",
			TBTB_BTN_HIDE_ALL_FILES_TT = "Schová veškeré soubory v tabulce, tj. nastaví je jako skryté.",
			TBTB_BTN_SHOW_ALL_FILES_TEXT = "Zviditelnit vše",
			TBTB_BTN_SHOW_ALL_FILES_TT = "Zobrazí všechny soubory v tabulce, tj. nebudou skryté (odeberou se tečky z názvů souborů).",
			TBTB_BTN_HIDE_SELECTED_FILES_TEXT = "Skrýt označené",
			TBTB_BTN_HIDE_SELECTED_FILES_TT = "Skryje veškeré označené soubory v tabulce, tj. přidá se desetinná tečka před název souboru.",
			TBTB_BTN_SHOW_SELECTED_FILES_TEXT = "Zviditelnit označené",
			TBTB_BTN_SHOW_SELECTED_FILES_TT = "Zviditelní označené soubory v tabulce, tj. odebere tečku ze začátku názvu souboru.";
	
	
	
	// Texty do třídy PopupMenu v balíčku jTree:
	public static final String PM_ITEM_EXPAND_ALL = "Rozšířit vše", PM_ITEM_COLLAPSE_ALL = "Zavřít vše",
			PM_ITEM_SET_SOURCE_DIR = "Nastavit zdrojový adresář",
			PM_ITEM_SET_DESTINATION_DIR = "Nastavit cílový adresář", PM_ITEM_CREATE_FILE = "Vytvořit soubor",
			PM_ITEM_CREATE_DIR = "Vytvořit adresář", PM_ITEM_RENAME_FILE = "Přejmenovat",
			PM_ITEM_DELETE_FILE = "Smazat", PM_ITEM_REPAINT = "Aktualizovat",
			PM_TXT_SAME_DESTINATION_DIR_ERROR_TEXT = "Tento adresář nelze nastavit jako zdrojový. Je shodný s cílovým adresářem.",
			PM_TXT_SAME_DESTINATION_DIR_ERROR_TITLE = "Duplicita",
			PM_TXT_SAME_SOURCE_DIR_ERROR_TEXT = "Tento adresář nelze nastavit jako cílový. je shodný se zdrojovým adresářem.",
			PM_TXT_SAME_SOURCE_DIR_ERROR_TITLE = "Duplicita", PM_TXT_DELETE_FILE_TEXT = "Opravdu chcete smazat soubor?",
			PM_TXT_DELETE_FILE_TITLE = "Opravdu smazat";
	
	
	
	// Texty do třídy Tree v balíčku jTree:
	public static final String T_TXT_EXPAND_ACTION = "Rozšířit", T_TXT_COLLAPSE_ACTION = "Zavřít";
	
	
	
	
	
	
	
	// Texty do třídy LanguagePanel v balíčku settingsForm:
	public static final String LP_LBL_LANGUAGE = "Jazyk", LP_BTN_CHOOSE_LANGUAGE_TEXT = "Zvolit",
			LP_BTN_CHOOSE_LANGUAGE_TT = "Nastaví zvolený jazyk do aplikace.",
			LP_LBL_CHOOSE_LANGUAGE = "Zvolit jiný jazyk", LP_BTN_CHOOSE_DIFFERENT_LANGUAGE_TEXT = "Vybrat",
			LP_BTN_CHOOSE_DIFFERENT_LANGUAGE_TT = "Otevře dialogové okno pro výběr souboru typu .Properties, který obsahuje texty pro aplikaci v požadovaném jazyce.",
			LP_LBL_SAVE_LANGUAGE = "Uložit jazyk", LP_BTN_SAVE_LANGUAGE_TEXT = "Uložit",
			LP_BTN_SAVE_LANGUAGE_TITLE = "Otevře dialogové okno pro zvolení umístení a zadání názvu souboru.",
			
			LP_TXT_TITLE = "Zvolte jazyk",
			LP_TXT_WRONG_FILE_TEXT = "Je možné označit pouze soubory s příponou .properties!",
			LP_TXT_WRONG_FILE_TITLE = "Chybný soubor",
			
			LP_TXT_FAILED_LOAD_PROPERTIES_FILE_TEXT_1 = "Došlo k chybě při pokusu o načtení souboru. Soubor s texty pro aplikaci nebyl načten.",
			LP_TXT_FAILED_LOAD_PROPERTIES_FILE_TEXT_2 = "Soubor", LP_TXT_FAILED_LOAD_PROPERTIES_FILE_TITLE = "Chyba",
			
			LP_TXT_SAVE_LANGUAGE_TITLE = "Uložit jazyk",
			LP_TXT_JOP_SAVE_LANGUAGE_TEXT = "Je třeba zadat název souboru s příponou '.properties'.",
			LP_TXT_JOP_SAVE_LANGUAGE_TITLE = "Chybný soubor";
	
	
	
	
	
	// Texty do třídy SettingsForm v balíčku settingsForm:
	public static final String SF_DIALOG_TITLE = "Nastavení",
			SF_LBL_INFO = "Provedené změny jsou uchovány pouze pro aktuální běh aplikace, po restartu bude obnoveno výchozí nastavení.",
			SF_CHCB_SHOW_HIDDEN_FILES_IN_TREE = "Zobrazit skryté soubory ve stromové struktuře.",
			SF_LBL_SIZE_FORMAT_TEXT = "Formát pro zobrazení velikosti souborů",
			SF_LBL_SIZE_FORMAT_TT = "Ve vybraném formátu bude zobrazena velikost souborů v tabulce v průzkumníku souborů (například v KB nebo v MB apod.).";
	
	
	
	
	
	
	
	
	// Texty do třídy RenamePanel v balíčku renamePanel:
	public static final String RP_LBL_NEW_NAME = "Nový název", RP_LBL_NAME_TEXT = "Název",
			RP_LBL_NAME_TT = "Název, na který se přejmenují soubory dle nastavených parametrů.",
			RP_LBL_ERROR_INFO_TEXT = "Název obsahuje nepovolené znaky nebo chybnou syntaxi.",
			RP_LBL_ERROR_INFO_TT = "Povolené znaky: Velká a malá písmena s / bez diakritiky, číslice, podtržítka, tečky, vykřičník, pomlčka.",
			RP_TXT_NOT_SPECIFIED = "Neuvedeno", RP_LBL_INDEXATION_TEXT = "Indexace",
			RP_LBL_INDEXATION_TT = "Nastavení parametrů pro indexování souborů.",
			RP_CHCB_INDEX_BY_NUMBER = "Indexovat dle čísel", RP_CHCB_INDEX_BY_LETTERS_TEXT = "Indexovat dle písmen.",
			RP_CHCB_INDEX_BY_LETTERS_TT = "V případě, že dojdou písmena abecedy, bude se pokračovat indexováním dle čísel.",
			RP_LBL_STYLE_NUMBERING = "Styl číslování",
			RP_CHCB_INDEX_IN_FRONT_OF_TEXT = "Přidávat index před zadaný název.",
			RP_CHCB_INDEX_BEHIND_OF_TEXT = "Přidávat index za zadaný název.",
			RP_CHCB_USE_BIG_LETTERS = "Použít velká písmena.", RP_CHCB_USE_SMALL_LETTERS = "Použít malá písmena.",
			CHCB_LETTERS_IN_FRONT_OF_TEXT = "Přidávat písmena před zadaný název.",
			RP_CHCB_LETTERS_BEHIND_OF_TEXT = "Přidávat písmena za zadaný název.",
			RP_LBL_STYLE_OF_INDEXATION_BY_LETTERS = "Způsob indexování", RP_LBL_NEW_NAME_EXAMPLE_TEXT = "Ukázka",
			RP_TXT_LBL_NEW_NAME_EXAMPLE_START = "Ukázka",
			RP_LBL_NEW_NAME_EXAMPLE_TT = "Příklad toho, jak budou vypadat soubory po přejmenování dle nastavených parametrů.",
			RP_LBL_RENAME_FILE_IN_SUBDIRS = "Soubory v podadresářích",
			RP_CHCB_RENAME_FILES_IN_SUB_DIRS = "Přejmenovat soubory i v podadresářích.",
			RP_CHCB_START_INDEXING_FROM_ZERO_IN_SUBDIRS_TEXT = "Indexovat soubory v podadresářích od začátku.",
			RP_CHCB_START_INDEXING_FROM_ZERO_IN_SUBDIRS_TT = "Pokaždé, když se vstoupí do podadresáře, začne indexování souborů od začátku. V opačném případě jsou soubory v adresářích indexovány dle pořadí načtení adresářů v aplikaci.",
			RP_CHCB_LEAVE_FILE_TYPE = "Ponechat typ souboru.", RP_CHCB_CHANGE_FILE_TYPE_TEXT = "Změnit typ souboru.",
			RP_CHCB_CHANGE_FILE_TYPE_TT = "Změny typů souborů nejsou hlídány, například je možné změnit soubor typu textový dokument na obrázek apod.",
			RP_PNL_CHANGE_FILE_TYPE_TABLE_DATA = "Změnit přípony", RP_BTN_CREATE_EXTENSION_TEXT = "Přidat",
			RP_BTN_CREATE_EXTENSION_TT = "Přidá nový řádek do tabulky", RP_BTN_DELETE_EXTENSION_TEXT = "Odebrat",
			RP_BTN_DELETE_EXTENSION_TT = "Odebre označený řádek z tabulky.",
			RP_BTN_SWAP_EXTENSIONS_TEXT = "Zaměnit přípony",
			RP_BTN_SWAP_EXTENSIONS_TT = "Prohodí se přípony na označeném řádku. (Přípona v levém sloupci se přesune do pravého sloupce a naopak.)",
			RP_BTN_LOAD_EXTENSIONS_TEXT = "Načíst přípony",
			RP_BTN_LOAD_EXTENSIONS_TT = "Načtou se přípony souborů ve zvoleném zdrojovém adresáři do tabulky (původní data budou vymazána).",
			RP_TXT_ROW_IS_NOT_SELECTED_TEXT = "Není označena položka pro smazání / odebrání.",
			RP_TXT_ROW_IS_NOT_SELECTED_TITLE = "Neoznačena položka",
			RP_LBL_FILE_FOR_RENAME_TEXT = "Soubory (původní názvy) pro přejmenování",
			RP_LBL_FILE_FOR_RENAME_TT = "Jedná se o definování souborů / názvů souborů, které se přejmenují dle nastavených parametrů výše.",
			RP_CHCB_RENAME_ALL_FILES = "Přejmenovat všechny soubory.",
			RP_CHCB_RENAME_FILE_CONTAINS_TEXT_TEXT = "Přejmenovat soubory obsahující text",
			RP_CHCB_RENAME_FILE_CONTAINS_TEXT_TT = "Jedná se pouze o názvy souborů, ne přípony.",
			RP_CHCB_IGNORE_LETTER_SIZE = "Ignorovat velikosti písmen.",
			RP_CHCB_RENAME_FILES_BY_REG_EX = "Přejmenovat soubory splňující regulární výraz.",
			RP_CHCB_INCLUDE_EXTENSION_TEXT = "Včetně přípony",
			RP_CHCB_INCLUDE_EXTENSION_TT = "Názvy souborů se budou testovat, zda projdou regulárním výrazem včetně přípony (např. fileName.jpg).",
			RP_LBL_REG_EX_TEXT = "Regulární výraz",
			RP_LBL_REG_EX_TT = "Regulární výraz, dle kterého se budou rozpoznávat soubory, které se přejmenují.",
			RP_LBL_TEST_REG_EX_TEXT = "Otestovat regulární výraz", RP_BTN_TEST_TEXT_FOR_REG_EX = "Otestovat",
			RP_CHCB_RENAME_FILE_WITH_EXTENSION_TEXT = "Přejmenovat pouze soubory typu",
			RP_CHCB_RENAME_FILE_WITH_EXTENSIONS_TT = "Budou přejmenovány pouze soubory s označenými příponami.",
			RP_LBL_SKIP_FILES_TEXT = "Vynechat soubory",
			RP_LBL_SKIP_FILES_TT = "Soubory zvoleného typu nebo názvu nebudou přejmenovány.",
			RP_CHCB_SKIP_FILES_WITH_EXTENSION = "Vynechat soubory typu",
			RP_CHCB_SKIP_FILES_WITH_NAME = "Vynechat soubory",
			RP_BTN_SELECT_ALL_ITEMS_LIST_FILE_EXTENSIONS = "Označit vše",
			RP_BTN_DESELECT_ALL_ITEMS_LIST_FILE_EXTENSIONS = "Odznačit vše",
			RP_BTN_SELECT_ALL_ITEMS_WITH_EXTENSIONS = "Označit vše",
			RP_BTN_DESELECT_ALL_ITEMS_WITH_EXTENSIONS = "Odznačit vše", RP_2_BTN_CONFIRM = "Přejmenovat",
			RP_2_TXT_EMPTY_TABLE_TEXT = "Tabulka neobsahuje žádná data!",
			RP_2_TXT_EMPTY_TABLE_TITLE = "Prázdná tabulka",
			RP_TXT_UNSELECTED_TABLE_ROW_TEXT = "Není označen řádek v tabulkce, kde se mají prohodit přípony.",
			RP_TXT_UNSELECTED_TABLE_ROW_TITLE = "Neoznačen řádek",
			RP_TXT_COPY_DIR_ERROR_TEXT = "Došlo k chybě při pokusu o zkopírování souborů ze zdrojového adresáře do cílového adresáře, soubory nebyly zkopírovány.",
			RP_TXT_COPY_DIR_ERROR_TITLE = "Kopírování selhalo",
			RP_TXT_ERROR_OR_EMPTY_FIELD_TEXT = "Pole pro nový název souborů je prázdné, nebo neodpovídá požadované syntaxi!",
			RP_TXT_ERROR_OR_EMPTY_FIELD_TITLE = "Chyba",
			RP_TXT_EMPTY_TABLE_CHANGE_TYPE_TEXT = "Tabulka s informacemi o nahrazení přípon souborů je prázdná!",
			RP_TXT_EMPTY_TABLE_CHANGE_TYPE_TITLE = "Prázdná tabulka",
			RP_2_TXT_EMPTY_FIELD_IN_TABLE_TEXT = "Tabulka s informacemi o nahrazení přípon souborů obsahuje prázdné pole!",
			RP_2_TXT_EMPTY_FIELD_IN_TABLE_TITLE = "Prázdné pole",
			RP_TXT_EMPTY_FIELD_FILE_FOR_RENAME_TEXT = "Textové pole pro zadání textu, který mají obsahovat názvy souborů, aby byly přejmenovány je prázdné!",
			RP_TXT_EMPTY_FIELD_FILE_FOR_RENAME_TITLE = "Prázdné pole",
			RP_TXT_REG_EX_EMPTY_FIELD_TEXT = "Textové pole pro regulární výraz, dle kterého se mají rozpoznávat soubory, které se přejmenují je prázdné!",
			RP_TXT_REG_EX_EMPTY_FIELD_TITLE = "Prázdné pole",
			RP_TXT_SOURCE_DIR_IS_EMPTY_TEXT = "List s příponami souborů je prázdný, označený zdrojový adresář je prázdný!",
			RP_TXT_SOURCE_DIR_IS_EMPTY_TITLE = "Prázdný adresář / list s příponami",
			RP_TXT_UNSELECTED_ITEMS_IN_LIST_TEXT = "Je třeba označit alespoň jednu příponu, dle které / kterých se pozná, jaké soubory se mají přejmenovat.",
			RP_TXT_UNSELECTED_ITEMS_IN_LIST_TITLE = "Neoznačeno",
			RP_2_TXT_RENAME_FILE_ERROR_TEXT_1 = "Došlo k chybě při pokusu o změnu přípony souboru. Soubor nebyl změněn.",
			RP_2_TXT_RENAME_FILE_ERROR_TEXT_2 = "Původní název", RP_TXT_RENAME_FILE_ERROR_TEXT_3 = "Nový název",
			RP_2_TXT_RENAME_FILE_ERROR_TITLE = "Chyba přejmenování",
			RP_TXT_RENAME_FILE_NAME_ERROR_TEXT_1 = "Došlo k chybě při pokusu o přejmenování souboru. Soubor nebyl přejmenován.",
			RP_TXT_RENAME_FILE_NAME_ERROR_TEXT_2 = "Původní název", RP_TXT_RENAME_FILE_NAME_ERROR_TEXT_3 = "Nový název",
			RP_TXT_RENAME_FILE_NAME_ERROR_TITLE = "Chyba přejmenování";
	
	
	
	
	
	
	
	
	
	
	//Texty do třídy InfoForm v balíčku fileExplorer:
	public static final String IF_DIALOG_TITLE = "Info", IF_TEXT_1 = "Základní informace",
			IF_TEXT_2 = "V tabulce se zobrazí vždy jen soubory v označeném adresáři ve stromové struktuře v levé části dialogu.",
			IF_TEXT_3 = "Abstraktní tlačítka slouží pro vytvoření nového souboru či adresáře na zvolené umístění.",
			IF_TEXT_4 = "Dále je možné přejmenovat zvolený soubor či adresář nebo jej smazat.",

			IF_TEXT_5 = "Filtr", IF_TEXT_6 = "Filtrují se vždy položky ve zvoleném adresáři.",
			IF_TEXT_7 = "Je možné filtrovat soubory dle názvu, velikost, přípony, datumů a časů posledního přístupu či modifikace nebo vytvoření.",
			IF_TEXT_8 = "Je možné nastavit, aby se v tabulce zobrazovaly pouze skryté, viditelné nebo oba typy položek.",
			IF_TEXT_9 = "Lze označit / odznačit všechny soubory jedním tlačítkem.",
			IF_TEXT_10 = "Lze veškeré soubory v označeném adresáři označit jako skryté nebo viditelné.",

			IF_TEXT_11 = "Stromová struktura",
			IF_TEXT_12 = "Adresář se otevře dvojlikem na něj nebo na šipktu vedle názvu adresáře.",
			IF_TEXT_13 = "Je možné otevřít / skrýt celou stromovou strukturu.",
			IF_TEXT_14 = "Je možné nastavit zdrojový, případně i cílový adresář pro aplikaci -> pro přejmenování či nahrazení textů.",
			IF_TEXT_15 = "Pokud je označen adresář, je možné vytvořit jeho podadresář nebo nový soubor v označeném adresáři.",
			IF_TEXT_16 = "Dále smazat či přejmenovat soubor nebo adresář.",
			IF_TEXT_17 = "Nebo aktualizovat celou stromovou strukturu, která přenačte veškerá data od kořenových adresářů.",

			IF_TEXT_18 = "Tabulka",
			IF_TEXT_19 = "V tabulce je možné upravit název souboru, příponu souboru a zda je soubor skrytý nebo ne.";
}
