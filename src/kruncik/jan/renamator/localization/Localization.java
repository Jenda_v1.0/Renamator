package kruncik.jan.renamator.localization;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

/**
 * Tato třída slouží pro načtení a vytvoření souborů pro lokalizaci, resp.
 * soubory, které obsahuje veškeré texty pro aplikaci v nějakém jazyce.
 *
 * @see Properties
 * 
 * @author Jan Krunčík
 * @version 1.0
 * @since 1.8
 */

public class Localization {

	
	/**
	 * Metoda, která slouží pro načtení souboru s texty pro aplikaci.
	 * 
	 * Tento soubor se načte uvnitř souboru .jar - z "aplikace".
	 * 
	 * @param fileName
	 *            -název souboru s texty, který se má načíst.
	 * 
	 * @return soubor s texty pro aplikaci - načtený objekt typu properties nebo
	 *         null, pokud dojde k nějaké chybě.
	 */
	public static Properties loadDefaultLocalizationFile(final String fileName) {
		/*
		 * Ten separator v OS Windows nefungoval, nevím proč?
		 * 
		 * Ale je možné, že se to týká pouze souborů, které směřují ke "zdrojovým
		 * adresářům v projektu (pak v souboru .jar)???
		 */
//		final InputStream input = Localization.class.getResourceAsStream(File.separator + fileName + ".properties");
		final InputStream input = Localization.class.getResourceAsStream("/" + fileName + ".properties");

		return loadProperties(input);
	}
	
	
	
	
	
	
	
	
	
	
	/**
	 * Metoda, která slouží pro načtení souboru s texty pro aplikaci.
	 * 
	 * @param path
	 *            - cesta k souboru, který se má načíst.
	 * 
	 * @return soubor s texty pro aplikaci - načtený objekt typu properties nebo
	 *         null, pokud dojde k nějaké chybě.
	 */
	public static Properties loadLocalizationFile(final String path) {
		try {
			final InputStream input = new FileInputStream(new File(path));

			return loadProperties(input);
		} catch (FileNotFoundException e) {
			e.printStackTrace();
			return null;
		}
	}
	
	
	
	
	
	
	/**
	 * Metoda, která přímo načte ten soubor a vrátí objekt typu properties (případně
	 * null).
	 * 
	 * @param input
	 *            - soubor, ktrý se má načíst.
	 * 
	 * @return objekt typu properties s texty pro aplikaci ve zvoleném jazyce nebo
	 *         null.
	 */
	private static Properties loadProperties(final InputStream input) {
		final Properties prop = new Properties();

		try {
			prop.load(input);

			if (input != null)
				input.close();
		} catch (IOException e) {
			e.printStackTrace();
		}

		return prop;
	}
	
	
	
	
	
	
	
	
	
	
	/**
	 * Metoda, která vytvoří soubor s texty pro apliaci ve zvoleném jazyce -
	 * language.
	 * 
	 * @param path
	 *            - cesta, kam se má soubor uložit, resp. kde se má soubor vytvořit
	 *            a název souboru.
	 *
	 * @param language
	 *            - jazyk, v jakém se mají texty vytvořit
	 */
	public static void createLocalizationFile(final String path, final LocalizationLanguage language) {
		if (language == LocalizationLanguage.CZ) {
			CreateLocalization.createCz(path);
		} else if (language == LocalizationLanguage.EN) {
			CreateLocalization.createEn(path);
		}
	}
}
