package kruncik.jan.renamator.localization;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.OutputStream;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Properties;
import java.util.Vector;

import org.apache.commons.configuration2.PropertiesConfiguration;
import org.apache.commons.configuration2.PropertiesConfigurationLayout;
import org.apache.commons.configuration2.ex.ConfigurationException;

/**
 * Tato třída obsahuje metody pro vytvoření lokalizačních souborů, resp.
 * souborů, které obsahuje veškeré texty pro aplikaci ve zvoleném jazyce
 * 
 * @see Properties
 * @see Map
 * @see PropertiesConfiguration
 * @see PropertiesConfigurationLayout
 * @see SimpleDateFormat
 * @see List
 * @see BufferedReader
 * @see BufferedWriter
 * @see OutputStream
 * @see Collections
 * 
 * @author Jan Krunčík
 * @version 1.0
 * @since 1.8
 */

public class CreateLocalization {

	/**
	 * Mapa, do které se vkládají komentáře, je typu <String, String>, jako klíč se
	 * vkládá index properties a jako hodnota se vkládá samotný komentář k dané
	 * vlastnosti.
	 */
	private static final Map<String, String> COMMENTS_MAP = new HashMap<>();
	
	/**
	 * Metoda, která vytvoří soubor se zadaným názvem na zadané cestě. Soubor bude
	 * obsahovat veškeré texty pro aplikaci, aby jej mohl například uživatel upravit
	 * apod.
	 * 
	 * Texty budou v jazyce českém.
	 * 
	 * @param path
	 *            - umístění a název souboru.
	 * 
	 * @return true v případě, že se soubor vytvoří v pořádku, jinak false.
	 */
	static boolean createCz(final String path) {
		// Vyčístím mapu pro komentáře:
		COMMENTS_MAP.clear();
		
		
		/*
		 * Třída, která slouží řazení přidávaných položek.
		 */
		final SortedProperties prop = new SortedProperties();
		
//		final Properties prop = new Properties();
		
		// Texty aplikace:
		COMMENTS_MAP.put("App_Title", "Titulek aplikace.");
		prop.setProperty("App_Title", Constants.TITLE);
		
		
		
		
		COMMENTS_MAP.put("Menu_Menu_App", "Texty do hlavního menu aplikace.");
		// Texty do hlavního menu v hlavním okně aplikace (Menu.java v balíčku app)
		prop.setProperty("Menu_Menu_App", Constants.MENU_MENU_APP);
		prop.setProperty("Menu_Menu_Info", Constants.MENU_MENU_INFO);
		prop.setProperty("Menu_Item_Settings", Constants.MENU_ITEM_SETTINGS);
		prop.setProperty("Menu_Item_Close", Constants.MENU_ITEM_CLOSE);
		prop.setProperty("Menu_Item_Info_About_App", Constants.MENU_ITEM_INFO_ABOUT_APP);
		prop.setProperty("Menu_Item_Info_Author", Constants.MENU_ITEM_INFO_AUTHOR);
		
		// Popisky do tlačítek v hlavním menu v hlavním okně aplikace (Menu.java v
		// balíčku app)
		COMMENTS_MAP.put("Menu_TT_Item_Setting", "Popisky do tlačítek v hlavním menu aplikace.");
		prop.setProperty("Menu_TT_Item_Setting", Constants.MENU_TT_ITEM_SETTING);
		prop.setProperty("Menu_TT_Item_Close", Constants.MENU_TT_ITEM_CLOSE);
		prop.setProperty("Menu_TT_Item_Info_About_App", Constants.MENU_TT_ITEM_INFO_ABOUT_APP);
		prop.setProperty("Menu_TT_Item_Info_Author", Constants.MENU_TT_ITEM_INFO_AUTHOR);
		
		
		
		// Texty do dialogu s informacemi o aplikaci - dialog AboutAppForm v balíčku
		// forms:
		COMMENTS_MAP.put("About_App_Form_Dialog_Title", "Texty do dialogu s informacemi o aplikaci");
		prop.setProperty("About_App_Form_Dialog_Title", Constants.ABOUT_APP_FORM_DIALOG_TITLE);
		
		// Texty pro info o aplikaci:
		prop.setProperty("AAF_Info_Text_1", Constants.AAF_INFO_TEXT_1);
		prop.setProperty("AAF_Info_Text_2", Constants.AAF_INFO_TEXT_2);
		prop.setProperty("AAF_Info_Text_3", Constants.AAF_INFO_TEXT_3);
		prop.setProperty("AAF_Info_Text_4", Constants.AAF_INFO_TEXT_4);
		prop.setProperty("AAF_Info_Text_5", Constants.AAF_INFO_TEXT_5);
		prop.setProperty("AAF_Info_Text_6", Constants.AAF_INFO_TEXT_6);
		prop.setProperty("AAF_Info_Text_7", Constants.AAF_INFO_TEXT_7);
		prop.setProperty("AAF_Info_Text_8", Constants.AAF_INFO_TEXT_8);
		prop.setProperty("AAF_Info_Text_9", Constants.AAF_INFO_TEXT_9);
		prop.setProperty("AAF_Info_Text_10", Constants.AAF_INFO_TEXT_10);
		prop.setProperty("AAF_Info_Text_11", Constants.AAF_INFO_TEXT_11);
		prop.setProperty("AAF_Info_Text_12", Constants.AAF_INFO_TEXT_12);
		prop.setProperty("AAF_Info_Text_13", Constants.AAF_INFO_TEXT_13);
		
		
		
		
		
		
		// Texty do dialogu s informacemi o autorovi aplikace - třída AboutAuthorForm v
		// balíčku forms:
		prop.setProperty("About_Author_Form_Author", Constants.ABOUT_AUTHOR_FORM_DIALOG_TITLE);
		prop.setProperty("About_Author_Form_Lbl_Image", Constants.ABOUT_AUTHOR_FORM_LBL_IMAGE);
		prop.setProperty("About_Author_Form_Lbl_First_Name", Constants.ABOUT_AUTHOR_FORM_LBL_FIRST_NAME);
		prop.setProperty("About_Author_Form_Lbl_Last_Name", Constants.ABOUT_AUTHOR_FORM_LBL_LAST_NAME);
		prop.setProperty("About_Author_Form_Lbl_Email", Constants.ABOUT_AUTHOR_FORM_LBL_EMAIL);
		prop.setProperty("About_Author_Form_Lbl_Telephone", Constants.ABOUT_AUTHOR_FORM_LBL_TELEPHONE);
		
		// Texty pro potenciální rozšíření:
		prop.setProperty("AAF_Text_1", Constants.AAF_TEXT_1);
		prop.setProperty("AAF_Text_2", Constants.AAF_TEXT_2);
		prop.setProperty("AAF_Text_3", Constants.AAF_TEXT_3);
		prop.setProperty("AAF_Text_4", Constants.AAF_TEXT_4);
		prop.setProperty("AAF_Text_5", Constants.AAF_TEXT_5);
		prop.setProperty("AAF_Text_6", Constants.AAF_TEXT_6);
		prop.setProperty("AAF_Text_7", Constants.AAF_TEXT_7);
		prop.setProperty("AAF_Text_8", Constants.AAF_TEXT_8);
		
		
		
		
		
		
		
		
		// Texty do panelu pro zdrojový a cílový adresář v hlavním okně aplikace - třída
		// FoldersPanel v balíčku panels.foldersPanel:
		prop.setProperty("FP_Lbl_Source_Dir", Constants.FP_LBL_SOURCE_DIR);
		prop.setProperty("FP_Lbl_Destination_Dir", Constants.FP_LBL_DESTINATION_DIR);
		prop.setProperty("FP_Btn_Source_Dir_Text", Constants.FP_BTN_SELECT_SOURCE_DIR_TEXT);
		prop.setProperty("FP_Btn_Destination_Dir_Text", Constants.FP_BTN_SELECT_DESTINATION_DIR_TEXT);
		prop.setProperty("FP_Btn_Select_Source_Dir_TT", Constants.FP_BTN_SELECT_SOURCE_DIR_TT);
		prop.setProperty("FP_Btn_Select_Destination_Dir_TT", Constants.FP_BTN_SELECT_DESTINATION_DIR_TT);
		prop.setProperty("FP_Btn_Open_File_Explorer_Text", Constants.FP_BTN_OPEN_FILE_EXPLORER_TEXT);
		prop.setProperty("FP_Btn_Open_File_Explorer_TT", Constants.FP_BTN_OPEN_FILE_EXPLORER_TT);
		prop.setProperty("FP_Txt_Select_Source_Dir", Constants.FP_TXT_SELECT_SOURCE_DIR);
		prop.setProperty("FP_Txt_Select_Destination_Dir", Constants.FP_TXT_SELECT_DESTINATION_DIR);
		
		prop.setProperty("FP_Txt_Destination_Dir_TT", Constants.FP_TXT_DESTINATION_DIR_TT);
		
		prop.setProperty("FP_Chcb_Count_Hidden_Files_Text", Constants.FP_CHCB_COUNT_HIDDEN_FILES_TEXT);
		prop.setProperty("FP_Chcb_Count_Hidden_Files_TT", Constants.FP_CHCB_COUNT_HIDDEN_FILES_TT);
		
		prop.setProperty("FP_Txt_Missing_Path", Constants.FP_TXT_MISSING_PATH);
		prop.setProperty("FP_Txt_Wrong_Path", Constants.FP_TXT_WRONG_PATH);
		prop.setProperty("FP_Txt_Dir_Doesnt_Exist", Constants.FP_TXT_DIR_DOESNT_EXIST);
		prop.setProperty("FP_Txt_Dir_Doesnt_Exist_Will_Be_Created", Constants.FP_TXT_DIR_DOESNT_EXIST_WILL_BE_CREATED);
		prop.setProperty("FP_Txt_Path_Is_Not_Heading_To_Dir", Constants.FP_TXT_PATH_IS_NOT_HEADING_TO_DIR);
		prop.setProperty("FP_Txt_Same_Paths", Constants.FP_TXT_SAME_PATHS);
		
		
		
		
		
		
		
		// Texty do třídy App v balíčku app:
		// Texty do komponenta JTabbedPane:
		prop.setProperty("App_TP_Rename_Tab_text", Constants.APP_TP_RENAME_TAB_TEXT);
		prop.setProperty("App_TP_Replace_Tab_Text", Constants.APP_TP_REPLACE_TAB_TEXT);
		prop.setProperty("App_TP_Rename_Tab_TT", Constants.APP_TP_RENAME_TAB_TT);
		prop.setProperty("App_TP_Replace_Tab_TT", Constants.APP_TP_REPLACE_TAB_TT);
		
		// Chybové hlášky:
		prop.setProperty("App_Jop_Source_Dir_Doesnt_Exist_Text", Constants.TXT_JOP_SOURCE_DIR_DOESNT_EXIST_TEXT);
		prop.setProperty("App_Jop_Source_Dir_Doesnt_Exist_Title", Constants.TXT_JOP_SOURCE_DIR_DOESNT_EXIST_TITLE);
		
		
		
		
		
		
		// Texty do třídy ChangeFileTypeTableModel v balíčku panels:		
		prop.setProperty("CFTTM_Jop_Wrong_Extension_Text", Constants.CFTTM_JOP_WRONG_EXTENSION_TEXT);
		prop.setProperty("CFTTM_Jop_Wrong_Extension_Title", Constants.CFTTM_JOP_WRONG_EXTENSION_TITLE);
		
		
		
		// Texty do třídy ReplaceTextTableModel:
		prop.setProperty("RTTM_Columns", createTextFromArray(Constants.REPLACE_TEXT_COLUMNS));
		
		prop.setProperty("RTTM_Txt_Original_Value", Constants.TXT_ORIGINAL_VALUE);
		prop.setProperty("RTTM_Txt_New_Value", Constants.TXT_NEW_VALUE);
		
		prop.setProperty("RTTM_Jop_Txt_Wrong_Syntax_Text", Constants.RTTM_JOP_TXT_WRONG_SYNTAX_TEXT);
		prop.setProperty("RTTM_Jop_Txt_Wrong_Syntax_Title", Constants.RTTM_JOP_TXT_WRONG_SYNTAX_TITLE);
		
		
		
		
		
		
		// Texty do třídy SkipFilesWithNamePanel v balíčku panels.
		prop.setProperty("SFWNP_Btn_Search_File_Name", Constants.SFWNP_BTN_SEARCH_FILE_NAME);
		prop.setProperty("SFWNP_Btn_Select_All_Items_List_Skip_Files_With_Name", Constants.SFWNP_BTN_SELECT_ALL_ITEMS_LIST_SKIP_FILES_WITH_NAME);
		prop.setProperty("SFWNP_Btn_Delect_All_Items_List_Skip_Files_With_Name", Constants.SFWNP_BTN_DELECT_ALL_ITEMS_LIST_SKIP_FILES_WITH_NAME);
		
		
		
		
		
		
		
		
		// Texy do třídy ChcbsPanels v balíčku replacePanel:
		prop.setProperty("CP_Lbl_Title", Constants.CP_LBL_TITLE);
		prop.setProperty("CP_Chcb_Ignore_Letter_Size_Text", Constants.CP_CHCB_IGNORE_LETTER_SIZE_TEXT);
		prop.setProperty("CP_Chcb_Ignore_Letter_Size_TT", Constants.CP_CHCB_IGNORE_LETTER_SIZE_TT);
		prop.setProperty("CP_Chcb_Replace_Directory_Name", Constants.CP_CHCB_REPLACE_DIRECTORY_NAME);
		prop.setProperty("CP_Chcb_Search_Subdirectories", Constants.CP_CHCB_SEARCH_SUBDIRECTORIES);
		
		
		
		
		
		
		
		// Texty do třídy ReplacePanel v balíčku replacePanel:
		prop.setProperty("RP_Pnl_Table_Values", Constants.RP_PNL_TABLE_VALUES);
		prop.setProperty("RP_Btn_Add_Row_Text", Constants.RP_BTN_ADD_ROW_TEXT);
		prop.setProperty("RP_Btn_Add_Row_Tt", Constants.RP_BTN_ADD_ROW_TT);
		prop.setProperty("RP_Btn_Delete_Row_Text", Constants.RP_BTN_DELETE_ROW_TEXT);
		prop.setProperty("RP_Btn_Delete_Row_Tt", Constants.RP_BTN_DELETE_ROW_TT);
		prop.setProperty("RP_TxtUnselected_Row_Text", Constants.RP_TXT_UNSELECTED_ROW_TEXT);
		prop.setProperty("RP_Txt_Unselected_Row_Title", Constants.RP_TXT_UNSELECTED_ROW_TITLE);
		prop.setProperty("RP_Btn_Swap_Columns_Text", Constants.RP_BTN_SWAP_COLUMNS_TEXT);
		prop.setProperty("RP_Btn_Swap_Columns_Tt", Constants.RP_BTN_SWAP_COLUMNS_TT);
		prop.setProperty("RP_Txt_Empty_Table_Text", Constants.RP_TXT_EMPTY_TABLE_TEXT);
		prop.setProperty("RP_Txt_Empty_Table_Title", Constants.RP_TXT_EMPTY_TABLE_TITLE);
		prop.setProperty("RP_Txt_Unselected_Row_For_Text_Swap_Text", Constants.RP_TXT_UNSELECTED_ROW_FOR_TEXT_SWAP_TEXT);
		prop.setProperty("RP_Txt_Unselected_Row_For_Text_Swap_Title", Constants.RP_TXT_UNSELECTED_ROW_FOR_TEXT_SWAP_TITLE);		
		prop.setProperty("RP_Btn_Load_All_Name_Text", Constants.RP_BTN_LOAD_ALL_NAMES_TEXT);
		prop.setProperty("RP_Btn_Load_All_Name_TT", Constants.RP_BTN_LOAD_ALL_NAMES_TT);
		prop.setProperty("RP_Txt_Source_Dir_Doesnt_Exist_Or_Didnt_Set_Text", Constants.RP_TXT_SOURCE_DIR_DOESNT_EXIST_OR_DIDNT_SET_TEXT);
		prop.setProperty("RP_Txt_Source_Dir_Doesnt_Exist_Or_Didnt_Set_TT", Constants.RP_TXT_SOURCE_DIR_DOESNT_EXIST_OR_DIDNT_SET_TT);				
		prop.setProperty("RP_Lbl_Skip_Files", Constants.RP_LBL_SKIP_FILES);
		prop.setProperty("RP_Chcb_Skip_Files_With_Extensions", Constants.RP_CHCB_SKIP_FILES_WITH_EXTENSIONS);
		prop.setProperty("RP_Chcb_Skip_Specific_Files", Constants.RP_CHCB_SKIP_SPECIFIC_FILES);
		prop.setProperty("RP_Btn_Select_All_Item_Extensions", Constants.RP_BTN_SELECT_ALL_ITEM_EXTENSIONS);
		prop.setProperty("RP_Btn_Deselect_All_Item_Extensions", Constants.RP_BTN_DESELECT_ALL_ITEM_EXTENSIONS);
		prop.setProperty("RP_Btn_Confirm", Constants.RP_BTN_CONFIRM);
		prop.setProperty("RP_Txt_Copy_Error_Text", Constants.RP_TXT_COPY_ERROR_TEXT);
		prop.setProperty("RP_Txt_Copy_Error_Title", Constants.RP_TXT_COPY_ERROR_TITLE);
		prop.setProperty("RP_Txt_Empty_Field_In_Table_Text", Constants.RP_TXT_EMPTY_FIELD_IN_TABLE_TEXT);
		prop.setProperty("RP_Txt_Empty_Field_In_Table_Title", Constants.RP_TXT_EMPTY_FIELD_IN_TABLE_TITLE);
		prop.setProperty("RP_Txt_Rename_File_Error_Text_1", Constants.RP_TXT_RENAME_FILE_ERROR_TEXT_1);
		prop.setProperty("RP_Txt_Rename_File_Error_Text_2", Constants.RP_TXT_RENAME_FILE_ERROR_TEXT_2);
		prop.setProperty("RP_Txt_Rename_File_Error_Title", Constants.RP_TXT_RENAME_FILE_ERROR_TITLE);
		
		
		
		
		
		
		
		// Texty do třídy AbstractActions v balíčku fileExplorer:
		prop.setProperty("AA_Txt_Create_File", Constants.AA_TXT_CREATE_FILE);
		prop.setProperty("AA_Txt_Create_Dir", Constants.AA_TXT_CREATE_DIR);
		prop.setProperty("AA_Txt_Edit_File", Constants.AA_TXT_EDIT_FILE);
		prop.setProperty("AA_Txt_Delete_File", Constants.AA_TXT_DELETE_FILE);
		prop.setProperty("AA_Txt_Create_File_TT", Constants.AA_TXT_CREATE_FILE_TT);
		prop.setProperty("AA_Txt_Create_Dir_TT", Constants.AA_TXT_CREATE_DIR_TT);
		prop.setProperty("AA_Txt_Edit_File_TT", Constants.AA_TXT_EDIT_FILE_TT);
		prop.setProperty("AA_Txt_Delete_File_TT", Constants.AA_TXT_DELETE_FILE_TT);
		prop.setProperty("AA_Txt_Choose_Dir_Title", Constants.AA_TXT_CHOOSE_DIR_TITLE);
		prop.setProperty("AA_Txt_Edit_File_Error_Text", Constants.AA_TXT_EDIT_FILE_ERROR_TEXT);
		prop.setProperty("AA_Txt_Edit_File_Error_Title", Constants.AA_TXT_EDIT_FILE_ERROR_TITLE);
		prop.setProperty("AA_Txt_Delete_File_Error_Text", Constants.AA_TXT_DELETE_FILE_ERROR_TEXT);
		prop.setProperty("AA_Txt_Delete_File_Error_Title", Constants.AA_TXT_DELETE_FILE_ERROR_TITLE);
		prop.setProperty("AA_Txt_Delete_File_Last_Check_Text", Constants.AA_TXT_DELETE_FILE_LAST_CHECK_TEXT);
		prop.setProperty("AA_Txt_Delete_File_Last_Check_Title", Constants.AA_TXT_DELETE_FILE_LAST_CHECK_TITLE);
		prop.setProperty("AA_Txt_Create_File_Error_Text_1", Constants.AA_TXT_CREATE_FILE_ERROR_TEXT_1);
		prop.setProperty("AA_Txt_Create_File_Error_Text_2", Constants.AA_TXT_CREATE_FILE_ERROR_TEXT_2);
		prop.setProperty("AA_Txt_Create_File_Error_Title", Constants.AA_TXT_CREATE_FILE_ERROR_TITLE);
		prop.setProperty("AA_Txt_Create_Dir_Error_Text_1", Constants.AA_TXT_CREATE_DIR_ERROR_TEXT_1);
		prop.setProperty("AA_Txt_Create_Dir_Error_Text_2", Constants.AA_TXT_CREATE_DIR_ERROR_TEXT_2);
		prop.setProperty("AA_Txt_Create_Dir_Error_Title", Constants.AA_TXT_CREATE_DIR_ERROR_TITLE);
		prop.setProperty("AA_Txt_Rename_File_Error_Text", Constants.AA_TXT_RENAME_FILE_ERROR_TEXT);
		prop.setProperty("AA_Txt_Rename_File_Error_Title", Constants.AA_TXT_RENAME_FILE_ERROR_TITLE);
		prop.setProperty("AA_Txt_Delete_Error_Text_1", Constants.AA_TXT_DELETE_ERROR_TEXT_1);
		prop.setProperty("AA_Txt_Delete_Error_Text_2", Constants.AA_TXT_DELETE_ERROR_TEXT_2);
		prop.setProperty("AA_Txt_Delete_Error_Title", Constants.AA_TXT_DELETE_ERROR_TITLE);
		prop.setProperty("AA_Txt_Delete_File_Error_Text_1", Constants.AA_TXT_DELETE_FILE_ERROR_TEXT_1);
		prop.setProperty("AA_Txt_Delete_File_Error_Text_2", Constants.AA_TXT_DELETE_FILE_ERROR_TEXT_2);
		prop.setProperty("AA_Txt_Jop_Delete_File_Error_Title", Constants.AA_TXT_JOP_DELETE_FILE_ERROR_TITLE);
		
		
		// Texty do třídy FileExplorer v balíčku fileExplorer:
		prop.setProperty("FE_Dialog_Title", Constants.FE_DIALOG_TITLE);
		prop.setProperty("FE_Jsp_Table_Title", Constants.FE_JSP_TABLE_TITLE);
		
		
		
		// Texty do třídy FileTableModel v balíčku fileExplorer - model pro tabulku -
		// sloupce a další texty pro chybove hlasky apod.:
		prop.setProperty("FTM_Txt_File_Columns", createTextFromArray(Constants.FILE_COLUMNS));
		prop.setProperty("FTM_Txt_New_Name_Error_Text", Constants.FTM_TXT_NEW_NAME_ERROR_TEXT);
		prop.setProperty("FTM_Txt_New_Name_Error_Title", Constants.FTM_TXT_NEW_NAME_ERROR_TITLE);
		prop.setProperty("FTM_Txt_New_Extension_Error_Text", Constants.FTM_TXT_NEW_EXTENSION_ERROR_TEXT);
		prop.setProperty("FTM_Txt_New_Extension_Error_Title", Constants.FTM_TXT_NEW_EXTENSION_ERROR_TITLE);
		prop.setProperty("FTM_Txt_Rename_File_Error_Text_1", Constants.FTM_TXT_RENAME_FILE_ERROR_TEXT_1);
		prop.setProperty("FTM_Txt_Rename_File_Error_Text_2", Constants.FTM_TXT_RENAME_FILE_ERROR_TEXT_2);
		prop.setProperty("FTM_Txt_Rename_File_Error_Text_3", Constants.FTM_TXT_RENAME_FILE_ERROR_TEXT_3);
		prop.setProperty("FTM_Txt_Rename_File_Error_Title", Constants.FTM_TXT_RENAME_FILE_ERROR_TITLE);
		
		
		
		
		
		// Texty do třídy FilterToolBar, pole, text pro komponenty a chybové hlášky:
		prop.setProperty("FT_Cmb_Model", createTextFromArray(Constants.CMB_MODEL));
		prop.setProperty("FT_Cmb_Show_Files_Model", createTextFromArray(Constants.CMB_SHOW_FILES_MODEL));
		prop.setProperty("FT_Lbl_Filter_Text", Constants.FT_LBL_FILTER_TEXT);
		prop.setProperty("FT_Lbl_Filter_TT", Constants.FT_LBL_FILTER_TT);
		prop.setProperty("FT_Btn_Search", Constants.FT_BTN_SEARCH);
		prop.setProperty("FT_Lbl_Show_Files", Constants.FT_LBL_SHOW_FILES);
		prop.setProperty("FT_Txt_Example_Name", Constants.FT_TXT_EXAMPLE_NAME);
		prop.setProperty("FT_Wrong_Time_Text", Constants.FT_TXT_WRONG_TIME_TEXT);
		prop.setProperty("FT_Txt_Wrong_Time_Title", Constants.FT_TXT_WRONG_TIME_TITLE);
		prop.setProperty("FT_Txt_Wrong_Date_Text", Constants.FT_TXT_WRONG_DATE_TEXT);
		prop.setProperty("FT_Txt_Wrong_Date_Title", Constants.FT_TXT_WRONG_DATE_TITLE);
		
		
		
		
		
		// Texty do třídy Menu v balíčku fileExplorer:
		prop.setProperty("MN_MN_Dialog", Constants.MN_MN_DIALOG);
		prop.setProperty("MN_MN_Actions", Constants.MN_MN_ACTIONS);
		prop.setProperty("MN_Item_Info_Text", Constants.MN_ITEM_INFO_TEXT);
		prop.setProperty("MN_Item_Info_TT", Constants.MN_ITEM_INFO_TT);
		prop.setProperty("MN_Item_Close_Text", Constants.MN_ITEM_CLOSE_TEXT);
		prop.setProperty("MN_Item_Close_TT", Constants.MN_ITEM_CLOSE_TT);
		
		
		
		
		// Texty do třídy RenameFileForm v balíčku fileExplorer:
		prop.setProperty("RFF_Dialog_Title_1", Constants.RFF_DIALOG_TITLE_1);
		prop.setProperty("RFF_Dialog_Title_2", Constants.RFF_DIALOG_TITLE_2);
		prop.setProperty("RFF_Dialog_Title_3", Constants.RFF_DIALOG_TITLE_3);
		prop.setProperty("RFF_Lbl_Name", Constants.RFF_LBL_NAME);
		prop.setProperty("RFF_Chcb_With_Extension_Text", Constants.RFF_CHCB_WITH_EXTENSION_TEXT);
		prop.setProperty("RFF_Chcb_With_Extension_TT", Constants.RFF_CHCB_WITH_EXTENSION_TT);
		prop.setProperty("RFF_Btn_Rename_1", Constants.RFF_BTN_RENAME_1);
		prop.setProperty("RFF_Btn_Rename_2", Constants.RFF_BTN_RENAME_2);
		prop.setProperty("RFF_Btn_Close", Constants.RFF_BTN_CLOSE);
		prop.setProperty("RFF_Txt_Empty_Field", Constants.RFF_TXT_EMPTY_FIELD);
		prop.setProperty("RFF_Txt_Reg_Ex_Error", Constants.RFF_TXT_REG_EX_ERROR);
		prop.setProperty("RFF_Txt_Extension_Error", Constants.RFF_TXT_EXTENSION_ERROR);
		prop.setProperty("RFF_Txt_Same_Names", Constants.RFF_TXT_SAME_NAMES);
		prop.setProperty("RFF_Txt_Missing_Extension", Constants.RFF_TXT_MISSING_EXTENSION);
		
		
		
		
		
		// Texty do třídy ToolBarTableButtons v balíčku fileExplorer:
		prop.setProperty("TBTB_Btn_Select_All_Items_Text", Constants.TBTB_BTN_SELECT_ALL_ITEMS_TEXT);
		prop.setProperty("TBTB_Btn_Select_All_Items_TT", Constants.TBTB_BTN_SELECT_ALL_ITEMS_TT);
		prop.setProperty("TBTB_Btn_Deselect_All_Items_Text", Constants.TBTB_BTN_DESELECT_ALL_ITEMS_TEXT);
		prop.setProperty("TBTB_Btn_Deselect_All_Items_TT", Constants.TBTB_BTN_DESELECT_ALL_ITEMS_TT);
		prop.setProperty("TBTB_Btn_Hide_All_Files_Text", Constants.TBTB_BTN_HIDE_ALL_FILES_TEXT);
		prop.setProperty("TBTB_Btn_Hide_All_Files_TT", Constants.TBTB_BTN_HIDE_ALL_FILES_TT);
		prop.setProperty("TBTB_Btn_Show_All_Files_Text", Constants.TBTB_BTN_SHOW_ALL_FILES_TEXT);
		prop.setProperty("TBTB_Btn_Show_All_Files_TT", Constants.TBTB_BTN_SHOW_ALL_FILES_TT);
		prop.setProperty("TBTB_Btn_Hide_Selected_Files_Text", Constants.TBTB_BTN_HIDE_SELECTED_FILES_TEXT);
		prop.setProperty("TBTB_Btn_Hide_Selected_Files_TT", Constants.TBTB_BTN_HIDE_SELECTED_FILES_TT);
		prop.setProperty("TBTB_Btn_Show_Selected_Files_Text", Constants.TBTB_BTN_SHOW_SELECTED_FILES_TEXT);
		prop.setProperty("TBTB_Btn_Show_Selected_Files_TT", Constants.TBTB_BTN_SHOW_SELECTED_FILES_TT);
		
		
		
		// Texty do třídy PopupMenu v balíčku jTree:
		prop.setProperty("PM_Item_Expand_All", Constants.PM_ITEM_EXPAND_ALL);
		prop.setProperty("PM_Item_Collapse_All", Constants.PM_ITEM_COLLAPSE_ALL);
		prop.setProperty("PM_Item_Set_Source_Dir", Constants.PM_ITEM_SET_SOURCE_DIR);
		prop.setProperty("PM_Item_Set_Destination_Dir", Constants.PM_ITEM_SET_DESTINATION_DIR);
		prop.setProperty("PM_Item_Create_File", Constants.PM_ITEM_CREATE_FILE);
		prop.setProperty("PM_Item_Create_Dir", Constants.PM_ITEM_CREATE_DIR);
		prop.setProperty("PM_Item_Rename_File", Constants.PM_ITEM_RENAME_FILE);
		prop.setProperty("PM_Item_Delete_File", Constants.PM_ITEM_DELETE_FILE);
		prop.setProperty("PM_Item_Repaint", Constants.PM_ITEM_REPAINT);
		prop.setProperty("PM_Txt_Same_Destination_Dir_Error_Text", Constants.PM_TXT_SAME_DESTINATION_DIR_ERROR_TEXT);
		prop.setProperty("PM_Txt_Same_Destination_Dir_Error_Title", Constants.PM_TXT_SAME_DESTINATION_DIR_ERROR_TITLE);
		prop.setProperty("PM_Txt_Same_Source_Dir_Error_Text", Constants.PM_TXT_SAME_SOURCE_DIR_ERROR_TEXT);
		prop.setProperty("PM_Txt_Same_Source_Dir_Error_Title", Constants.PM_TXT_SAME_SOURCE_DIR_ERROR_TITLE);
		prop.setProperty("PM_Txt_Delete_File_Text", Constants.PM_TXT_DELETE_FILE_TEXT);
		prop.setProperty("PM_Txt_Delete_File_Title", Constants.PM_TXT_DELETE_FILE_TITLE);
		
		
		
		// Texty do třídy Tree v balíčku jTree:
		prop.setProperty("T_Txt_Expand_Action", Constants.T_TXT_EXPAND_ACTION);
		prop.setProperty("T_Txt_Collapse_Action", Constants.T_TXT_COLLAPSE_ACTION);
		
		
		
		
		
		
		
		
		// Texty do třídy LanguagePanel v balíčku settingsForm:
		prop.setProperty("LP_Lbl_Language", Constants.LP_LBL_LANGUAGE);
		prop.setProperty("LP_Btn_Choose_Language_Text", Constants.LP_BTN_CHOOSE_LANGUAGE_TEXT);
		prop.setProperty("LP_Btn_Choose_Language_TT", Constants.LP_BTN_CHOOSE_LANGUAGE_TT);
		prop.setProperty("LP_Lbl_Choose_Language", Constants.LP_LBL_CHOOSE_LANGUAGE);
		prop.setProperty("LP_Btn_Choose_Different_Language_Text", Constants.LP_BTN_CHOOSE_DIFFERENT_LANGUAGE_TEXT);
		prop.setProperty("LP_Btn_Choose_Different_Language_TT", Constants.LP_BTN_CHOOSE_DIFFERENT_LANGUAGE_TT);
		prop.setProperty("LP_Lbl_Save_Language", Constants.LP_LBL_SAVE_LANGUAGE);
		prop.setProperty("LP_Btn_Save_Language_Text", Constants.LP_BTN_SAVE_LANGUAGE_TEXT);
		prop.setProperty("LP_Btn_Save_Language_Title", Constants.LP_BTN_SAVE_LANGUAGE_TITLE);
		prop.setProperty("LP_Txt_Title", Constants.LP_TXT_TITLE);
		prop.setProperty("LP_Txt_Wrong_File_Text", Constants.LP_TXT_WRONG_FILE_TEXT);
		prop.setProperty("LP_Txt_Wrong_File_Title", Constants.LP_TXT_WRONG_FILE_TITLE);
		prop.setProperty("LP_Txt_Failed_Load_Properties_File_Text_1", Constants.LP_TXT_FAILED_LOAD_PROPERTIES_FILE_TEXT_1);
		prop.setProperty("LP_Txt_Failed_Load_Properties_File_Text_2", Constants.LP_TXT_FAILED_LOAD_PROPERTIES_FILE_TEXT_2);
		prop.setProperty("LP_Txt_Failed_Load_Properties_File_Title", Constants.LP_TXT_FAILED_LOAD_PROPERTIES_FILE_TITLE);
		prop.setProperty("LP_Txt_Save_Language_Title", Constants.LP_TXT_SAVE_LANGUAGE_TITLE);
		prop.setProperty("LP_Txt_Jop_Save_Language_Text", Constants.LP_TXT_JOP_SAVE_LANGUAGE_TEXT);
		prop.setProperty("LP_Txt_Jop_Save_Language_Title", Constants.LP_TXT_JOP_SAVE_LANGUAGE_TITLE);
		
		
		
		// Texty do třídy SettingsForm v balíčku settingsForm:
		prop.setProperty("SF_Dialog_Title", Constants.SF_DIALOG_TITLE);
		prop.setProperty("SF_Lbl_Info", Constants.SF_LBL_INFO);
		prop.setProperty("SF_Chcb_Show_Hidden_Files_In_Tree", Constants.SF_CHCB_SHOW_HIDDEN_FILES_IN_TREE);
		prop.setProperty("SF_Lbl_Size_Format_Text", Constants.SF_LBL_SIZE_FORMAT_TEXT);
		prop.setProperty("SF_Lbl_Size_Format_TT", Constants.SF_LBL_SIZE_FORMAT_TT);
		
		
		
		
		
		
		// Texty do třídy RenamePanel v balíčku renamePanel:
		prop.setProperty("CFTTM_Columns", createTextFromArray(Constants.CHANGE_FILE_TYPE_COLUMNS));
		
		prop.setProperty("RP_2_Lbl_New_Name", Constants.RP_LBL_NEW_NAME);
		prop.setProperty("RP_2_Lbl_Name_Text", Constants.RP_LBL_NAME_TEXT);
		prop.setProperty("RP_2_Lbl_Name_TT", Constants.RP_LBL_NAME_TT);
		prop.setProperty("RP_2_Lbl_Error_Info_Text", Constants.RP_LBL_ERROR_INFO_TEXT);
		prop.setProperty("RP_2_Lbl_Error_Info_TT", Constants.RP_LBL_ERROR_INFO_TT);
		prop.setProperty("RP_2_Txt_Not_Specified", Constants.RP_TXT_NOT_SPECIFIED);
		prop.setProperty("RP_2_Lbl_Indexation_Text", Constants.RP_LBL_INDEXATION_TEXT);
		prop.setProperty("RP_2_Lbl_Indexation_TT", Constants.RP_LBL_INDEXATION_TT);
		prop.setProperty("RP_2_Chcb_Index_By_Number", Constants.RP_CHCB_INDEX_BY_NUMBER);
		prop.setProperty("RP_2_Chcb_Index_By_Letters_Text", Constants.RP_CHCB_INDEX_BY_LETTERS_TEXT);
		prop.setProperty("RP_2_Chcb_Index_By_Letters_TT", Constants.RP_CHCB_INDEX_BY_LETTERS_TT);
		prop.setProperty("RP_2_Lbl_Style_Numbering", Constants.RP_LBL_STYLE_NUMBERING);
		prop.setProperty("RP_2_Chcb_Index_In_Front_Of_Text", Constants.RP_CHCB_INDEX_IN_FRONT_OF_TEXT);
		prop.setProperty("RP_2_Chcb_Index_Behind_Of_Text", Constants.RP_CHCB_INDEX_BEHIND_OF_TEXT);
		prop.setProperty("RP_2_Chcb_Use_Big_Letters", Constants.RP_CHCB_USE_BIG_LETTERS);
		prop.setProperty("RP_2_Chcb_Use_Small_Letters", Constants.RP_CHCB_USE_SMALL_LETTERS);
		prop.setProperty("RP_2_Chcb_Letters_In_Front_Of_Text", Constants.CHCB_LETTERS_IN_FRONT_OF_TEXT);
		prop.setProperty("RP_2_Chcb_Letters_Behind_Of_Text", Constants.RP_CHCB_LETTERS_BEHIND_OF_TEXT);
		prop.setProperty("RP_2_Lbl_Style_Of_Indexation_By_Letters", Constants.RP_LBL_STYLE_OF_INDEXATION_BY_LETTERS);
		prop.setProperty("RP_2_Lbl_New_Name_Example_Text", Constants.RP_LBL_NEW_NAME_EXAMPLE_TEXT);
		prop.setProperty("RP_2_Txt_Lbl_New_Name_Example_Start", Constants.RP_TXT_LBL_NEW_NAME_EXAMPLE_START);
		prop.setProperty("RP_2_Lbl_New_Name_Example_TT", Constants.RP_LBL_NEW_NAME_EXAMPLE_TT);
		prop.setProperty("RP_2_Lbl_Rename_File_In_Subdirs", Constants.RP_LBL_RENAME_FILE_IN_SUBDIRS);
		prop.setProperty("RP_2_Chcb_Rename_Files_In_Sub_Dirs", Constants.RP_CHCB_RENAME_FILES_IN_SUB_DIRS);
		prop.setProperty("RP_2_Chcb_Start_Indexing_From_Zero_In_Subdirs_Text", Constants.RP_CHCB_START_INDEXING_FROM_ZERO_IN_SUBDIRS_TEXT);
		prop.setProperty("RP_2_Chcb_Start_Indexing_From_Zero_In_Subdirs_TT", Constants.RP_CHCB_START_INDEXING_FROM_ZERO_IN_SUBDIRS_TT);
		prop.setProperty("RP_2_Chcbc_Leave_File_Type", Constants.RP_CHCB_LEAVE_FILE_TYPE);
		prop.setProperty("RP_2_Chcb_Change_File_Type_Text", Constants.RP_CHCB_CHANGE_FILE_TYPE_TEXT);
		prop.setProperty("RP_2_Chcb_Change_File_Type_TT", Constants.RP_CHCB_CHANGE_FILE_TYPE_TT);
		prop.setProperty("RP_2_Pnl_Change_File_Type_Table_Data", Constants.RP_PNL_CHANGE_FILE_TYPE_TABLE_DATA);
		prop.setProperty("RP_2_Btn_Create_Extension_Text", Constants.RP_BTN_CREATE_EXTENSION_TEXT);
		prop.setProperty("RP_2_Btn_Create_Extension_TT", Constants.RP_BTN_CREATE_EXTENSION_TT);
		prop.setProperty("RP_2_Btn_Delete_Extension_Text", Constants.RP_BTN_DELETE_EXTENSION_TEXT);
		prop.setProperty("RP_2_Btn_Delete_Extension_TT", Constants.RP_BTN_DELETE_EXTENSION_TT);
		prop.setProperty("RP_2_Btn_Swap_Extensions_Text", Constants.RP_BTN_SWAP_EXTENSIONS_TEXT);		
		prop.setProperty("RP_2_Btn_Swap_Extensions_TT", Constants.RP_BTN_SWAP_EXTENSIONS_TT);
		prop.setProperty("RP_2_Btn_Load_Extensions_Text", Constants.RP_BTN_LOAD_EXTENSIONS_TEXT);
		prop.setProperty("RP_2_Btn_Load_Extensions_TT", Constants.RP_BTN_LOAD_EXTENSIONS_TT);
		prop.setProperty("RP_2_Txt_Row_Is_Not_Selected_Text", Constants.RP_TXT_ROW_IS_NOT_SELECTED_TEXT);
		prop.setProperty("RP_2_Txt_Row_Is_Not_Selected_Title", Constants.RP_TXT_ROW_IS_NOT_SELECTED_TITLE);
		prop.setProperty("RP_2_Lbl_File_For_Rename_Text", Constants.RP_LBL_FILE_FOR_RENAME_TEXT);
		prop.setProperty("RP_2_Lbl_File_For_Rename_TT", Constants.RP_LBL_FILE_FOR_RENAME_TT);
		prop.setProperty("RP_2_Chcb_Rename_All_Files", Constants.RP_CHCB_RENAME_ALL_FILES);
		prop.setProperty("RP_2_Chcb_Rename_File_Contains_Text_Text", Constants.RP_CHCB_RENAME_FILE_CONTAINS_TEXT_TEXT);
		prop.setProperty("RP_2_Chcb_Rename_File_Contains_Text_TT", Constants.RP_CHCB_RENAME_FILE_CONTAINS_TEXT_TT);
		prop.setProperty("RP_2_Chcb_Ignore_Letter_Size", Constants.RP_CHCB_IGNORE_LETTER_SIZE);
		prop.setProperty("RP_2_Chcb_Rename_Files_By_Reg_Ex", Constants.RP_CHCB_RENAME_FILES_BY_REG_EX);
		prop.setProperty("RP_2_Chcb_Include_Extension_Text", Constants.RP_CHCB_INCLUDE_EXTENSION_TEXT);
		prop.setProperty("RP_2_Chcb_Include_Extension_TT", Constants.RP_CHCB_INCLUDE_EXTENSION_TT);
		prop.setProperty("RP_2_Lbl_Reg_Ex_Text", Constants.RP_LBL_REG_EX_TEXT);
		prop.setProperty("RP_2_Lbl_Reg_Ex_TT", Constants.RP_LBL_REG_EX_TT);
		prop.setProperty("RP_2_Lbl_Test_Reg_Ex_Text", Constants.RP_LBL_TEST_REG_EX_TEXT);
		prop.setProperty("RP_2_Btn_Test_Text_For_Reg_Ex", Constants.RP_BTN_TEST_TEXT_FOR_REG_EX);
		prop.setProperty("RP_2_Chcb_Rename_File_With_Extension_Text", Constants.RP_CHCB_RENAME_FILE_WITH_EXTENSION_TEXT);
		prop.setProperty("RP_2_Chcb_Rename_File_With_Extensions_TT", Constants.RP_CHCB_RENAME_FILE_WITH_EXTENSIONS_TT);
		prop.setProperty("RP_2_Lbl_Skip_Files_Text", Constants.RP_LBL_SKIP_FILES_TEXT);
		prop.setProperty("RP_2_Lbl_Skip_Files_TT", Constants.RP_LBL_SKIP_FILES_TT);
		prop.setProperty("RP_2_Chcb_Skip_Files_With_Extension", Constants.RP_CHCB_SKIP_FILES_WITH_EXTENSION);
		prop.setProperty("RP_2_Chcb_Skip_Files_With_Name", Constants.RP_CHCB_SKIP_FILES_WITH_NAME);
		prop.setProperty("RP_2_Btn_Select_All_Items_List_File_Extensions", Constants.RP_BTN_SELECT_ALL_ITEMS_LIST_FILE_EXTENSIONS);
		prop.setProperty("RP_2_Btn_Deselect_All_Items_List_File_Extensions", Constants.RP_BTN_DESELECT_ALL_ITEMS_LIST_FILE_EXTENSIONS);
		prop.setProperty("RP_2_Btn_Select_All_Items_With_Extensions", Constants.RP_BTN_SELECT_ALL_ITEMS_WITH_EXTENSIONS);
		prop.setProperty("RP_2_Btn_Deselect_All_Items_With_Extensions", Constants.RP_BTN_DESELECT_ALL_ITEMS_WITH_EXTENSIONS);
		prop.setProperty("RP_2_Btn_Confirm", Constants.RP_2_BTN_CONFIRM);
		prop.setProperty("RP_2_Txt_Empty_Table_Text", Constants.RP_2_TXT_EMPTY_TABLE_TEXT);
		prop.setProperty("RP_2_Txt_Empty_Table_Title", Constants.RP_2_TXT_EMPTY_TABLE_TITLE);
		prop.setProperty("RP_2_Txt_Unselected_Table_Row_Text", Constants.RP_TXT_UNSELECTED_TABLE_ROW_TEXT);
		prop.setProperty("RP_2_Txt_Unselected_Table_Row_Title", Constants.RP_TXT_UNSELECTED_TABLE_ROW_TITLE);
		prop.setProperty("RP_2_Txt_Copy_Dir_Error_Text", Constants.RP_TXT_COPY_DIR_ERROR_TEXT);
		prop.setProperty("RP_2_Txt_Copy_Dir_Error_Title", Constants.RP_TXT_COPY_DIR_ERROR_TITLE);
		prop.setProperty("RP_2_Txt_Error_Or_Empty_Field_Text", Constants.RP_TXT_ERROR_OR_EMPTY_FIELD_TEXT);
		prop.setProperty("RP_2_Txt_Error_Or_Empty_Field_Title", Constants.RP_TXT_ERROR_OR_EMPTY_FIELD_TITLE);
		prop.setProperty("RP_2_Txt_Empty_Table_Change_Type_Text", Constants.RP_TXT_EMPTY_TABLE_CHANGE_TYPE_TEXT);
		prop.setProperty("RP_2_Txt_Empty_Table_Change_Type_Title", Constants.RP_TXT_EMPTY_TABLE_CHANGE_TYPE_TITLE);
		prop.setProperty("RP_2_Txt_Empty_Field_In_Table_Text", Constants.RP_2_TXT_EMPTY_FIELD_IN_TABLE_TEXT);
		prop.setProperty("RP_2_Txt_Empty_Field_In_Table_Title", Constants.RP_2_TXT_EMPTY_FIELD_IN_TABLE_TITLE);
		prop.setProperty("RP_2_Txt_Empty_Field_File_For_Rename_Text", Constants.RP_TXT_EMPTY_FIELD_FILE_FOR_RENAME_TEXT);
		prop.setProperty("RP_2_Txt_Empty_Field_File_For_Rename_Title", Constants.RP_TXT_EMPTY_FIELD_FILE_FOR_RENAME_TITLE);
		prop.setProperty("RP_2_Txt_Reg_Ex_Empty_Field_Text", Constants.RP_TXT_REG_EX_EMPTY_FIELD_TEXT);
		prop.setProperty("RP_2_Txt_Reg_Ex_Empty_Field_Title", Constants.RP_TXT_REG_EX_EMPTY_FIELD_TITLE);
		prop.setProperty("RP_2_Txt_Source_Dir_Is_Empty_Text", Constants.RP_TXT_SOURCE_DIR_IS_EMPTY_TEXT);
		prop.setProperty("RP_2_Txt_Source_Dir_Is_Empty_Title", Constants.RP_TXT_SOURCE_DIR_IS_EMPTY_TITLE);
		prop.setProperty("RP_2_Txt_Unselected_Items_In_List_Text", Constants.RP_TXT_UNSELECTED_ITEMS_IN_LIST_TEXT);
		prop.setProperty("RP_2_Txt_Unselected_Items_In_List_Title", Constants.RP_TXT_UNSELECTED_ITEMS_IN_LIST_TITLE);
		prop.setProperty("RP_2_Txt_Rename_File_Error_Text_1", Constants.RP_2_TXT_RENAME_FILE_ERROR_TEXT_1);
		prop.setProperty("RP_2_Txt_Rename_File_Error_Text_2", Constants.RP_2_TXT_RENAME_FILE_ERROR_TEXT_2);
		prop.setProperty("RP_2_Txt_Rename_File_Error_Text_3", Constants.RP_TXT_RENAME_FILE_ERROR_TEXT_3);
		prop.setProperty("RP_2_Txt_Rename_File_Error_Title", Constants.RP_2_TXT_RENAME_FILE_ERROR_TITLE);
		prop.setProperty("RP_2_Txt_Rename_File_Name_Error_Text_1", Constants.RP_TXT_RENAME_FILE_NAME_ERROR_TEXT_1);
		prop.setProperty("RP_2_Txt_Rename_File_Name_Error_Text_2", Constants.RP_TXT_RENAME_FILE_NAME_ERROR_TEXT_2);
		prop.setProperty("RP_2_Txt_Rename_File_Name_Error_Text_3", Constants.RP_TXT_RENAME_FILE_NAME_ERROR_TEXT_3);
		prop.setProperty("RP_2_Txt_Rename_File_Name_Error_Title", Constants.RP_TXT_RENAME_FILE_NAME_ERROR_TITLE);
		
		
		
		
		
		
		
		//Texty do třídy InfoForm v balíčku fileExplorer:
		prop.setProperty("IF_Dialog_Title", Constants.IF_DIALOG_TITLE);
		prop.setProperty("IF_Text_1", Constants.IF_TEXT_1);
		prop.setProperty("IF_Text_2", Constants.IF_TEXT_2);
		prop.setProperty("IF_Text_3", Constants.IF_TEXT_3);
		prop.setProperty("IF_Text_4", Constants.IF_TEXT_4);
		prop.setProperty("IF_Text_5", Constants.IF_TEXT_5);
		prop.setProperty("IF_Text_6", Constants.IF_TEXT_6);
		prop.setProperty("IF_Text_7", Constants.IF_TEXT_7);
		prop.setProperty("IF_Text_8", Constants.IF_TEXT_8);
		prop.setProperty("IF_Text_9", Constants.IF_TEXT_9);
		prop.setProperty("IF_Text_10", Constants.IF_TEXT_10);
		prop.setProperty("IF_Text_11", Constants.IF_TEXT_11);
		prop.setProperty("IF_Text_12", Constants.IF_TEXT_12);
		prop.setProperty("IF_Text_13", Constants.IF_TEXT_13);
		prop.setProperty("IF_Text_14", Constants.IF_TEXT_14);
		prop.setProperty("IF_Text_15", Constants.IF_TEXT_15);
		prop.setProperty("IF_Text_16", Constants.IF_TEXT_16);
		prop.setProperty("IF_Text_17", Constants.IF_TEXT_17);
		prop.setProperty("IF_Text_18", Constants.IF_TEXT_18);
		prop.setProperty("IF_Text_19", Constants.IF_TEXT_19);
		
		
		
		
		
			
			
			
			
		
		
		
		// Uložení / vytvoření souboru:
//		return createFile(prop, path, "Texty pro aplikaci Renamator v českém jazyce.");
		
		
		final boolean createdFile = createFile(prop, path);
		
		/*
		 * Otestuji, zda se podařilo vytvořit soubor a zda se mají vytvořit komentáře s mezerama.
		 */
		if (createdFile) {
			addComment(path, 3);

			/*
			 * (Toto se přidá vždy místo, aby se to přidávalo do toho komentáře při zápisu
			 * properties)
			 * 
			 * Když se přidá komentář, tak je třeba ještě dodatečně zapsat ty první dva
			 * řádky s komentářem k samotnému souboru a ten datum vytvoření.
			 */
			addFirstsComments(path, Constants.TXT_TEXT_IN_CZ_LANGUAGE);

			return true;
		}
		return false;
	}
	
	
	
	/**
	 * Metoda pro přidání komentářu do souboru properties.
	 * 
	 * V metodě se projde mapa s komentáři a vloží se do souboru.
	 * 
	 * Zdroj:
	 * https://stackoverflow.com/questions/565932/a-better-class-to-update-property-files/565996#565996
	 * 
	 * @param path
	 *            - cesta k souboru .properties, kam se mají přidat komentáře.
	 * 
	 * @param numberBlankLines
	 *            - počet volných řádků, které se mají vložit před komentář.
	 */
	private static void addComment(final String path, final int numberBlankLines) {
		
		if (COMMENTS_MAP.isEmpty())
			return;
		
		
		final PropertiesConfiguration config = new PropertiesConfiguration();
		final PropertiesConfigurationLayout layout = new PropertiesConfigurationLayout();
		try {
			layout.load(config, new FileReader(path));

			COMMENTS_MAP.forEach((key, value) -> {
				layout.setBlancLinesBefore(key, numberBlankLines);
				layout.setComment(key, value);
			});

			layout.save(config, new FileWriter(path));
		} catch (ConfigurationException | IOException e) {
			e.printStackTrace();
		}
	}
	
	
	
	
	/**
	 * Metoda, která zapíše první komentář na začátek souboru - tj. texty v jakem
	 * jazyce jsou v souboru apod. a jako druhý komentář zapíše datum a čas
	 * vytvoření toho souboru.
	 * 
	 * Komentáře se zapíšou na začátek souboru.
	 * 
	 * @param path
	 *            - cesta k souboru typu .properties, kam se mají koemntáře zapsat.
	 * 
	 * @param commentForFile
	 *            - první komentář do souboru, pod něj se zapíše ten čas.
	 */
	private static void addFirstsComments(final String path, final String commentForFile) {
		/*
		 * Formát, ve kterém se zapíše vytvoření souboru.
		 */
		final SimpleDateFormat formatter = new SimpleDateFormat("E MMM dd HH:mm:ss z yyyy", Locale.ENGLISH);
		
		/*
		 * List, do kterého si načtu všechny řádky ze souboru, které následně zapíšu. 
		 */
		final List<String> rows = new ArrayList<>();

		// Načtu si všechen text, který je aktuálně v souboru:
		try (final BufferedReader reader = new BufferedReader(new FileReader(path))) {
			String line;
			
			while ((line = reader.readLine()) != null)
				rows.add(line);

		} catch (IOException e) {
			e.printStackTrace();
		}
			
			
		
		// Zápis:
		try (final BufferedWriter out = new BufferedWriter(new FileWriter(path));) {
			// Nejprve zapíšu komentár a datum vytvoření:
			out.write("#" + commentForFile);
			out.newLine();
			out.write("#" + formatter.format(new Date()));
			out.newLine();
			
			// Zapíšu zbytek:
			rows.forEach(r -> {
				try {
					out.write(r);
					out.newLine();
				} catch (IOException e) {
					e.printStackTrace();
				}				
			});

		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	
	
	
	
	
	
	
	/**
	 * Metoda, která vytvoří soubor se zadaným názvem na zadané cestě. Soubor bude
	 * obsahovat veškeré texty pro aplikaci, aby jej mohl například uživatel upravit
	 * apod.
	 * 
	 * Texty budou v jazyce anglickém.
	 * 
	 * @param path
	 *            - umístění a název souboru.
	 * 
	 * @return true v případě, že se soubor vytvoří v pořádku, jinak false.
	 */
	static boolean createEn(final String path) {
		/*
		 * Třída, která slouží řazení přidávaných položek.
		 */
		final SortedProperties prop = new SortedProperties();
		
//		final Properties prop = new Properties();
		
		// Texty aplikace:
		prop.setProperty("App_Title", Constants.TITLE);
		
		
		
		// Texty aplikace:
		COMMENTS_MAP.put("App_Title", "Application title.");
		prop.setProperty("App_Title", Constants.TITLE);
		
		
		
		
		COMMENTS_MAP.put("Menu_Menu_App", "Texts in the main application menu.");
		// Texty do hlavního menu v hlavním okně aplikace (Menu.java v balíčku app)
		prop.setProperty("Menu_Menu_App", "Application");
		prop.setProperty("Menu_Menu_Info", "Info");
		prop.setProperty("Menu_Item_Settings", "Settings");
		prop.setProperty("Menu_Item_Close", "Close");
		prop.setProperty("Menu_Item_Info_About_App", "Application");
		prop.setProperty("Menu_Item_Info_Author", "Author");
		
		// Popisky do tlačítek v hlavním menu v hlavním okně aplikace (Menu.java v
		// balíčku app)
		COMMENTS_MAP.put("Menu_TT_Item_Setting", "Button labels in the main application menu.");
		prop.setProperty("Menu_TT_Item_Setting", "Opens the application setup dialog.");
		prop.setProperty("Menu_TT_Item_Close", "Exits application.");
		prop.setProperty("Menu_TT_Item_Info_About_App", "Opens a dialog box with information about this application.");
		prop.setProperty("Menu_TT_Item_Info_Author", "Opens a dialog box with information about the author of this application.");
		
		
		
		// Texty do dialogu s informacemi o aplikaci - dialog AboutAppForm v balíčku
		// forms:
		COMMENTS_MAP.put("About_App_Form_Dialog_Title", "Texts in the Application Info dialog");
		prop.setProperty("About_App_Form_Dialog_Title", "Information");
		
		// Texty pro info o aplikaci:
		prop.setProperty("AAF_Info_Text_1", "This is an application that is used to rename (index) files and replace text in file names.");
		prop.setProperty("AAF_Info_Text_2", "As a source directory, you need to mark the directory with which files will work.");
		prop.setProperty("AAF_Info_Text_3", "If the destination directory is specified (if it does not exist, it will be created), all the files in the source directory are then copied into it, so they will be renamed, cast, etc.");
		prop.setProperty("AAF_Info_Text_4", "It is possible to open a project explorer, where you can mark both directories and browse the files on the disk, or rename it 'after one', etc.");
		prop.setProperty("AAF_Info_Text_5", "'Rename' tab");
		prop.setProperty("AAF_Info_Text_6", "Here you have to enter a new name for the files and choose the method of indexing, then you can choose the files to be rename, which should be omitted and not renamed.");
		prop.setProperty("AAF_Info_Text_7", "'Replace' tab");
		prop.setProperty("AAF_Info_Text_8", "Here you can enter texts that contain file names, or filenames to be renamed, or replace the entered text in the file name with a new one.");
		prop.setProperty("AAF_Info_Text_9", "In this case, you can mark some options for 'finding' files to replace their name (or at least part).");
		prop.setProperty("AAF_Info_Text_10", "It is also possible to mark some files that, even if their names contain the entered text, are omitted, respectively. will not be renamed.");
		prop.setProperty("AAF_Info_Text_11", "Settings");
		prop.setProperty("AAF_Info_Text_12", "In the settings you can choose the language for the application according to the offered options, or you can save the language to a file on the disk, which can be edited and overwrite the texts according to their own needs and retrieve it into the application.");
		prop.setProperty("AAF_Info_Text_13", "It is also possible to set up file schemas in File Explorer in the tree structure.");
		
		
		
		
		
		
		// Texty do dialogu s informacemi o autorovi aplikace - třída AboutAuthorForm v
		// balíčku forms:
		prop.setProperty("About_Author_Form_Author", "Author");
		prop.setProperty("About_Author_Form_Lbl_Image", "Not available");
		prop.setProperty("About_Author_Form_Lbl_First_Name", "Name");
		prop.setProperty("About_Author_Form_Lbl_Last_Name", "Surname");
		prop.setProperty("About_Author_Form_Lbl_Email", "E-mail");
		prop.setProperty("About_Author_Form_Lbl_Telephone", "Telephone number");
		
		// Texty pro potenciální rozšíření:
		prop.setProperty("AAF_Text_1", "I wrote this application to gain new knowledge and experience about Java language, algorithms, SW development techniques, lambda and regulator usage, etc.");
		prop.setProperty("AAF_Text_2", "Possible extensions");
		prop.setProperty("AAF_Text_3", "Adding an option to flag any number of source directories, files would always be copied to one destination or renamed in the designated source directories.");
		prop.setProperty("AAF_Text_4", "Another dialog to show file rename results, for example, a user on the Rename tab will fill out rename criteria and click on the 'Show results' button to open a new dialog where the list of directories with original and new file names even directories.");
		prop.setProperty("AAF_Text_5", "File explorer");
		prop.setProperty("AAF_Text_6", "You can add the 'Settings' dialog box, which will include the components for setting the file format (date, time, date and time, etc.) or whether they should be displayed at all.");
		prop.setProperty("AAF_Text_7", "Similarly, there are icons, ie whether they should be displayed in a table.");
		prop.setProperty("AAF_Text_8",  "I dismissed this diaog, because I did not add a function to save the settings somewhere in the files (for example, when running the application, etc.). That's why I did not even mark it in the application, just as a potential extension.");
		
		
		
		
		
		
		
		
		// Texty do panelu pro zdrojový a cílový adresář v hlavním okně aplikace - třída
		// FoldersPanel v balíčku panels.foldersPanel:
		prop.setProperty("FP_Lbl_Source_Dir", "Source directory");
		prop.setProperty("FP_Lbl_Destination_Dir", "Destination directory");
		prop.setProperty("FP_Btn_Source_Dir_Text", "Choose");
		prop.setProperty("FP_Btn_Destination_Dir_Text", "Choose");
		prop.setProperty("FP_Btn_Select_Source_Dir_TT", "Opens the dialog for selecting the source directory.");
		prop.setProperty("FP_Btn_Select_Destination_Dir_TT", "Opens the dialog for selecting the destination directory.");
		prop.setProperty("FP_Btn_Open_File_Explorer_Text", "Open the file explorer");
		prop.setProperty("FP_Btn_Open_File_Explorer_TT", "Opens a dialog where you can browse files on disk (s) and view file information.");
		prop.setProperty("FP_Txt_Select_Source_Dir", "Select the source directory");
		prop.setProperty("FP_Txt_Select_Destination_Dir", "Select the destination directory");
		
		prop.setProperty("FP_Txt_Destination_Dir_TT", "If the path to the destination directory is specified, the files are first copied to the directory and then made the required changes.");
		
		prop.setProperty("FP_Chcb_Count_Hidden_Files_Text", "Count hidden files and directories.");
		prop.setProperty("FP_Chcb_Count_Hidden_Files_TT", "Hidden files or files with their suffixes will also be changed.");
		
		prop.setProperty("FP_Txt_Missing_Path", "The path to the source directory is not specified!");
		prop.setProperty("FP_Txt_Wrong_Path", "The specified path contains invalid characters or is not allowed in syntax!");
		prop.setProperty("FP_Txt_Dir_Doesnt_Exist", "The directory on the specified path does not exist!");
		prop.setProperty("FP_Txt_Dir_Doesnt_Exist_Will_Be_Created", "The directory on the specified path does not exist (will be created)!");
		prop.setProperty("FP_Txt_Path_Is_Not_Heading_To_Dir", "The specified path does not point to the directory!");
		prop.setProperty("FP_Txt_Same_Paths", "Directory paths match!");
		
		
		
		
		
		
		
		// Texty do třídy App v balíčku app:
		// Texty do komponenta JTabbedPane:
		prop.setProperty("App_TP_Rename_Tab_text", "Rename");
		prop.setProperty("App_TP_Replace_Tab_Text", "Replace");
		prop.setProperty("App_TP_Rename_Tab_TT", "This tab contains components for setting file renaming parameters.");
		prop.setProperty("App_TP_Replace_Tab_TT", "This tab contains components for setting parameters to replace 'filename' of file names.");
		
		// Chybové hlášky:
		prop.setProperty("App_Jop_Source_Dir_Doesnt_Exist_Text", "Marked source directory no longer exists!");
		prop.setProperty("App_Jop_Source_Dir_Doesnt_Exist_Title", "Directory does not exist");
		
		
		
		
		
		
		// Texty do třídy ChangeFileTypeTableModel v balíčku panels:		
		prop.setProperty("CFTTM_Jop_Wrong_Extension_Text", "The suffix must be in the syntax '.x' -> after x it is possible to get 1-10 characters (uppercase and lower case without diacritics).");
		prop.setProperty("CFTTM_Jop_Wrong_Extension_Title", "Bad syntax");
		
		
		
		// Texty do třídy ReplaceTextTableModel:
		prop.setProperty("RTTM_Columns", createTextFromArray(new String[] {"Original text", "New text"}));
		
		prop.setProperty("RTTM_Txt_Original_Value", "Original text");
		prop.setProperty("RTTM_Txt_New_Value", "New text");
		
		prop.setProperty("RTTM_Jop_Txt_Wrong_Syntax_Text", "The text you entered does not match the required syntax (most likely contains invalid characters)!");
		prop.setProperty("RTTM_Jop_Txt_Wrong_Syntax_Title", "Bad syntax");
		
		
		
		
		
		
		// Texty do třídy SkipFilesWithNamePanel v balíčku panels.
		prop.setProperty("SFWNP_Btn_Search_File_Name", "Search");
		prop.setProperty("SFWNP_Btn_Select_All_Items_List_Skip_Files_With_Name", "Mark all");
		prop.setProperty("SFWNP_Btn_Delect_All_Items_List_Skip_Files_With_Name", "Unmark everything");
		
		
		
		
		
		
		
		
		// Texy do třídy ChcbsPanels v balíčku replacePanel:
		prop.setProperty("CP_Lbl_Title", "Replace texts / letters in file names");
		prop.setProperty("CP_Chcb_Ignore_Letter_Size_Text", "Ignore font size.");
		prop.setProperty("CP_Chcb_Ignore_Letter_Size_TT", "When comparing file names with the replacement text you have / will not recognize the font size.");
		prop.setProperty("CP_Chcb_Replace_Directory_Name", "Replace text with directory names as well.");
		prop.setProperty("CP_Chcb_Search_Subdirectories", "Search for subdirectories.");
		
		
		
		
		
		
		
		// Texty do třídy ReplacePanel v balíčku replacePanel:
		prop.setProperty("RP_Pnl_Table_Values", "Texts for replacement");
		prop.setProperty("RP_Btn_Add_Row_Text", "Add text");
		prop.setProperty("RP_Btn_Add_Row_Tt", "Adds a new row to the table.");
		prop.setProperty("RP_Btn_Delete_Row_Text", "Remove text");
		prop.setProperty("RP_Btn_Delete_Row_Tt", "Removes the marked row from the table.");
		prop.setProperty("RP_TxtUnselected_Row_Text", "No line to delete!");
		prop.setProperty("RP_Txt_Unselected_Row_Title", "Untagged line");
		prop.setProperty("RP_Btn_Swap_Columns_Text", "Swap text");
		prop.setProperty("RP_Btn_Swap_Columns_Tt", "The texts will be scrolled in columns on the indicated line. (The text in the left column moves to the right column and vice versa.)");
		prop.setProperty("RP_Txt_Empty_Table_Text", "Table does not contain any data!");
		prop.setProperty("RP_Txt_Empty_Table_Title", "Empty table");
		prop.setProperty("RP_Txt_Unselected_Row_For_Text_Swap_Text", "There is no line in the table where the texts are to be scrolled.");
		prop.setProperty("RP_Txt_Unselected_Row_For_Text_Swap_Title", "Untagged line");		
		prop.setProperty("RP_Btn_Load_All_Name_Text", "Retrieve names");
		prop.setProperty("RP_Btn_Load_All_Name_TT", "The table will read the names of all files in the selected source directory (if 'Search subdirectories' is marked, filenames will be loaded from subdirectories). The original data will be erased.");
		prop.setProperty("RP_Txt_Source_Dir_Doesnt_Exist_Or_Didnt_Set_Text", "Source directory has not been specified or does not exist!");
		prop.setProperty("RP_Txt_Source_Dir_Doesnt_Exist_Or_Didnt_Set_TT", "Error");				
		prop.setProperty("RP_Lbl_Skip_Files", "Skip files");
		prop.setProperty("RP_Chcb_Skip_Files_With_Extensions", "Skip extensions files");
		prop.setProperty("RP_Chcb_Skip_Specific_Files", "Skip files");
		prop.setProperty("RP_Btn_Select_All_Item_Extensions", "Mark all");
		prop.setProperty("RP_Btn_Deselect_All_Item_Extensions", "Unmark everything");
		prop.setProperty("RP_Btn_Confirm", "Replace");
		prop.setProperty("RP_Txt_Copy_Error_Text", "An error occurred while trying to copy files from the source directory to the destination directory, the files were not copied.");
		prop.setProperty("RP_Txt_Copy_Error_Title", "Copying failed");
		prop.setProperty("RP_Txt_Empty_Field_In_Table_Text", "The table or some of its fields is empty!");
		prop.setProperty("RP_Txt_Empty_Field_In_Table_Title", "Empty field");
		prop.setProperty("RP_Txt_Rename_File_Error_Text_1", "An error occurred while attempting to rename a file!\nPrecipient name");
		prop.setProperty("RP_Txt_Rename_File_Error_Text_2", "New name");
		prop.setProperty("RP_Txt_Rename_File_Error_Title", "Error");
		
		
		
		
		
		
		
		// Texty do třídy AbstractActions v balíčku fileExplorer:
		prop.setProperty("AA_Txt_Create_File", "Create a file");
		prop.setProperty("AA_Txt_Create_Dir", "Create a directory");
		prop.setProperty("AA_Txt_Edit_File", "Rename");
		prop.setProperty("AA_Txt_Delete_File", "Delete");
		prop.setProperty("AA_Txt_Create_File_TT", "Opens a dialog box for entering a new file name and extension.");
		prop.setProperty("AA_Txt_Create_Dir_TT", "Opens the Directory Selection dialog box and then opens the New Directory Name dialog box.");
		prop.setProperty("AA_Txt_Edit_File_TT", "Opens a dialog box for entering a new filename (or extension) of a file (for tagged files).");
		prop.setProperty("AA_Txt_Delete_File_TT", "The files marked in the table are deleted.");
		prop.setProperty("AA_Txt_Choose_Dir_Title", "Select a directory");
		prop.setProperty("AA_Txt_Edit_File_Error_Text", "No renaming file is marked!");
		prop.setProperty("AA_Txt_Edit_File_Error_Title", "Unmarked");
		prop.setProperty("AA_Txt_Delete_File_Error_Text", "No file to delete!");
		prop.setProperty("AA_Txt_Delete_File_Error_Title", "Unmarked");
		prop.setProperty("AA_Txt_Delete_File_Last_Check_Text", "Are you sure you want to delete marked files?");
		prop.setProperty("AA_Txt_Delete_File_Last_Check_Title", "Really delete");
		prop.setProperty("AA_Txt_Create_File_Error_Text_1", "There was an error trying to create a file. File not created!");
		prop.setProperty("AA_Txt_Create_File_Error_Text_2", "File");
		prop.setProperty("AA_Txt_Create_File_Error_Title", "Error");
		prop.setProperty("AA_Txt_Create_Dir_Error_Text_1", "There was an error trying to create a directory. Directory not created!");
		prop.setProperty("AA_Txt_Create_Dir_Error_Text_2", "Address book");
		prop.setProperty("AA_Txt_Create_Dir_Error_Title", "Error");
		prop.setProperty("AA_Txt_Rename_File_Error_Text", "There was an error trying to rename the file. The file has not been renamed.");
		prop.setProperty("AA_Txt_Rename_File_Error_Title", "Error renaming");
		prop.setProperty("AA_Txt_Delete_Error_Text_1", "There was an error trying to delete the directory. The directory was not deleted.");
		prop.setProperty("AA_Txt_Delete_Error_Text_2", "File");
		prop.setProperty("AA_Txt_Delete_Error_Title", "Error deleting");
		prop.setProperty("AA_Txt_Delete_File_Error_Text_1", "An error occurred while trying to delete the file. File not deleted.");
		prop.setProperty("AA_Txt_Delete_File_Error_Text_2", "File");
		prop.setProperty("AA_Txt_Jop_Delete_File_Error_Title", "Error deleting");
		
		
		// Texty do třídy FileExplorer v balíčku fileExplorer:
		prop.setProperty("FE_Dialog_Title", "File explorer");
		prop.setProperty("FE_Jsp_Table_Title", "Items in the selected directory");
		
		
		
		// Texty do třídy FileTableModel v balíčku fileExplorer - model pro tabulku -
		// sloupce a další texty pro chybove hlasky apod.:
		prop.setProperty("FTM_Txt_File_Columns", createTextFromArray(new String[] { "Mark", "Icon", "Name", "Extension",
				"Parent Directory", "Hidden", "Size", "Creation Date", "Last Access", "Last Modified" }));
		prop.setProperty("FTM_Txt_New_Name_Error_Text", "The new name does not match the syntax you want.");
		prop.setProperty("FTM_Txt_New_Name_Error_Title", "Bad syntax");
		prop.setProperty("FTM_Txt_New_Extension_Error_Text", "The specified suffix does not match the required syntax (uppercase and lower case letters without diacritics and digits).");
		prop.setProperty("FTM_Txt_New_Extension_Error_Title", "Bad syntax");
		prop.setProperty("FTM_Txt_Rename_File_Error_Text_1", "An error occurred while attempting to migrate the file.");
		prop.setProperty("FTM_Txt_Rename_File_Error_Text_2", "Original name");
		prop.setProperty("FTM_Txt_Rename_File_Error_Text_3", "New name");
		prop.setProperty("FTM_Txt_Rename_File_Error_Title", "Rename Error");
		
		
		
		
		
		// Texty do třídy FilterToolBar, pole, text pro komponenty a chybové hlášky:
		prop.setProperty("FT_Cmb_Model",
				createTextFromArray(new String[] { "Name", "Size", "Extension", "Access Date", "Date Created",
						"Date of change", "Access date older than", "Access date younger than",
						"Date of creation older than", "Date of change younger than", "Date of change older than",
						"Change date younger than", "Access time", "Creation time", "Changing time",
						"Access time older than", "Access time younger than", "Creation time older than",
						"Creation time younger than", "Changing time older than", "Changing time younger than" }));
		prop.setProperty("FT_Cmb_Show_Files_Model",
				createTextFromArray(new String[] { "All", "Hidden Files", "Visible Files" }));
		prop.setProperty("FT_Lbl_Filter_Text", "Filter");
		prop.setProperty("FT_Lbl_Filter_TT", "Files are searched according to specified criteria only in the marked directory.");
		prop.setProperty("FT_Btn_Search", "Search");
		prop.setProperty("FT_Lbl_Show_Files", "Display");
		prop.setProperty("FT_Txt_Example_Name", "File Name");
		prop.setProperty("FT_Wrong_Time_Text", "Created the wrong time!");
		prop.setProperty("FT_Txt_Wrong_Time_Title", "Bad time");
		prop.setProperty("FT_Txt_Wrong_Date_Text", "Created a wrong date!");
		prop.setProperty("FT_Txt_Wrong_Date_Title", "Bad date");
		
		
		
		
		
		// Texty do třídy Menu v balíčku fileExplorer:
		prop.setProperty("MN_MN_Dialog", "Dialogue");
		prop.setProperty("MN_MN_Actions", "Action");
		prop.setProperty("MN_Item_Info_Text", "Info");
		prop.setProperty("MN_Item_Info_TT", "Displays the project explorer information dialog.");
		prop.setProperty("MN_Item_Close_Text", "Close");
		prop.setProperty("MN_Item_Close_TT", "Closes this dialog (file explorer).");
		
		
		
		
		// Texty do třídy RenameFileForm v balíčku fileExplorer:
		prop.setProperty("RFF_Dialog_Title_1", "Rename");
		prop.setProperty("RFF_Dialog_Title_2", "Create a file");
		prop.setProperty("RFF_Dialog_Title_3", "Create a directory");
		prop.setProperty("RFF_Lbl_Name", "Name");
		prop.setProperty("RFF_Chcb_With_Extension_Text", "Enter name including suffix.");
		prop.setProperty("RFF_Chcb_With_Extension_TT", "It is necessary to enter the name, including the suffix, it may differ from the original.");
		prop.setProperty("RFF_Btn_Rename_1", "Rename");
		prop.setProperty("RFF_Btn_Rename_2", "Create");
		prop.setProperty("RFF_Btn_Close", "Close");
		prop.setProperty("RFF_Txt_Empty_Field", "The field is empty!");
		prop.setProperty("RFF_Txt_Reg_Ex_Error", "The name you entered does not match the syntax you want.");
		prop.setProperty("RFF_Txt_Extension_Error", "The file extension is not listed in the correct syntax!");
		prop.setProperty("RFF_Txt_Same_Names", "The new name is the same as the original one. It needs to be changed.");
		prop.setProperty("RFF_Txt_Missing_Extension", "The file extension is not listed.");
		
		
		
		
		
		// Texty do třídy ToolBarTableButtons v balíčku fileExplorer:
		prop.setProperty("TBTB_Btn_Select_All_Items_Text", "Mark all");
		prop.setProperty("TBTB_Btn_Select_All_Items_TT", "Marks all items in the table.");
		prop.setProperty("TBTB_Btn_Deselect_All_Items_Text", "Unmark everything");
		prop.setProperty("TBTB_Btn_Deselect_All_Items_TT", "Denotes all items in the table.");
		prop.setProperty("TBTB_Btn_Hide_All_Files_Text", "Hide everything");
		prop.setProperty("TBTB_Btn_Hide_All_Files_TT", "Hides all files in the table, ie sets them as hidden.");
		prop.setProperty("TBTB_Btn_Show_All_Files_Text", "See everything");
		prop.setProperty("TBTB_Btn_Show_All_Files_TT", "Displays all files in the table, ie they will not be hidden (remove dots from filenames).");
		prop.setProperty("TBTB_Btn_Hide_Selected_Files_Text", "Hide marked");
		prop.setProperty("TBTB_Btn_Hide_Selected_Files_TT", "Hides all tagged files in the table, ie adds a decimal point before the file name.");
		prop.setProperty("TBTB_Btn_Show_Selected_Files_Text", "Visible marked");
		prop.setProperty("TBTB_Btn_Show_Selected_Files_TT", "Visible labeled files in a table, ie removing a dot from the beginning of the file name.");
		
		
		
		// Texty do třídy PopupMenu v balíčku jTree:
		prop.setProperty("PM_Item_Expand_All", "Expand all");
		prop.setProperty("PM_Item_Collapse_All", "Close all");
		prop.setProperty("PM_Item_Set_Source_Dir", "Set source directory");
		prop.setProperty("PM_Item_Set_Destination_Dir", "Set the destination directory");
		prop.setProperty("PM_Item_Create_File", "Create a file");
		prop.setProperty("PM_Item_Create_Dir", "Create a directory");
		prop.setProperty("PM_Item_Rename_File", "Rename");
		prop.setProperty("PM_Item_Delete_File", "Delete");
		prop.setProperty("PM_Item_Repaint", "Update");
		prop.setProperty("PM_Txt_Same_Destination_Dir_Error_Text", "This directory can not be set as source. It is the same as the destination directory.");
		prop.setProperty("PM_Txt_Same_Destination_Dir_Error_Title", "Duplicate");
		prop.setProperty("PM_Txt_Same_Source_Dir_Error_Text", "This directory can not be set as target. is the same as the source directory.");
		prop.setProperty("PM_Txt_Same_Source_Dir_Error_Title", "Duplicate");
		prop.setProperty("PM_Txt_Delete_File_Text", "Are you sure you want to delete a file?");
		prop.setProperty("PM_Txt_Delete_File_Title", "Really delete");
		
		
		
		// Texty do třídy Tree v balíčku jTree:
		prop.setProperty("T_Txt_Expand_Action", "Extend");
		prop.setProperty("T_Txt_Collapse_Action", "Close");
		
		
		
		
		
		
		
		
		// Texty do třídy LanguagePanel v balíčku settingsForm:
		prop.setProperty("LP_Lbl_Language", "Language");
		prop.setProperty("LP_Btn_Choose_Language_Text", "Choose");
		prop.setProperty("LP_Btn_Choose_Language_TT", "Sets the selected language to the application.");
		prop.setProperty("LP_Lbl_Choose_Language", "Choose another language");
		prop.setProperty("LP_Btn_Choose_Different_Language_Text", "Choose");
		prop.setProperty("LP_Btn_Choose_Different_Language_TT", "Opens a dialog box for selecting a .Properties file that contains text for the application in the required language.");
		prop.setProperty("LP_Lbl_Save_Language", "Save Language");
		prop.setProperty("LP_Btn_Save_Language_Text", "Save");
		prop.setProperty("LP_Btn_Save_Language_Title", "Opens a dialog box to select the location and file name.");
		prop.setProperty("LP_Txt_Title", "Choose language");
		prop.setProperty("LP_Txt_Wrong_File_Text", "Only files with the .properties extension can be labeled!");
		prop.setProperty("LP_Txt_Wrong_File_Title", "Bad file");
		prop.setProperty("LP_Txt_Failed_Load_Properties_File_Text_1", "There was an error trying to load the file. The application text file was not loaded.");
		prop.setProperty("LP_Txt_Failed_Load_Properties_File_Text_2", "File");
		prop.setProperty("LP_Txt_Failed_Load_Properties_File_Title", "Error");
		prop.setProperty("LP_Txt_Save_Language_Title", "Save Language");
		prop.setProperty("LP_Txt_Jop_Save_Language_Text", "You need to specify a file name with the '.properties' extension.");
		prop.setProperty("LP_Txt_Jop_Save_Language_Title", "Bad file");
		
		
		
		// Texty do třídy SettingsForm v balíčku settingsForm:
		prop.setProperty("SF_Dialog_Title", "Settings");
		prop.setProperty("SF_Lbl_Info", "Changes made are only retained for the current application run, restarting will restore the default settings.");
		prop.setProperty("SF_Chcb_Show_Hidden_Files_In_Tree", "Show hidden files in the tree structure.");
		prop.setProperty("SF_Lbl_Size_Format_Text", "Format to display file sizes");
		prop.setProperty("SF_Lbl_Size_Format_TT", "In the selected format, the file size of the file in the file explorer (for example, KB or MB, etc.) will be displayed.");
		
		
		
		
		
		// Texty do třídy RenamePanel v balíčku renamePanel:
		prop.setProperty("CFTTM_Columns", createTextFromArray(new String[] { "Original extension", "New extension" }));
		
		prop.setProperty("RP_2_Lbl_New_Name", "New name");
		prop.setProperty("RP_2_Lbl_Name_Text", "Name");
		prop.setProperty("RP_2_Lbl_Name_TT", "Name to rename files according to the set parameters.");
		prop.setProperty("RP_2_Lbl_Error_Info_Text", "Name contains illegal characters or erroneous syntax.");
		prop.setProperty("RP_2_Lbl_Error_Info_TT", "Allowed characters: Lower case with / without diacritics, numbers, underscores, dots, exclamation point, hyphen");
		prop.setProperty("RP_2_Txt_Not_Specified", "Not reported");
		prop.setProperty("RP_2_Lbl_Indexation_Text", "Indexing");
		prop.setProperty("RP_2_Lbl_Indexation_TT", "Set parameters for indexing files.");
		prop.setProperty("RP_2_Chcb_Index_By_Number", "Index by numbers");
		prop.setProperty("RP_2_Chcb_Index_By_Letters_Text", "Index by letter.");
		prop.setProperty("RP_2_Chcb_Index_By_Letters_TT", "If the letters of the alphabet run out, it will continue indexing by numbers.");
		prop.setProperty("RP_2_Lbl_Style_Numbering", "Numbering style");
		prop.setProperty("RP_2_Chcb_Index_In_Front_Of_Text", "Add the index before the specified name.");
		prop.setProperty("RP_2_Chcb_Index_Behind_Of_Text", "Add the index for the specified name.");
		prop.setProperty("RP_2_Chcb_Use_Big_Letters", "Use upper case letters.");
		prop.setProperty("RP_2_Chcb_Use_Small_Letters", "Use lowercase letters.");
		prop.setProperty("RP_2_Chcb_Letters_In_Front_Of_Text", "Add letters before the specified name.");
		prop.setProperty("RP_2_Chcb_Letters_Behind_Of_Text", "Add letters for a given name.");
		prop.setProperty("RP_2_Lbl_Style_Of_Indexation_By_Letters", "Indexing method");
		prop.setProperty("RP_2_Lbl_New_Name_Example_Text", "Demo");
		prop.setProperty("RP_2_Txt_Lbl_New_Name_Example_Start", "Demo");
		prop.setProperty("RP_2_Lbl_New_Name_Example_TT", "An example of how files will look after renaming according to the set parameters.");
		prop.setProperty("RP_2_Lbl_Rename_File_In_Subdirs", "Files in subdirectories");
		prop.setProperty("RP_2_Chcb_Rename_Files_In_Sub_Dirs", "Rename files even in subdirectories.");
		prop.setProperty("RP_2_Chcb_Start_Indexing_From_Zero_In_Subdirs_Text", "Index files in subdirectories from the beginning.");
		prop.setProperty("RP_2_Chcb_Start_Indexing_From_Zero_In_Subdirs_TT", "Each time it enters a subdirectory, it starts indexing files from the beginning. Otherwise, the files in the directories are indexed according to the directory loading order in the application.");
		prop.setProperty("RP_2_Chcbc_Leave_File_Type", "Leave file type.");
		prop.setProperty("RP_2_Chcb_Change_File_Type_Text", "Change file type.");
		prop.setProperty("RP_2_Chcb_Change_File_Type_TT", "Changes to file types are not monitored, for example, you can change a text document file to a picture, and so on");
		prop.setProperty("RP_2_Pnl_Change_File_Type_Table_Data", "Change extensions");
		prop.setProperty("RP_2_Btn_Create_Extension_Text", "Add");
		prop.setProperty("RP_2_Btn_Create_Extension_TT", "Adds a new row to the table");
		prop.setProperty("RP_2_Btn_Delete_Extension_Text", "Remove");
		prop.setProperty("RP_2_Btn_Delete_Extension_TT", "Odebre marked line from table.");
		prop.setProperty("RP_2_Btn_Swap_Extensions_Text", "Swap extensions");		
		prop.setProperty("RP_2_Btn_Swap_Extensions_TT", "Extensions will be scrolled on the indicated line. (The suffix in the left column moves to the right column and vice versa.)");
		prop.setProperty("RP_2_Btn_Load_Extensions_Text", "Load extensions");
		prop.setProperty("RP_2_Btn_Load_Extensions_TT", "The file extensions in the selected source directory are loaded into the table (the original data will be deleted).");
		prop.setProperty("RP_2_Txt_Row_Is_Not_Selected_Text", "No deletion / removal entry is marked.");
		prop.setProperty("RP_2_Txt_Row_Is_Not_Selected_Title", "Unmarked item");
		prop.setProperty("RP_2_Lbl_File_For_Rename_Text", "Files (original names) for renaming");
		prop.setProperty("RP_2_Lbl_File_For_Rename_TT", "This is the definition of files / filenames that are renamed according to the set parameters above.");
		prop.setProperty("RP_2_Chcb_Rename_All_Files", "Rename all files.");
		prop.setProperty("RP_2_Chcb_Rename_File_Contains_Text_Text", "Rename files containing text");
		prop.setProperty("RP_2_Chcb_Rename_File_Contains_Text_TT", "These are only filenames, not suffixes.");
		prop.setProperty("RP_2_Chcb_Ignore_Letter_Size", "Ignore letter sizes.");
		prop.setProperty("RP_2_Chcb_Rename_Files_By_Reg_Ex", "Rename files that meet a regular expression.");
		prop.setProperty("RP_2_Chcb_Include_Extension_Text", "Including suffix");
		prop.setProperty("RP_2_Chcb_Include_Extension_TT", "File names will be tested for regular expression including suffix (eg fileName.jpg).");
		prop.setProperty("RP_2_Lbl_Reg_Ex_Text", "Regular expression");
		prop.setProperty("RP_2_Lbl_Reg_Ex_TT", "Regular expression to recognize files that are renamed.");
		prop.setProperty("RP_2_Lbl_Test_Reg_Ex_Text", "Test regular expression");
		prop.setProperty("RP_2_Btn_Test_Text_For_Reg_Ex", "Test it");
		prop.setProperty("RP_2_Chcb_Rename_File_With_Extension_Text", "Rename only type files");
		prop.setProperty("RP_2_Chcb_Rename_File_With_Extensions_TT", "Only files with marked extensions will be renamed.");
		prop.setProperty("RP_2_Lbl_Skip_Files_Text", "Skip files");
		prop.setProperty("RP_2_Lbl_Skip_Files_TT", "Files of the selected type or name will not be renamed.");
		prop.setProperty("RP_2_Chcb_Skip_Files_With_Extension", "Skip type files");
		prop.setProperty("RP_2_Chcb_Skip_Files_With_Name", "Skip files");
		prop.setProperty("RP_2_Btn_Select_All_Items_List_File_Extensions", "Mark all");
		prop.setProperty("RP_2_Btn_Deselect_All_Items_List_File_Extensions", "Unmark everything");
		prop.setProperty("RP_2_Btn_Select_All_Items_With_Extensions", "Mark all");
		prop.setProperty("RP_2_Btn_Deselect_All_Items_With_Extensions", "Unmark everything");
		prop.setProperty("RP_2_Btn_Confirm", "Rename");
		prop.setProperty("RP_2_Txt_Empty_Table_Text", "Table does not contain any data!");
		prop.setProperty("RP_2_Txt_Empty_Table_Title", "Empty table");
		prop.setProperty("RP_2_Txt_Unselected_Table_Row_Text", "There is no line in the table where the suffixes should be swapped.");
		prop.setProperty("RP_2_Txt_Unselected_Table_Row_Title", "Untagged line");
		prop.setProperty("RP_2_Txt_Copy_Dir_Error_Text", "An error occurred while trying to copy files from the source directory to the destination directory, the files were not copied.");
		prop.setProperty("RP_2_Txt_Copy_Dir_Error_Title", "Copying Failed");
		prop.setProperty("RP_2_Txt_Error_Or_Empty_Field_Text", "The field for the new file name is empty or does not match the required syntax!");
		prop.setProperty("RP_2_Txt_Error_Or_Empty_Field_Title", "Error");
		prop.setProperty("RP_2_Txt_Empty_Table_Change_Type_Text", "Table of file suffix replacement information table is empty!");
		prop.setProperty("RP_2_Txt_Empty_Table_Change_Type_Title", "Empty table");
		prop.setProperty("RP_2_Txt_Empty_Field_In_Table_Text", "The file extension replacement file table contains a blank field!");
		prop.setProperty("RP_2_Txt_Empty_Field_In_Table_Title", "Empty field");
		prop.setProperty("RP_2_Txt_Empty_Field_File_For_Rename_Text", "The text entry text boxes to contain the filenames to be renamed is empty!");
		prop.setProperty("RP_2_Txt_Empty_Field_File_For_Rename_Title", "Empty field");
		prop.setProperty("RP_2_Txt_Reg_Ex_Empty_Field_Text", "The regular expression text box for recognizing files that are renamed is empty!");
		prop.setProperty("RP_2_Txt_Reg_Ex_Empty_Field_Title", "Empty field");
		prop.setProperty("RP_2_Txt_Source_Dir_Is_Empty_Text", "File with file extensions is empty, the source directory is empty!");
		prop.setProperty("RP_2_Txt_Source_Dir_Is_Empty_Title", "Empty directory / worksheet with extensions");
		prop.setProperty("RP_2_Txt_Unselected_Items_In_List_Text", "At least one suffix is ​​required to identify which files are to be renamed.");
		prop.setProperty("RP_2_Txt_Unselected_Items_In_List_Title", "Unmarked");
		prop.setProperty("RP_2_Txt_Rename_File_Error_Text_1", "An error occurred while trying to change the file extension. The file has not been changed.");
		prop.setProperty("RP_2_Txt_Rename_File_Error_Text_2", "Original name");
		prop.setProperty("RP_2_Txt_Rename_File_Error_Text_3", "New name");
		prop.setProperty("RP_2_Txt_Rename_File_Error_Title", "Rename Error");
		prop.setProperty("RP_2_Txt_Rename_File_Name_Error_Text_1", "There was an error trying to rename the file. The file has not been renamed.");
		prop.setProperty("RP_2_Txt_Rename_File_Name_Error_Text_2", "Original name");
		prop.setProperty("RP_2_Txt_Rename_File_Name_Error_Text_3", "New name");
		prop.setProperty("RP_2_Txt_Rename_File_Name_Error_Title", "Rename Error");
		
		
		
		
		
		
		
		//Texty do třídy InfoForm v balíčku fileExplorer:
		prop.setProperty("IF_Dialog_Title", "Info");
		prop.setProperty("IF_Text_1", "Basic information");
		prop.setProperty("IF_Text_2", "Only the files in the tagged directory in the tree structure on the left of the dialog will always appear in the table.");
		prop.setProperty("IF_Text_3", "Abstract buttons are used to create a new file or directory on the selected location.");
		prop.setProperty("IF_Text_4", "You can also rename the selected file or directory or delete it.");
		prop.setProperty("IF_Text_5", "Filter");
		prop.setProperty("IF_Text_6", "Always filter items in the selected directory.");
		prop.setProperty("IF_Text_7", "It is possible to filter files by name, size, suffix, date and time of last access or modification or creation.");
		prop.setProperty("IF_Text_8", "It is possible to set the table to show only hidden, visible or both types of items.");
		prop.setProperty("IF_Text_9", "You can mark / unmark all files with one button.");
		prop.setProperty("IF_Text_10", "You can mark any files in the marked directory as hidden or visible.");
		prop.setProperty("IF_Text_11", "Tree structure");
		prop.setProperty("IF_Text_12", "The directory opens by double-clicking on it or the arrow next to the directory name");
		prop.setProperty("IF_Text_13", "It is possible to open / hide the whole tree structure.");
		prop.setProperty("IF_Text_14", "It is possible to set source and / or destination directory for application -> to rename or replace text.");
		prop.setProperty("IF_Text_15", "If a directory is marked, you can create a subdirectory or a new file in the tagged directory.");
		prop.setProperty("IF_Text_16", "In addition, delete or rename a file or directory.");
		prop.setProperty("IF_Text_17", "Or update the entire tree structure to transfer all data from the root directories.");
		prop.setProperty("IF_Text_18", "Table");
		prop.setProperty("IF_Text_19", "In the table, you can edit the file name, file extension, and whether the file is hidden or not.");
		
		

		
		
		
		// Uložení / vytvoření souboru:
//		return createFile(prop, path);
		
		final boolean createdFile = createFile(prop, path);
		
		/**
		 * Otestuji, zda se podařilo vytvořit soubor a zda se mají vytvořit komentáře s mezerama.
		 */
		if (createdFile) {
			// if (addComment)
			addComment(path, 3);

			/*
			 * (Toto se přidá vždy místo, aby se to přidávalo do toho komentáře při zápisu
			 * properties)
			 * 
			 * Když se přidá komentář, tak je třeba ještě dodatečně zapsat ty první dva
			 * řádky s komentářem k samotnému souboru a ten datum vytvoření.
			 */
			addFirstsComments(path, Constants.TXT_TEXT_IN_EN_LANGUAGE);

			return true;
		}
		return false;
	}
	
	
	
	
	
	
	
	
	/**
	 * Metoda,která slouží pro vytvoření souboru typu .properties
	 * 
	 * @param prop
	 *            - objekt typu Properties, který obsahuje texty aplikace v nějakém
	 *            jazyce.
	 * 
	 * @param path
	 *            - cesta a název souboru, kam se mají texty aplikace uložit.
	 * 
	 * @return true, pokud se soubor v opřádku vytvoří, jinak false.
	 */
	private static boolean createFile(final Properties prop, final String path) {
		try {
			final OutputStream out = new FileOutputStream(path);

			prop.store(out, null);

			out.close();
		} catch (IOException e) {
			e.printStackTrace();
			// Zde došlo k chybě, vrátím false:
			return false;
		}

		// Zde se soubor vytvořil v pořádku, vrátím true:
		return true;
	}
	
	
	
	
	
	
	
	
	
	/**
	 * Metoda, která vytvoří proměnnou typu String z jednorozměrného pole typu
	 * String.
	 * 
	 * 
	 * @param array
	 *            - Jednorozměrné pole typu String, které se převede na jednu
	 *            proměnnou text.
	 * 
	 * @return výše popsanou proměnnou typu String, která obsahuje veškeré prvky
	 *         jednorozměrného pole array oddělené nezvyklým způsobem: "|;|"
	 */
	private static String createTextFromArray(final String[] array) {
		String temp = "";
		
		for (final String s : array)
			temp += s + " |;| ";
		
		return temp;
	}
	
	
	
	
	
	
	
	
	
	
	/**
	 * Metoda, která rozdělí text v parametru této metody dle definovaného symbolu
	 * "|;|" pro oddělení prvků jednorozměrného pole a vrátí vzniklé pole obshující
	 * položky oddělené dle uvedeného symbolu.
	 * 
	 * @param text
	 *            - proměnná typu String obshující načtené pole ze souboru
	 *            .properties.
	 * @return výše popsané jednorozměrné pole typu String obsahující prvky oddělené
	 *         dle uvedeného symbolu nebo prázdné pole v případě, že bude proměnná
	 *         text null.
	 */
	public static String[] getArrayFromText(final String text) {
		if (text != null)
			return text.split("\\s*(\\|;\\|)\\s*");
		
		return new String[] {};
	}
	
	
	
	
	
	
	/**
	 * Třída, která souží pro seřazení hodnot před vložením hodnot do souboru
	 * .properties, který obsahuje veškeré texty pro aplikaci.
	 * 
	 * Postup je takový, že v třídě vždy existuje mapa, do které se vkládá index pro
	 * properties a index typu long, který značí pořadí té hodnoty do properties, v
	 * jakém se má vložit do souboru, pokaždé, když se přidá nová hodnota, tak se do
	 * této mapy vloží také hodnota s novým indexem a klíčem té hodnoty do
	 * properties.
	 * 
	 * Pak v metodě keys se pouze ty hodnoty seřadí dle indexu a vrátí se.
	 * 
	 * Zdroje:
	 * http://www.java2s.com/Tutorial/Java/0140__Collections/SortPropertieswhensaving.htm
	 * 
	 * https://stackoverflow.com/questions/10275862/how-to-sort-properties-in-java
	 * 
	 * @see Properties
	 * @see Map
	 * @see Collections
	 * @see Vector
	 * 
	 * @author Jan Krunčík
	 * @version 1.0
	 * @since 1.8
	 */
	private static final class SortedProperties extends Properties {

		private static final long serialVersionUID = 1L;
		
		
		/**
		 * Index pro indexování pořadí, v jakém se má vložit klíč s hodnotou do souboru
		 * .properties.
		 */
		private long index;
		
		/**
		 * Mapa, do které si budu vkládat hodnoty, které pak seřadím před samotným
		 * vložením do souboru. Jako klíč budu vkládat index typu Long, který značí
		 * pořadí vložení do souboru a jako hodnotu vložím "index", resp. text, dle
		 * kterého se načte hodnota ze souboru .properties
		 */
		private final Map<Long, String> map;
		
		
		
		/**
		 * Konstruktor této třídy.
		 * 
		 * Jedná se o nastavení indexu index na hodnotu -1 a vytvoření mapy pro vkládání
		 * hodnot.
		 */
		SortedProperties() {
			super();
			
			index = -1l;
			map = new HashMap<>();
		}
		
		
		
		
		@Override
		public synchronized Object put(Object key, Object value) {		
			/*
			 * Vložím do mapy jako klíč inkrementovaný index, abych poznal, v jakém pořadí
			 * jej mám pak vložit do výsledného souboru a jako hodnotu vložím do mapy text,
			 * který slouží jako "index" v souboru properties, del kterého se pozná samotný
			 * text pro načtení.
			 */
			map.put(++index, (String) key);
			
			// Toto "klasicky" vložím do mapy pro samotný soubor properties:
			return super.put(key, value);
		}
		
		@Override
		public synchronized Enumeration keys() {
			// PŮVODNÍ - nepoužito:
//		     Enumeration<?> keysEnum = super.keys();
//		     Vector<String> keyList = new Vector<String>();
//		     
//		     while(keysEnum.hasMoreElements()){
//		       keyList.add((String)keysEnum.nextElement());
//		     }
//		     
//		     System.out.println("Keys: " + Arrays.toString(keyList.toArray()));
//		     
//		     Collections.sort(keyList);
//		     return keyList.elements();
			
			
			
			
			// Řazení dle abecedy těch klíčových slov (klíčů, které slouží jako index) (sestupně / vzestupně)
		     /**
			 * Do této kolekce si vložím indexy klíčů (indexů) z mapy a seřadím je
			 */
//			final Vector<String> sortedKeys = new Vector<>(map.values());
//			// Řazení dle normálního pořadí (1, 2, 3, ...)
//			Collections.sort(sortedKeys);
//			
//			// Pokud se má řadit dle sestupného pořadí, pak je třeba nejprve kolekce seradit normálne a pak
//			// obrácene, jinak mi to nefungovalo:
////			Collections.reverse(sortedKeys);
//
//			/**
//			 * Vrátím hodnoty ve správném pořadí pro vložení do souboru.
//			 */
//			return sortedKeys.elements();
			
			
			
			
			
			
			// Řazení dle mých indexů (vzestupně je to dle pridavani a sestupně je to
			// naopak, ale vždy sejedná o poradi, ve kterem jsem je ja napsal, jako že se
			// pridaji do mapy.):
		     /*
			 * Do této kolekce si vložím indexy klíčů (indexů) z mapy a seřadím je
			 */
			final List<Long> sortedKeys = new ArrayList<>(map.keySet());
			// Řazení dle normálního pořadí (1, 2, 3, ...)
			Collections.sort(sortedKeys);
			
			// Řazení dle sestupného pořadí (X, ... 3, 2, 1, 0)
//			Collections.reverse(sortedKeys);

			/*
			 * Vytvořím si vector, do kterého vložím hodnoty ve správném pořadí, v jakém se
			 * mají vložit do souboru.
			 */
			final Vector<String> results = new Vector<>();

			/*
			 * Vložím do vectoru hodnoty ve správném pořadí.
			 */
			sortedKeys.forEach(v -> results.add(map.get(v)));

			/*
			 * Vrátím hodnoty ve správném pořadí pro vložení do souboru.
			 */
			return results.elements();
		}
	}
}
